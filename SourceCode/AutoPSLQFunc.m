(*---------------------------------------
 |                                      |
 |   AutoPSLQFunc.m of AsyInt v.1.0     |
 |                                      |
 |   by Hantian Zhang, July 15, 2024    |
 |                                      | 
 ----------------------------------------*)

(* Basic functions for PSLQ algorithm, need to load MB.m *)    
  
FindNullVecPSLQ[MBexp_, ConstList_, Zcontour_, PrecList_, KnowRes_ : 0] := Module[
  {i, num, nullvec, nullvecList, MaxPrec, reg, varlist, Zlist, Zvar, 
   kvar, SumRep1D, SumRepExtra, SumRepExp, SumSeries, SumDelta, j, MBexplist},
  nullvecList = {};
  MaxPrec = Max[PrecList];
   If[MBintOneByOne === True && Head[Expand[MBexp]] === Plus,
      Print["-------------------------------------------"];
      Print["One-by-One MB numerical integration On: ", 
       MBintOneByOne === True];
      Print["----- Note: this mode is for MB integrations over complicated HypergeometricPFQ functions  -----"];
      MBexplist = List @@ Expand[MBexp];
      Print["Total length of MB integrals: ", Length[MBexplist]];
      num = 0;
      For[j = 1, j <= Length[MBexplist], j++,
       Print["-- Integrate MBint #", j];
       num = 
        num + 
         MBintegrate[{MBint[MBexplist[[j]], {{}, Zcontour}]}, {}, PrecisionGoal -> MaxPrec, 
          AccuracyGoal -> MaxPrec, WorkingPrecision -> MaxPrec*12/10, 
          MaxNIntegrateDim -> 3, MaxRecursion -> 10^6, 
          MaxPoints -> 10^9, Verbose -> False];
       ];
      ,
      num = MBintegrate[{MBint[MBexp, {{}, Zcontour}]}, {}, PrecisionGoal -> MaxPrec, 
         AccuracyGoal -> MaxPrec, WorkingPrecision -> MaxPrec*12/10, 
         MaxNIntegrateDim -> 3, MaxRecursion -> 10^6, MaxPoints -> 10^9, Verbose -> False];
    ];
   
  Print["numerical values:", N[num, MaxPrec], " with max prec = ", 
   MaxPrec];
  If[
   Sqrt[Sum[num[[i]]^2, {i, 1, Length[num]}]] < 10^(-MaxPrec/2), 
   If[KnowRes === 0,
     Print["MB integral is actually 0 and KnownRes = 0. Return 0 !!!"];
     Return[0];
     ,
     Print["MB integral is actually 0 and KnowRes != 0. Overwrite num[[1]] to strict 0 !!!"];
     num[[1]]=0;
     ];
   ];
  For[i = 1, i <= Length[PrecList], i++,
   nullvec = 
    FindIntegerNullVector[
     Join[{N[num[[1]]+KnowRes, PrecList[[i]]]}, ConstList], 
     WorkingPrecision -> PrecList[[i]]];
   Print["nullvec: ", nullvec, " with prec = ", PrecList[[i]]];
   AppendTo[nullvecList, nullvec];
   ];
  If[nullvecList[[-1]] === nullvecList[[-2]],
   Print["Stable PSLQ solution found."];
   Return[nullvecList[[-1]]];
   ,
   Print["PSLQ solution unstable!"];
   Return[False];
   ];
  ]

FindNonZeros[list_] := Module[{i, nonzeros},
  nonzeros = {};
  For[i = 1, i <= Length[list], i++,
   If[list[[i]] =!= 0,
     AppendTo[nonzeros, i];
     ];
   ];
  Return[nonzeros];
  ]




(* improved version with auto PrecList increase *)

RecPSLQ[MBexp_, ConstList_, Zcontour_, PrecList_, KnowRes_ : 0] :=
  Module[{NullVecList, FullList, NZeroList, RecExp, EqExp, sol, count,
    flag, PrecListTmp, MaxTry, PrecStep, ConstListSym, ConstList2PSLQ},
  count = 0;
  flag = False;
  PrecListTmp = PrecList;
  MaxTry = 10;
  PrecStep = 300;
  If[FreeQ[ConstList, "ConstNumRule"] === False,
        Print["---------------------------------------"];
        Print["New constants with NumRule present!"];
        Print["---------------------------------------"];
        ConstListSym = ConstList[[1]];
        ConstList2PSLQ = ConstList[[1]] /. ConstList[[2]];
        ,
        If[FreeQ[ConstList, "NumConst"] === False,
            Print["---------------------------------------"];
            Print["New constants with NumConst present!"];
            Print["---------------------------------------"];
            ConstListSym = ConstList[[1]];
            ConstList2PSLQ = ConstList[[2]];
            ,
            ConstListSym = ConstList;
            ConstList2PSLQ = ConstList;
        ];
    ];
  While[flag === False && count <= MaxTry,
   If[count > 0,
    Print["------------------"];
    Print["Re-do round #", count, " with Precision list = ", 
     PrecListTmp];
    ];
   NullVecList = 
    FindNullVecPSLQ[MBexp, ConstList2PSLQ, Zcontour, PrecListTmp, KnowRes];
   If[NullVecList === 0,
    flag = True;
    Return[0];
    ];
   If[NullVecList =!= False,
    flag = True;
    NZeroList = FindNonZeros[NullVecList];
    EqExp = 
     RecExp*NullVecList[[1]] + 
      Sum[
       ConstListSym[[NZeroList[[i]] - 1]]*
        NullVecList[[NZeroList[[i]]]], {i, 2, Length[NZeroList]}];
    (*Print["EqExp = ",EqExp];*)
    sol = Solve[EqExp == 0, RecExp];
    Return[RecExp /. sol[[1]] // Expand];
    ,
    PrecListTmp = PrecListTmp + PrecStep;
    count = count + 1;
    ];
   ];
  If[flag === False && count > MaxTry,
   Print["Re-do ", MaxTry, 
    " times all failed!!! with PrecisionList = ", PrecListTmp];
   Abort[];
   ]
  ]
  


(* main interface *)

AINumRec1DMB[MBexp_, KinList_, TransParaList_, ConstList_, Zcontour_, 
  PrecList_, MaxPower_ : 200,  KnowRes_ : 0] := 
 Module[{P1list, P2list, P3list, i, j, k, xMaxPower, x2MaxPower, 
   output, MBParaNestList, MBParaList, dim, powerTuples, 
   TransParaPowerList, x, x2, dummy, inv1, inv2},
  output = 0;
  Switch[Length[KinList],
   0, {x = dummy, x2 = dummy},
   1, {x = KinList[[1]], x2 = dummy},
   2, {x = KinList[[1]], x2 = KinList[[2]]}
   ];
  If[Length[Zcontour] > 4,
   Print[
    "Integration variable too many, please reduce MB integrals \
first."];
   Abort[];
   ];
  If[Length[KinList] > 2,
   Print[
    "Automation of more than 2 kinematic scales not implemented."];
   Print["Please split/sort your MB integrals."];
   Abort[];
   ];
  MBParaNestList = CoefficientList[MBexp + KnowRes, TransParaList];
  dim = Dimensions[MBParaNestList];
  powerTuples = Tuples[Range /@ dim];
  TransParaPowerList = 
   Map[Apply[Times, MapThread[Power, {TransParaList, # - 1}]] &, 
    powerTuples];
  Print["Dimensions of TransParaList: ", dim];
  Print["Powers Tuples of TransParaList: ", TransParaPowerList];
  MBParaList = Flatten[MBParaNestList];
  Print["Length of TransParaPowerList: ", 
   Length[TransParaPowerList]];
  Print["Length of MBParaList: ", Length[MBParaList]];
  For[i = 1, i <= Length[MBParaList], i++,
   xMaxPower = 
    Max[Exponent[MBParaList[[i]] /. {x -> 1/inv1}, inv1], 0];
   x2MaxPower = 
    Max[Exponent[MBParaList[[i]] /. {x2 -> 1/inv2}, inv2], 0];
   Print[x, " Max Inverse Power: ", xMaxPower];
   Print[x2, " Max Inverse Power: ", x2MaxPower];
   output = 
    output + 
     AutoPSLQExp2Kin1D[
       MBParaList[[i]]*(x^xMaxPower)*(x2^x2MaxPower) // Expand // 
        PowerExpand, x, x2, ConstList, Zcontour, PrecList, MaxPower]*
      TransParaPowerList[[i]]/(x^xMaxPower*x2^x2MaxPower);
   ];
  Return[output];
  ]


(* improved version with possibility of combining known results and ParallelSum *)

AutoPSLQExp2Kin1D[MBexp_, x_, x2_, ConstList_, Zcontour_, PrecList_, MaxPower_ : 200 ] := 
 Module[{xlist, xlogxlist, xlogxx2list, xlogxx2logx2list, i, j, k, l, 
   nn, SSpower, RecConst, RecFunc, output, ToCalc, KnowRes, ToPSLQ, 
   ToPSLQlist, ToPSLQ2, mm, Zvar},
  output = 0;
  Zvar = Table[Zcontour[[mm, 1]], {mm, 1, Length[Zcontour]}];
  xlist = CoefficientList[MBexp, x];
  Print["=========================="];
  Print["Max power of ", x^Min[ Length[xlist],MaxPower+1 ] ];
  For[i = 1, i <= Min[ Length[xlist],MaxPower+1 ], i++,
   Print["-------------"];
   (* Print["Compute part i = ", i]; *)
   xlogxlist = CoefficientList[xlist[[i]], Log[x]];
   (* Print["Length of ", Log[x], " power: ", xlogxlist // Length]; *)
   For[j = 1, j <= Length[xlogxlist], j++,
    (* Print["Compute part (i, j) = ", {i, j}]; *)
    xlogxx2list = CoefficientList[xlogxlist[[j]], x2];
    Print["Max power of ", x2^Min[Length[xlogxx2list],MaxPower+1] ];
    For[k = 1, k <= Min[Length[xlogxx2list],MaxPower+1], k++,
     (* Print["Compute part (i,j,k)", {i, j, k}]; *)
     xlogxx2logx2list = CoefficientList[xlogxx2list[[k]], Log[x2]];
     (* Print["Length of ", Log[x2], " power: ", 
      xlogxx2logx2list // Length]; *)
     For[l = 1, l <= Length[xlogxx2logx2list], l++,
      Print["Compute power shifted part: ", {x^(i-1), Log[x]^(j-1), x2^(k-1), Log[x2]^(l-1) }];
      ToCalc = SortPatternList[xlogxx2logx2list[[l]], Zvar];
      If[Length[ToCalc] === 0,
       ToPSLQ = 0;
       KnowRes = 0;
       ,
       If[Length[ToCalc] === 1,
         
         If[
           And @@ Table[FreeQ[ToCalc, Zvar[[mm]]], {mm, 1, Length[Zvar]}],
           (* FreeQ[ToCalc, Z1] && FreeQ[ToCalc, Z2] && 
            FreeQ[ToCalc, Z3] && FreeQ[ToCalc, Z4], *)
           KnowRes = ToCalc[[1]];
           ToPSLQ = 0;
           ,
           ToPSLQ = ToCalc[[1]] //. polygam;
           KnowRes = 0;
           ];
         ,
         If[Length[ToCalc] === 2,
           ToPSLQ = ToCalc[[2]] //. polygam;
           KnowRes = ToCalc[[1]];
           ,
           Print[""];
           Print["!!!!!!!!!!!---------------------------!!!!!!!!!!"];
           Print[
            "To Calc length > 2. More than 1 type of integration. \
Stop and revisit!!! "];
           Abort[];
           ];
         ];
       ];
      Print["ToPSLQ expression obtained."];
      (*ToPSLQlist=DeleteCases[sort4DMB[ToPSLQ,PolyGamma,Zeta,Pi,
      EulerGamma],0];*)
      
      If[ NoGammaSimplify =!= True,
        ToPSLQlist = 
        Create4PatternList[sort1DMB[ToPSLQ, PolyGamma], Pi, Zeta[3], 
            Zeta[5], EulerGamma];
        Print[
        "ToPSLQ list obtained!! Start Gamma & PolyGamma List Recursive Simplification with ParallelTable ...... "];
        ToPSLQ2 = 
            Collect[Sum[
                GammaSimplifyList[ToPSLQlist[[nn]]], {nn, 1, 
                Length[ToPSLQlist]}], {_PolyGamma, _Gamma, Pi, _Zeta, 
                EulerGamma}, Together];
        ,
        ToPSLQ2 = ToPSLQ;
        ];
            
      Print[""];
      Print["At order: ", (x^(i - 1))*(Log[x]^(j - 1))*(x2^(k - 1))*(Log[
           x2]^(l - 1))];
      Print[""];
      Print["Known result: ", KnowRes];
      Print[""];
      Print["To PSLQ fit: ", ToPSLQ2];
      RecConst = 
       RecPSLQ[ToPSLQ2, ConstList, Zcontour, PrecList, KnowRes];
      RecFunc = 
       RecConst*(x^(i - 1))*(Log[x]^(j - 1))*(x2^(k - 1))*(Log[
           x2]^(l - 1));
      Print["Reconstruncted function: ", RecFunc];
      output = output + RecFunc;
      ];
     ];
    ];
   ];
  Return[output];
  ]


Create4PatternList[explist_, pattern1_, pattern2_, pattern3_, 
  pattern4_] := 
 Module[{i, j, k, l, m, P1list, P2list, P3list, P4list, newexp, 
   newexplist, newlist},
  newlist = {};
  If[Head[explist] === List,
   For[i = 1, i <= Length[explist], i++,
    P1list = CoefficientList[explist[[i]], pattern1];
    For[j = 1, j <= Length[P1list], j++,
     P2list = CoefficientList[P1list[[j]], pattern2];
     For[k = 1, k <= Length[P2list], k++,
      P3list = CoefficientList[P2list[[k]], pattern3];
      For[l = 1, l <= Length[P3list], l++,
       P4list = CoefficientList[P3list[[l]], pattern4];
       For[m = 1, m <= Length[P4list], m++,
        newexp = 
         P4list[[m]]*pattern1^(j - 1)*pattern2^(k - 1)*
           pattern3^(l - 1)*
           pattern4^(m - 1) /. {PolyGamma[0, a__] :> 
            PolyGamma[0, a]*tagPG[0], 
           PolyGamma[1, a__] :> PolyGamma[1, a]*tagPG[1],
           PolyGamma[2, a__] :> PolyGamma[2, a]*tagPG[2], 
           PolyGamma[3, a__] :> PolyGamma[3, a]*tagPG[3]};
        newexplist = 
         DeleteCases[
           sort4DMB[newexp, tagPG[0], tagPG[1], tagPG[2], tagPG[3]], 
           0] /. tagPG[a_] :> 1;
        AppendTo[newlist, newexplist];
        ];
       ];
      ];
     ];
    ];
   Return[DeleteCases[newlist // Flatten, 0]]
   ,
   Print["Error: Expression List is not a List! Stop."];
   Abort[]
   ];
  ]


NoGammaSimplify = False;
MBintOneByOne = False;
