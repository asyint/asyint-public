(*---------------------------------------
 |                                      |
 |   AutoSumFunc.m of AsyInt v.1.0      |
 |                                      |
 |   by Hantian Zhang, July 15, 2024    |
 |                                      | 
 ----------------------------------------*)


(* Description of optional inputs:
AddShift = "level of additional shifts: 0 (default), 1 (check denominator Gamma function poles), 2 (manually add 1 additional shift), 3 (strict numerical value checks on MaxRight and MinLeft poles). \
Recommended initial setting: 0 for 1D/2D MB to reduce size of expression, 1 for 2D MB."
IgnoreBDconst = "decide whether boundary constants / Log. Div. at infinity arc is acceptable: 0 (default), 1 (accept constant BDs), 2 (accept Logarithmic divergent BDs). \
Recommended initial setting: 1."
LRpreset = "preset L / R pole decisions: 0(default or vanishing integral), 1 (Left), 2 (Right), {1, 2} = {L, R} etc. "
*)


$Assumptions = 
  k1 \[Element] Integers && k1 >= 0 && k2 \[Element] Integers && 
   k2 >= 0 && k3 \[Element] Integers &&
   k3 >= 0 && k4 \[Element] Integers &&
   k4 >= 0;
   
Zcontour = {Z1 -> -1/7, Z2 -> -1/11, Z3 -> -1/17, Z4 -> -1/19};


(* Main interfaces  *)

AISum1DMB[MBexp_, Z1_, k1_, Zcontour_, SmallInv_, MBscale_ : "none", 
  NPLMBscale2_ : "none", LRpreset_ : "R", AddShift_ : 1, 
  BSpresent_ : 0, IgnoreBD_ : 0] := Module[{sumrep, SetLR, res},
  Switch[LRpreset, "R", SetLR = 2, "L", SetLR = 1];
  If[MBscale === "none",
   Print["=========================================="];
   Print["    Calculate scaleless MB integrals      "];
   ,
   Print["=========================================="];
   Print["     Calculate scaleful MB integrals      "];
   Print["MB scale ", MBscale, 
    " is present, please make sure the limit ", MBscale^Z1, 
    "is convergent with your L/R choice."];
   ];
  Print["Choice of Left/Right semi-circle: ", LRpreset];
  Print["=========================================="];
  Print["        Extract Sum Representation        "];
  sumrep = 
   AutoSumRep1D[MBexp // PowerExpand, Z1, k1, Zcontour, SmallInv, 
    MBscale, SetLR, AddShift, IgnoreBD];
  Print["=========================================="];
  Print["             Start Summation              "];
  If[NPLMBscale2 =!= "none",
   Print["Additional MB scale ", NPLMBscale2, 
    " is present for non-planar integrals"];
   res = 
    AutoMultiSum1D2Kin[sumrep, k1, MBscale, NPLMBscale2] // 
     ToMathematicaConstants;
   ,
   If[BSpresent === 0,
     res = 
       AutoMultiSum1D[sumrep, k1, MBscale] // ToMathematicaConstants;
     ,
     Print["Binomial sum present, use BS sum version."];
     res = 
      AutoMultiSum1DBS[sumrep, k1, MBscale] // 
       ToMathematicaConstants;
     ];
   ];
  Return[res];
  ]


AISum2DMB[MBexp_, Z1_, Z2_, k1_, k2_, Zcontour_, SmallInv_, 
  MBscale_ : "none", MBscale2_ : "none", LRpreset_ : {"R", "R"}, 
  AddShift_ : 1, split_ : 0, IgnoreBD_ : 0] := 
 Module[{sumrep, SetLR, res},
  Switch[LRpreset,
   {"R", "R"}, SetLR = {2, 2},
   {"R", "L"}, SetLR = {2, 1},
   {"L", "L"}, SetLR = {1, 1},
   {"L", "R"}, SetLR = {1, 2}];
  If[MBscale === "none",
   Print["=========================================="];
   Print["    Calculate scaleless MB integrals      "];
   ,
   Print["=========================================="];
   Print["     Calculate scaleful MB integrals      "];
   Print["MB scale ", MBscale, 
    " is present, please make sure the limit ", MBscale^Z1, 
    "is convergent with your L/R choice."];
   If[MBscale2 =!= "none",
    Print["!!! ----------------- !!!"];
    Print[
     "Note: second scale is present in 2D MB integrals, please use \
Expand&Fit method."];
    Print[
     "AsyInt will try to solve them, but no guaranteed output."];
    ];
    Print["!!! ----------------- !!!"];
   ];
  Print["Choice of Left/Right semi-circle: ", LRpreset];
  Print["=========================================="];
  Print["        Extract Sum Representation        "];
  sumrep = 
   AutoSumRep2D[MBexp // PowerExpand, Z1, k1, Z2, k2, Zcontour, 
    SmallInv, MBscale, SetLR, AddShift, IgnoreBD, split];
  Print["=========================================="];
  Print["             Start Summation              "];
  If[MBscale2 =!= "none",
   res = 
     AutoMultiSum2D2Kin[sumrep, k1, k2, MBscale, MBscale2] // 
      ToMathematicaConstants;
   ,
   res = 
     AutoMultiSum2D[sumrep, k1, k2, MBscale] // ToMathematicaConstants;
   ];
  Return[res];
  ]



(* Sum representation extraction *)

CheckMergePole[ex_, varGamma_, Z_] := 
 Module[{i, j, sumij, MergePoleShift, MaxShift},
  MergePoleShift = {};
  (*Print[varGamma];*)
  For[i = 1, i <= Length[varGamma], i++,
   If[Not[FreeQ[varGamma[[i]], Z]],
     sumij = ex + varGamma[[i]];
     (*Print["sumij:",sumij];*)
     If[NumericQ[sumij],
      If[sumij <= 0,
        AppendTo[MergePoleShift, Abs[sumij] + 1];
        ];
      ];
     ];
   ];
  If[MergePoleShift === {},
   MaxShift = 0;,
   MaxShift = Max[MergePoleShift];
   ];
  Return[MaxShift];
  ]

CheckMinLeftPole[ex_, varGamma_, Z_, AddShift_ : 0, Zcontour_ : {}] :=
 Module[{i, j, diff, MinLeftPoleShift, MaxShift},
  MinLeftPoleShift = {};
  (*Print[varGamma];*)
  For[i = 1, i <= Length[varGamma], i++,
   If[Not[FreeQ[varGamma[[i]], Z]],
     If[AddShift <= 1,
        diff = ex - varGamma[[i]];,
        If[AddShift === 2,
            diff = ex - varGamma[[i]] - 1;,
            If[AddShift === 3,
            diff = (ex - varGamma[[i]]) /. Zcontour;
            ];
        ];
     ];
     If[NumericQ[diff],
      If[diff < 0,
        AppendTo[MinLeftPoleShift, Floor[Abs[diff]]];
        ];
      ];
     ];
   ];
  If[MinLeftPoleShift === {},
   MaxShift = 0;,
   MaxShift = Max[MinLeftPoleShift];
   ];
  Return[MaxShift];
  ]

CheckMaxRightPole[ex_, varGamma_, Z_, AddShift_ : 0, Zcontour_ : {}] :=
 Module[{i, j, diff, MaxRightPoleShift, MaxShift},
  MaxRightPoleShift = {};
  (*Print[varGamma];*)
  For[i = 1, i <= Length[varGamma], i++,
   If[Not[FreeQ[varGamma[[i]], Z]],
     If[AddShift <= 1,
        diff = ex - varGamma[[i]];,
        If[AddShift === 2,
            diff = ex - varGamma[[i]] - 1;,
            If[AddShift === 3,
            diff = (ex - varGamma[[i]]) /. Zcontour;
            ];
        ];
     ];
     If[NumericQ[diff],
      If[diff < 0,
        AppendTo[MaxRightPoleShift, Floor[Abs[diff]]];
        ];
      ];
     ];
   ];
  If[MaxRightPoleShift === {},
   MaxShift = 0;,
   MaxShift = Max[MaxRightPoleShift];
   ];
  Return[MaxShift];
  ]
  
GetDenVar[Den_, Zcontour_] := Module[{DenList, NewList, Zvar, Znum, i, j, count},
  DenList = (List @@ Den) /. {Power[a__, x_] :> a};
  NewList = {};
  If[Length[DenList] == 0,
   DenList = {DenList};
   ];
  Zvar = Flatten[
    Table[Zcontour[[i]][[1]], {i, 1, Length[Zcontour]}]];
  Znum = Length[Zvar];
  For[i = 1, i <= Length[DenList], i++,
   count = 0;
   For[ j=1, j <= Znum, j++,
     If[Not[FreeQ[DenList[[i]], Zvar[[j]]]],
        count = count +1;
     ];
   ];
   (* If[FreeQ[DenList[[i]], Zvar[[1]]] && 
      FreeQ[DenList[[i]], Zvar[[2]]] && FreeQ[DenList[[i]], Zvar[[3]]],
     DenList = Delete[DenList, i];
     ];*)
   If[ count > 0,
        AppendTo[NewList,DenList[[i]]];
     ];
   ];
  Return[NewList];
  ]

GetResList[MBex_, Z_, Zcontour_, AddShift_ : 0, PrevK_ : 0] :=
 Module[{varGamma, varGammaFull, i, j, k, sumij, sol, sign, ResLeft,
   ResRight, MergePoles, shift, counter, Zpole, Zline, ZpoleLmax,
   ZpoleRmin, NestVarCategory, shiftMerge},
  varGamma =
   Union[
    Join[
     Cases[MBex // Numerator, _Gamma, Infinity] /. {Gamma[x_] :> x},
     Cases[MBex // Numerator, _PolyGamma,
       Infinity] /. {PolyGamma[a_, x_] :> x}]];
  If[AddShift === 0,
   varGammaFull = varGamma;,
   varGammaFull = 
  Union[Join[varGamma, 
    Cases[(MBex // Together) // Denominator, _Gamma, 
      Infinity] /. {Gamma[x_] :> x}, 
    GetDenVar[(MBex // Together) // Denominator, Zcontour]/. {Gamma[x_] :> x}]];
   ];
  If[PrevK =!= 0,
   varGamma = DeleteCases[varGamma /. {PrevK -> 0}, 0];
   varGammaFull = DeleteCases[varGammaFull /. {PrevK -> 0}, 0];
   ];
  Zline = Z /. Zcontour;
  (*Print["Z:",Z];
  Print["Zline:",Zline];*)
  ZpoleLmax = {-500, -500};
  ZpoleRmin = {500, 500};
  ResLeft = {Lpole};
  ResRight = {Rpole};
  For[i = 1, i <= Length[varGamma], i++,
   sol = Solve[varGamma[[i]] == 0, Z];
   If[sol =!= {},
    sign = Sign[D[varGamma[[i]], Z]];
    If[sign === 1,
     (*Left Zpole*)
     Zpole = Z /. sol[[1]];
     If[NumericQ[Zpole], Zpole = -1;
      shift = CheckMinLeftPole[-Zpole + Z, varGammaFull, Z, AddShift, Zcontour];
      shiftMerge = CheckMergePole[-Zpole + shift + Z, varGammaFull, Z];
      AppendTo[ResLeft, {Z -> -1, shift + shiftMerge}];,
      (*Zpole contains other MB Z variables*)

      NestVarCategory =
       Sign[Coefficient[Zpole, Variables[Zpole][[1]]]] /. {-1 :> 1,
         1 :> 2};
      If[(Zpole /. Zcontour) > ZpoleLmax[[NestVarCategory]],
       (*Print["Initial Left Zpole/. Zcontour =",Zpole/. Zcontour];*)

              While[(Zpole /. Zcontour) >= Zline,
        Zpole = Zpole - 1;
        ];
       While[((Zpole + 1) /. Zcontour) < Zline,
        Zpole = Zpole + 1;
        ];
       (*Print["Final Left Zpole/. Zcontour =",Zpole/. Zcontour];*)

            ZpoleLmax[[NestVarCategory]] = Zpole /. Zcontour;
       (*Print["ZpoleLmax:",ZpoleLmax];*)

       shift = CheckMinLeftPole[-Zpole + Z, varGammaFull, Z, AddShift, Zcontour];
       shiftMerge =
        CheckMergePole[-Zpole + shift + Z, varGammaFull, Z];
       AppendTo[
        ResLeft, {Z -> Zpole, shift + shiftMerge,
         Reduce[Zpole < Zline]}];
       ];
      ];
     ,
     If[sign === -1,
       (*Right Zpole*)
       Zpole = Z /. sol[[1]];
       If[NumericQ[Zpole],
        Zpole = 0;
        shift = CheckMaxRightPole[Zpole - Z, varGammaFull, Z, AddShift, Zcontour];
        shiftMerge =
         CheckMergePole[Zpole + shift - Z, varGammaFull, Z];
        AppendTo[ResRight, {Z -> 0, shift + shiftMerge}];,
        (*Zpole contains other MB Z variables*)

        NestVarCategory =
         Sign[Coefficient[Zpole, Variables[Zpole][[1]]]] /. {-1 :> 1,
           1 :> 2};
        If[(Zpole /. Zcontour) < ZpoleRmin[[NestVarCategory]],
         (*Print["Initial Right Zpole/. Zcontour =",Zpole/.
         Zcontour];*)
         While[(Zpole /. Zcontour) <= Zline,
          Zpole = Zpole + 1;
          ];
         While[((Zpole - 1) /. Zcontour) > Zline,
          Zpole = Zpole - 1;
          ];
         (*Print["Final Right Zpole/. Zcontour =",Zpole/. Zcontour];*)

                  ZpoleRmin[[NestVarCategory]] = Zpole /. Zcontour;
         (*Print["ZpoleRmin:",ZpoleRmin];*)

         shift = CheckMaxRightPole[Zpole - Z, varGammaFull, Z, AddShift, Zcontour];

         shiftMerge =
          CheckMergePole[Zpole + shift - Z, varGammaFull, Z];

         AppendTo[
          ResRight, {Z -> Zpole, shift + shiftMerge,
           Reduce[Zpole > Zline]}];
         ];
        ];
       , Print["Error: Sign =", sign];
       Return["Error: neither left nor right poles appearing."];
       ];
     ];
    ];
   ];
  Return[{Union[ResLeft], Union[ResRight]}];
  ]

DecideLR[PoleList_] := Module[{i, LR, Lpart, Rpart},
  Lpart = Length[PoleList[[1]]];
  Rpart = Length[PoleList[[2]]];
  If[Max[Lpart, Rpart] === 1,
   Return[0];
   ];
  If[Rpart <= Lpart,
   LR = 2;,
   LR = 1;
   ];
  Return[LR];
  ]

TakeRes[MBex_, PoleList_, Z_, k_, LR_] := 
 Module[{i, j, ResList, shift, offset, extra, res, Zpole, constrain},
  (*LR=1: take left poles, LR=2: take right poles*)
  ResList = {};
  For[i = 2, i <= Length[PoleList[[LR]]], i++,
   shift = PoleList[[LR, i, 2]];
   offset = (-1)^LR*shift;
   Zpole = Z /. PoleList[[LR, i, 1]];
   extra = 
    Sum[(-1)^(LR + 1)*Residue[MBex, {Z, Zpole + (-1)^LR*(j - 1)}], {j,
       1, shift}];
   res = (-1)^(LR + 1)*
     Residue[MBex, {Z, Zpole + offset + (-1)^LR*k}];
   If[Length[PoleList[[LR, i]]] === 3,
    AppendTo[ResList, {extra, res, PoleList[[LR, i, 3]]}];,
    AppendTo[ResList, {extra, res}];
    ];
   ];
  Return[ResList];
  ]

GetNestResList[MBres_, PrevK_, Z_, Zcontour_, AddShift_:0] := 
 Module[{i, MBLength, ResList, LR},
  MBLength = Length[MBres];
  For[i = 1, i <= MBLength, i++,
   If[Length[MBres[[i]]] === 2,
     ResList[
        i] = {GetResList[MBres[[i, 1]], Z, Zcontour, AddShift, PrevK],
        GetResList[MBres[[i, 2]], Z, Zcontour, AddShift, PrevK]};,
     If[Length[MBres[[i]]] === 3,
       LR = 
        Sign[Z /. 
           
           FindInstance[{MBres[[i, 3]], Abs[Z] > 20,
              Z \[Element] Integers}, Z][[1]]] /. {-1 :> 1, 1 :> 2};
       ResList[
         i] = {GetResList[MBres[[i, 1]], Z, Zcontour, AddShift, PrevK],
         GetResList[MBres[[i, 2]], Z, Zcontour, AddShift, PrevK], LR};
       ,
       (*else*)
       
       Return["Error: {Extra, ResSumRep, Constrain} structure does not \
apply."]
       ];
     ];
   ];
  Return[Table[ResList[i], {i, 1, MBLength}]];
  ]

DecideNestLR[MultiPoleList_, LRpreset_:0] :=
 Module[{PoleLength, i, j, LR, Lpart, Rpart, NestLRList, NestLR},
  PoleLength = Length[MultiPoleList];
  For[i = 1, i <= PoleLength, i++,
   NestLRList = {};
   If[Length[MultiPoleList[[i]]] === 2,
    For[j = 1, j <= 2, j++,
      If[LRpreset===0,
        LR = DecideLR[MultiPoleList[[i, j]]];,
        LR = LRpreset;
        ];
      AppendTo[NestLRList, LR];
      ];
    ,
    If[Length[MultiPoleList[[i]]] === 3,
      For[j = 1, j <= 2, j++,
        LR = DecideLR[MultiPoleList[[i, j]]];
        If[LR =!= 0,
         LR = MultiPoleList[[i, 3]];
         ];
        AppendTo[NestLRList, LR];
        ];
      ];
    ];
   NestLR[i] = NestLRList;
   ];
  Return[Table[NestLR[i], {i, 1, PoleLength}]];
  ]

TakeNestRes[MBex_, PoleList_, Z_, k_, LR_] := 
 Module[{i, j, MBLength, ResList, NestResList, res},
  (*LR=1: take left poles, LR=2: take right poles*)
  
  MBLength = Length[MBex];
  For[i = 1, i <= MBLength, i++,
   ResList = {};
   For[j = 1, j <= 2, j++,
    res = TakeRes[MBex[[i, j]], PoleList[[i, j]], Z, k, LR[[i, j]]];
    AppendTo[ResList, Flatten[res]]
    ];
   NestResList[i] = ResList;
   ];
  Return[Table[NestResList[i], {i, 1, MBLength}]]
  ]


(* Functions for 2-dim MB summation *)  
  
CheckInfinity2D[MBres_, k1_, k2_, SmallInv_, kin_ : "none", IgnoreBDconst_ : 0, LRinit_: 0] :=
 Module[{i, j, k, pass, BDnum, Pass, NumK, NumX, NumKCheck, BDnumNew, RelDiff, KinLim},
  Pass = True;
  NumK = 100;
  If[ LRinit === 0,
    NumX = 1;
    ,
    If[ LRinit === 2,
        NumX = 0.1;
        ,
        If [ LRinit === 1,
            NumX = 10;
        ];
    ];
  ];
  KinLim = {SS -> 1, TT -> 1, UU -> 1, SmallInv -> 1/2, 
   kin -> NumX, x -> NumX, y -> NumX, w -> NumX, xi -> NumX};
  Print["LR init : ", LRinit, ", and set ", kin, " = ", NumX, ", and set ", SmallInv, " = 1/2"];
  Print["Note SS = TT = UU = 1 and ",{x,y,w,xi}," = ", NumX ," by default for numerical semi-circle arc checks"];

  For[i = 1, i <= Length[MBres], i++,
   For[j = 1, j <= Length[MBres[[i]]], j++,
     If[MBres[[i, j]] =!= {},
       For[k = 1, k <= Length[MBres[[i, j]]], k++,
         If[
           Not[FreeQ[MBres[[i, j, k]], k1]] ||
            Not[FreeQ[MBres[[i, j, k]], k2]],
           BDnum =
            MBres[[i, j, k]] /. KinLim /. {k1 ->
                NumK, k2 -> NumK} // N;
           Print["BD numeric value: ", BDnum, " at ", {i,j,k}];

           If[
            Abs[BDnum] > 0.6 || Not[FreeQ[BDnum, Infinity]] || Not[FreeQ[BDnum, ComplexInfinity]] ||
             Not[FreeQ[BDnum, Indeterminate]],
             Print["Warning: Boundary is not vanishing with BDnum = ", BDnum,
  ", at indices {i,j,k} =", {i, j, k}];

            If[
              IgnoreBDconst === 0 ||
               Not[FreeQ[BDnum, Infinity]] || Not[FreeQ[BDnum, ComplexInfinity]] ||
               Not[FreeQ[BDnum, Indeterminate]],
              Pass = False;
              ,

              Print[
               "IgnoreBDconst present, test whether BD at infinity is \
constant or divergent."];
              NumKCheck = {NumK*3, NumK*4};

              BDnum =
               MBres[[i, j, k]] /. KinLim /. {k1 ->
                    NumKCheck[[1]], k2 -> NumKCheck[[1]]} // N;

              BDnumNew =
               MBres[[i, j, k]] /. KinLim /. {k1 ->
                    NumKCheck[[2]], k2 -> NumKCheck[[2]]} // N;
              
              
              If[Abs[BDnumNew] < 0.3,
                Print["BD at infinity is (almost) vanishing with BDnumNew = ", BDnumNew];
                RelDiff = 0;
                ,
                RelDiff = Abs[(Abs[BDnumNew] - Abs[BDnum])/ Min[Abs[BDnumNew], Abs[BDnum]] ];
              ];
              
              If[RelDiff > 0.02,
                Print["BD at infinity is NOT constant with Relative Deviation: ",
                  RelDiff, " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];
                If[IgnoreBDconst === 2,
                    Print["IgnoreBDconst = 2, test whether BD at infinity is Logarithmic divergent or not."];
                    RelDiff = Abs[(Abs[BDnumNew] - Abs[BDnum])/Max[Abs[BDnumNew], Abs[BDnum]]];
                    If[RelDiff > 0.4,
                        Print["BD at infinity is severely DIVERGENT with relaxed Relative Deviation: ", RelDiff, " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];
                        Pass = False;
                        ,
                        Print["BD at infinity is Logarithmic divergent with relaxed Relative Deviation: ", RelDiff, " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];
                        ];
                    ,
                    Pass = False;
                    ];
                ,
                Print["BD at infinity is constant: {BDnum, BDnumNew} = ", {BDnum,
                BDnumNew}, ", with Relative Deviation: ", RelDiff];
                ];
              ];
            ];
           ];
         ];
       ];
     ];
   ];
  Return[Pass];
  ]

ConvertToSigma2D[MBres_, k1_, k2_] := 
 Module[{i, j, k, MBk1k2, MBk1, MBk2, MBextra},
  MBk1 = 0;
  MBk2 = 0;
  MBk1k2 = 0;
  MBextra = 0;
  For[i = 1, i <= Length[MBres], i++,
   For[j = 1, j <= Length[MBres[[i]]], j++,
     If[MBres[[i, j]] =!= {},
       For[k = 1, k <= Length[MBres[[i, j]]], k++,
         If[FreeQ[MBres[[i, j, k]], k2] && FreeQ[MBres[[i, j, k]], k1],
           MBextra = MBextra + (MBres[[i, j, k]] // ToSigma),
           If[FreeQ[MBres[[i, j, k]], k2],
             MBk1 = MBk1 + (MBres[[i, j, k]] // ToSigma);,
             If[FreeQ[MBres[[i, j, k]], k1],
               MBk2 = MBk2 + (MBres[[i, j, k]] // ToSigma);,
               MBk1k2 = MBk1k2 + (MBres[[i, j, k]] // ToSigma);
               ];
             ];
           ];
         ];
       ];
     ];
   ];
  Return[{MBextra, MBk1, MBk2, MBk1k2}];
  ]




(* from beta version *)

CheckInfinity2DSigma[MBres_, k1_, k2_, SmallInv_, kin_ : "none", IgnoreBDconst_ : 0, LRinit_ : 0] := 
 Module[{i, pass, BDnum, Pass, NumK, NumX, NumKCheck, BDnumNew, RelDiff, MBresLocal, KinLim}, 
   Pass = True;
  NumK = 130;
  If[LRinit === 0, 
    NumX = 1;
    , 
   If[LRinit === 2, 
    NumX = 0.1;
    , 
    If[LRinit === 1, 
      NumX = 10;
      ];
    ];
   ];
  Print["LR init : ", LRinit, " and set x numerical value: ", NumX];

  KinLim = {SS -> 1, TT -> 1, UU -> 1, SmallInv -> 1/2, 
   kin -> NumX, x -> NumX, y -> NumX, w -> NumX, xi -> NumX};
  (*
  Print["LR init : ", LRinit, ", and set ", kin, " = ", NumX, ", and set ", SmallInv, " = 1/2"];
  Print["Note SS = TT = UU = 1 and ",{x,y,w,xi}," = ", NumX ," by default for numerical semi-circle arc checks"];
  *)

  For[i = 1, i <= Length[MBres], i++,
   If[MBres[[i]] =!= {},
     If[Not[FreeQ[MBres[[i]], k1]] || Not[FreeQ[MBres[[i]], k2]],
       MBresLocal = 
        ReduceConstants[MBres[[i]], ToKnownConstants -> True];
       BDnum = 
        MBresLocal /. KinLim /. {k1 -> NumK, k2 -> NumK} // N;
       Print["BD numeric value: ", BDnum, " at ", {i}];
       (*Print["BD numeric value: ",BDnum," at ",{i}, "for MB res: ", 
       MBres[[i]]//ReduceConstants];*)
       
       If[Abs[BDnum] > 0.6 || Not[FreeQ[BDnum, Infinity]] || 
         Not[FreeQ[BDnum, ComplexInfinity]] || 
         Not[FreeQ[BDnum, Indeterminate]], 
        Print["Warning: Boundary is not vanishing with BDnum = ", 
         BDnum, ", at indices {i,j,k} =", {i}];
        If[
         IgnoreBDconst === 0 || Not[FreeQ[BDnum, Infinity]] || 
          Not[FreeQ[BDnum, ComplexInfinity]] || 
          Not[FreeQ[BDnum, Indeterminate]], Pass = False;, 
         Print["IgnoreBDconst present, test whether BD at infinity is \
constant or divergent."];
         NumKCheck = {NumK*2, NumK*3};
         BDnum = 
          MBresLocal /. KinLim /. {k1 -> 
              NumKCheck[[1]], k2 -> NumKCheck[[1]]} // N;
         BDnumNew = 
          MBresLocal /. KinLim /. {k1 -> 
              NumKCheck[[2]], k2 -> NumKCheck[[2]]} // N;
         RelDiff = 
          Abs[(Abs[BDnumNew] - Abs[BDnum])/
            Max[Abs[BDnumNew], Abs[BDnum]]];
         If[Abs[BDnumNew] < 0.6, 
          Print["BD at infinity is (almost) vanishing with BDnumNew = \
", BDnumNew];
          RelDiff = 0;];
         If[RelDiff > 0.02, 
          Print["BD at infinity is NOT constant with Relative \
Deviation: ", RelDiff, " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];
          
          If[IgnoreBDconst === 2, 
           Print["IgnoreBDconst = 2, test whether BD at infinity is \
Logarithmic divergent or not."];
           
           RelDiff = 
            Abs[(Abs[BDnumNew] - Abs[BDnum])/
              Max[Abs[BDnumNew], Abs[BDnum]]];
           
           If[RelDiff > 0.4, 
            Print["BD at infinity is severely DIVERGENT with relaxed \
Relative Deviation: ", RelDiff, 
             " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];
            Pass = False;, 
            Print["BD at infinity is Logarithmic divergent with \
relaxed Relative Deviation: ", RelDiff, 
              " and {BDnum, BDnumNew} = ", {BDnum, BDnumNew}];];, 
           Pass = False;];, 
          Print["BD at infinity is constant: {BDnum, BDnumNew} = ", \
{BDnum, BDnumNew}, ", with Relative Deviation: ", RelDiff];
          ];
         ];
        ];
       ];
      ];
     ];
  Return[Pass];
]
  

TakeNestResSplit[MBex_, PoleList_, Z_, k_, LR_] := 
 Module[{i, j, m, MBLength, MBList, ResList, NestResList, res},
  (*LR=1:take left poles,LR=2:take right poles*)
  
  MBLength = Length[MBex];
  For[i = 1, i <= MBLength, i++,
   ResList = {};
   For[j = 1, j <= 2, j++,
    If[Head[MBex[[i, j]] // Expand] === Plus,
     MBList = List @@ Expand[MBex[[i, j]]];
     ,
     MBList = {MBex[[i, j]]};
     ];
    Print["Total nested integral number #", Length[MBList]];
    res = 0;
    For[m = 1, m <= Length[MBList], m++,
     (*Print["Computing nested residue #",m," in {i,j} = ",{i,j}];
     Print["Nested MB exp: ",MBList[[m]]];*)
     Switch[m,
      IntegerPart[Length[MBList]/4], 
      Print["1/4 MB integrals obtained! with #", m];,
      IntegerPart[Length[MBList]/2], 
      Print["1/2 MB integrals obtained!! with #", m];,
      IntegerPart[Length[MBList]*3/4], 
      Print["3/4 MB integrals obtained!!! with #", m];
      ];
     res = 
      res + TakeRes[MBList[[m]], PoleList[[i, j]], Z, k, LR[[i, j]]];
     ];
    AppendTo[ResList, Flatten[res]];
    ];
   NestResList[i] = ResList;
   ];
  Return[Table[NestResList[i], {i, 1, MBLength}]];
  ]


GetSumRep2D[MBexp_, Zinit_, kinit_, Znest_, knest_, Zcontour_, SmallInv_, MBscale_ : "none",
  LRpreset_ : {0, 0}, AddShift_ : 0, IgnoreBDconst_ : 0, split_ : 0] := 
 Module[{InitResList, InitLR, Res1, NestResList, NestLR, Res2, pass, 
   ResSigma, pass2},
   (*Print["Initial MB integral:",MBexp];*)
  InitResList = GetResList[MBexp, Zinit, Zcontour, AddShift];
  Print["Initial Res List:", InitResList];
  If[LRpreset[[1]] === 0, 
    InitLR = DecideLR[InitResList];
   Print["Initial L/R choice auto decided: ", InitLR];, 
   InitLR = LRpreset[[1]];
   Print["Initial L/R choice overwrite to present: ", InitLR];];
  Res1 = TakeRes[MBexp, InitResList, Zinit, kinit, InitLR];
  (*Print["MB 1st SumRep:",Res1];*)
  If[AddShift >= 2,
   NestResList = GetNestResList[Res1, kinit, Znest, Zcontour, 1];,
   NestResList = 
     GetNestResList[Res1, kinit, Znest, Zcontour, AddShift];
   ];
  Print["Nested Res List:", NestResList];
  NestLR = DecideNestLR[NestResList, LRpreset[[2]]];
  Print["Nest L/R choice auto decided: ", NestLR, ", with LRpreset: ",
    LRpreset];
  If[split === 0,
    Res2 = TakeNestRes[Res1, NestResList, Znest, knest, NestLR];
    ,
    Res2 = TakeNestResSplit[Res1, NestResList, Znest, knest, NestLR];
    ];
  (*Print["MB 2st SumRep:",Res2];*)
  
  pass = CheckInfinity2D[Res2, knest, kinit, SmallInv, MBscale, IgnoreBDconst, InitLR];
  If[pass === True,
   Print["Boundary check PASS."];
   Print[""];
   Print["Double BD check start:"];
   pass2 = 
    CheckInfinity2DSigma[ConvertToSigma2D[Res2, knest, kinit], knest, 
     kinit, SmallInv, MBscale,  IgnoreBDconst, InitLR];
   Print["Cross check on BD: ", pass2]
   ,
   Print["Boundary check NOT pass !!!"];
   ];
  Return[{pass && pass2, Res2}];
]


AutoSumRep2D[MBexp_, Zinit_, kinit_, Znest_, knest_, Zcontour_, SmallInv_, MBscale_ : "none",
  LRpreset_ : {0, 0}, AddShift_ : 0, IgnoreBDconst_ : 0, split_ : 0] := 
 Module[{MBList, i, SumRepi, SumRepk1, SumRepk2, SumRepk1k2, 
   SumRepExtra, LRList, SumResi, count, AddSFTcount}, 
  If[Head[MBexp] === Plus, MBList = List @@ Expand[MBexp];
   Print["Total integral number #", Length[MBList]];, 
   If[Head[MBexp] === Times, MBList = {MBexp};
     Print["Only 1 integral to be computed."];, 
     Print["Head of MBexp is ", MBexp];
     Print["MB expression: ", MBexp];
     Print["Do not compute anything."];
     Return[MBexp];];];
  SumRepk1 = 0;
  SumRepk2 = 0;
  SumRepk1k2 = 0;
  SumRepExtra = 0;
  If[MBscale =!= "none",
    LRList = {{LRpreset[[1]], 1}, {LRpreset[[1]], 2}};
    ,
    LRList = {{2, 1}, {2, 2}, {1, 1}, {1, 2}};
  ];
  For[i = 1, i <= Length[MBList], i++, Print[""];
   Print["==================================="];
   Print["Calculation integral #", i];
   Print["Initial MB integral:", MBList[[i]]];
   SumResi = 
    GetSumRep2D[MBList[[i]], Zinit, kinit, Znest, knest, Zcontour, SmallInv, MBscale,
     LRpreset, AddShift, IgnoreBDconst, split];
   count = 1;
   AddSFTcount = AddShift;
   While[Not[SumResi[[1]]], Print["-------------------"];
    Print["Boundary check FAILS, try other LRpresets #", count, 
     " with ", LRList[[count]], " and AddShift: ", AddSFTcount];
    SumResi = 
     GetSumRep2D[MBList[[i]], Zinit, kinit, Znest, knest, Zcontour, SmallInv, MBscale,
      LRList[[count]], AddSFTcount, IgnoreBDconst, split];
    count = count + 1;
    If[count > 2 && AddSFTcount <= 2, 
     If[SumResi[[1]] === False, Print[""];
       Print[
        "All 4 LRpreset FAIL. Increase AddShift parameter and reset \
LRprest count."];
       AddSFTcount = AddSFTcount + 1;
       count = 1;];];
    If[count > 2 && AddSFTcount > 2, 
     If[SumResi[[1]] === False, Print[""];
       Print["All LRpreset + AddShift FAIL!!! Abort."];
       Print[
        "May try to set IgnoreBDconst = 2 to allow Log divergence at \
infinity."];
       Abort[];];];];
   SumRepi = ConvertToSigma2D[SumResi[[2]], knest, kinit];
   SumRepExtra = SumRepExtra + SumRepi[[1]];
   SumRepk1 = SumRepk1 + SumRepi[[2]];
   SumRepk2 = SumRepk2 + SumRepi[[3]];
   SumRepk1k2 = SumRepk1k2 + SumRepi[[4]];
   ];
  Return[{SumRepExtra, SumRepk1, SumRepk2, SumRepk1k2}];
]


 (* summation functions *)


AutoMultiSum2D[SumRep_, kinit_, knest_, x_] := 
 Module[{i, extra, sumkinit, sumknest, Dsumknest, Dsumkinit}, 
  sumkinit = 0;
  sumknest = 0;
  extra = 0;
  Dsumkinit = 0;
  Dsumknest = 0;
  For[i = 1, i <= Length[SumRep], i++,
   If[FreeQ[SumRep[[i]], kinit] && FreeQ[SumRep[[i]], knest],
     extra = 
       SumRep[[i]] // ToHLogs // ToMathematicaConstants // Simplify;,
     If[FreeQ[SumRep[[i]], knest],
       Print[""];
       Print["Start Sum: EvaluateMultiSum on K_init: ", kinit];
       sumkinit = 
        TransformToSSums[
           EvaluateMultiSum[
             SigmaReduceList[SumRep[[i]], 
              kinit], {{kinit, 0, Infinity}}, {}, {}, {}, 
             Assumptions -> 0 < x < 1] /. {LinInf -> Infinity}, 
           Assumptions -> 0 < x < 1] // SinfToH // 
         VariableToArgument;
       Print["EvaluateMultiSum of K_init: ", sumkinit];
       ,
       If[FreeQ[SumRep[[i]], kinit],
         Print[""];
         Print["Start Sum: EvaluateMultiSum on K_nest: ", knest];
         sumknest = 
          TransformToSSums[
             EvaluateMultiSum[
               SigmaReduceList[SumRep[[i]], 
                knest], {{knest, 0, Infinity}}, {}, {}, {}, 
               Assumptions -> 0 < x < 1] /. {LinInf -> Infinity}, 
             Assumptions -> 0 < x < 1] // SinfToH // 
           VariableToArgument;
         Print["EvaluateMultiSum of K_nest: ", sumknest];
         ,
         (*Double Summation*)
         Print[""];
         Print["Start Double Sum: EvaluateMultiSum on K_nest: ", 
          knest];
         Dsumknest = 
          EvaluateMultiSum[
           SumRep[[i]], {{knest, 0, 
             Infinity}}, {kinit}, {0}, {Infinity}, 
           Assumptions -> 0 < x < 1];
         Print["EvaluateMultiSum of K_nest: ", Dsumknest];
         Print[""];
         Print["Start Double Sum: EvaluateMultiSum on K_init: ", 
          kinit];
         Dsumkinit = 
          TransformToSSums[
             EvaluateMultiSum[
               SigmaReduceList[Dsumknest, 
                kinit], {{kinit, 0, Infinity}}, {}, {}, {}, 
               Assumptions -> 0 < x < 1] /. {LinInf -> Infinity}, 
             Assumptions -> 0 < x < 1] // SinfToH // 
           VariableToArgument;
         ];
       ];
     ];
   ];
  Return[ReduceConstants[(extra + sumkinit + sumknest + Dsumkinit) // 
       ToHLogs, ToKnownConstants -> True] // ToMathematicaConstants //
     Simplify];
  ]


AutoMultiSum2D2Kin[SumRep_, kinit_, knest_, x_, y_, 
  NoSigmaReduce_ : 0, combine_ : 1] := Module[
  {i, extra, sumkinit, sumknest, Dsumknest, Dsumkinit, SigRed, 
   SumRepKinit, DSumRepLast, xinv, yinv, invrep},
  sumkinit = 0;
  sumknest = 0;
  extra = 0;
  Dsumkinit = 0;
  Dsumknest = 0;
  SumRepKinit = 0;
  invrep = {1/Sigma`Summation`Objects`Private`MyPower[x, kinit] :> 
     Sigma`Summation`Objects`Private`MyPower[xinv, kinit], 
    1/Sigma`Summation`Objects`Private`MyPower[x, knest] :> 
     Sigma`Summation`Objects`Private`MyPower[xinv, knest],
    1/Sigma`Summation`Objects`Private`MyPower[y, kinit] :> 
     Sigma`Summation`Objects`Private`MyPower[yinv, kinit], 
    1/Sigma`Summation`Objects`Private`MyPower[y, knest] :> 
     Sigma`Summation`Objects`Private`MyPower[yinv, knest]};
  Print["Use SigmaReduce? : ", NoSigmaReduce === 0, 
   ", and Combine? : ", combine === 1];
  For[i = 1, i <= Length[SumRep], i++,
   If[FreeQ[SumRep[[i]], kinit] && FreeQ[SumRep[[i]], knest],
     extra = 
       SumRep[[i]] // ToHLogs // ToMathematicaConstants // Simplify;
     ,
     If[FreeQ[SumRep[[i]], knest],
       If[combine === 0,
         Print[""];
         Print["Start Sum: EvaluateMultiSum on K_init: ", kinit];
         If[NoSigmaReduce === 0,
          SigRed = SigmaReduceList[SumRep[[i]], kinit] /. invrep;
          ,
          SigRed = SumRep[[i]] /. invrep;
          ];
         sumkinit = 
          TransformToSSums[
             EvaluateMultiSum[
               SigRed, {{kinit, 0, Infinity}}, {}, {}, {}, 
               Assumptions -> 
                0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
                 0 < yinv < 1] /. {LinInf -> Infinity}, 
             Assumptions -> 
              0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
               0 < yinv < 1] // SinfToH // VariableToArgument;
         Print["EvaluateMultiSum of K_init: ", sumkinit];
         ,
         Print["Store Sum Rep of Kinit ", kinit, 
          " and combine it with double summation."];
         SumRepKinit = SumRep[[i]];
         ];
       ,
       If[FreeQ[SumRep[[i]], kinit],
         Print[""];
         Print["Start Sum: EvaluateMultiSum on K_nest: ", knest];
         If[NoSigmaReduce === 0,
          SigRed = SigmaReduceList[SumRep[[i]], knest] /. invrep;
          ,
          SigRed = SumRep[[i]] /. invrep;
          ];
         sumknest = 
          TransformToSSums[
             EvaluateMultiSum[
               SigRed, {{knest, 0, Infinity}}, {}, {}, {}, 
               Assumptions -> 
                0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
                 0 < yinv < 1] /. {LinInf -> Infinity}, 
             Assumptions -> 
              0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
               0 < yinv < 1] // SinfToH // VariableToArgument;
         Print["EvaluateMultiSum of K_nest: ", sumknest];
         ,
         (*Double Summation*)
         Print[""];
         Print["Start Double Sum: EvaluateMultiSum on K_nest: ", 
          knest];
         Dsumknest = 
          EvaluateMultiSum[
           SigmaReduceList[SumRep[[i]], knest] /. 
            invrep, {{knest, 0, Infinity}}, {kinit}, {0}, {Infinity}, 
           
           Assumptions -> 
            0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 0 < yinv < 1];
         Print["EvaluateMultiSum of K_nest: ", Dsumknest];
         If[combine === 0,
          Print[""];
          
          Print["Start Double Sum: EvaluateMultiSum on K_init: ", 
           kinit];
          If[NoSigmaReduce === 0,
           DSumRepLast = SigmaReduceList[Dsumknest, kinit] /. invrep;
           ,
           DSumRepLast = Dsumknest (* /.KinVarReplace*);
           ];
          ,
          DSumRepLast = Dsumknest + SumRepKinit /. invrep;
          Print["Combine 1D & 2D Sum Rep of Kinit ", kinit];
          (*Print["Combined Sum Rep: ",DSumRepLast];*)
          ];
         Dsumkinit = 
          TransformToSSums[
             EvaluateMultiSum[
               DSumRepLast, {{kinit, 0, Infinity}}, {}, {}, {}, 
               Assumptions -> 
                0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
                 0 < yinv < 1] /. {LinInf -> Infinity}, 
             Assumptions -> 
              0 < x < 1 && 0 < y < 1 && 0 < xinv < 1 && 
               0 < yinv < 1] // SinfToH // VariableToArgument;
         ];
       ];
     ];
   ];
  Return[ReduceConstants[(extra + sumkinit + sumknest + 
         Dsumkinit) /. {xinv -> 1/x, yinv -> 1/y} // ToHLogs, 
     ToKnownConstants -> True] // ToMathematicaConstants];
  ]


AutoHarmonicSum2D[SumRep_, kinit_, knest_, x_] := Module[
  {i, extra, sumkinit, sumknest, Dsumknest, Dsumkinit},
  sumkinit = 0;
  sumknest = 0;
  extra = 0;
  Dsumkinit = 0;
  Dsumknest = 0;
  For[i = 1, i <= Length[SumRep], i++,
   If[FreeQ[SumRep[[i]], kinit] && FreeQ[SumRep[[i]], knest],
     extra = 
       SumRep[[i]] // ToHLogs // ToMathematicaConstants // Simplify;,
     If[FreeQ[SumRep[[i]], knest],
       Print[""];
       Print["Start Sum: HarmonicSums on K_init: ", kinit];
       sumkinit = 
        TransformToSSums[
           HarmonicSumsSum[
            SigmaReduce[SumRep[[i]], kinit], {kinit, 0, Infinity}], 
           Assumptions -> 0 < x < 1] // SinfToH // VariableToArgument;
       Print["HarmonicSums Output of K_init: ", sumkinit];
       ,
       If[FreeQ[SumRep[[i]], kinit],
        Print[""];
        Print["Start Sum: HarmonicSums on K_nest: ", knest]; 
        sumknest = 
         TransformToSSums[
            HarmonicSumsSum[
             SigmaReduce[SumRep[[i]], knest], {knest, 0, Infinity}], 
            Assumptions -> 0 < x < 1] // SinfToH // VariableToArgument;
        Print["HarmonicSums Output on K_nest: ", sumknest];
        ,
        (*Double Summation*)
        Print[""];
        Print["Start Double Sum: EvaluateMultiSum on K_nest: ", knest];
        Dsumknest = 
          EvaluateMultiSum[
            SumRep[[i]], {{knest, 0, 
              Infinity}}, {kinit}, {0}, {Infinity}, 
            Assumptions -> 0 < x < 1] /. LinInf -> Infinity;
        
        Print["EvaluateMultiSum Output on K_nest:", Dsumknest];
        Print[""];
        Print["Start Double Sum: HarmonicSums on K_init: ", kinit];
        Dsumkinit = 
         TransformToSSums[
            HarmonicSumsSum[
             SigmaReduce[Dsumknest, kinit], {kinit, 0, Infinity}], 
            Assumptions -> 0 < x < 1] // SinfToH // VariableToArgument;
        ];
       ];
     ];
   ];
  Return[ReduceConstants[(extra + sumkinit + sumknest + Dsumkinit) // 
       ToHLogs, ToKnownConstants -> True] // ToMathematicaConstants //
     Simplify];
  ]
  
AutoGenerateFuncSum2D[SumRep_, kBS_, knest_, split_ : 0] := Module[
  {i, j, extra, sumkBS, sumknest, Dsumknest, DsumkBS, xi},
  sumkBS = 0;
  sumknest = 0;
  extra = 0;
  DsumkBS = 0;
  Dsumknest = 0;
  For[i = 1, i <= Length[SumRep], i++,
    If[SumRep[[i]] =!= {},
     If[FreeQ[SumRep[[i]], kBS] && FreeQ[SumRep[[i]], knest],
       extra =
         SumRep[[i]] // ToHLogs // ToMathematicaConstants // Simplify;,
       If[FreeQ[SumRep[[i]], knest],
         Print[""];
         Print["Start Sum: GeneratingFunction on K_BS: ", kBS];
         If[split === 0,
          sumkBS =
            ComputeGeneratingFunction[SigmaReduce[SumRep[[i]], kBS],
              xi, {kBS, 0, Infinity}] /. xi -> 1;,
          Print["split present: ", split];
          sumkBS = SumGeneretingFunc[SumRep[[i]], kBS];
          ];
         Print["EvaluateMultiSum of K_BS: ", sumkBS];
         ,
         If[FreeQ[SumRep[[i]], kBS],
           Print[""];
           Print["Start Sum: EvaluateMultiSum on K_nest: ", knest];

           sumknest =
            TransformToSSums[
               EvaluateMultiSum[
                 SigmaReduce[SumRep[[i]],
                  knest], {{knest, 0, Infinity}}, {}, {}, {},
                 Assumptions -> 0 < x < 1] /. {LinInf -> Infinity}] //
              SinfToH // VariableToArgument;
           Print["EvaluateMultiSum of K_nest: ", sumknest];
           ,
           (*Double Summation*)
           Print[""];

           Print["Start Double Sum: EvaluateMultiSum on K_nest: ",
            knest];

           Dsumknest =
            EvaluateMultiSum[
             SumRep[[
              i]], {{knest, 0, Infinity}}, {kBS}, {0}, {Infinity},
             Assumptions -> 0 < x < 1];
           Print["EvaluateMultiSum of K_nest: ", Dsumknest];
           Print[""];
           Print["Start Double Sum: GeneratingFunctions on K_BS: ", kBS];
           If[split === 0,

            DsumkBS =
              ComputeGeneratingFunction[SigmaReduce[Dsumknest, kBS],
                xi, {kBS, 0, Infinity}] /. xi -> 1;,
            (*split generating functions*)

            Print["split present: ", split];
            DsumkBS = SumGeneretingFunc[Dsumknest, kBS];
            ];
           ];
         ];
       ];
     ];
   ];
  Return[(extra + sumkBS + sumknest + DsumkBS) // SpecialGLToH //
       GLToH // ToHLogs // ToMathematicaConstants // Simplify];
  ]
  

AutoGenerateFuncSum2DX[SumRep_, kBS_, knest_, x_, split_ : 0] :=
 Module[
  {i, j, extra, sumkBSlist, sumkBS, sumknest, Dsumknest, DsumkBSlist,
   DsumkBS, xi},
  sumkBS = 0;
  sumknest = 0;
  extra = 0;
  DsumkBS = 0;
  Dsumknest = 0;
  For[i = 1, i <= Length[SumRep], i++,
   If[FreeQ[SumRep[[i]], kBS] && FreeQ[SumRep[[i]], knest],
     extra =
       SumRep[[i]] // ToHLogs // ToMathematicaConstants // Simplify;
     ,
     If[FreeQ[SumRep[[i]], knest], Print[""];
       Print["Start Sum: GeneratingFunction on K_BS: ", kBS];
       sumkBSlist = List @@ Collect[SumRep[[i]], x, Simplify];
       Print["Decompose via kinematic x dependence into list: ",
        sumkBSlist, " with Length = ", Length[sumkBSlist]];
       For[j = 1, j <= Length[sumkBSlist], j++,
        Print["Working on part # ", j];
        If[split === 0,

         sumkBS =
           ComputeGeneratingFunction[
             SigmaReduce[sumkBSlist[[j]], kBS],
             xi, {kBS, 0, Infinity}] /. xi -> 1;,
         Print["split present: ", split];
         sumkBS = SumGeneretingFunc[sumkBSlist[[j]], kBS];
         ];
        ];
       Print["EvaluateMultiSum of K_BS: ", sumkBS];
       ,
       If[FreeQ[SumRep[[i]], kBS],
         Print[""];
         Print["Start Sum: EvaluateMultiSum on K_nest: ", knest];

         sumknest =
          TransformToSSums[
             EvaluateMultiSum[
               SigmaReduce[SumRep[[i]],
                knest], {{knest, 0, Infinity}}, {}, {}, {},
               Assumptions -> 0 < x < 1] /. {LinInf -> Infinity}] //
            SinfToH // VariableToArgument;
         Print["EvaluateMultiSum of K_nest: ", sumknest];,
         (*Double Summation*)
         Print[""];

         Print["Start Double Sum: EvaluateMultiSum on K_nest: ",
          knest];

         Dsumknest =
          EvaluateMultiSum[
           SumRep[[i]], {{knest, 0,
             Infinity}}, {kBS}, {0}, {Infinity},
           Assumptions -> 0 < x < 1];
         Print["EvaluateMultiSum of K_nest: ", Dsumknest];
         Print[""];
         Print["Start Double Sum: GeneratingFunctions on K_BS: ", kBS];
         DsumkBSlist = List @@ Collect[Dsumknest, x, Simplify];

         Print["Decompose via kinematic x dependence into list: ",
          DsumkBSlist, " with Length = ", Length[DsumkBSlist]];
         For[j = 1, j <= Length[DsumkBSlist], j++,
          Print["Working on part # ", j];
          If[split === 0,

           DsumkBS =
             ComputeGeneratingFunction[
               SigmaReduce[DsumkBSlist[[j]], kBS],
               xi, {kBS, 0, Infinity}] /.
              xi -> 1;,(*split generating functions*)

           Print["split present: ", split];
           DsumkBS = SumGeneretingFunc[DsumkBSlist[[j]], kBS];
           ];
          ];
         ];
       ];
     ];
   ];
  Return[(extra + sumkBS + sumknest + DsumkBS) // SpecialGLToH //
       GLToH // ToHLogs // ToMathematicaConstants // Simplify];
  ]


  
(* Functions for 1-dim MB summation *)

CheckInfinity1D[MBres_, k1_, SmallInvariant_, kin_ : "none", IgnoreBDconst_ : 0, LRinit_: 0] := 
 Module[{i, k, pass, BDnum, Pass, NumK, NumX, NumKCheck, BDnumNew, RelDiff, KinLim},
  Pass = True;
  NumK = 100;
  If[ LRinit === 0,
    NumX = 1;
    ,
    If[ LRinit === 2,
        NumX = 0.1;
        ,
        If [ LRinit === 1,
            NumX = 2;
        ];
    ];
  ];
  KinLim = {SS -> 1, TT -> 1, UU -> 1, SmallInvariant -> 1/2, 
   kin -> NumX, x -> NumX, y -> NumX, w -> NumX, xi -> NumX};
  Print["LR init : ", LRinit, ", and set ", kin, " = ", NumX, ", and set ", SmallInvariant, " = 1/2"];
  Print["Note SS = TT = UU = 1 and ",{x,y,w,xi}," = ", NumX ," by default for numerical semi-circle arc checks"];

  For[i = 1, i <= Length[MBres], i++,
   If[MBres[[i]] =!= {},
     For[k = 1, k <= Length[MBres[[i]]], k++, 
       If[Not[FreeQ[MBres[[i, k]], k1]], 
         BDnum = MBres[[i, k]] /. KinLim /. {k1 -> NumK} //
            N;
         If[
          Abs[BDnum] > 0.08 || Not[FreeQ[BDnum, ComplexInfinity]] ||
           Not[FreeQ[BDnum, Indeterminate]], 
          Print["Warning: Boundary is not vanishing with BDnum = ", 
           BDnum, ", at indices {i,j,k} =", {i, k}];
          
          If[IgnoreBDconst === 0 || 
            Not[FreeQ[BDnum, ComplexInfinity]] || 
            Not[FreeQ[BDnum, Indeterminate]],
           Pass = False;
           ,
           
           Print["IgnoreBDconst present, test whether BD at infinity \
is constant or divergent."];
           NumKCheck = {NumK*2, NumK*3};
           
           BDnum = 
            MBres[[i, k]] /. KinLim /. {k1 -> 
                NumKCheck[[1]]} // N;
           
           BDnumNew = 
            MBres[[i, k]] /. KinLim /. {k1 -> 
                NumKCheck[[2]]} // N;
           
           If[Abs[BDnumNew] < 0.05,
                Print["BD at infinity is actually vanishing with BDnumNew = ", BDnumNew];
                RelDiff = 0;
                ,
                RelDiff = Abs[(BDnumNew - BDnum)/BDnum];
              ]
           
           If[RelDiff > 0.01, 
            Print["BD at infinity is NOT constant with Relative \
Deviation: ", RelDiff, " and BDnumNew = ", BDnumNew];
            Pass = False;, 
            Print["BD at infinity is constant: ", BDnumNew, 
              ", with Relative Deviation: ", RelDiff];
            ];
           ];
          ];
         ];
       ];
     ];
   ];
  Return[Pass];
  ]

ConvertToSigma1D[MBres_, k1_] := Module[{i, k, MBextra, MBk1},
  MBextra = 0;
  MBk1 = 0;
  For[i = 1, i <= Length[MBres], i++,
   If[MBres[[i]] =!= {},
     For[k = 1, k <= Length[MBres[[i]]], k++,
       If[FreeQ[MBres[[i, k]], k1],
         MBextra = MBextra + (MBres[[i, k]] // ToSigma);,
         MBk1 = MBk1 + (MBres[[i, k]] // ToSigma);
         ];
       ];
     ];
   ];
  Return[{MBextra, MBk1}];
  ]

GetSumRep1D[MBexp_, Zinit_, kinit_, Zcontour_, SmallInvariant_, kin_ : "none", LRpreset_ : 0, 
  AddShift_ : 0, IgnoreBDconst_ : 0] := Module[
  {InitResList, InitLR, Res1, NestResList, NestLR, Res2, pass, 
   ResSigma},
  Print["Initial MB integral:", MBexp];
  InitResList = GetResList[MBexp, Zinit, Zcontour, AddShift];
  Print["Initial Res List:", InitResList];
  If[LRpreset === 0, 
   InitLR = DecideLR[InitResList];
   Print["Initial L/R choice auto decided: ", InitLR];
   , 
   InitLR = LRpreset;
   Print["Initial L/R choice overwrite to LR present: ", InitLR];];
  Res1 = TakeRes[MBexp, InitResList, Zinit, kinit, InitLR];
  (*Print["MB 1st SumRep:",Res1];*)
  
  pass = CheckInfinity1D[Res1, kinit, SmallInvariant, kin, IgnoreBDconst, InitLR];
  If[pass === True,
   Print["Boundary check PASS."];
   ,
   Print["Boundary check NOT pass !!!"];
   ];
  Return[{pass, Res1}];
  ]


AutoSumRep1D[MBexp_, Zinit_, kinit_, Zcontour_, SmallInvariant_, kin_ : "none", LRpreset_ : 0, 
  AddShift_ : 0, IgnoreBDconst_ : 0] := Module[
  {MBList, i, SumRepi, SumRepk1, SumRepk2, SumRepk1k2, SumRepEx, 
   LRList, SumResi, count, AddSFTcount},
   
  If[Head[MBexp] === Plus,
    MBList = List @@ Expand[MBexp];
    Print["Total integral number #", Length[MBList]];
    ,
  If[Head[MBexp] === Times,
      MBList = {MBexp};
      Print["Only 1 integral to be computed."];
      ,
      Print["Head of MBexp is ", MBexp];
      Print["MB expression: ", MBexp];
      Print["Do not compute anything."];
      Return[MBexp];
    ];
  ];
  
  SumRepk1 = 0;
  SumRepEx = 0;
  LRList = {2, 1};
  For[i = 1, i <= Length[MBList], i++, Print[""];
   Print["==================================="];
   Print["Calculation integral #", i];
   SumResi = 
    GetSumRep1D[MBList[[i]], Zinit, kinit, Zcontour, SmallInvariant, kin, LRpreset, 
     AddShift, IgnoreBDconst];
   count = 1;
   AddSFTcount = AddShift;
   While[Not[SumResi[[1]]],
    Print["-------------------"];
    Print["Boundary check FAILS, try other LRpresets #", count, 
     " with ", LRList[[count]], " and AddShift: ", AddSFTcount];
    SumResi = 
     GetSumRep1D[MBList[[i]], Zinit, kinit, Zcontour, SmallInvariant, kin, LRList[[count]],
       AddSFTcount, IgnoreBDconst];
    count = count + 1;
    If[count > 2 && AddSFTcount === 0,
     If[SumResi[[1]] === False, Print[""];
       Print[
        "All 2 LRpreset FAIL. Set AddShift parameter and reset \
LRprest count."];
       AddSFTcount = AddSFTcount + 1;
       count = 1;
       ];
     ];
    If[count > 2 && AddSFTcount > 0,
     If[SumResi[[1]] === False, Print[""];
       Print["All 4 LRpreset + AddShift FAIL!!! Abort."];
       Abort[];
       ];
     ];
    ];
   SumRepi = ConvertToSigma1D[SumResi[[2]], kinit];
   SumRepEx = SumRepEx + SumRepi[[1]];
   SumRepk1 = SumRepk1 + SumRepi[[2]];
   ];
  Return[{SumRepEx, SumRepk1}];
  ]


ConvertToMMaSum1D[MBres_, k1_] := Module[{i, k, MBextra, MBk1},
  MBextra = 0;
  MBk1 = 0;
  For[i = 1, i <= Length[MBres], i++,
   If[MBres[[i]] =!= {},
     For[k = 1, k <= 2, k++,
       If[FreeQ[MBres[[i, k]], k1],
         MBextra = MBextra + MBres[[i, k]];
         ,
         MBk1 = MBk1 + Sum[MBres[[i, k]], {k1, 0, Infinity}];
         ];
       ];
     ];
   ];
  Return[{MBextra, MBk1}];
  ]  
  


AutoSum1DforPSLQ[MBexp_, Zinit_, kinit_, Zcontour_, SmallInvariant_, kin_ : "none", LRpreset_ : 0, 
  AddShift_ : 0, IgnoreBDconst_ : 0, ToNotSum_ : 0] := Module[
  {MBList, i, SumRepi, SumRepk1, SumRepk2, SumRepk1k2, SumRepEx, 
   LRList, SumResi, count, AddSFTcount},
  If[Head[MBexp] === Plus,
   MBList = List @@ Expand[MBexp];
   Print["Total integral number #", Length[MBList]];
   ,
   If[Head[MBexp] === Times,
     MBList = {MBexp};
     Print["Only 1 integral to be computed."];, 
     Print["Head of MBexp is ", MBexp];
     Print["MB expression: ", MBexp];
     Print["Do not compute anything."];
     Return[MBexp];
     ];
   ];
  SumRepk1 = 0;
  SumRepEx = 0;
  LRList = {1, 2};
  For[i = 1, i <= Length[MBList], i++, Print[""];
   Print["==================================="];
   Print["Calculation integral #", i];
   SumResi = 
    GetSumRep1D[MBList[[i]], Zinit, kinit, Zcontour, SmallInvariant, kin, LRpreset, 
     AddShift, IgnoreBDconst];
   count = 1;
   AddSFTcount = AddShift;
   While[Not[SumResi[[1]]], Print["-------------------"];
    Print["Boundary check FAILS, try other LRpresets #", count, 
     " with ", LRList[[count]], " and AddShift: ", AddSFTcount];
    SumResi = 
     GetSumRep1D[MBList[[i]], Zinit, kinit, Zcontour, SmallInvariant, kin, LRList[[count]],
       AddSFTcount, IgnoreBDconst];
    count = count + 1;
    If[count > 2 && AddSFTcount === 0, 
     If[SumResi[[1]] === False, Print[""];
       Print[
        "All 2 LRpreset FAIL. Set AddShift parameter and reset \
LRprest count."];
       AddSFTcount = AddSFTcount + 1;
       count = 1;];];
    If[count > 2 && AddSFTcount > 0, 
     If[SumResi[[1]] === False, Print[""];
       Print["All 4 LRpreset + AddShift FAIL!!! Abort."];
       Abort[];];];];
   Print["SumResi", SumResi];
   If[ ToNotSum =!= 0,
     Print["ToNotSum present: ", ToNotSum];
     Return[SumResi[[2]]];
   ];
   SumRepi = ConvertToMMaSum1D[SumResi[[2]], kinit];
   SumRepEx = SumRepEx + SumRepi[[1]];
   SumRepk1 = SumRepk1 + SumRepi[[2]];
   ];
  Return[{SumRepEx, SumRepk1}];
  ]


AutoMultiSum1D[SumRep_, k1_, x_, NoSigmaRed_: 0] := Module[{extra, sum, SigRed, SumRed, output},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Factor;
  Print["Use SigmaReduce? : ", NoSigmaRed === 0];
  If[NoSigmaRed === 0,
    SigRed = SigmaReduceList[SumRep[[2]], k1];
    Print["0. SigmaReduceList Done! "];
    ,
    SigRed = SumRep[[2]];
    ];
  sum = EvaluateMultiSum[SigRed, {{k1, 0, Infinity}}, {}, {}, {}, 
    Assumptions -> 0 < x < 1];
  Print["1. EvaluateMultiSum Done! "];
  SumRed = 
   TransformToSSums[sum /. {LinInf -> Infinity}, 
      Assumptions -> 0 < x < 1] // SinfToH // VariableToArgument;
  Print["2. Transform to HPL Done! "];
  output = ReduceConstants[extra + SumRed // ToHLogs, 
      ToKnownConstants -> True] // ToMathematicaConstants // Collect[#, {_H,Pi,_Zeta}, Factor]&;
  Print["3. Simplification Done! Output! "];
  Return[output];
  ]


AutoMultiSum1DBS[SumRep_, k1_, x_, NoSigmaRed_ : 0] := 
 Module[{extra, sum, SigRed, SumRed},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  Print["Use SigmaReduce? : ", NoSigmaRed === 0];
  If[NoSigmaRed === 0,
   SigRed = SigmaReduceList[SumRep[[2]], k1];
   Print["0. SigmaReduceList Done! "];
   ,
   SigRed = SumRep[[2]];
   ];
  sum = EvaluateMultiSum[SigRed, {{k1, 0, Infinity}}, {}, {}, {}, 
    Assumptions -> 0 < x < 1];
  Print["1. EvaluateMultiSum Done! "];
  SumRed = 
   ReduceToHBasis[
      BSToGL[TransformToSSums[sum /. {LinInf -> Infinity}, 
          Assumptions -> 0 < x < 1], x] // SpecialGLToH // GLToH] // 
     PowerExpand // Simplify;
  Print["2. Transform to HPL Done! "];
  Return[ReduceConstants[extra + SumRed // ToHLogs, 
       ToKnownConstants -> True] // ToMathematicaConstants // 
     PowerExpand // Simplify];
  ]


AutoMultiSum1D2Kin[SumRep_, k1_, x_, y_ : "none", NoSigmaRed_ : 0] := Module[
  {extra, sum, SigRed, SumRed},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  Print["Use SigmaReduce? : ", NoSigmaRed === 0];
  If[NoSigmaRed === 0,
   SigRed = SigmaReduceList[SumRep[[2]], k1];
   ,
   SigRed = SumRep[[2]];
   ];
  sum = EvaluateMultiSum[SigRed, {{k1, 0, Infinity}}, {}, {}, {}, 
    Assumptions -> 0 < x < 1 && 0 < y < 1];
  SumRed = 
   TransformToSSums[sum /. {LinInf -> Infinity}, 
      Assumptions -> 0 < x < 1 && 0 < y < 1] // SinfToH // 
    VariableToArgument;
  Return[ReduceConstants[extra + SumRed // ToHLogs, 
      ToKnownConstants -> True] // ToMathematicaConstants // Simplify];
  ]

  
AutoMultiSum1DParallel[SumRep_, k1_, x_, TransParaList_ : {}] := 
 Module[
  {extra, sum, SigRed, output, dummy, ParaList, SigRedList, i, 
   SigRed2, ToSumList, SumList, ParaSum, ParaSumRed, n, SumRed},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Factor;
  SigRed = SigmaReduceList[SumRep[[2]], k1];
  Print["0: Preliminary SigmaReduceList Done! Start to make list of \
expressions... "];
  If[Length[TransParaList] <= 4,
   ParaList = 
     Join[TransParaList, 
      ConstantArray[dummy, 4 - Length[TransParaList]]];
   ,
   ParaList = TransParaList[[1 ;; 4]];
   Print["Length of TransParaList > 0 not implemented. Use only first \
4 parameters."];
   ];
  SigRedList = 
   DeleteCases[sort4DMB[SigRed, Sequence @@ ParaList], 0];
  ToSumList = {};
  For[i = 1, i <= Length[SigRedList], i++,
   SigRed2 = SigmaReduceList[SigRedList[[i]], k1];
   If[Head[SigRed2] === Plus,
    AppendTo[ToSumList, List @@ SigRed2];
    ,
    If[Head[SigRed2] === Times,
      AppendTo[ToSumList, SigRed2];
      ,
      If[SigRed2 =!= 0,
        Print["Non-zero expression with Head: ", Head[SigRed2], 
         " appear. Stop and debug."];
        Print["Problemtic expression: ", SigRed2];
        Abort[];
        ];
      ];
    ];
   ];
  SumList = Flatten[ToSumList];
  Print["Parallel Summation List prepared with Lenght #", 
   Length[SumList]];
  (*Print["Parallel SumList = ",SumList];*)
  SumRed = ParallelSum[
    Print["Start summation for #", n];
    EvaluateMultiSum[SumList[[n]], {{k1, 0, Infinity}}, {}, {}, {}, 
      Assumptions -> 0 < x < 1] // 
     VariableToArgument[
       SinfToH[TransformToSSums[# /. {LinInf -> Infinity}, 
         Assumptions -> 0 < x < 1]]] &
    , {n, 1, Length[SumList]}];
  output = 
   ReduceConstants[extra + SumRed // ToHLogs, 
      ToKnownConstants -> True] // ToMathematicaConstants // 
    Collect[#, {_H, Pi, _Zeta}, Factor] &;
  Print["3. Simplification Done! Output! "];
  Return[output];
  ]
  
  
AutoMultiSum1DAddScale[SumRep_, k1_, x_, NoSigmaRed_ : 0] := 
 Module[{extra, sum, SigRed, SumRed, xi},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  Print["Use SigmaReduce? : ", NoSigmaRed === 0];
  If[NoSigmaRed === 0,
   SigRed = SigmaReduceList[SumRep[[2]]*xi^k1 // ToSigma, k1];
   Print["0. SigmaReduceList Done! "];
   ,
   SigRed = SumRep[[2]]*xi^k1 // ToSigma;
   ];
  sum = EvaluateMultiSum[
    SigRed // ToSigma, {{k1, 0, Infinity}}, {}, {}, {}, 
    Assumptions -> 0 < x < 1 && 0 < xi < 1];
  Print["1. EvaluateMultiSum Done! "];
  SumRed = 
   ReduceToGLBasis[
        BSToGL[TransformToSSums[sum /. {LinInf -> Infinity}, 
           Assumptions -> 0 < x < 1 && 0 < xi < 1], xi] /. xi -> 1, 
        1] // SpecialGLToH // GLToH // SinfToH // VariableToArgument;
  Print["2. Transform to HPL Done! "];
  Return[extra + SumRed // ToHLogs // ToMathematicaConstants // 
    Expand];
  ]

AutoHarmonicSum1DAddScale[SumRep_, k1_, x_, NoSigmaRed_ : 0] := 
 Module[{extra, sum, SigRed, SumRed, xi, ToSsum},
  extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  Print["Use SigmaReduce? : ", NoSigmaRed === 0];
  If[NoSigmaRed === 0,
   SigRed = SigmaReduceList[SumRep[[2]]*xi^k1 // ToSigma, k1];,
   SigRed = SumRep[[2]]*xi^k1 // ToSigma;
   ];
  sum = HarmonicSumsSum[SigRed, {k1, 0, Infinity}];
  ToSsum = 
   TransformToSSums[
    TransformToSSums[sum, Assumptions -> 0 < x < 1 && 0 < xi < 1] // 
     ToHarmonicSumsSum, Assumptions -> 0 < x < 1 && 0 < xi < 1];
  SumRed = 
   ReduceToGLBasis[BSToGL[ToSsum, xi] /. xi -> 1, 1] // SpecialGLToH //
       GLToH // SinfToH // VariableToArgument;
  Return[extra + SumRed // ToHLogs // ToMathematicaConstants // 
    Expand];
  ]
  
AutoHarmonicSum1D[SumRep_, k1_, x_] :=
 Module[{extra, sum, SumRed}, extra = 0;
  sum = 0;
  SumRed = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  sum = HarmonicSumsSum[
    SigmaReduce[SumRep[[2]], k1], {k1, 0, Infinity}];
  SumRed =
   SinfToH[TransformToSSums[sum, Assumptions -> 0 < x < 1]] //
    VariableToArgument;
  Return[
   ReduceConstants[extra + SumRed // ToHLogs,
      ToKnownConstants -> True] // ToMathematicaConstants // Simplify];
  ]

AutoGenerateFuncSum1D[SumRep_, k1_, split_ : 0] :=
 Module[{extra, sum, xi, sumreplist, i, func},
  extra = 0;
  sum = 0;
  extra = SumRep[[1]] // ToHLogs // ToMathematicaConstants // Simplify;
  If[split === 0,
   sum = (ComputeGeneratingFunction[SigmaReduce[SumRep[[2]], k1],
        xi, {k1, 0, Infinity}] /. xi -> 1);,
   (*split generating functions*)
   Print["split present: ", split];
   sumreplist = List @@ Expand[SigmaReduce[SumRep[[2]], k1], S[__]];
   Print["List after Sigma reduce:", sumreplist];
   Print["Toal exp to sum #:", Length[sumreplist]];
   For[i = 1, i <= Length[sumreplist], i++,
    Print["Sum exp #", i];
    func =
     ComputeGeneratingFunction[sumreplist[[i]],
       xi, {k1, 0, Infinity}] /. xi -> 1;
    Print["GeneratingFunc: ", func];
    sum = sum + func;
    ];
   ];
  Return[(extra + sum) // SpecialGLToH // GLToH //
     ToMathematicaConstants // Simplify];
  ]



(* generalised Barnes lemma, primitive version *)
GeneraliseBarnes[ex_, Z_, RegA_, RegB_] := 
 Module[{ex1, ex2, ListNoZ, ListZ, ParaList, n1, n2, N1, N2, a1, a2, 
   b1, b2, b3, c, Barnes, FinalExp, i},
  ex1 = List @@ ex;
  Print["original exp: ", ex];
  ListNoZ = {};
  ListZ = {};
  Do[{If[FreeQ[ex1[[i]], Z],
      AppendTo[ListNoZ, ex1[[i]]],
      AppendTo[ListZ, ex1[[i]]]
      ];}, {i, 1, Length[ex1]}];
  ex2 = {Times @@ ListNoZ, Times @@ ListZ};
  Print["split exp: ", ex2];
  ParaList = 
   ex2[[2]] /. {Gamma[aa1_ - Z] Gamma[aa2_ - Z] Gamma[bb1_ + Z] Gamma[
        bb2_ + Z] Gamma[bb3_ + Z]/Gamma[cc_ + Z] :> {{aa1, aa2}, {bb1,
         bb2, bb3}, {cc}}};
  Print["parameter list ({a1,a2},{b1,b2,b3},{c}) = ", ParaList];
  a1 = ParaList[[1, 1]];
  a2 = ParaList[[1, 2]];
  b1 = ParaList[[2, 2]];
  b2 = ParaList[[2, 3]];
  b3 = ParaList[[2, 1]];
  c = ParaList[[3, 1]];
  n1 = a1 + b1;
  n2 = a2 + b2;
  N1 = n1 /. {RegA -> 0, RegB -> 0};
  N2 = n2 /. {RegA -> 0, RegB -> 0};
  Print["{a1,a2,b1,b2,b3,c}, {n1,n2}, {N1,N2} = ", {a1, a2, b1, b2, 
    b3, c}, ", ", {n1, n2}, ", ", {N1, N2}];
  Barnes = Sum[Sum[
     (-1)^(i1 + i2)/(b1 - b2 - n1 + n2 + i1 - 
         i2) (Gamma[-b1 + b3 + n1 + i2]/Gamma[1 - b1 + c - n2 + i2] - 
        Gamma[-b2 + b3 + n2 + i1]/Gamma[1 - b2 + c - n1 + i1])*
      (Gamma[n1] Gamma[n2] Gamma[-b1 + b2 + n1] Gamma[
          b1 - b2 + n2] Gamma[1 - b3 + c - n1 - n2])/(Gamma[
          i1 + 1] Gamma[i2 + 1] Gamma[n1 - i1] Gamma[
          n2 - i2] Gamma[-b3 + c])
     , {i2, 0, N2 - 1}], {i1, 0, N1 - 1}];
  Print["Barnes lemma applied: ", Barnes];
  (*Prefactor=(Gamma[a1+b1]Gamma[a2+b1]Gamma[a1+b2]Gamma[a2+b2]Gamma[
  a1+b3]Gamma[a2+b3])/(Gamma[a1+a2+b1+b3]Gamma[a1+a2+b2+b3]Gamma[-b3+
  c]);*)
  (*Print["Prefactor Gamma functions:",Prefactor];*)
  
  Return[FinalExp = ex2[[1]]*Barnes];
  ]  
  
  

  
(* other utility functions *)  

KinVarReplace = {
  1/Sigma`Summation`Objects`Private`MyPower[x, k1] :> 
   Sigma`Summation`Objects`Private`MyPower[y, k1],
  1/Sigma`Summation`Objects`Private`MyPower[x, k2] :> 
   Sigma`Summation`Objects`Private`MyPower[y, k2],
  1/Sigma`Summation`Objects`Private`MyPower[y, k1] :> 
   Sigma`Summation`Objects`Private`MyPower[x, k1],
  1/Sigma`Summation`Objects`Private`MyPower[y, k2] :> 
   Sigma`Summation`Objects`Private`MyPower[x, k2]
 };



Off[General::munfl]
Off[General::ovfl]
Off[N::meprec]
