(*---------------------------------------
 |                                      |
 |  MBNumRegionFunc.m of AsyInt v.1.0   |
 |                                      |
 |  by Hantian Zhang, July 15, 2024     |
 |                                      | 
 ----------------------------------------*)


DAlphaRep[U_, F_, l_, a__, x__, IdxDx_, d_] := 
 Module[{GammaDen, alphaNum, i, j, alpharep, alphaTmp, Dalpharep, 
   AlphaRepNum},
  alphaNum = 1;
  GammaDen = 1;
  For[i = 1, i <= Length[a], i++, 
   alphaNum = alphaNum*x[[i]]^(a[[i]] - 1);
   GammaDen = GammaDen*Gamma[a[[i]]];];
  alpharep = 1/GammaDen*U^(-d/2)*Exp[-F/U];
  (*Print["alpha rep before derivative: ",alpharep];*)
  
  alphaTmp = alpharep;
  For[j = 1, j <= Length[IdxDx], j++,
   Dalpharep = (-1)*D[alphaTmp, x[[IdxDx[[j]]]]] // Factor;
   alphaTmp = Dalpharep;
   alphaNum = alphaNum;
   ];
  AlphaRepNum = 
   Collect[
    alphaNum*Dalpharep /. 
      Table[
       Variables[a[[IdxDx[[j]]]]][[1]] -> 0, {j, 1, 
        Length[IdxDx]}] /. 
     Table[ x[[IdxDx[[j]]]] -> 0, {j, 1, Length[IdxDx]}], mts, 
    Simplify];
  Return[AlphaRepNum];
  ]
  
CleanPowerD = {Power[x__, D/2] :> (Expand[x])^(D/2), 
   Power[x__, a_ + D/2] :> (Expand[x])^(a + D/2), 
   Power[x__, a_ - D/2] :> (Expand[x])^(a - D/2), 
   Power[x__, -D/2] :> (Expand[x])^(-D/2)};


  

  


(* Functions for NPL integrals *)


AlphaExpNumRegions[AlphaRepNum_, UF__, Scaling__, SmallInvariant_, dlist_, xlist_, D_, 
  rhos_, order_, OrdRhoUp_ : 12] := 
 Module[{U, F, Ulist, UBase, RhoBase, i, n, alpharep, alphaexp, 
   alphaBase, alphaHigh, alphaHighList, AlphaHigherRegions, Uexp, 
   Uexp2, Fexp, Fexp2, RhoPowU, RhoPowF, alphaBaseOrigin, NumShift, 
   loops, NumExp, NumExp2, RhoPowNum, NumShiftScale, rhoInv, 
   NegRhoPow, NumExpLead, alphaNumExp},
  AlphaHigherRegions = {};
  loops = UF[[3]];
  OrdRho = Max[OrdRhoUp, order];

  alphaBaseOrigin = 
   AlphaRep[UF[[1]], UF[[2]], loops, dlist + 1, xlist, D];
  (*Print["AlphaRepNum: ",AlphaRepNum];
  Print["alphaBaseOrigin: ",alphaBaseOrigin];*)
  
  NumShift = AlphaRepNum/alphaBaseOrigin /. CleanPowerD // Simplify;
  Print["Original Numerator Shift: ", NumShift];
  (*start expanding UF poly and numerator shifts via scalings*)
  
  U = UF[[1]] /. Scaling /. rhos -> rho^2 // PowerExpand;
  F = UF[[2]] /. Scaling /. rhos -> rho^2 // PowerExpand // Factor;
  NumShiftScale = 
   NumShift /. Scaling /. rhos -> rho^2 // PowerExpand // Factor;
  (*Print["{U,F}=",{U,F}];*)
  (*Print["Expanded U=",Series[U,{rho,0,
  2}]];*)
  (*Ulist=CoefficientList[U,rho]//Simplify;
  Print["Ulist: ",Ulist];*)
  For[i = 1, i <= Length[Scaling], i++,
   Print["=================================="];
   Print["Calculating region #", i];
   UBase = DeleteCases[CoefficientList[U[[i]], rho] // Factor, 0][[1]];
   (*Print["UBase: ",UBase];*)
   alphaHighList = {UBase};
   Uexp = Series[U[[i]], {rho, 0, OrdRho}] // Normal;
   RhoPowU = (Position1stNonZero[CoefficientList[Uexp, rho]] - 1);
   Uexp2 = Collect[Uexp*rho^(-RhoPowU) // PowerExpand, rho, Simplify];
   (* Print["Uexp2: ", Uexp2]; *)
   Fexp = Series[F[[i]], {rho, 0, OrdRho}] // Normal;
   RhoPowF = (Position1stNonZero[CoefficientList[Fexp, rho]] - 1);
   Fexp2 = Collect[Fexp*rho^(-RhoPowF) // PowerExpand, rho, Simplify];
   (* Print["Fexp2: ", Fexp2]; *)
   NumExp = Series[NumShiftScale[[i]], {rho, 0, OrdRho}] // Normal;
   (*Print["NumExp: ",NumExp];*)
   
   NegRhoPow = -Exponent[NumExp /. rho -> 1/rhoInv, rhoInv];
   (*Print["NegRhoPow: ",NegRhoPow];*)
   
   RhoPowNum = (Position1stNonZero[
       CoefficientList[NumExp*rho^(-NegRhoPow), rho]] - 1);
   (*Print["RhoPowNum: ",RhoPowNum];*)
   
   NumExp2 = 
    Collect[NumExp*rho^(-RhoPowNum - NegRhoPow) // PowerExpand, {rho, SmallInvariant, SS, TT, UU, D}, 
     Factor];
   (*Print["NumExp2: ", NumExp2];*)
   NumExpLead = Coefficient[NumExp2, rho, 0];
   Print["NumExpLead: ", NumExpLead];
   (*build leading alpha representation from scaled UF poly and \
numerator shifts*)
   
   alpharep = AlphaRep[Uexp2, Fexp2, loops, dlist + 1, xlist, D];
   alphaexp = 
    Series[alpharep, {rho, 0, order}] // Normal // PowerExpand;
   alphaBase = 
    Coefficient[alphaexp, rho, 0] // Factor // PowerExpand;
   (*Print["Alpha Rep Exp: ",alphaexp];
   Print["Alpha Rep Base: ",alphaBase];*)
   
   AppendTo[alphaHighList, alphaBase];
   AppendTo[alphaHighList, NumExpLead];
   alphaNumExp = 
    Series[
       AlphaRep[Uexp2, Fexp2, loops, dlist + 1, xlist, D]*NumExp2, {rho, 
        0, order+2}] // Normal // PowerExpand;
   For[n = 1, n <= order, n++,
    Print["Higher order in rho:", n];
    alphaHigh = 
     Collect[(Coefficient[alphaNumExp, rho, n]/alphaBase) // Factor //
        PowerExpand, {SmallInvariant, SS, TT, UU, D}, Simplify];
    (*Print["alpha Higher order: ", alphaHigh];*)
    AppendTo[alphaHighList, alphaHigh];
    ];
   AppendTo[AlphaHigherRegions, alphaHighList];
   ];
  Return[AlphaHigherRegions];
  ]
  

