(*-----------------------------------------
 |                                        |
 |  MBHighRegionFunc.m of AsyInt v.1.0.1  |
 |                                        |
 |  by Hantian Zhang, Oct. 08, 2024       |
 |                                        | 
 ------------------------------------------*)



AlphaExpHighRegions[UF__, Scaling__, SmallInvariant_, dlist_, xlist_, order_, rhos_, 
  OrdRho_ : 20] := 
 Module[{U, F, Ulist, UBase, RhoBase, i, n, alpharep, alphaexp, 
   alphaBase, alphaHigh, alphaHighList, AlphaHigherRegions, Uexp, 
   Uexp2, Fexp, Fexp2, RhoPowU, RhoPowF, loops},
  AlphaHigherRegions = {};
  U = UF[[1]] /. Scaling /. rhos -> rho^2 // PowerExpand;
  F = UF[[2]] /. Scaling /. rhos -> rho^2 // PowerExpand // Simplify;
  loops = UF[[3]];
  For[i = 1, i <= Length[Scaling], i++,
   Print["=================================="];
   Print["Calculating region #", i];
   UBase = 
    DeleteCases[CoefficientList[U[[i]], rho] // Simplify, 0][[1]];
   (*Print["UBase: ",UBase];*)
   alphaHighList = {UBase};
   Uexp = Series[U[[i]], {rho, 0, OrdRho}] // Normal;
   RhoPowU = (Position1stNonZero[CoefficientList[Uexp, rho]] - 1);
   Uexp2 = 
    Collect[Uexp*rho^(-RhoPowU) // PowerExpand, rho, Simplify];
   Fexp = Series[F[[i]], {rho, 0, OrdRho}] // Normal;
   RhoPowF = (Position1stNonZero[CoefficientList[Fexp, rho]] - 1);
   Fexp2 = 
    Collect[Fexp*rho^(-RhoPowF) // PowerExpand, rho, Simplify];
   If[verbose === True,
    Print["U-exp: ", Uexp2];
    Print["F-exp: ", Fexp2];
    ];
   alpharep = AlphaRep[Uexp2, Fexp2, loops, dlist + 1, xlist, D];
   alphaexp = 
    Series[alpharep, {rho, 0, 2*order}] // Normal // PowerExpand;
   alphaBase = 
    Coefficient[alphaexp, rho, 0] // Simplify // PowerExpand;
   (*Print["Alpha Rep Exp: ",alphaexp];
   Print["Alpha Rep Base: ",alphaBase];*)
   
   AppendTo[alphaHighList, alphaBase];
   For[n = 1, n <= order, n++,
    Print["Higher order in rho:", n];
     alphaHigh = 
       Collect[(Coefficient[alphaexp, rho, n]/alphaBase) // Factor // 
         PowerExpand, {SmallInvariant, SS, TT, UU, D}, Simplify];
    If[verbose === True,
     Print["alpha Higher order: ", alphaHigh];
     ];
    AppendTo[alphaHighList, alphaHigh];
    ];
   (*Print["alpha Higher order List: ",alphaHighList];*)
   
   AppendTo[AlphaHigherRegions, alphaHighList];
   ];
  Return[AlphaHigherRegions];
  ]

  

Position1stNonZero[list_] := Module[{i, nonzero},
  nonzero = False;
  For[i = 1, i <= Length[list], i++,
   If[list[[i]] =!= 0,
     nonzero = True;
     If[nonzero === True,
      Return[i];
      ];
     ];
   ];
  Print["No Non-zero element. Abort!"];
  Abort[];
  ]



ShiftOP[Rtmp_, d_, dlist_] := Module[{dvar},
  (* dvar = Rtmp[[1, d]]; *)
  dvar = Variables[ dlist[[d]] ][[1]];
  Return[Pochhammer[1 + dvar, 1]*(Rtmp /. dvar -> dvar + 1)];
  ]
ShiftOP[Rtmp_, d_, n_, dlist_] := Module[{dvar},
  (* dvar = Rtmp[[1, d]]; *)
  dvar = Variables[ dlist[[d]] ][[1]];
  Return[Pochhammer[1 + dvar, n]*(Rtmp /. dvar -> dvar + n)];
  ]



ShiftOperatorsNPL[AlphaHigh_, vars_, order_] := 
  Module[{n, i, DenCheck, AlphaHighList, U, Ufactor, Shifts, Upower, 
    ShiftList, ShiftOps, expExp},
   U = AlphaHigh[[1]];
   ShiftOps = {};
   For[n = 1, n <= order, n++, 
    Print["-----------------------------"];
    Print["start extract shift operators in rho-expansion order #", n];
    ShiftList = {};
    expExp = Expand[AlphaHigh[[2 + n]]];
    If[Head[expExp] === Plus,
        AlphaHighList = List @@ expExp;
        ,
        If[Head[expExp] === Plus || Head[expExp] === Integer || 
            Head[expExp] === Power || Head[expExp] === Times,
            AlphaHighList = {expExp};
            ,
            Print["Unclassified Head: ", Head[expExp]];
            Print["With expression: ", expExp];
            Abort[];
            ];
        ];
    (*Print["AlphaHighList:",AlphaHighList];*)
    Print["Length of shifted operator list: ", Length[AlphaHighList]];
    
    For[i = 1, i <= Length[AlphaHighList], i++,
      Switch[i,
        IntegerPart[Length[AlphaHighList]/4], 
        Print["1/4 operators obtained! with #", i];,
        IntegerPart[Length[AlphaHighList]/2], 
        Print["1/2 operators obtained!! with #", i];,
        IntegerPart[Length[AlphaHighList]*3/4], 
        Print["3/4 operators obtained!!! with #", i];
      ];
     DenCheck = Not[FreeQ[Denominator[AlphaHighList[[i]]] // Simplify, _x]];
     Upower = 0;
     While[DenCheck, Upower = Upower + 1;
      DenCheck = 
       Not[
        FreeQ[
         Denominator[(AlphaHighList[[i]]*U^(Upower)) // Simplify // 
           Together], _x]];
      ];
     Ufactor = U^Upower;
     Shifts = {Uinv[
        Upower], (AlphaHighList[[i]]*Ufactor // Simplify) /. {x[d_] :>
           OP[d]}};
     (* Print["U power:", Upower];
     Print["U factor:", Ufactor];
     Print["Shifts:", Shifts];*)
     AppendTo[ShiftList, Shifts];
     ];
    AppendTo[ShiftOps, ShiftList];
    ];
   Return[ShiftOps];
   ];


ApplyShiftOPNPL[Rtmp_, ShiftList_, dlist_, order_, SmallInvariant_, mtsMax_ : 10, dlistNum_: {}] := 
 Module[{Int, n, i, shift, UinvSft, OPSft, R, MBSft, MBSftList, 
   mtsDim, PowerShift}, MBSftList = {};
  For[n = 1, n <= order, n++, Int = {};
   Print["-----------------------------"];
   Print["Apply shift operators at asy exp higher order: ", n];
   (*Print["Shifts: ", ShiftList[[n]]];*)
   For[i = 1, i <= Length[ShiftList[[n]]], i++,
    shift = ShiftList[[n, i]];
    UinvSft = 
     shift[[1]]*R[dlist, ep] /. {Uinv[x_] R[d__, ep_] :> R[d, ep - x]};
    (*Print["Uinv shift:",UinvSft];*)
    
    If[ dlistNum ==={},
      OPSft = (shift[[2]]*UinvSft // Expand) //. {OP[d_]^n_*R[a__] :> 
          ShiftOP[R[a], d, n, dlist], OP[d_]*R[a__] :> ShiftOP[R[a], d, dlist]};
      ,
      OPSft = (shift[[2]]*UinvSft // Expand) //. {OP[d_]^n_*R[a__] :> 
          ShiftOP[R[a], d, n, dlistNum], OP[d_]*R[a__] :> ShiftOP[R[a], d, dlistNum]};
    ];
    (*Print["OPs shift:",OPSft];*)
    MBSft = OPSft /. {R :> Rtmp};
    (*Print["MB Integral shifted:",
    MBSft];*)
    PowerShift = 10;
    (*Check m^2 dimensionality*)
    If[MBSft =!= 0,
     mtsDim = 
        Union[Cases[
                MBSft*SmallInvariant^PowerShift /. {Gamma[a__] :> 1} 
                /. Table[Variables[dlist[[i]]][[1]] -> 0, {i, 1, Length[dlist]}] 
                /. {ep -> 0} //Expand // PowerExpand, 
                Power[SmallInvariant, _], Infinity] 
                /. {Power[SmallInvariant, x_] :> x} ] - PowerShift;
     (*Print["m^2 diemsion: ",mtsDim[[1]]];*)
     
     If[mtsDim === {},
      mtsDim={0};
      ];

     If[Length[mtsDim] === 1,
        If[mtsDim[[1]] > mtsMax,
            MBSft = 0;
            (*Print["MB Integral # ",i," of ",Length[ShiftList[[n]]],
            " with higher m^2 order ",mtsDim[[1]],", set to 0: ",
            MBSft];*)
            ];
        ,
        Print["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"];
        Print["!!! Warning: more than 1 mass dimension in 1 region occurs !!!"];
        Print["!!! Switch on verbose = True for more outputs !!!"];
        Print["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"];
        If[verbose === True,
            Print["m^2 Dim = ", mtsDim];
            ];
        If[Min[mtsDim] > mtsMax,
            MBSft = 0;
            If[verbose === True,
                Print["Min m^2 dimension is already too high. Discard !!!"];
                ];
            ,
            If[verbose === True,
                Print["Min m^2 dimension is needed. Keep everything !!!"];
                ];
            ];
        ];
     ];
    AppendTo[Int, PowerExpand[MBSft /. {D -> 4 - 2 ep}]];
    ];
   AppendTo[MBSftList, Int];
   ];
  Return[MBSftList];
  ]

AutoHighAsyRegionsNPL[AlphaHighRegion_, TempIntList_, HardIntList_, 
  VarList_, dlist_, order_, SmallInvariant_, mtsMax_, dlistNum_: {}] := 
 Module[{r, Hard, rAsyInit, ShiftOPAsy, ShiftedMB, HighAsyList},
  HighAsyList = {};
  Hard = Length[HardIntList];
  rAsyInit = Length[HardIntList] + 1;
  For[r = rAsyInit, r <= Length[AlphaHighRegion], r++,
  Print["==========================="];
   Print["Asy Region #", r];
   ShiftOPAsy = 
    ShiftOperatorsNPL[AlphaHighRegion[[r]], VarList, order];
   If[Hard === 1,
    ShiftedMB = 
      ApplyShiftOPNPL[TempIntList[[r - 1]], ShiftOPAsy, dlist, order, SmallInvariant,
       mtsMax, dlistNum];
    ,
    If[Hard === 0,
      ShiftedMB = 
        ApplyShiftOPNPL[TempIntList[[r]], ShiftOPAsy, dlist, order, SmallInvariant,
         mtsMax, dlistNum];
      ,
      (*else*)
      Return["Error: More than 1 Hard region !!!"];
      ];
    ];
   AppendTo[HighAsyList, PowerExpand[ShiftedMB]];
   ];
  Return[HighAsyList];
  ]

  
verbose = False;


