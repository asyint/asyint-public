(*---------------------------------------
 |                                      |
 |    MBfunc.m of AsyInt v.1.0          |
 |                                      |
 |    by Hantian Zhang, July 15, 2024   |
 |                                      | 
 ----------------------------------------*)

SimplifyUF[UFpoly_, KinList_ : {SS, TT, UU}, EuclInvSum_ : 0] := 
 Module[{CoeffList, Coeff, i, common, Fsim, UFpolySim},
  Coeff = (Expand@Coefficient[UFpoly[[2]], #] & /@ 
      KinList) /. {0 -> {}};
  CoeffList = {};
  For[i = 1, i <= Length[Coeff], i++,
   If[Head[Coeff[[i]]] === Plus,
     AppendTo[CoeffList, List @@ Coeff[[i]]];
     ,
     AppendTo[CoeffList, {Coeff[[i]]}];
     ];
   ];
  common = 
   Plus @@ 
     Intersection[## & @@ CoeffList]*((Plus @@ KinList) - 
      EuclInvSum);
  Fsim = Collect[UFpoly[[2]] - common, KinList, FullSimplify];
  UFpolySim = {UFpoly[[1]] // FullSimplify, Fsim, UFpoly[[3]]} // 
    Simplify;
  Return[UFpolySim];
  ]
  
AlphaRep[U_, F_, l_, a__, x__, d_] :=
 Module[{GammaDen, alphaNum, i}, alphaNum = 1;
  GammaDen = 1;
  For[i = 1, i <= Length[a],
   i++, {alphaNum = alphaNum*x[[i]]^(a[[i]] - 1),
     GammaDen = GammaDen*Gamma[a[[i]]]};
     ];
  Return[ alphaNum/GammaDen*U^(-d/2)*Exp[-F/U] ];
  ]

RescaleRegions[regions_] := Module[{i, RescaleRegion},
  RescaleRegion = {};
  For[i = 1, i <= Length[regions], i++,
   If[Union[Cases[regions[[i]], -1]] != {},
     AppendTo[RescaleRegion, regions[[i]] + 1],
     If[Union[Cases[regions[[i]], -1/2]] != {},
       AppendTo[RescaleRegion, regions[[i]] + 1/2],
       AppendTo[RescaleRegion, regions[[i]]];
       ];
     ];
   ];
  Return[RescaleRegion];
  ]


ScaleRules[regions_, x_, mt2_, rhos_] := Module[{i, j, rules},
  rules = {};
  For[i = 1, i <= Length[regions], i++,
   AppendTo[rules, {mt2 -> rhos*mt2}];
   For[j = 1, j <= Length[regions[[i]]], j++,
    If[regions[[i, j]] =!= 0,
      AppendTo[rules[[i]], 
        x[[j]] -> (rhos)^(regions[[i, j]])*x[[j]]];
      ];
    ];
   ];
  Return[rules];
  ]



GenerateInput[loops_, props_, kinematics_, SmallInvariant_, 
  ScalePara_, xlist_, EuclInvariants_ : {SS, TT, UU}, EuclInvSum_ : 0,
  UserDefineRelation_ : {}] := Module[
  {loopOrd, UFpoly, UFpolySim, Regions, ResRegions, Scalings},
  loopOrd = Length[loops];
  UFpoly = 
   Collect[UF[loops, props, kinematics], 
    Join[EuclInvariants, {SmallInvariant}], Simplify];
  If[UserDefineRelation === {},
   Print["Impose default kinematical relation: ", 
  Plus @@ EuclInvariants - EuclInvSum == 0];
   Print["If user-defined relation is preferred, please provide \
invariants and relations in the input."];
   UFpolySim = SimplifyUF[UFpoly, EuclInvariants, EuclInvSum];
   ,
   Print["Impose user-defined kinematical relation: ", 
    UserDefineRelation];
   UFpolySim = UFpoly /. UserDefineRelation // Simplify;
   ];
  Regions = 
   WilsonExpand[UFpolySim[[2]], UFpolySim[[1]], 
    xlist, {SmallInvariant -> x}, Delta -> True];
  ResRegions = Sort[RescaleRegions[Regions]];
  Print["Asymptotic expansion regions: ", ResRegions];
  Scalings = 
   ScaleRules[ResRegions, xlist, SmallInvariant, ScalePara];
  Return[{UFpolySim, ResRegions, Scalings}];
  ]
  

GenerateInputNum[loops_, props_, numidx_, kinematics_, 
  SmallInvariant_, ScalePara_, xlistNum_, 
  EuclInvariants_ : {SS, TT, UU}, EuclInvSum_ : 0, 
  UserDefineRelation_ : {}] := Module[
  {loopOrd, UFpoly, UFpolySim, Regions, ResRegions, Scalings, 
   UFpolyNumSim, UFpolyNum, xlist},
  loopOrd = Length[loops];
  UFpolyNum = 
   Collect[UF[loops, props, kinematics], 
    Join[EuclInvariants, {SmallInvariant}], Simplify];
  xlist = 
   DeleteCases[
    xlistNum /. 
     Table[xlistNum[[numidx[[n]]]] -> 0, {n, 1, Length[numidx]}], 0];
  UFpoly = 
   UFpolyNum /. 
    Table[xlistNum[[numidx[[n]]]] -> 0, {n, 1, Length[numidx]}];
  If[UserDefineRelation === {},
   Print["Impose default kinematical relation: ", 
    Plus @@ EuclInvariants - EuclInvSum == 0];
   Print[
    "If user-defined relation is preferred, please provide invariants \
and relations in the input."];
   UFpolyNumSim = SimplifyUF[UFpolyNum, EuclInvariants, EuclInvSum];
   UFpolySim = SimplifyUF[UFpoly, EuclInvariants, EuclInvSum];
   ,
   Print["Impose user-defined kinematical relation: ", 
    UserDefineRelation];
   UFpolyNumSim = 
    Collect[UFpolyNum /. UserDefineRelation, 
     Join[EuclInvariants, {SmallInvariant}], Simplify];
   UFpolySim = 
    Collect[UFpoly /. UserDefineRelation, 
     Join[EuclInvariants, {SmallInvariant}], Simplify];
   ];
  Regions = 
   WilsonExpand[UFpolySim[[2]], UFpolySim[[1]], 
    xlist, {SmallInvariant -> x}, Delta -> True];
  ResRegions = Sort[RescaleRegions[Regions]];
  Print["Asymptotic expansion regions: ", ResRegions];
  Scalings = 
   ScaleRules[ResRegions, xlist, SmallInvariant, ScalePara];
  Return[{UFpolySim, ResRegions, Scalings, UFpolyNumSim}];
  ]  

  
AlphaRepForTempInt[UF__, Scaling__, dlist_, xlist_, rhos_,
  order_ : 6] := Module[
  {U, F, Uexp, Fexp, Ulist, Flist, i, AlphaScaleRegions, UOddPow, 
   FOddPow, rho, loop},
  If[InverseScaling === True,
    shift = 10;
    ,
    shift = 0;
    ];
  loop = UF[[3]];
  AlphaScaleRegions = {};
  For[i = 1, i <= Length[Scaling], i++,
   Print["--------------------------"];
   Print["Region #", i];
   Uexp = 
    Series[
      UF[[1]] /. Scaling[[i]] /. rhos -> rho^2 // PowerExpand, {rho, 
       0, order}] // Normal;
   Fexp = 
    Series[
      UF[[2]] /. Scaling[[i]] /. rhos -> rho^2 // PowerExpand, {rho, 
       0, order}] // Normal;
   Ulist = DeleteCases[CoefficientList[Uexp * rho^shift, rho], 0];
   Flist = DeleteCases[CoefficientList[Fexp * rho^shift, rho], 0];
   U = Ulist[[1]] // Simplify;
   F = Collect[Flist[[1]], {SS, TT, UU, mts}, Simplify];
   Print["Leading U: ", U];
   Print["Leading F: ", F];
   Print["alpha rep: ", 
    AlphaRep[U, F, loop, dlist + 1, xlist, 4 - 2 ep]];
   AppendTo[AlphaScaleRegions, 
    AlphaRep[U, F, loop, dlist + 1, xlist, 4 - 2 ep] // Simplify];
   ];
  Return[AlphaScaleRegions//PowerExpand];
  ]


(* MB integration routines *)

integrateTypeA[exp_, var_] := 
 exp /. Exp[a_] :> 
        Exp[Collect[a, var, Simplify]] /.
      {var^a_*
          Exp[b_ - var*A_] :> Exp[b]*Gamma[1 + a] A^(-1 - a) /. 
        a_^b_ :> Simplify[a]^b, 
       var^a_*Exp[-var*A_] :> Gamma[1 + a] A^(-1 - a) /. 
        a_^b_ :> Simplify[a]^b,
       var^a_*Exp[var*A_] :> Gamma[1 + a] (-A)^(-1 - a) /. 
        a_^b_ :> Simplify[a]^b} //. {(a_*b_)^c_ :> 
       a^c*b^c} //. {(a_*b_)^c_ :> a^c*b^c, (1/a_)^b_ :> a^(-b) /. 
      Exp[a_] :> Exp[Factor[a]],
     var^a_*Exp[b_ - var^c_*A_] :> 
       Exp[b]*Gamma[(1 + a)/c] A^((-1 - a)/c)/c /. 
      a_^b_ :> Simplify[a]^b, 
     var^a_*Exp[-var*A_] :> Gamma[1 + a] A^(-1 - a) /. 
      a_^b_ :> Simplify[a]^b,
     var^a_*Exp[var^c_*A_] :> Gamma[(1 + a)/c] (-A)^((-1 - a)/c)/c /. 
      a_^b_ :> Simplify[a]^b} //. {(a_*b_)^c_ :> 
     a^c*b^c} //. {(a_*b_)^c_ :> a^c*b^c, (1/a_)^b_ :> a^(-b) /. 
    Exp[a_] :> Exp[Factor[a]]
   }

integrateTypeB[exp_, var_] :=
 exp /. a_^b_ :> Collect[a, var, Simplify]^b /.
  {
   var^a_*(var + A_)^b_ :>
    A^(1 + a + b) Gamma[1 + a] Gamma[-1 - a - b]/Gamma[-b],
   var^a_*(A_ + var*B_)^b_ :>
    A^(1 + a + b) B^(-1 - a) Gamma[1 + a] Gamma[-1 - a - b]/
      Gamma[-b],
   var^a_*(var + 1)^b_ :> Gamma[1 + a] Gamma[-1 - a - b]/Gamma[-b]
   }

rescale[exp_, var1_, var2_] :=
 var2*exp /. var1 -> var1*var2 /.
    a_^b_ :> Simplify[a]^b //. {(a_*b_)^c_ :> a^c*b^c} /.
  Exp[a_] :> Exp[Factor[a]]

MBsplit[exp_, t1_, t2_, var_] :=
 exp /. (t1 + t2)^c_ :>
     1/Gamma[-c] Gamma[-var] Gamma[-c + var] t1^
       var t2^(c - var) //. {
    (a_*b_)^c_ :> a^c*b^c,
    (1/a_)^b_ :> a^(-b)
    } /. (x_^a_)^b_ :> x^(a*b)

IntegrateTrivial[integrand_, xlist_, dlist_] :=
 Module[{invx, nontrivial, trivial, i, tmp},
  invx = xlist^(-dlist);
  nontrivial =
   Union[
    Cases[(integrand // Numerator)*
      Product[invx[[i]], {i, 1, Length[xlist]}], _x, Infinity]];
  trivial = Complement[xlist, nontrivial];
  tmp = integrand;
  For[i = 1, i <= Length[trivial], i++,
   tmp = integrateTypeA[tmp, trivial[[i]]];
   ];
  Return[tmp]
  ]

IntTypeA[integrand_, var_, VarResList_ : {}] := Module[{i, tmp},
  tmp = integrand;
  For[i = 1, i <= Length[VarResList], i++,
   tmp = rescale[tmp, VarResList[[i]], var];
   ];
  Return[integrateTypeA[tmp, var]];
  ]

IntTypeB[integrand_, var_, VarResList_ : {}] := Module[{i, tmp},
  tmp = integrand;
  For[i = 1, i <= Length[VarResList], i++,
   tmp = rescale[tmp, VarResList[[i]], var];
   ];
  Return[integrateTypeB[tmp, var]];
  ]

SortAdd = {a_ (b_ + c_) :> a*b + a*c};
CombineAdd = {a_*b_ + a_*c_ :> a(b+c), a_ + a_*b_ :> a*(1+b)};
SortPower = {(a_*b_)^c_ :> a^c*b^c, (a_^b_)^c_ :> a^(b*c)};
