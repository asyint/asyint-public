(*---------------------------------------
 |                                      |
 |   UtilityFunc.m of AsyInt v.1.0      |
 |                                      |
 |   by Hantian Zhang, July 15, 2024    |
 |                                      | 
 ----------------------------------------*)


GammaSimplify = {Gamma[n_Integer + a_*Z_] /; 
     n >= 1 :> (n - 1 + a*Z) Gamma[n - 1 + a*Z], 
   Gamma[n_Integer + Z_] /; n >= 1 :> (n - 1 + Z) Gamma[n - 1 + Z]};

PolyGammaSimplify = {
   PolyGamma[d_Integer, n_Integer + a_*Z_] /; n >= 1 :> 
    PolyGamma[d, n - 1 + a*Z] + (-1)^d*(d!)*(n - 1 + a*Z)^(-d - 1),
   PolyGamma[d_Integer, n_Integer + Z_] /; n >= 1 :> 
    PolyGamma[d, n - 1 + Z] + (-1)^d*(d!)*(n - 1 + Z)^(-d - 1)
   };
   
GammaSimplify2 = {Gamma[n_Integer + a_ Z_] /; 
     n >= 1 :> (n - 1 + a Z) Gamma[n - 1 + a Z], 
   Gamma[n_Integer + Z_] /; n >= 1 :> (n - 1 + Z) Gamma[n - 1 + Z],
   Gamma[n_Integer + a_ Z_] /; n < 0 :> Gamma[n + 1 + a Z]/(n + a*Z), 
   Gamma[n_Integer + Z_] /; n < 0 :> Gamma[n + 1 + Z]/(n + Z)};
   
PolyGammaSimplify2 = {PolyGamma[d_Integer, n_Integer + a_ Z_] /; 
    n >= 1 :> 
   PolyGamma[d, n - 1 + a Z] + (-1)^d d! (n - 1 + a Z)^(-d - 1), 
  PolyGamma[d_Integer, n_Integer + Z_] /; n >= 1 :> 
   PolyGamma[d, n - 1 + Z] + (-1)^d d! (n - 1 + Z)^(-d - 1),
  PolyGamma[d_Integer, n_Integer + a_ Z_] /; n < 0 :> 
   PolyGamma[d, n + 1 + a Z] - (-1)^d d! (n + a Z)^(-d - 1), 
  PolyGamma[d_Integer, n_Integer + Z_] /; n < 0 :> 
   PolyGamma[d, n + 1 + Z] - (-1)^d d! (n + Z)^(-d - 1)}

Csc2Gamma = {Csc[\[Pi] Z_] :> Gamma[-Z]*Gamma[1 + Z]/(-Pi), 
  Csc[\[Pi] (Z1_ + Z2_)] :> 
   Gamma[-(Z1 + Z2)]*Gamma[1 + (Z1 + Z2)]/(-Pi)}

Cot2Gamma = {Cot[
    Pi*z_] :> (Gamma[
      1 + z] Gamma[-z]/(Gamma[3/2 + z] Gamma[-1/2 - z])), 
  Cot[Pi*(Z1_ + Z2_)] :> (Gamma[
      1 + (Z1 + Z2)] Gamma[-(Z1 + Z2)]/(Gamma[
         3/2 + (Z1 + Z2)] Gamma[-1/2 - (Z1 + Z2)]))}   

HarmoNum2Gamma = {HarmonicNumber[a_] :> 
    EulerGamma + PolyGamma[0, a + 1]};       
         


RecursiveGammaSimplify[exp_] := 
 Module[{flag, exp2, func1, func2, count},
  flag = True;
  count = 0;
  exp2 = exp;
  While[flag,
   exp2 = 
    Collect[exp2 /. GammaSimplify2 /. 
      PolyGammaSimplify2, {_PolyGamma, _Gamma}, Together];
   func1 = 
    Join[Cases[exp2, _PolyGamma, Infinity], 
      Cases[exp2, _Gamma, Infinity]] // Union;
   func2 = func1 /. GammaSimplify2 /. PolyGammaSimplify2;
   If[func1 === func2,
    flag = False;
    ];
   count = count + 1;
   ];
  Return[exp2];
  ]

GammaSimplifyList[exp_] := 
 Module[{newlist, blocksize, MaxLength, partition, i, j, k, sublist, 
   resTab, res},
  blocksize = 100;
  If[Head[Expand[exp]] === Plus,
   newlist = List @@ Expand[exp];
   MaxLength = Length[newlist];
   partition = IntegerPart[MaxLength/blocksize] + 1;
   For[i = 1, i <= partition, i++,
    sublist[i] = 
     Sum[newlist[[j]], {j, 1 + blocksize*(i - 1), 
       Min[blocksize*i, MaxLength]}]
    ];
   Print["partition list created with total #", partition];
   resTab = 
    ParallelTable[
     RecursiveGammaSimplify[sublist[k]], {k, 1, partition}];
   Print["partition list simplification done !!!"];
   res = Collect[
     Sum[resTab[[k]], {k, 1, partition}], {_PolyGamma, _Gamma}, 
     Together];
   Return[res];
   ,
   If [ NoTrivialGammaSimplify = True,
      res = exp;
      ,
      res = exp //. GammaSimplify2 //. PolyGammaSimplify2 // Factor;
      ];
   Return[res];
   ];
  ]


sort1DMB[ex_, Z1_] := Module[{ex1, l1, l2},
  If[Head[Expand[ex]] === Plus,
    ex1 = List @@ Expand[ex];
    l1 = {};
    l2 = {};
    Do[{If[FreeQ[ex1[[i]], Z1],
        AppendTo[l1, ex1[[i]]],
        AppendTo[l2, ex1[[i]]]
        ];}, {i, 1, Length[ex1]}];
    Return[{Plus @@ l1, Plus @@ l2}]
    ,
    Return[{ex}];
    ];
  ]

sort2DMB[ex_, Z1_, Z2_] := Module[{ex1, l1, l2, l3, l4},
  If[Head[Expand[ex]] === Plus,
    ex1 = List @@ Expand[ex];
    l1 = {};
    l2 = {};
    l3 = {};
    l4 = {};
    Do[{If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2],
        AppendTo[l1, ex1[[i]]],
        If[
         FreeQ[ex1[[i]], Z2],
         AppendTo[l2, ex1[[i]]],
         If[
          FreeQ[ex1[[i]], Z1],
          AppendTo[l3, ex1[[i]]],
          AppendTo[l4, ex1[[i]]]
          ]
         ]
        
        ];}, {i, 1, Length[ex1]}];
    Return[{Plus @@ l1, Plus @@ l2, Plus @@ l3, Plus @@ l4}]
    ,
    Return[{ex}];
    ];
  ]
  
sort3DMB[ex_, Z1_, Z2_, Z3_] := 
 Module[{ex1, l1, l2, l3, l4, l5, l6, l7, l8},
  If[Head[Expand[ex]] === Plus,
    ex1 = List @@ Expand[ex];
    l1 = {};
    l2 = {};
    l3 = {};
    l4 = {};
    l5 = {};
    l6 = {};
    l7 = {};
    l8 = {};
    Do[{If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2] && 
         FreeQ[ex1[[i]], Z3],
        AppendTo[l1, ex1[[i]]],
        If[
         FreeQ[ex1[[i]], Z2] && FreeQ[ex1[[i]], Z3],
         AppendTo[l2, ex1[[i]]],
         If[
          FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z3],
          AppendTo[l3, ex1[[i]]],
          If[
           FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2],
           AppendTo[l4, ex1[[i]]],
           If[
            FreeQ[ex1[[i]], Z3],
            AppendTo[l5, ex1[[i]]],
            If[
             FreeQ[ex1[[i]], Z2],
             AppendTo[l6, ex1[[i]]],
             If[
              FreeQ[ex1[[i]], Z1],
              AppendTo[l7, ex1[[i]]],
              AppendTo[l8, ex1[[i]]]
              ]
             ]
            ]
           ]
          ]
         ]
        ];}, {i, 1, Length[ex1]}];
    Return[{Plus @@ l1, Plus @@ l2, Plus @@ l3, Plus @@ l4, Plus @@ l5, 
      Plus @@ l6, Plus @@ l7, Plus @@ l8}]
    ,
    Return[{ex}];
    ];
  ]

sort4DMB[ex_, Z1_, Z2_, Z3_, Z4_] := Module[
  {ex1, l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, 
   l15, l16},
  If[Head[Expand[ex]] === Plus,
    ex1 = List @@ Expand[ex];
    l1 = {};
    l2 = {};
    l3 = {};
    l4 = {};
    l5 = {};
    l6 = {};
    l7 = {};
    l8 = {};
    l9 = {};
    l10 = {};
    l11 = {};
    l12 = {};
    l13 = {};
    l14 = {};
    l15 = {};
    l16 = {};
    Do[{
      If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2] && 
         FreeQ[ex1[[i]], Z3] && FreeQ[ex1[[i]], Z4],
        AppendTo[l1, ex1[[i]]],
        If[FreeQ[ex1[[i]], Z2] && FreeQ[ex1[[i]], Z3] && 
           FreeQ[ex1[[i]], Z4],
          AppendTo[l2, ex1[[i]]],
          If[
            FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z3] && 
             FreeQ[ex1[[i]], Z4],
            AppendTo[l3, ex1[[i]]],
            
            If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2] && 
               FreeQ[ex1[[i]], Z4],
              AppendTo[l4, ex1[[i]]],
              
              If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2] && 
                 FreeQ[ex1[[i]], Z3],
                AppendTo[l5, ex1[[i]]],
                If[FreeQ[ex1[[i]], Z3] && FreeQ[ex1[[i]], Z4],
                  AppendTo[l6, ex1[[i]]],
                  If[FreeQ[ex1[[i]], Z2] && FreeQ[ex1[[i]], Z4],
                    AppendTo[l7, ex1[[i]]],
                    If[FreeQ[ex1[[i]], Z2] && FreeQ[ex1[[i]], Z3],
                      AppendTo[l8, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z4],
                      AppendTo[l9, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z3],
                      AppendTo[l10, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z1] && FreeQ[ex1[[i]], Z2],
                      AppendTo[l11, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z4],
                      AppendTo[l12, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z3],
                      AppendTo[l13, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z2],
                      AppendTo[l14, ex1[[i]]],
                      If[FreeQ[ex1[[i]], Z1],
                      AppendTo[l15, ex1[[i]]],
                      AppendTo[l16, ex1[[i]]];
                      ];
                      ];
                      ];
                      ];
                      ];
                      ];
                      ];
                      ];
                    ];
                  ];
                ];
              ];
            ];
          ];
        ];
      }, {i, 1, Length[ex1]}];
    Return[{Plus @@ l1, Plus @@ l2, Plus @@ l3, Plus @@ l4, Plus @@ l5, 
      Plus @@ l6, Plus @@ l7, Plus @@ l8, Plus @@ l9, Plus @@ l10, 
      Plus @@ l11, Plus @@ l12, Plus @@ l13, Plus @@ l14, Plus @@ l15, 
      Plus @@ l16}];
    ,
    Return[{ex}];
    ];
  ]  


SortPatternList[exp_, patternlist_] := Module[{output},
  Switch[Length[patternlist],
   1, output = 
    DeleteCases[sort1DMB[exp, Sequence @@ patternlist], 0],
   2, output = 
    DeleteCases[sort2DMB[exp, Sequence @@ patternlist], 0],
   3, output = 
    DeleteCases[sort3DMB[exp, Sequence @@ patternlist], 0],
   4, output = DeleteCases[sort4DMB[exp, Sequence @@ patternlist], 0]
   ];
  If[Length[patternlist] > 4,
   Print[
    "Note: more than 4 pattern sorting function not implemented, use first \
4 patterns"];
   output = 
    DeleteCases[sort4DMB[exp, Sequence @@ patternlist[[1 ;; 4]]], 0];
   ];
  Return[output];
  ]


AbsorbZtoGamma[exp_, Z2_, ZnotTouch_] := 
 exp /. {Z2^4 Gamma[Z2] -> 
           Z2^2 Gamma[2 + Z2] - Z2^3 Gamma[Z2]} /. {Z2^3 Gamma[Z2] -> 
          Z2*Gamma[2 + Z2] - Z2^2 Gamma[Z2]} /. {Z2^2 Gamma[Z2] -> 
         Gamma[2 + Z2] - Z2 Gamma[Z2]} /. {Z2 Gamma[Z2] -> 
        Gamma[1 + Z2]} /. {1/(1 + ZnotTouch + Z2) -> 
       Gamma[1 + ZnotTouch + Z2]/
        Gamma[2 + ZnotTouch + Z2]} /. {Z2^2 Gamma[2 + Z2] -> 
      Z2*Gamma[3 + Z2] - 2 Z2*Gamma[2 + Z2]} /. {Z2*Gamma[2 + Z2] -> 
     Gamma[3 + Z2] - 2*Gamma[2 + Z2]} /. {Z2*Gamma[3 + Z2] -> 
    Gamma[4 + Z2] - 3*Gamma[3 + Z2]} 
 

KinToX[ex_, TT_, SS_, x_, Z_] := ex //. {
    SS^(Z)*TT^(-Z + c_) :> (TT/SS)^(-Z)*TT^c, 
    SS^(Z + c_)*TT^(-Z) :> (TT/SS)^(-Z)*SS^c,
    SS^(-Z)*TT^(Z + c_) :> (TT/SS)^(Z)*TT^c, 
    SS^(-Z + c_)*TT^(Z) :> (TT/SS)^(Z)*SS^c,
    SS^(Z + b_)*TT^(-Z + c_) :> (TT/SS)^(-Z)*TT^c*SS^b,
    SS^(-Z + b_)*TT^(Z + c_) :> (TT/SS)^(Z)*TT^c*SS^b,
    SS^(Z)*TT^(-Z) :> (TT/SS)^(-Z),
    SS^(-Z)*TT^(Z) :> (TT/SS)^(Z)
    } //. {TT/SS -> x}
    
VarSub[exp_] := exp /. {TT -> SS*x} /. {UU -> SS*y} /. {UU -> TT/w}

InvVarSub[exp_] := exp /. {w -> TT/UU, x -> TT/SS, y -> UU/SS}

DuplicateForm = {Gamma[1/2 + Z1_] :> 
   2^(1 - 2 Z1) Sqrt[Pi] Gamma[2 Z1]/Gamma[Z1], 
  Gamma[-1/2 - Z1_] :> 
   2^(3 + 2 Z1) Sqrt[Pi] Gamma[-2 Z1 - 2]/Gamma[-Z1 - 1]};

NumLimit = {SS -> 1, TT -> 1/3, UU -> 1/2, mts -> 1/10};




crep1 = {
H[{3, 0}, 1] -> \[Pi]/(3 Sqrt[3]), 
 H[0, {3, 1}, 1] -> 
  1/36 (2 \[Pi]^2 - Sqrt[3] \[Pi] Log[3] - 
     6 I Sqrt[
      3] (PolyLog[2, 1/6 (3 - I Sqrt[3])] - 
        PolyLog[2, 1/6 (3 + I Sqrt[3])]))
        };

 crep2 = {
  H[{3, 0}, 1] -> Pi/(3 Sqrt[3]), 
  H[0, {3, 1}, 1] ->  1/18 (Pi^2 - PolyGamma[1, 1/3] + PolyGamma[1, 2/3]),
  H[{3, 1}, 1] -> -(\[Pi]/(6 Sqrt[3])) + Log[3]/2,
  H[-3, 1] -> 2 Log[2] - Log[3]
   } /. {PolyGamma[1, 2/3] -> (4 \[Pi]^2)/3 - PolyGamma[1, 1/3]}
   
        
polygam={PolyGamma[0, -7/2] -> 352/105 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -5/2] -> 46/15 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -3/2] -> 8/3 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -1/2] -> 2 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 1/2] -> -EulerGamma - 2*Log[2], 
 PolyGamma[0, 3/2] -> 2 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 5/2] -> 8/3 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 7/2] -> 46/15 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 9/2] -> 352/105 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 11/2] -> 1126/315 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 13/2] -> 13016/3465 - EulerGamma - 2*Log[2],
 PolyGamma[2, 1] -> -2*Zeta[3], PolyGamma[2, 2] -> 2*(1 - Zeta[3]), 
 PolyGamma[2, 3] -> 2*(9/8 - Zeta[3]), PolyGamma[2, 4] -> 
  2*(251/216 - Zeta[3]), PolyGamma[2, 5] -> 2*(2035/1728 - Zeta[3]), 
 PolyGamma[2, 6] -> 2*(256103/216000 - Zeta[3]), 
 PolyGamma[2, 7] -> 2*(28567/24000 - Zeta[3]), 
 PolyGamma[2, 8] -> 2*(9822481/8232000 - Zeta[3]), 
 PolyGamma[2, 9] -> 2*(78708473/65856000 - Zeta[3]), 
 PolyGamma[2, 10] -> 2*(19148110939/16003008000 - Zeta[3]), 
 PolyGamma[4, 1] -> -24*Zeta[5], PolyGamma[4, 2] -> 24*(1 - Zeta[5]), 
 PolyGamma[4, 3] -> 24*(33/32 - Zeta[5]), PolyGamma[4, 4] -> 
  24*(8051/7776 - Zeta[5]), PolyGamma[4, 5] -> 24*(257875/248832 - Zeta[5]), 
 PolyGamma[4, 6] -> 24*(806108207/777600000 - Zeta[5]), 
 PolyGamma[4, 7] -> 24*(268736069/259200000 - Zeta[5]), 
 PolyGamma[4, 8] -> 24*(4516906311683/4356374400000 - Zeta[5]), 
 PolyGamma[4, 9] -> 24*(144545256245731/139403980800000 - Zeta[5]), 
 PolyGamma[4, 10] -> 24*(105375212839937899/101625502003200000 - Zeta[5]), 
 PolyGamma[6, 1] -> -720*Zeta[7], PolyGamma[6, 2] -> 720*(1 - Zeta[7]), 
 PolyGamma[6, 3] -> 720*(129/128 - Zeta[7]), 
 PolyGamma[6, 4] -> 720*(282251/279936 - Zeta[7]), 
 PolyGamma[6, 5] -> 720*(36130315/35831808 - Zeta[7]), 
 PolyGamma[6, 6] -> 720*(2822716691183/2799360000000 - Zeta[7]), 
 PolyGamma[6, 7] -> 720*(940908897061/933120000000 - Zeta[7]), 
 PolyGamma[6, 8] -> 720*(774879868932307123/768464444160000000 - Zeta[7]), 
 PolyGamma[6, 9] -> 720*(99184670126682733619/98363448852480000000 - 
    Zeta[7]), PolyGamma[6, 10] -> 
  720*(650750755630450535274259/645362587921121280000000 - Zeta[7]), 
 PolyGamma[8, 1] -> -40320*Zeta[9], PolyGamma[8, 2] -> 40320*(1 - Zeta[9]), 
 PolyGamma[8, 3] -> 40320*(513/512 - Zeta[9]), 
 PolyGamma[8, 4] -> 40320*(10097891/10077696 - Zeta[9]), 
 PolyGamma[8, 5] -> 40320*(5170139875/5159780352 - Zeta[9]), 
 PolyGamma[8, 6] -> 40320*(10097934603139727/10077696000000000 - Zeta[9]), 
 PolyGamma[8, 7] -> 40320*(373997614931101/373248000000000 - Zeta[9]), 
 PolyGamma[8, 8] -> 40320*(15092153145114981831307/15061903105536000000000 - 
    Zeta[9]), PolyGamma[8, 9] -> 
  40320*(7727182467755471289426059/7711694390034432000000000 - Zeta[9]), 
 PolyGamma[8, 10] -> 40320*(4106541588424891370931874221019/
     4098310578334288576512000000000 - Zeta[9]), 
 PolyGamma[10, 1] -> -3628800*Zeta[11], PolyGamma[10, 2] -> 
  3628800*(1 - Zeta[11]), PolyGamma[10, 3] -> 3628800*(2049/2048 - Zeta[11]), 
 PolyGamma[10, 4] -> 3628800*(362976251/362797056 - Zeta[11]), 
 PolyGamma[10, 5] -> 3628800*(743375539195/743008370688 - Zeta[11]), 
 PolyGamma[10, 6] -> 3628800*(36297634492764230063/36279705600000000000 - 
    Zeta[11]), PolyGamma[10, 7] -> 
  3628800*(12099211530921410021/12093235200000000000 - Zeta[11]), 
 PolyGamma[10, 8] -> 3628800*(23924094541398110665791491603/
     23912277370348953600000000000 - Zeta[11]), 
 PolyGamma[10, 9] -> 3628800*(48996545626484461837262019724819/
     48972344054474656972800000000000 - Zeta[11]), 
 PolyGamma[10, 10] -> 3628800*(26038773205113879830003552224577534179/
     26025911496654066176281804800000000000 - Zeta[11])};
