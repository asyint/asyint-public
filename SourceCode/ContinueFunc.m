(*-----------------------------------------
 |                                        |
 |   ContinueFunc.m of AsyInt v.1.0.1     |
 |                                        |
 |   by Hantian Zhang, Oct. 08, 2024      |
 |                                        | 
 ------------------------------------------*)


Zrule = {Z1 -> -1/7, Z2 -> -1/11, Z3 -> -1/17, Z4 -> -1/19, Z5 -> -1/23, Z6 -> -1/29};

AddZrules[rules_, Zrule_] := Module[{NewRule}, NewRule = rules;
   For[i = 1, i <= Length[Zrule], i++, 
    AppendTo[NewRule[[2]], Zrule[[i]]]];
   NewRule];



MBintContExp::MBext0err = "Exactly 0 appears.";

ResolveMBexact0 = Fasle;

MBintContExp[MBint_, RuleRx_, delta_, ep_, Normalisation_, epOrder_] :=
   Module[{cont, exp, MBtmp, expsum, ExpList, i, j, k}, 
   ExpList = {};
   MBtmp = MBint;
   If[verbose === True,
      Print["!!! Rules:", RuleRx];
   ];
   For[i = 1, i <= Length[delta], i++,
    (*Print["!!! Start continuation of delta:",delta[[i]]];*)
    
    cont = MBcontinue[MBtmp, delta[[i]] -> 0, RuleRx, Verbose -> False];
    exp = MBmerge[MBexpand[cont, 1, {delta[[i]], 0, 0}]];
    expsum = 0;
    For[j = 1, j <= Length[exp], j++, 
    expsum = expsum + exp[[j, 1]]];
    MBtmp = Collect[expsum, Join[{ep}, delta], Together];
    AppendTo[ExpList, MBtmp];];
   (*calculate ep expansion*)
   (*Print[
   "!!! Start ep continuation !!"];*)
   (*Print["!!! Rules:",
   RuleRx];*)
   cont = MBcontinue[MBtmp, ep -> 0, RuleRx, Verbose -> False];
   exp = MBexpand[cont, Normalisation, {ep, 0, epOrder}];
   If[ResolveMBexact0 === True,
     If[Not[FreeQ[exp, {}[[1]]]],
       Print["Warining: Exactly 0 appears in MBexpand! Try to locate and \
  delete it"];
       For[k = 1, k <= Length[exp], k++,
       If[Not[FreeQ[exp[[k]], {}[[1]]]],
          Message[MBintContExp::MBext0err];
          Print["Problematic term found: ", exp[[k]]];
          exp[[k]] = 0;
          ];
       ];
       (* Print["Issue resolved with output: ", MBmerge[exp]]; *)
       Print["====  Issue resolved.   ===="];
      ];
     ];
   exp = MBmerge[exp];
   expsum = 0;
   For[j = 1, j <= Length[exp], j++, expsum = expsum + exp[[j, 1]]];
   MBtmp = Collect[expsum, Join[{ep}, delta],Together];
   AppendTo[ExpList, MBtmp];
   Return[Last[ExpList]];
   ];
   
AutoMB[TempList_, dList_, Zrules_, Constrain_, ep_, Normalisation_, 
  epOrder_] := Module[
  {RuleTmp, NestRuleTmp, ResultTmp, i, sum, redo, count, 
   NewConstrains},
  sum = 0;
  Print["Total MB integral to be continued: ", Length[TempList]];
  For[i = 1, i <= Length[TempList], i++,
   Print["=========================="];
   Print["Start analytical continuation of MB integral of region # ", i];
   RuleTmp = 
    Check[MBoptimizedRules[TempList[[i]][dList, ep] /. Zrules, ep -> 0, 
     Constrain, Join[dList, {ep}]], err, MBrules::norules];
   If[RuleTmp =!= err,
      NestRuleTmp = AddZrules[RuleTmp, Zrules];
      ResultTmp = 
        Collect[Check[MBintContExp[TempList[[i]][dList, ep], NestRuleTmp, dList, ep, Normalisation, epOrder], err, MBresidues::contour], 
        Join[{ep}, dList], Together];
      ,
      Print[">>>>>  No MB rules found !!!  <<<<<<"];
   ];
   If[(ResultTmp === err) || (RuleTmp === err),
    redo = True;
    count = 1;
    While[redo,
     Print["@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"];
     Print[
      "!!!  Start to RE-DO with different contour constrains  !!!"];
     Print["RE-DO round #", count];
     If[count <= 8,
       NewConstrains = RandomConstrain[dList, Zrules];
       ,
       Print["RE-DO count > 8, remove all constrains on regulators"];
       NewConstrains = {};
     ];
     RuleTmp = 
      Check[MBoptimizedRules[TempList[[i]][dList, ep] /. Zrules, ep -> 0, 
       NewConstrains, Join[dList, {ep}]], err, MBrules::norules];
     If[RuleTmp =!= err,
        NestRuleTmp = AddZrules[RuleTmp, Zrules];
        ResultTmp = 
          Collect[Check[MBintContExp[TempList[[i]][dList, ep], NestRuleTmp, dList, ep,Normalisation, epOrder], err, MBresidues::contour], 
          Join[dList, {ep}], Together];
        ,
        Print[">>>>>  No MB rules found !!!  <<<<<<"];
     ];
     count = count + 1;
     If[
      (ResultTmp =!= err) && (RuleTmp =!= err),
      redo = False;
      ];
     If[count > 10,
      Print[">>>>>>>>>><<<<<<<<<<<<"];
      Print["RE-DO NOT successful !!!"];
      Print["This is a No MB rules problem, or a nearly Gamma[0] problem caused by the regulator choices."];
      Print["Try to use different contour choice, or use AsyInt default Zrule, or add more auxiliary MB integration variables (Z -> 1/(prime number)) to allow more constrains for finding the good analytic contunation ruels."];
      Abort[];
      ];
     ];
    ];
   (* 
   Print["Original MB exp: ", TempList[[i]][dList, ep]];
   Print["Continued MB exp: ", ResultTmp];
   *)
   sum = sum + ResultTmp;
   ];
  Return[sum];
  ]
  
AutoMBshift[TempList_, dList_, Zrules_, Constrain_, ep_, 
  Normalisation_, epOrder_, ShiftIdx_, ShiftNumber_] := Module[
  {RuleTmp, NestRuleTmp, ResultTmp, i, sum, ShiftMB, redo, count, 
   NewConstrains},
  sum = 0;
  Print["Total MB integral to be continued: ", Length[TempList]];
  For[i = 1, i <= Length[TempList], i++,
   Print["=========================="];
   Print["Start analytical continuation of MB integral of region # ", i];
   ShiftMB = 
    TempList[[i]][dList, ep] /. {ShiftIdx -> ShiftIdx + ShiftNumber};
   RuleTmp = 
    MBoptimizedRules[ShiftMB /. Zrules, ep -> 0, Constrain, 
     Join[dList, {ep}]];
   
   If[RuleTmp =!= err,
    NestRuleTmp = AddZrules[RuleTmp, Zrules];
    ResultTmp = 
     Collect[Check[
       MBintContExp[ShiftMB, NestRuleTmp, dList, ep, Normalisation, 
        epOrder], err, MBresidues::contour], Join[{ep}, dList], 
      Together];
    ,
    Print[">>>>>  No MB rules found !!!  <<<<<<"];
    ];
   If[(ResultTmp === err) || (RuleTmp === err),
    redo = True;
    count = 1;
    While[redo, 
     Print["@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"];
     Print[
      "!!!  Start to RE-DO with different contour constrains  !!!"];
     Print["RE-DO round #", count];
     If[count <= 8,
       NewConstrains = RandomConstrain[dList, Zrules];
       ,
       Print["RE-DO count > 8, remove all constrains on regulators"];
       NewConstrains = {};
     ];
     RuleTmp = 
      Check[MBoptimizedRules[ShiftMB /. Zrules, ep -> 0, 
        NewConstrains, Join[dList, {ep}]], err, MBrules::norules];
     If[RuleTmp =!= err,
      NestRuleTmp = AddZrules[RuleTmp, Zrules];
      ResultTmp = 
       Collect[Check[
         MBintContExp[ShiftMB, NestRuleTmp, dList, ep, Normalisation, 
          epOrder], err, MBresidues::contour], Join[dList, {ep}], 
        Together];
      ,
      Print[">>>>>  No MB rules found !!!  <<<<<<"];
      ];
     count = count + 1;
     If[(ResultTmp =!= err) && (RuleTmp =!= err),
      redo = False;
      ];
     If[count > 10,
      Print[">>>>>>>>>><<<<<<<<<<<<"];
      Print["RE-DO NOT successful !!!"];
      Print["This is a No MB rules problem, or a nearly Gamma[0] problem caused by the regulator choices."];
      Print["Try to use different contour choice, or use AsyInt default Zrule, or add more auxiliary MB integration variables (Z -> 1/(prime number)) to allow more constrains for finding the good analytic contunation ruels."];
      Abort[];
      ];
     ];
    ];
   If[verbose === True,
     Print["Original MB integral: ", TempList[[i]][dList, ep]];
     Print["Shifted MB integral: ", ShiftMB];
     Print["Continued MB integral: ", ResultTmp];
   ];
   
   sum = sum + ResultTmp;
   ];
  Return[sum];
  ]




AutoTestContourHighAsySplit[MBHighList_, dList_, Zrules_, Constrain_, 
  ep_, mtsOrder_] := 
 Module[{RuleTmp, NestRuleTmp, ResultTmp, i, j, k, sum, sumReg, 
   LocalList, Pass}, sum = 0;
  Pass = True;
  Print["Total MB integral to be continued: ", Length[MBHighList]];
  Print["=========================="];
  For[i = 1, i <= Length[MBHighList], i++,
   Print[""];
   Print["Find Contour for Analytical Continuation of MB integral of \
Region # ", i, ", at higher mts order: ", mtsOrder];
   For[j = 1, j <= Length[MBHighList[[i, mtsOrder]]], j++,
    (*Head is Not Times, split into nested lists*)
    
    If[Head[MBHighList[[i, mtsOrder, j]]] === Plus,
      LocalList = List @@ MBHighList[[i, mtsOrder, j]];
      (*Print["local list: ",LocalList];*)
      
      For[k = 1, k <= Length[LocalList], k++,
       RuleTmp = 
        MBoptimizedRules[LocalList[[k]] /. Zrules, ep -> 0, Constrain,
          Join[dList, {ep}]];
       Print["Contour of integral #",k," in Sub-Shifts # ", j, 
        " is :", RuleTmp];
       If[RuleTmp === {}, Pass = False;];
       ];,
      (*Head is Times, DO NOT split into nested lists*)
      
      If[Head[MBHighList[[i, mtsOrder, j]]] === Times,
        LocalList = MBHighList[[i, mtsOrder, j]];
        (*Print["local list: ",LocalList];*)
        
        RuleTmp = 
         MBoptimizedRules[LocalList /. Zrules, ep -> 0, Constrain, 
          Join[dList, {ep}]];
        Print["Contour of integral in Sub-Shifts # ", j, " is :", 
         RuleTmp];
        If[RuleTmp === {}, Pass = False;];
        ];
      ];
    ];
   ];
  Return[Pass];]

  
RandomConstrain[dlist_, Zrule_] := Module[{primes, constrains, redo, Zvar},
  primes = RandomPrime[{100, 1000}, Length[dlist] + 1];
  (*Print["random generated prime list: ",primes];*)
  
  If[Length[Union[primes]] < (Length[dlist] + 1),
   redo = True;
   While[redo,
    primes = RandomPrime[{100, 1000}, Length[dlist] + 1];
    (*Print["random generated prime list: ",primes];*)
    
    If[Length[Union[primes]] === (Length[dlist] + 1),
     redo = False;
     ];
    ];
   ];
  Zvar = Table[Zrule[[mm, 1]], {mm, 1, Length[Zrule]}];
  constrains = 
    Join[Table[{Abs[Re[dlist[[i]]]] < (2*primes[[i]] + 1)/primes[[i]], 
      Re[dlist[[i]]] != 0}, {i, 1, Length[dlist]}], 
    Flatten[
     Table[{Re[dlist[[i]]] != Zvar[[j]], 
       Re[dlist[[i]]] != -Zvar[[j]]}, {i, 1, Length[dlist]}, {j, 1, 
       Length[Zvar]}]], {Abs[Re[ep]] < (primes[[-1]] - 1)/
       primes[[-1]], Re[ep] != 0},
       Flatten[Table[{Re[ep] != Zvar[[j]], Re[ep] != -Zvar[[j]]}, {j, 1, 
   Length[Zvar]}]] 
   ] /. Zrule // Flatten;
  Return[constrains];
  ]
  


AutoMBHighAsySplit[MBHighList_, dList_, Zrules_, Constrain_, ep_, 
  Normalisation_, epOrder_, mtsOrder_, mtsMaxOrder_] := 
 Module[{RuleTmp, NestRuleTmp, ResultTmp, i, j, k, sum, sumReg, 
   ContourCheck, LocalList, redo, NewConstrains, count},
  sum = 0;
  Print["Total MB integral to be continued: ", Length[MBHighList]];
  For[i = 1, i <= Length[MBHighList], i++,
   Print["=========================="];
   Print["Start Analytical Continuation of MB integral of Region # ", 
    i, ", at higher expansion order: ", mtsOrder];
   sumReg = 0;
   Print["Total number of Sub-shift = ", 
    Length[MBHighList[[i, mtsOrder]]]];
   For[j = 1, j <= Length[MBHighList[[i, mtsOrder]]], j++,
    Print["Start AC of MB integral of Region # ", i, 
     " with Sub-Shifts # ", j];
    (*Head is Plus,split into nested lists*)
    
    If[Head[MBHighList[[i, mtsOrder, j]]] === Plus,
     LocalList = List @@ MBHighList[[i, mtsOrder, j]];
     ,
     (*Head is Times, do not split into lists*)
     
     If[Head[MBHighList[[i, mtsOrder, j]]] === Times,
       LocalList = {MBHighList[[i, mtsOrder, j]]};
       ,
       (*else*)
       If[MBHighList[[i, mtsOrder, j]] === 0,
         LocalList = {};
         If[verbose === True,
          Print["Nothing to compute."];
          ];
         ,
         Print["!!! ---------- WARNING -------------  !!!"];
         
         Print["Unclassified MB integral: ", 
          MBHighList[[i, mtsOrder, j]]];
         
         Print["Head of this term: ", 
          Head[MBHighList[[i, mtsOrder, j]]] ];
         Print["Abort and debug !!"];
         Abort[];
         ];
       ];
     ];
    If[verbose === True,
     Print["MB LocalList: ", LocalList];
     ];
    
    For[k = 1, k <= Length[LocalList], k++,
     If[verbose === True,
      Print["AC of MB integral # ", k, " inside Sub-Shifts # ", j, 
        " of region #", i];
      ];
     RuleTmp = 
      Check[
       MBoptimizedRules[LocalList[[k]] /. Zrules, ep -> 0, Constrain, 
        Join[dList, {ep}]], err, MBrules::norules];
     If[RuleTmp =!= err,
      NestRuleTmp = AddZrules[RuleTmp, Zrules];
      ResultTmp = 
       Collect[
        Check[
         MBintContExp[LocalList[[k]], NestRuleTmp, dList, ep, 
          Normalisation, epOrder], err, MBresidues::contour], 
        Join[dList, {ep}], Together];
      ,
      Print[">>>>>  No MB rules found !!!  <<<<<<"];
      ];
     If[(ResultTmp === err) || (RuleTmp === err),
      redo = True;
      count = 1;
      While[redo,
       Print["@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"];
       Print[
        "!!!  Start to RE-DO with different contour constrains  \
!!!"];
       Print["RE-DO round #", count];
       If[count <= 8,
        NewConstrains = RandomConstrain[dList, Zrules];
        ,
        Print[
         "RE-DO count > 8, remove all constrains on regulators"];
        NewConstrains = {};
        ];
       RuleTmp = 
        Check[
         MBoptimizedRules[LocalList[[k]] /. Zrules, ep -> 0, 
          NewConstrains, Join[dList, {ep}]], err, MBrules::norules];
       If[RuleTmp =!= err,
        NestRuleTmp = AddZrules[RuleTmp, Zrules];
        ResultTmp = 
         Collect[
          Check[
           MBintContExp[LocalList[[k]], NestRuleTmp, dList, ep, 
            Normalisation, epOrder], err, MBresidues::contour], 
          Join[dList, {ep}], Together];
        ,
        Print[">>>>>  No MB rules found !!!  <<<<<<"];
        ];
       count = count + 1;
       If[(ResultTmp =!= err) && (RuleTmp =!= err),
        redo = False;
        Print["-----------------------"];
        Print["RE-DO sucessful! "];
        Print["@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"];
        ];
       If[count > 10,
        Print[">>>>>>>>>><<<<<<<<<<<<"];
        Print["RE-DO NOT successful !!!"];
        Print[
         "This is a No MB rules problem, or a nearly Gamma[0] problem \
caused by the regulator choices."];
        Print[
         "Try to use different contour choice, or use AsyInt default \
Zrule, or add more auxiliary MB integration variables (Z -> 1/(prime \
number)) to allow more constrains for finding the good analytic \
contunation ruels."];
        Abort[];
        ];
       ];
      ];
     sumReg = sumReg + ResultTmp;
     ];
    ];
   sum = sum + sumReg;
   ];
  Return[Collect[sum, Join[dlist, {ep}], Together]];
  ]
   




   






(* ------- automated functions --------  *)
ExpSafetyCheck = True;

AutoAnalyContMBHighAsy[MBHighList_, TempIntList_, dList_, Zrules_, 
  Constrain_, ep_, Normalisation_, epOrder_, mtsMaxOrder_, ExpDepth_, 
  DotShift_ : {}] := 
 Module[{ACMBLeadAsy, ACMBHighAsy, order, ACMBList, ACMBFull},
  order = 0;
  ACMBList = {};
  Print["%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"];
  Print["Start AC for leading expansion order # ", order];
  Print[""];
  If[DotShift === {},
   ACMBLeadAsy = 
     AutoMB[TempIntList, dList, Zrules, Constrain, ep, Normalisation, 
      epOrder];
   ,
   Print[
    "Dot propagator present with shift: ", {DotShift[[1]] -> 
      DotShift[[1]] + DotShift[[2]]}];
   Print[
    "Shift leading MB Temp. Integral for AC (AutoMBshift routine)."];
   Print[
    "Note that high-order MB expansion integrals are already shifted \
(no further shifts needed)."];
   ACMBLeadAsy = 
    AutoMBshift[TempIntList, dList, Zrules, Constrain, ep, 
     Normalisation, epOrder, DotShift[[1]], DotShift[[2]]];
   ];
  AppendTo[ACMBList, ACMBLeadAsy];
  For[order = 1, order <= ExpDepth, order++,
   Print["%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"];
   Print["Start AC for higher expansion order # ", order];
   Print[""];
   ACMBHighAsy = 
    AutoMBHighAsySplit[MBHighList, dList, Zrules, Constrain, ep, 
     Normalisation, epOrder, order, mtsMaxOrder];
   AppendTo[ACMBList, ACMBHighAsy];
   ];
  ACMBFull = Sum[ACMBList[[i]], {i, 1, Length[ACMBList]}];
  Return[ACMBFull];
  ]


AutoMBNumeratorHighAsyFullExp[MBHighList_, TempIntList_, dList_, 
  Zrules_, Constrain_, ep_, Normalisation_, epOrder_, mtsMaxOrder_, 
  expDepth_] := 
 Module[{ACMBLeadAsy, ACMBHighAsy, order, ACMBList, ACMBFull, 
   mtsDepth}, order = 0;
  ACMBList = {};
  For[order = 1, order <= expDepth, order++,
   Print["%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"];
   Print["Start AC at rho expansion order # ", (order - 1)];
   Print[""];
   ACMBHighAsy = 
    AutoMBHighAsySplit[MBHighList, dList, Zrules, Constrain, ep, 
     Normalisation, epOrder, order, mtsMaxOrder];
   AppendTo[ACMBList, ACMBHighAsy];
   ];
  ACMBFull = Sum[ACMBList[[i]], {i, 1, Length[ACMBList]}];
  Return[ACMBFull];
  ]
   




AsyExp2MB[UFpoly_, Scaling_, dlist_, xlist_, ExpLowOrd_, 
  ExpMaxOrd_, SmallInvariant_, ScalePara_, TempIntList_, HardIntList_, Zrule_,
   epOrd_, DotShift_ : {}, AddExpOrd_ : 0] := 
 Module[{AlphaHighRegs, expOrder, MBHighAsyRegions, check, ShiftRule, 
   PrimeList, ConstRelax, loop, epNormEulerGamma, ACMBFull, ACMBsim, Zvar, mm, i, j},
  Print["Start generating expansion terms at UF polynomial level."];
  Print["To properly handle non-planar topology scaling e.g. ", 
   1/Sqrt[SmallInvariant]];
  Print["the following scaling parameters are used: ", 
   ScalePara -> rho^2];
  ShiftRule = {};
  If[DotShift =!= {},
   Print["-------------------------------"];
   ShiftRule = {DotShift[[1]] -> DotShift[[1]] + DotShift[[2]]};
   Print[
    "Dot propagator present with shift: ", {DotShift[[1]] -> 
      DotShift[[1]] + DotShift[[2]]}];
   ];
  expOrder = 2*(ExpMaxOrd - ExpLowOrd) + AddExpOrd;
  If[ExpSafetyCheck === True,
   Print["-------------------------------"];
   Print[
    "Note: expansion safety check on (default), expansion depth is \
increased."];
   expOrder = expOrder + 2;
   ];
  AlphaHighRegs = 
   AlphaExpHighRegions[UFpoly, Scaling, SmallInvariant, dlist /. ShiftRule, xlist, 
    expOrder, ScalePara];
  Print["-----------------------------------------------"];
  Print["-----------------------------------------------"];
  Print["Start generating Mellin-Barnes integral representation \
through template integrals for asymptotic expanded regions, e.g. \
soft,collinear,ultra-soft etc."];

  MBHighAsyRegions = 
   AutoHighAsyRegionsNPL[AlphaHighRegs, TempIntList, HardIntList, {}, 
    dlist /. ShiftRule, expOrder, SmallInvariant, ExpMaxOrd];
  If[ExpSafetyCheck === True,
   check = 
    Join[
      Table[
         MBHighAsyRegions[[i]][[-1]], {i, 1, 
          Length[MBHighAsyRegions]}] // Flatten // 
       DeleteCases[#, 0] &, 
      Table[
         MBHighAsyRegions[[i]][[-2]], {i, 1, 
          Length[MBHighAsyRegions]}] // Flatten // 
       DeleteCases[#, 0] &] // Flatten;
   If[check =!= {},
    Print[
     "!! Expansion safety check not passed. Stop and debug output."];
    Print["Please try to increase the AddExpOrd to values > 0."];
    Return[{"Abort. Expansion safety check not pass!", AlphaHighRegs, 
      MBHighAsyRegions}];
    ];
   Print["Pass expansion safety check."];
   expOrder = expOrder - 2;
   ];
  Print["-------------------------------------------------"];
  Print["-------------------------------------------------"];
  Print["-- Start analytic continuation of MB integrals --"];
  Print["-------------------------------------------------"];
  PrimeList = {23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 
   83};
  Zvar = Table[Zrule[[mm, 1]], {mm, 1, Length[Zrule]}];
  ConstRelax = 
    Join[Table[{Abs[Re[dlist[[i]]]] < (2*PrimeList[[i]] + 1)/
        PrimeList[[i]], Re[dlist[[i]]] != 0}, {i, 1, Length[dlist]}], 
    Flatten[
     Table[{Re[dlist[[i]]] != Zvar[[j]], 
       Re[dlist[[i]]] != -Zvar[[j]]}, {i, 1, Length[dlist]}, {j, 1, 
       Length[Zvar]}]], {Abs[Re[ep]] < (PrimeList[[-1]] - 1)/
       PrimeList[[-1]], Re[ep] != 0}] /. Zrule // Flatten;

  loop = UFpoly[[3]];
  epNormEulerGamma = Exp[loop*EulerGamma*ep];
  Print["EulerGamma normalisation factor = ", epNormEulerGamma];
  Print["Integration contour on real-axis fixed at ", Zrule];
  ACMBFull = 
   AutoAnalyContMBHighAsy[MBHighAsyRegions, TempIntList, dlist, Zrule,
     ConstRelax, ep, epNormEulerGamma, epOrd, ExpMaxOrd, expOrder, 
    DotShift];
  ACMBsim = 
   Collect[ACMBFull /. polygam, Join[{ep, SmallInvariant}, dlist] ];
  Return[ACMBsim];
  ]




AsyExpNum2MB[UFpolyNum_, numidx_, UFpoly_, Scaling_, dlistNum_, 
  xlistNum_, ExpLowOrd_, ExpMaxOrd_, SmallInvariant_, ScalePara_, 
  TempIntList_, HardIntList_, Zrule_, epOrd_, AddExpOrd_ : 0] := 
 Module[{AlphaHighRegs, expOrder, MBHighAsyRegions, check, ShiftRule, 
   PrimeList, ConstRelax, loop, epNormEulerGamma, ACMBFull, ACMBsim, 
   alpharepNum, xlist, dlist, Zvar, mm, i, j}, 
  Print["Start generating expansion terms at UF polynomial level."];
  Print["To properly handle non-planar topology scaling e.g. ", 
   1/Sqrt[SmallInvariant]];
  Print["the following scaling parameters are used: ", 
   ScalePara -> rho^2];
  expOrder = 
   2*(ExpMaxOrd - ExpLowOrd) + 2*Length[numidx] + AddExpOrd;
  loop = UFpoly[[3]];
  xlist = 
   DeleteCases[
    xlistNum /. 
     Table[xlistNum[[numidx[[n]]]] -> 0, {n, 1, Length[numidx]}], 0];
  dlist = 
   DeleteCases[
    dlistNum /. 
     Table[dlistNum[[numidx[[n]]]] -> 0, {n, 1, Length[numidx]}], 0];
  If[ExpSafetyCheck === True,
   Print["-------------------------------"];
   Print[
    "Note: expansion safety check on (default), expansion depth is \
increased."];
   expOrder = expOrder + 2;
   ];
  alpharepNum = (DAlphaRep[UFpolyNum[[1]], UFpolyNum[[2]], loop, 
      dlistNum + 1, xlistNum, numidx, D]) // Simplify;
  AlphaHighRegs = 
   AlphaExpNumRegions[alpharepNum, UFpoly, Scaling, SmallInvariant, 
    dlist, xlist, D, ScalePara, expOrder];
  
  Print["-----------------------------------------------"];
  Print["-----------------------------------------------"];
  Print["Start generating Mellin-Barnes integral representation \
through template integrals for asymptotic expanded regions, e.g. \
soft,collinear,ultra-soft etc."];
  MBHighAsyRegions = 
   AutoHighAsyRegionsNPL[AlphaHighRegs, TempIntList, HardIntList, {}, 
    dlist, expOrder, SmallInvariant, ExpMaxOrd, dlistNum];
  If[ExpSafetyCheck === True,
   check = 
    Join[
      Table[
         MBHighAsyRegions[[i]][[-1]], {i, 1, 
          Length[MBHighAsyRegions]}] // Flatten // 
       DeleteCases[#, 0] &, 
      Table[
         MBHighAsyRegions[[i]][[-2]], {i, 1, 
          Length[MBHighAsyRegions]}] // Flatten // 
       DeleteCases[#, 0] &] // Flatten;
   If[check =!= {},
    Print[
     "!! Expansion safety check not passed. Stop and debug output."];
    Print["Please try to increase the AddExpOrd to values > 0."];
    Return[{"Abort. Expansion safety check not pass!", AlphaHighRegs, 
      MBHighAsyRegions}];
    ];
   Print["Pass expansion safety check."];
   expOrder = expOrder - 2;];
  Print["-------------------------------------------------"];
  Print["-------------------------------------------------"];
  Print["-- Start analytic continuation of MB integrals --"];
  Print["-------------------------------------------------"];
  PrimeList = {23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83};
  Zvar = Table[Zrule[[mm, 1]], {mm, 1, Length[Zrule]}];
  ConstRelax = 
    Join[Table[{Abs[Re[dlist[[i]]]] < (2*PrimeList[[i]] + 1)/
        PrimeList[[i]], Re[dlist[[i]]] != 0}, {i, 1, Length[dlist]}], 
    Flatten[
     Table[{Re[dlist[[i]]] != Zvar[[j]], 
       Re[dlist[[i]]] != -Zvar[[j]]}, {i, 1, Length[dlist]}, {j, 1, 
       Length[Zvar]}]], {Abs[Re[ep]] < (PrimeList[[-1]] - 1)/
       PrimeList[[-1]], Re[ep] != 0}] /. Zrule // Flatten;
  
  epNormEulerGamma = Exp[loop*EulerGamma*ep];
  Print["EulerGamma normalisation factor = ", epNormEulerGamma];
  Print["Integration contour on real-axis fixed at ", Zrule];
  ACMBFull = 
   AutoMBNumeratorHighAsyFullExp[MBHighAsyRegions, TempIntList, dlist,
     Zrule, ConstRelax, ep, epNormEulerGamma, epOrd, ExpMaxOrd, 
    expOrder];
  ACMBsim = 
   Collect[ACMBFull /. polygam, Join[{ep, SmallInvariant}, dlist]];
  Return[ACMBsim];
  ]




(* polygamma rules *)

polygam={PolyGamma[0, -7/2] -> 352/105 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -5/2] -> 46/15 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -3/2] -> 8/3 - EulerGamma - 2*Log[2], 
 PolyGamma[0, -1/2] -> 2 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 1/2] -> -EulerGamma - 2*Log[2], 
 PolyGamma[0, 3/2] -> 2 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 5/2] -> 8/3 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 7/2] -> 46/15 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 9/2] -> 352/105 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 11/2] -> 1126/315 - EulerGamma - 2*Log[2], 
 PolyGamma[0, 13/2] -> 13016/3465 - EulerGamma - 2*Log[2],
 PolyGamma[2, 1] -> -2*Zeta[3], PolyGamma[2, 2] -> 2*(1 - Zeta[3]), 
 PolyGamma[2, 3] -> 2*(9/8 - Zeta[3]), PolyGamma[2, 4] -> 
  2*(251/216 - Zeta[3]), PolyGamma[2, 5] -> 2*(2035/1728 - Zeta[3]), 
 PolyGamma[2, 6] -> 2*(256103/216000 - Zeta[3]), 
 PolyGamma[2, 7] -> 2*(28567/24000 - Zeta[3]), 
 PolyGamma[2, 8] -> 2*(9822481/8232000 - Zeta[3]), 
 PolyGamma[2, 9] -> 2*(78708473/65856000 - Zeta[3]), 
 PolyGamma[2, 10] -> 2*(19148110939/16003008000 - Zeta[3]), 
 PolyGamma[4, 1] -> -24*Zeta[5], PolyGamma[4, 2] -> 24*(1 - Zeta[5]), 
 PolyGamma[4, 3] -> 24*(33/32 - Zeta[5]), PolyGamma[4, 4] -> 
  24*(8051/7776 - Zeta[5]), PolyGamma[4, 5] -> 24*(257875/248832 - Zeta[5]), 
 PolyGamma[4, 6] -> 24*(806108207/777600000 - Zeta[5]), 
 PolyGamma[4, 7] -> 24*(268736069/259200000 - Zeta[5]), 
 PolyGamma[4, 8] -> 24*(4516906311683/4356374400000 - Zeta[5]), 
 PolyGamma[4, 9] -> 24*(144545256245731/139403980800000 - Zeta[5]), 
 PolyGamma[4, 10] -> 24*(105375212839937899/101625502003200000 - Zeta[5]), 
 PolyGamma[6, 1] -> -720*Zeta[7], PolyGamma[6, 2] -> 720*(1 - Zeta[7]), 
 PolyGamma[6, 3] -> 720*(129/128 - Zeta[7]), 
 PolyGamma[6, 4] -> 720*(282251/279936 - Zeta[7]), 
 PolyGamma[6, 5] -> 720*(36130315/35831808 - Zeta[7]), 
 PolyGamma[6, 6] -> 720*(2822716691183/2799360000000 - Zeta[7]), 
 PolyGamma[6, 7] -> 720*(940908897061/933120000000 - Zeta[7]), 
 PolyGamma[6, 8] -> 720*(774879868932307123/768464444160000000 - Zeta[7]), 
 PolyGamma[6, 9] -> 720*(99184670126682733619/98363448852480000000 - 
    Zeta[7]), PolyGamma[6, 10] -> 
  720*(650750755630450535274259/645362587921121280000000 - Zeta[7]), 
 PolyGamma[8, 1] -> -40320*Zeta[9], PolyGamma[8, 2] -> 40320*(1 - Zeta[9]), 
 PolyGamma[8, 3] -> 40320*(513/512 - Zeta[9]), 
 PolyGamma[8, 4] -> 40320*(10097891/10077696 - Zeta[9]), 
 PolyGamma[8, 5] -> 40320*(5170139875/5159780352 - Zeta[9]), 
 PolyGamma[8, 6] -> 40320*(10097934603139727/10077696000000000 - Zeta[9]), 
 PolyGamma[8, 7] -> 40320*(373997614931101/373248000000000 - Zeta[9]), 
 PolyGamma[8, 8] -> 40320*(15092153145114981831307/15061903105536000000000 - 
    Zeta[9]), PolyGamma[8, 9] -> 
  40320*(7727182467755471289426059/7711694390034432000000000 - Zeta[9]), 
 PolyGamma[8, 10] -> 40320*(4106541588424891370931874221019/
     4098310578334288576512000000000 - Zeta[9]), 
 PolyGamma[10, 1] -> -3628800*Zeta[11], PolyGamma[10, 2] -> 
  3628800*(1 - Zeta[11]), PolyGamma[10, 3] -> 3628800*(2049/2048 - Zeta[11]), 
 PolyGamma[10, 4] -> 3628800*(362976251/362797056 - Zeta[11]), 
 PolyGamma[10, 5] -> 3628800*(743375539195/743008370688 - Zeta[11]), 
 PolyGamma[10, 6] -> 3628800*(36297634492764230063/36279705600000000000 - 
    Zeta[11]), PolyGamma[10, 7] -> 
  3628800*(12099211530921410021/12093235200000000000 - Zeta[11]), 
 PolyGamma[10, 8] -> 3628800*(23924094541398110665791491603/
     23912277370348953600000000000 - Zeta[11]), 
 PolyGamma[10, 9] -> 3628800*(48996545626484461837262019724819/
     48972344054474656972800000000000 - Zeta[11]), 
 PolyGamma[10, 10] -> 3628800*(26038773205113879830003552224577534179/
     26025911496654066176281804800000000000 - Zeta[11])};

