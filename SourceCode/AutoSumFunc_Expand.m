(*-------------------------------------------
 |                                          |
 |   AutoSumFunc_Expand.m of AsyInt v.1.0   |
 |                                          |
 |   by Hantian Zhang, July 15, 2024        |
 |                                          | 
 --------------------------------------------*)


(* main interfaces *)

AIExpandI1[MBexp_, Zinit_, kinit_, Znest_, knest_, Zcontour_, 
  KinExpShift_, KinList_, TransParaList_, ConstList_, PrecList_, 
  LRpreset_ : {"R", "R"}, AddShift_ : 2] := 
 Module[{ToSum, explist, output, IgnoreBDconst},
  ToSum = 0;
  IgnoreBDconst = 0;
  Print["================================================"];
  Print["Start expanding 2D MB integrals in ", KinList ," -> 0 limit."];
  explist = 
   AIExpand2DMB[MBexp, Zinit, kinit, Znest, knest, Zcontour, 
    KinExpShift, LRpreset /. {"R" -> 2, "L" -> 1}, AddShift, 
    IgnoreBDconst, ToSum];
  Print["================================================"];
  Print["Start PSLQ for nested 1D MB integrals..."];
  output = 
   AINumRec1DMB[explist[[1]], KinList, TransParaList, ConstList, 
    Zcontour, PrecList, KinExpShift, explist[[2]] // FunctionExpand];
  Return[output];
  ]
  

AIExpandI2[MBexp_, Zinit_, kinit_, Znest_, knest_, Zcontour_, 
  KinExpShift_, MBscale_, MBscale2_, SmallInv_, 
  LRpreset_ : {"R", "R"}, AddShift_ : 2] := 
 Module[{ToSum, explist, output, IgnoreBDconst, MBList, i, sumres},
  ToSum = 0;
  IgnoreBDconst = 0;
  Print["================================================"];
  Print["Start expanding 2D MB integrals in ", MBscale, " -> 0 limit"];
  explist = 
   AIExpand2DMB[MBexp, Zinit, kinit, Znest, knest, Zcontour, 
    KinExpShift, LRpreset /. {"R" -> 2, "L" -> 1}, AddShift, 
    IgnoreBDconst, ToSum];
  MBList = CoefficientList[explist[[1]], MBscale];
  Print["================================================"];
  Print["Start summation for nested 1D MB integrals with second scale \
", MBscale2];
  output = explist[[2]];
  For[i = 1, i <= Length[MBList], i++,
   Print["----------------------------"];
   Print["calculte order ", MBscale^(i - 1)];
   sumres = 
    MBscale^(i - 1)*
     AISum1DMB[MBList[[i]], Znest, knest, Zcontour, SmallInv, 
      MBscale2, "none", "R", AddShift];
   output = output + sumres;
   ];
  Return[
   Collect[output, MBscale] /. MBscale^n_ /; n > KinExpShift :> 0 // FunctionExpand ];
  ]


(* Beta version with ParalleSum and furthe improvements to obtain even deeper expansions *)

TakeExtraResBeta[MBex_, PoleList_, Z_, k_, LR_, KinExpShift_] := 
 Module[
  {i, j, ResList, shift, offset, extra, res, Zpole, constrain, 
   RepExtra, ShiftExtra},
  (*LR=1:take left poles,LR=2:take right poles*)
  ResList = {};
  For[i = 2, i <= Length[PoleList[[LR]]], i++,
   shift = PoleList[[LR, i, 2]];
   offset = (-1)^LR*shift;
   Zpole = Z /. PoleList[[LR, i, 1]];
   ShiftExtra = 
    ParallelSum[(-1)^(LR + 1)*Residue[MBex, {Z, Zpole + (-1)^LR*(j - 1)}], {j, 1, shift}];
    Print["Shifted extra residue representation obtained with \
ParallelSum"];
   RepExtra = (-1)^(LR + 1)*
     Residue[MBex, {Z, Zpole + offset + (-1)^LR*k}];
   Print[
    "Extra residue representation obtained, entering ParallelSum \
step."];
   extra = 
    ShiftExtra + ParallelSum[RepExtra, {k, 0, KinExpShift - shift}];
   If[Not[FreeQ[extra, ComplexInfinity]] || 
     Not[FreeQ[extra, Indeterminate]] || Not[FreeQ[extra, Infinity]],
    Print["Infinity encountered !!! Stop!!"];
    Print["Try to increase mode of AddShift."];
    Abort[];
    ];
   If[Length[PoleList[[LR, i]]] === 3,
    AppendTo[ResList, {extra, 0, PoleList[[LR, i, 3]]}];
    ,
    AppendTo[ResList, {extra, 0}];
    ];
   ];
  Return[ResList];
  ]

TakeNestResExpKinBeta[MBex_, Z_, k_, KinExpShift_, kinit_, Zcontour_, 
  AddShift_ : 0, LRpreset_ : {0, 0}, ToSum_ : 0] := Module[
  {i, j, MBLength, ResList, NestResList, res, MB1D, MBex2, n, 
   NestPoleList, NestLR, constrain},
  (*LR=1:take left poles,LR=2:take right poles*)
  
  MBLength = Length[MBex];
  For[i = 1, i <= MBLength, i++,
   ResList = {};
   If[Length[MBex[[i]]] === 3,
    constrain = MBex[[i, 3]];
    For[j = 1, j <= 2, j++,
     If[Head[MBex[[i, j]]] === Plus,
      MBex2 = List @@ MBex[[i, j]];
      ,
      MBex2 = {MBex[[i, j]]};
      ];
     res = 0;
     Print["Total number of nested MB to expand: ", Length[MBex2]];
     For[n = 1, n <= Length[MBex2], n++,
      Print["-------------------"];
      Print["Nested MB integral #",n," out of ", Length[MBex2]];
      Print["Nested MB expression: ",MBex2[[n]] ];
      NestPoleList = 
       GetNestResList[{{MBex2[[n]], 0, constrain}}, kinit, Z, 
        Zcontour, AddShift];
      Print["Nested Res List:", NestPoleList];
      NestLR = DecideNestLR[NestPoleList, LRpreset[[2]]];
      Print["Nest L/R choice auto decided: ", NestLR, 
       ", with LRpreset: ", LRpreset];
      If[ToSum === 0,
       res = 
         res + 
          TakeExtraResBeta[MBex2[[n]], NestPoleList[[1, j]], Z, k, 
           NestLR[[1, j]], KinExpShift];
       ,
       Print["ToSum present: ", ToSum];
       res = 
        res + 
         TakeRes[MBex2[[n]], NestPoleList[[1, j]], Z, k, 
          NestLR[[1, j]]];
       ];
      ];
     AppendTo[ResList, Flatten[res]];
     ];
    ,
    MB1D = MBex[[i]];
    For[j = 1, j <= 2, j++, res = {};
     AppendTo[ResList, Flatten[res]];
     ];
    ];
   NestResList[i] = ResList;
   ];
  Return[{MB1D, Table[NestResList[i], {i, 1, MBLength}]}];
  ]

Expand2DMBKinBeta[MBexp_, Zinit_, kinit_, Znest_, knest_, Zcontour_, 
  KinExpShift_, LRpreset_ : {0, 0}, AddShift_ : 0, IgnoreBDconst_ : 0,
   ToSum_ : 0] := Module[
  {InitResList, InitLR, Res1, NestResList, NestLR, Res2, pass, 
   ResSigma},
  (*Print["Initial MB integral:",MBexp];*)
  
  InitResList = GetResList[MBexp, Zinit, Zcontour, AddShift];
  Print["Initial Res List:", InitResList];
  If[LRpreset[[1]] === 0, InitLR = DecideLR[InitResList];
   Print["Initial L/R choice auto decided: ", InitLR];
   ,
   InitLR = LRpreset[[1]];
   Print["Initial L/R choice overwrite to present: ", InitLR];
   ];
  Res1 = 
   TakeExtraResBeta[MBexp, InitResList, Zinit, kinit, InitLR, 
    KinExpShift];
  Print[""];
  Print["------ Start nested residue extraction ------"];
  (*Print["MB 1st SumRep:",Res1];*)
  
  Res2 = 
   TakeNestResExpKinBeta[Res1, Znest, knest, KinExpShift, kinit, 
    Zcontour, AddShift, LRpreset, ToSum];
  (*Print["MB 2st SumRep:",Res2];*)
  If[ToSum === 0,
   Print["ToSum = 0, no need to check boundary."];
   pass = True;
   ,
   Print["ToSum != 0, check boundary."];
   pass = 
    CheckInfinity2D[Res2[[2]], knest, kinit, IgnoreBDconst, InitLR];
   If[pass === True, Print["Boundary check PASS."];
    ,
    Print["Boundary check NOT pass !!!"];
    ];
   ];
  Return[{pass, Res2}];
  ]


AIExpand2DMB[MBexp_, Zinit_, kinit_, Znest_, knest_, 
  Zcontour_, KinExpShift_, LRpreset_ : {"R", "R"}, AddShift_ : 2, 
  IgnoreBDconst_ : 0, ToSum_ : 0] := 
 Module[{MBList, i, SumRepi, SumRepk1, SumRepk2, SumRepk1k2, 
   SumRepExtra, LRList, SumResi, count, AddSFTcount, SumMB1D, MB1D, output}, 
  If[Head[MBexp] === Plus, MBList = List @@ Expand[MBexp];
   Print["Total integral number #", Length[MBList]];, 
   If[Head[MBexp] === Times, MBList = {MBexp};
     Print["Only 1 integral to be computed."];, 
     Print["Head of MBexp is ", MBexp];
     Print["MB expression: ", MBexp];
     Print["Do not compute anything."];
     Return[MBexp];];];
  SumMB1D = 0;
  SumRepk1 = 0;
  SumRepk2 = 0;
  SumRepk1k2 = 0;
  SumRepExtra = 0;
  LRList = {{LRpreset[[1]], 1}, {LRpreset[[1]], 2}} /. {"R" -> 2, "L" -> 1};
  For[i = 1, i <= Length[MBList], i++, Print[""];
   Print["==================================="];
   Print["Calculation integral #", i];
   Print["Initial MB integral:", MBList[[i]]];
   SumResi = 
    Expand2DMBKinBeta[MBList[[i]], Zinit, kinit, Znest, knest, 
     Zcontour, KinExpShift, LRpreset/. {"R" -> 2, "L" -> 1}, AddShift, IgnoreBDconst, 
     ToSum];
   count = 1;
   AddSFTcount = AddShift;
   While[Not[SumResi[[1]]], Print["-------------------"];
    Print["Boundary check FAILS, try other LRpresets #", count, 
     " with ", LRList[[count]], " and AddShift: ", AddSFTcount];
    SumResi = 
     Expand2DMBKinBeta[MBList[[i]], Zinit, kinit, Znest, knest, 
      Zcontour, KinExpShift, LRList[[count]], AddSFTcount, 
      IgnoreBDconst, ToSum];
    count = count + 1;
    If[count > 2 && AddSFTcount <= 2, 
     If[SumResi[[1]] === False, Print[""];
       Print[
        "All 4 LRpreset FAIL. Increase AddShift parameter and reset \
LRprest count."];
       AddSFTcount = AddSFTcount + 1;
       count = 1;];];
    If[count > 2 && AddSFTcount > 2, 
     If[SumResi[[1]] === False, Print[""];
       Print["All LRpreset + AddShift FAIL!!! Abort."];
       Print[
        "May try to set IgnoreBDconst = 2 to allow Log divergence at \
infinity."];
       Abort[];
       ];
      ];
     ];
   (*1D MB integrals after expansion*)
   MB1D = SumResi[[2, 1, 1]];
   (*Print["MB1D:",MB1D];*)
   (*1D sum representation after expansion*)
   SumRepi = ConvertTo2DList[SumResi[[2, 2]], knest, kinit];
   SumMB1D = SumMB1D + MB1D;
   SumRepExtra = SumRepExtra + SumRepi[[1]];
   SumRepk1 = SumRepk1 + SumRepi[[2]];
   SumRepk2 = SumRepk2 + SumRepi[[3]];
   SumRepk1k2 = SumRepk1k2 + SumRepi[[4]];];
   If[ToSum === 0,
      output = {SumMB1D, SumRepExtra};
      ,
      output = {SumMB1D, {SumRepExtra, SumRepk1, SumRepk2, SumRepk1k2}};
   ];
   Return[output];
  ]


ConvertTo2DList[MBres_, k1_, k2_] := 
 Module[{i, j, k, MBk1k2, MBk1, MBk2, MBextra},
  MBk1 = 0;
  MBk2 = 0;
  MBk1k2 = 0;
  MBextra = 0;
  For[i = 1, i <= Length[MBres], i++,
   For[j = 1, j <= Length[MBres[[i]]], j++,
     If[MBres[[i, j]] =!= {}
       ,
       For[k = 1, k <= Length[MBres[[i, j]]], k++,
         If[FreeQ[MBres[[i, j, k]], k2] && FreeQ[MBres[[i, j, k]], k1],
           MBextra = MBextra + (MBres[[i, j, k]])
           ,
           If[FreeQ[MBres[[i, j, k]], k2],
             MBk1 = MBk1 + (MBres[[i, j, k]]);
             ,
             If[FreeQ[MBres[[i, j, k]], k1],
               MBk2 = MBk2 + (MBres[[i, j, k]]);
               ,
               MBk1k2 = MBk1k2 + (MBres[[i, j, k]]);
               ];
             ];
           ];
         ];
       ];
     ];
   ];
  Return[{MBextra, MBk1, MBk2, MBk1k2}];
  ]




(* analytic continuation function to physical region *)

ImRule = {im^2 -> -1, im^3 -> -im, im^4 -> 1, im^5 -> im, im^6 -> -1, 
   im^7 -> -im, im^8 -> 1, im^9 -> im, im^10 -> -1,
   1/im -> -im, 1/(im^2) -> -1, 1/(im^3) -> im, 1/(im^4) -> 1, 
   1/(im^5) -> -im, 1/(im^6) -> -1, 1/(im^7) -> im, 1/(im^8) -> 1};

ReduceToHBasisAdd[exp_] := 
 exp /. {H[0, 0, 0, 0, 0, 0, a_] :> 1/6! H[0, a]^6, 
   H[0, 0, 0, 0, 0, 0, 0, a_] :> 1/(7!) H[0, a]^7, 
   H[0, 0, 0, 0, 0, 0, 0, 0, a_] :> 1/(8!) H[0, a]^8}

ACsqrt[exp_,s_]:=exp/.{Power[Times[a_,Power[-s,-1]],Rational[n_,2]]:>(Sqrt[a/s]*im)^n,Power[Power[-s,-1],Rational[n_,2]]:>(Sqrt[1/s]*im)^n,Power[-s,Rational[n_,2]]:>((1/im)*Sqrt[s])^n}/.ImRule

AC2Phys0[exp_, SmallInv_, TT_, UU_, SS_, s_, 
  EuclKinRelation_ : {UU -> -TT - SS}] := 
 Collect[((exp /. EuclKinRelation /. 
             SS -> -s /. {H[0, SmallInv/(s - TT)] -> 
              H[0, SmallInv] - H[0, s - TT]} //. {H[0, -TT/s] :> 
             H[0, TT/s] + im*Pi, 
            H[0, -((s - TT)/s)] :> H[0, (s - TT)/s] + im*Pi, 
            H[0, -1 + TT/s] :> H[0, (s - TT)/s] + im*Pi, 
            H[0, -SmallInv/s] :> H[0, SmallInv/s] + im* Pi, 
            H[0, -s] :> H[0, s] - im* Pi} //. {H[a__, -TT/s] :> 
            TransformH[H[a, -TT/s], TT/s], 
           H[a__, -((s - TT)/s)] :> 
            TransformH[H[a, -((s - TT)/s)], (s - TT)/s], 
           H[a__, -SmallInv/s] :> 
            TransformH[H[a, -SmallInv/s], SmallInv/s]}) // 
        ReduceConstants // ReduceToHBasis // ReduceToHBasisAdd // 
     ToMathematicaConstants), {ep, SmallInv, im, _H}] //. ImRule

AC2Phys2s1[exp_, TT_, s_] := 
 Collect[
   ReduceConstants[
     exp /. H[0, s] -> H[0, sss] /. s -> 1 /. {H[a__, TT/(1 - TT)] :> 
          TransformH[H[a, TT/(1 - TT)], TT], 
         H[a__, 1 - TT] :> TransformH[H[a, 1 - TT], TT]} // 
       ReduceToHBasis // ReduceToHBasisAdd, 
     ToKnownConstants -> True] // ToMathematicaConstants, {ep, mts, 
    im, _H}] //. ImRule /. H[0, sss] -> H[0, s]

ReconstructPref[SS_, IntDef_, LoopOrd_] := Module[{power},
  power = LoopOrd*2 - Sum[IntDef[[i]], {i, 1, Length[IntDef]}];
  Return[(SS)^(-LoopOrd*ep)*SS^power];
  ]


AC2Phys[exp_, SmallInv_, TT_, UU_, SS_, s_, 
  EuclKinRelation_ : {UU -> -TT - SS}] := 
 AC2Phys2s1[AC2Phys0[exp, SmallInv, TT, UU, SS, s, EuclKinRelation], 
   TT, s] /. {TT -> TT/s, SmallInv -> SmallInv/s}


AIEucl2Phys[exp_, SmallInv_, TT_, UU_, SS_, s_, mus_, IntDef_, 
  LoopOrd_, epOrd_, NoEpExp_ : False, 
  EuclKinRelation_ : {UU -> -TT - SS}] := Module[
  {prefEucl, prefPhys, exp2, exp3, exp4, output},
  prefEucl = ReconstructPref[SS, IntDef, LoopOrd];
  prefPhys = ReconstructPref[s, IntDef, LoopOrd];
  Print["Euclidean prefactor = ", prefEucl];
  Print["Physical prefactor = ", prefPhys];
  If[NoEpExp === False,
   exp2 = 
    Collect[
     Normal[
       Series[
        prefEucl*(exp /. SS -> 1 /. {TT -> TT/SS, UU -> UU/SS, 
            SmallInv -> SmallInv/SS}), {ep, 0, epOrd}]] // 
      ToHLogs, {ep, SmallInv, _H, im}];
   Print["Re-expand to O(ep^", epOrd, ")"];
   ,
   Print["!! ====================== !!"];
   Print[
    "Note: ep-expansion switched off, please make sure your \
expressions does not contain ep-dependence."];
   exp2 = 
    Simplify[(SS)^(LoopOrd*ep)*
        prefEucl]*(exp /. SS -> 1 /. {TT -> TT/SS, UU -> UU/SS, 
         SmallInv -> SmallInv/SS}) // ToHLogs;
   ];
  exp3 = ACsqrt[exp2 /. SS -> -s, s];
  exp4 = AC2Phys0[exp3, SmallInv, TT, UU, SS, s, EuclKinRelation];
  (*Print["Check HPL list after 1st AC step: ",Cases[exp4,_H,
  Infinity]//Union];*)
  If[NoEpExp === False,
   output = Simplify[(mus)^(LoopOrd*ep)*
      prefPhys] * Collect[ (AC2Phys2s1[exp4, TT, s] /. {TT -> TT/s, UU -> UU/s, 
         SmallInv -> SmallInv/s}) // FunctionExpand, {ep, SmallInv, im, _H, _PolyGamma, _Zeta, Pi}, Factor];
   Print[
    "Analytic continuation to physical region done. Regularisation \
scale \[Mu]^2 (", mus, ") restored."];
   ,
   output = 
    Simplify[(s)^(LoopOrd*ep)*
       prefPhys]* Collect[ (AC2Phys2s1[exp4, TT, s] /. {TT -> TT/s, UU -> UU/s, 
        SmallInv -> SmallInv/s}), {ep, SmallInv, im, _H, _PolyGamma, _Zeta, Pi}, Factor];
   Print["Analytic continuation to physical region done."];
   Print["!! ====================== !!"];
   Print["Note: ep-expansion switched off, no (", mus/s, ")^", 
    LoopOrd*ep, " prefactor attached."];
   ];
  Return[output];
  ]




  
(* HPL ansatz up to weight-5 *)  

HPLbasis = Get["../AnsatzBasis/HPLbasis_W5.m"];

HPLbasisEucl = 
 Get["../AnsatzBasis/HPLbasis_W5.m"] /. H[a__, x] :> 0 /; Count[{a}, 1] > 0;

HPLbasisPhys = 
 DeleteCases[
  HPLbasis /. H[a__, x] :> 0 /; Count[{a}, -1] > 0, 0] 
 
BuildTransListW4[HPLbasis_, Ratbasis_] := 
 Module[{HPLw1, HPLw2, HPLw3, HPLw4, HPLlist},
  HPLw1[1] = DeleteCases[HPLbasis[[1]], 0];
  HPLw2[1] = 
   DeleteCases[
    Join[
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[1, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, i}]] // ReduceToHBasis, 
     HPLbasis[[2]]], 0];
  HPLw3[1] = 
   DeleteCases[
    Join[
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[1, j]]*HPLbasis[[1, k]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, i}, {k, 1, j}]] // 
      ReduceToHBasis,
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[2, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[2]]]}]] // 
      ReduceToHBasis, HPLbasis[[3]]], 0];
  HPLw4[1] = 
   DeleteCases[
    Join[
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[1, j]]*HPLbasis[[1, k]]*
         HPLbasis[[1, m]], {i, 1, Length[HPLbasis[[1]]]}, {j, 1, 
         i}, {k, 1, j}, {m, 1, k}]] // ReduceToHBasis,
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[1, k]]*HPLbasis[[2, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {k, 1, i}, {j, 1, 
         Length[HPLbasis[[2]]]}]] // ReduceToHBasis,
     Flatten[
       Table[
        HPLbasis[[1, i]]*HPLbasis[[3, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[3]]]}]] // 
      ReduceToHBasis, HPLbasis[[4]]], 0];
  HPLw1[2] = 
   Flatten[Table[HPLw1[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw2[2] = 
   Flatten[Table[HPLw2[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw3[2] = 
   Flatten[Table[HPLw3[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw4[2] = 
   Flatten[Table[HPLw4[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLlist = {Ratbasis, HPLw1[2], HPLw2[2], HPLw3[2], HPLw4[2]};
  Return[HPLlist]]


BuildTransListW5[HPLbasis_, Ratbasis_] := 
 Module[{HPLw1, HPLw2, HPLw3, HPLw4, HPLw5, HPLlist},
  HPLw1[1] = DeleteCases[HPLbasis[[1]], 0];
  HPLw2[1] = 
   DeleteCases[
    Join[Flatten[
       Table[HPLbasis[[1, i]]*HPLbasis[[1, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, i}]] // ReduceToHBasis, 
     HPLbasis[[2]]], 0];
  HPLw3[1] = DeleteCases[Join[
     Flatten[
       Table[HPLbasis[[1, i]]*HPLbasis[[1, j]]*HPLbasis[[1, k]], {i, 
         1, Length[HPLbasis[[1]]]}, {j, 1, i}, {k, 1, j}]] // 
      ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[2, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[2]]]}]] // 
      ReduceToHBasis,
     HPLbasis[[3]]], 0];
  HPLw4[1] = DeleteCases[Join[
     Flatten[
       Table[HPLbasis[[1, i]]*HPLbasis[[1, j]]*HPLbasis[[1, k]]*
         HPLbasis[[1, m]], {i, 1, Length[HPLbasis[[1]]]}, {j, 1, 
         i}, {k, 1, j}, {m, 1, k}]] // ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[1, k]]*HPLbasis[[2, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {k, 1, i}, {j, 1, 
         Length[HPLbasis[[2]]]}]] // ReduceToHBasis,
     Flatten[
       Table[HPLbasis[[2, i]]*HPLbasis[[2, j]], {i, 1, 
         Length[HPLbasis[[2]]]}, {j, 1, Length[HPLbasis[[2]]]}]] // 
      ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[3, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[3]]]}]] // 
      ReduceToHBasis,
     HPLbasis[[4]]], 0];
  HPLw5[1] = DeleteCases[Join[
     Flatten[
       Table[HPLbasis[[1, i]]*HPLbasis[[1, j]]*HPLbasis[[1, k]]*
         HPLbasis[[1, m]]*HPLbasis[[1, n]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, i}, {k, 1, j}, {m, 1, k}, {n, 
         1, m}]] // ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[1, k]]*HPLbasis[[1, m]]*
         HPLbasis[[2, j]], {i, 1, Length[HPLbasis[[1]]]}, {k, 1, 
         i}, {m, 1, k}, {j, 1, Length[HPLbasis[[2]]]}]] // 
      ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[2, j]]*HPLbasis[[2, s]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[2]]]}, {s, 1,
          j}]] // ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[1, k]]*HPLbasis[[3, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {k, 1, i}, {j, 1, 
         Length[HPLbasis[[3]]]}]] // ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[2, i]]*HPLbasis[[3, j]], {i, 1, 
         Length[HPLbasis[[2]]]}, {j, 1, Length[HPLbasis[[3]]]}]] // 
      ReduceToHBasis, 
     Flatten[Table[
        HPLbasis[[1, i]]*HPLbasis[[4, j]], {i, 1, 
         Length[HPLbasis[[1]]]}, {j, 1, Length[HPLbasis[[4]]]}]] // 
      ReduceToHBasis,
     HPLbasis[[5]]], 0];
  HPLw1[2] = 
   Flatten[Table[HPLw1[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw2[2] = 
   Flatten[Table[HPLw2[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw3[2] = 
   Flatten[Table[HPLw3[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw4[2] = 
   Flatten[Table[HPLw4[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLw5[2] = 
   Flatten[Table[HPLw5[1]*Ratbasis[[i]], {i, 1, Length[Ratbasis]}]];
  HPLlist = {Ratbasis, HPLw1[2], HPLw2[2], HPLw3[2], HPLw4[2], 
    HPLw5[2]};
  Return[HPLlist];
  ]
  
  
HPLansatz[HPLlist_, weight_] := Module[{output, w, i, j},
   output = 0;
   For[i = 0, i <= weight, i++,
    output = 
      output + 
       Sum[
        a[i, j]*HPLlist[[i + 1, j]], {j, 1, Length[HPLlist[[i + 1]]]}];
    ];
   Return[output];
   ];
  



(* improved Fit function *)

FitAnsatz[ansatz_, ResExp_, x_, mass_, coeff_, ord_, KinExpRule_, 
  TransPara_ : {Pi}, InitGuess_ : {}] := 
 Module[{KnowRelation, relation, KnowCoeff, ResExpList, i, j, k, n, 
   xinv, xinvPow, explist, ansatzlist, ansatzexp, xinvPowAnsatz, 
   ansatzexplist, fitted, ResExpParaList, ResExpTmp, ResExpListMTS, m,
    sol, ResExpParaNestList, dim, powerTuples, TransParaPowerList},
  ResExpListMTS = CoefficientList[ResExp /. H[0,a_] :> Log[a], Log[mass]];
  For[m = 1, m <= Length[ResExpListMTS], m++,
   Print["================================================"];
   Print["Start Ansatz fitting for Log[", mass, "]^", m - 1, " part."];
   ResExpList = CoefficientList[ResExpListMTS[[m]], Log[x]];
   fitted[m] = 0;
   For[i = Length[ResExpList], i >= 1, i--,
    Print["--------------------------------------------"];
    Print["Start Ansatz fitting for Log[", x, "]^", i - 1, " part."];
    ResExpParaNestList = CoefficientList[ResExpList[[i]], TransPara];
    dim = Dimensions[ResExpParaNestList];
    powerTuples = Tuples[Range /@ dim];
    TransParaPowerList = 
     Map[Apply[Times, MapThread[Power, {TransPara, # - 1}]] &, 
      powerTuples];
    Print["Dimensions of TransPara List: ", dim];
    Print["Powers Tuples of TransPara List: ", TransParaPowerList];
    ResExpParaList = Flatten[ResExpParaNestList];
    Print["Length of TransParaPowerList: ", 
     Length[TransParaPowerList]];
    Print["Length of ResExpParaList: ", Length[ResExpParaList]];
    fitted[m, i] = 0;
    For[n = 1, n <= Length[ResExpParaList], n++,
     Print["-----------------"];
     Print[
      "Start Ansatz fitting with transcendental parameter list #", n, 
      " out of ", Length[ResExpParaList]];
     Print["Transcendental parameter: ", TransParaPowerList[[n]]];
     xinvPow = -Max[0, 
        Exponent[ResExpParaList[[n]] /. x -> 1/xinv, xinv]];
     Print["Maximal inverse power of ", x, " = ", xinvPow];
     explist = 
      CoefficientList[
       Series[ResExpParaList[[n]]*x^(-xinvPow) // PowerExpand // 
          Expand, {x, 0, ord - xinvPow}] // Normal, x];
     (*Print["expanded result list =",explist];*)
     
     ansatzexp = 
      Collect[Series[ansatz /. KinExpRule /. InitGuess, {x, 0, ord}] //
         Normal, x, Factor];
     (*Print["expanded ansatz =",ansatzexp];*)
     
     KnowRelation = {};
     xinvPowAnsatz = -Max[0, Exponent[ansatzexp /. x -> 1/xinv, xinv]];
     If[xinvPowAnsatz < xinvPow,
      For[j = xinvPowAnsatz, j < xinvPow, j++,
        KnowCoeff = Coefficient[ansatzexp /. KnowRelation, x, j];
        relation = Solve[KnowCoeff == 0][[1]];
        (*Print["Relation deducted by inverse Power counting: ",
        relation];*)
        
        KnowRelation = Join[KnowRelation, relation];
        ];
      ];
     KnowRelation = Union[Flatten[KnowRelation]];
     Print["Known Relation deducted by inverse Power counting: ", 
      KnowRelation];
     Print[""];
     ansatzexplist = 
      CoefficientList[
       ansatzexp*x^(-xinvPow) /. KnowRelation // PowerExpand // 
        Expand, x];
     (*Print["expanded ansatz list =",ansatzexplist];*)
     
     If[Length[explist] < Length[ansatzexplist],
      explist = 
        Join[explist, 
         ConstantArray[0, Length[ansatzexplist] - Length[explist]]];
      ];
     If[Length[explist] != Length[ansatzexplist],
      Print["Length[explist] = ", Length[explist], 
       " not equal to Length[ansatzexplist] = ", 
       Length[ansatzexplist]];
      Abort[];
      ];
     (*Print["expanded result list =",explist];*)
     
     sol = Solve[ansatzexplist == explist, 
       Cases[ansatz /. KnowRelation /. InitGuess, _coeff, Infinity] //
         Union];
     (*Print["solution =",sol];*)
     If[sol =!= {},
      fitted[m, i, n] = 
        ansatz*Log[x]^(i - 1)*Log[mass]^(m - 1)*
            TransParaPowerList[[n]] /. InitGuess /. KnowRelation /. 
         sol[[1]];
      ,
      Print["Warning: Solution not found !!!"];
      Print["Abort: return system of equations to debug"];
      Return[{ansatzexplist, explist}];
      ];
     
     fitted[m, i] = fitted[m, i] + fitted[m, i, n];]
    ;
    Print["fitted result at Log[", mass, "]^", (m - 1), "* Log[", x, 
     "]^", (i - 1), " = ", fitted[m, i]];
    Print["-----------------"];
    fitted[m] = fitted[m] + fitted[m, i];
    ];
   Print["fitted result at Log[", mass, "]^", (m - 1), " = ", 
    fitted[m]];
   Print["--------------------------------------------"];
   ];
  Return[Sum[fitted[m], {m, 1, Length[ResExpListMTS]}]];
  ]
