# AsyInt 
A toolbox for analytic calculations of massive Feynman integrals in asymptotic limits -- version 1.0.1

Author: Hantian Zhang

## Description
**AsyInt** is a Mathematica toolbox for analytic calculations up to two-loop four-point Feynman integrals with massive internal and external particles in asymptotic limits, particularly in the high-energy limit.
With **AsyInt**, users can perform parametric integrations of Feynman integrals with the asymptotic expansion and the Mellin-Barnes technique, and obtain analytic results for higher-order terms in the expansion parameter and the dimensional regulator.

Technical details of **AsyInt** including mathematical aspects, program structure, commands usage, examples and analytic results are presented in:
- Hantian Zhang, "*Massive two-loop four-point Feynman integrals at high energies with AsyInt*", JHEP 09 (2024) 069. [hep-ph:2407.12107]. (<https://doi.org/10.1007/JHEP09(2024)069>)


## Dependencies
**AsyInt** depends on third party tools: 
- asy2.1.m (<https://www.ttp.kit.edu/~asmirnov/Tools-Regions.htm>)
- MB.m  (<https://mbtools.hepforge.org>)

and also modern analytic summation tools:
- HarmonicSums.m (<https://risc.jku.at/sw/harmonicsums/>)
- Sigma.m (<https://risc.jku.at/sw/sigma/>)
- EvaluateMultiSums.m (<https://www3.risc.jku.at/research/combinat/software/EvaluateMultiSums/>)

If **AsyInt** is used for a publication, please cite Ref. [1] and also the references of any used third-party tools.


## Get Started
**AsyInt** contains two toolkits: toolkit I for generating MB integrals and toolkit II for solving MB integrals.

Users can simply load "AsyInt.m" file to initialise the toolkit I, and load "AsyIntSolve.m" file to initialise the toolkit II.

## Working examples
The working example for the one-loop massive box integral is in the path: './Example/Box_1Loop/'. 

The working examples for the three-massive-line two-loop four-point planar and non-planar integrals are in the path: './Examples/PL1_2Loop/' and './Examples/NPL1_2Loop/'.

Note that these examples serve as demonstrations of **AsyInt** usage and do not contain the full calculation of higher-order terms presented in Ref. [1].
These examples are tested with Mathematica 12.3.

There is no working example for the fully-massive two-loop four-point integrals in the current release. The analytic results for the planar one can be found in Ref. [3], and the results for the non-planar one can be found in Refs. [1, 2].

An independent calculation of massless two-loop four-point planar and non-planar diagrams to weight-6 are stored in the path './MasslessMI/'.
These master integrals are needed for the hard-region calculation. The results for the massless MIs up to weight-4 can be found in Refs. [4,5].


## Reference
[1] H. Zhang, "*Massive two-loop four-point Feynman integrals at high energies with AsyInt*", JHEP 09 (2024) 069. [hep-ph:2407.12107]. (<https://doi.org/10.1007/JHEP09(2024)069>)

[2] J. Davies, K. Schönwald, M. Steinhauser and H. Zhang, "*Analytic next-to-leading order Yukawa and Higgs boson self-coupling corrections to gg → HH at high energies*". [hep-ph:2501.17920] (<https://arxiv.org/abs/2501.17920>)

[3] J. Davies, G. Mishima, K. Schönwald, M. Steinhauser and H. Zhang, "*Higgs boson contribution to the leading two-loop Yukawa corrections to gg → HH*", JHEP 08 (2022) 259. [hep-ph:2207.02587] (<https://doi.org/10.1007/JHEP08(2022)259>)

[4] V.A. Smirnov, "*Analytical result for dimensionally regularized massless on shell double box*", Phys.Lett.B 460 (1999) 397-404.

[5] J.B. Tausk, "*Nonplanar massless two loop Feynman diagrams with four on-shell legs*", Phys.Lett.B 469 (1999) 225-234.


## Acknowledgment
The author thanks Kay Schönwald for close collaboration on Feynman integral calculations that are valuable to the development of AsyInt. The author also thanks Joshua Davies, Go Mishima and Matthias Steinhauser for various helpful discussions during our collaborations.


## Version and Change Logs

October 08, 2024: update to version 1.0.1. Changes: resolve potential issues in *AsyExpNum2MB* command for integrals with numerators (not in the last indices).

July 15, 2024: initial release of AsyInt v.1.0.

## License
Copyright (C) 2024 Hantian Zhang

AsyInt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AsyInt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AsyInt.  If not, see <http://www.gnu.org/licenses/>.
  