(*---------------------------------------
 |                                      |
 |    Toolkit I: generate integrals     |
 |                                      |
 |    AsyInt v.1.0.1                    |
 |                                      |
 |    by Hantian Zhang, Oct. 08, 2024   |
 |                                      | 
 ----------------------------------------*)

initAsyInt[relativeFiles_] := Module[{baseDir, fullPaths},
  Print["AsyInt 1.0.1"]
  Print["by Hantian Zhang [hep-ph:2407.12107]"];
  Print["-----------------------------"];
  Print["Loading AsyInt toolkit I: generate integrals"];
  baseDir = DirectoryName[$InputFileName];
  fullPaths = FileNameJoin[{baseDir, #}] & /@ relativeFiles;
  Do[Get[file], {file, fullPaths}];
  Print["For AsyInt toolkit II: solve integrals, please load AsyIntSolve.m"]
  Print["-----------------------------"];
  Print["Note: please load asy.m and MB.m before AsyInt"];
  ]

FilesToLoad = {
   "/SourceCode/MBfunc.m", 
   "SourceCode/MBHighRegionFunc.m", 
   "SourceCode/MBNumRegionFunc.m", 
   "SourceCode/ContinueFunc.m"
};

initAsyInt[FilesToLoad];


(* main commands usage *)

GenerateInput::usage = "GenerateInput[loops, props, kinematics, SmallInv, ScalePara, xlist, EuclInv:{SS,TT,UU}, EuclInvSum:0, UserDefineRelation:{}] => {UFpoly, Region, Scaling}
    The inputs of this commands are defintion of Feynman integral without numerators and with expansion parameter 'SmallInv', and the output is a list of UF polynomials, all regions and corresponding scalings.
    For more details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

GenerateInputNum::usage = "GenerateInputNum[loops, props, numidx, kinematics, SmallInv, ScalePara, xlistNum, EuclInv:{SS,TT,UU}, EuclInvSum:0, UserDefineRelation:{}] => {UFpoly, Region, Scaling, UFpolyNum} \n
    The inputs of this commands are defintion of Feynman integral with numerators and expansion parameter 'SmallInv', and the output is a list of UF polynomials of the diagram, all regions and corresponding scalings of the diagram, and extended UF polynomials for integrals with numerators.
    Note that 'props' refers to propagators with numerators in this command.
    For more details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AlphaRepForTempInt::usage = "AlphaRepForTempInt[UFpoly, Scaling, dlist, xlist, ScalePara] => {AlphaRepRegion}
    This command generates alpha representations of template integrals in all regions. 'xlist' is the list of alpha parameters, and 'dlist' a list of delta regulators.
    For more details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AsyExp2MB::usage = "AsyExp2MB[UFpoly, Scaling, dlist, xlist, ExpLowOrd, ExpMaxOrd, SmallInv, ScalePara, TempIntList, HardIntList, Zrule, epOrd, DotShift: {}, AddExpOrd: 0] => {MBexp}
    This command performs higher-order asymptotic expansions through the templeate-integral approach, perform analytic continuations and output MB integrals.
    This command is for Feynman integrals without numerators. For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AsyExpNum2MB::usage = "AsyExpNum2MB[UFpolyNum, numidx, UFpoly, Scaling, dlistNum, xlistNum, ExpLowOrd, ExpMaxOrd, SmallInv, ScalePara, TempIntList, HardIntList, Zrule, epOrd, AddExpOrd:0] => {MBexp}
    This command performs higher-order asymptotic expansions through the templeate-integral approach, perform analytic continuations and output MB integrals.
    This command is for Feynman integrals with numerators. For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";