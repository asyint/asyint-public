{H[-3, 1] -> 2*Log[2] - Log[3], H[{3, 0}, 1] -> Pi/(3*Sqrt[3]), 
 H[{3, 1}, 1] -> -1/6*Pi/Sqrt[3] + Log[3]/2, 
 H[0, {3, 1}, 1] -> ((7*Pi^2)/3 - 2*PolyGamma[1, 1/3])/18, 
 H[{3, 0}, -1, 1] -> -1/27*Pi^2 + PolyGamma[1, 1/3]/18, 
 H[{3, 0}, 1, 1] -> (-4*Pi^2)/27 - (Pi*Log[3])/(6*Sqrt[3]) + 
   (2*PolyGamma[1, 1/3])/9, H[{3, 0}, {3, 0}, 1] -> Pi^2/54, 
 H[{3, 1}, 0, 1] -> (-7*Pi^2)/54 + PolyGamma[1, 1/3]/9, 
 H[{3, 1}, {3, 0}, 1] -> (-5*Pi^2)/108 + PolyGamma[1, 1/3]/18, 
 H[-1, {3, 0}, 1, 1] -> (19*Pi^3)/(360*Sqrt[3]) - 
   (2*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) - 
   (2*Im[PolyLog[3, 1/4 + (I/4)*Sqrt[3]]])/Sqrt[3] + (Pi^2*Log[2])/27 + 
   (Pi*Log[2]^2)/(6*Sqrt[3]) - (Pi*Log[2]*Log[3])/(6*Sqrt[3]) + 
   (Pi*Log[3]^2)/(120*Sqrt[3]) - (Log[32]*PolyGamma[1, 1/3])/90, 
 H[0, {3, 1}, {3, 1}, 1] -> (-29*Pi^3)/(1620*Sqrt[3]) - 
   (4*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) + (Pi^2*Log[3])/27 + 
   (Pi*Log[3]^2)/(60*Sqrt[3]) + (Pi*PolyGamma[1, 1/3])/(18*Sqrt[3]) - 
   (Log[3]*PolyGamma[1, 1/3])/18 - (11*Zeta[3])/54, 
 H[{3, 0}, 1, 1, 1] -> (-29*Pi^3)/(3240*Sqrt[3]) + 
   (8*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) + (Pi*Log[3]^2)/(120*Sqrt[3]), 
 H[{3, 0}, {3, 0}, -1, 1] -> (-4*Pi^3)/(81*Sqrt[3]) + 
   (2*Pi*PolyGamma[1, 1/3])/(27*Sqrt[3]) - (13*Zeta[3])/36, 
 H[{3, 0}, {3, 0}, 1, 1] -> -1/81*Pi^3/Sqrt[3] - (Pi^2*Log[3])/108 + 
   (Pi*PolyGamma[1, 1/3])/(54*Sqrt[3]) + Zeta[3]/27, 
 H[{3, 1}, {3, 0}, -1, 1] -> (-11*Pi^3)/(135*Sqrt[3]) + 
   (8*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) + 
   (2*Im[PolyLog[3, 1/4 + (I/4)*Sqrt[3]]])/Sqrt[3] - 
   (Pi*Log[2]^2)/(6*Sqrt[3]) - (Pi*Log[3]^2)/(30*Sqrt[3]) - 
   (Pi^2*Log[288])/27 - (Pi*PolyGamma[1, 1/3])/(27*Sqrt[3]) + 
   (5*Log[2]*PolyGamma[1, 1/3])/18 + (Log[3]*PolyGamma[1, 1/3])/9 + 
   (13*Zeta[3])/72, H[{3, 1}, {3, 0}, 1, 1] -> 
  -1/120*Pi^3/Sqrt[3] + (4*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) - 
   (Pi^2*Log[3])/72 - (Pi*Log[3]^2)/(60*Sqrt[3]) - 
   (Pi*PolyGamma[1, 1/3])/(108*Sqrt[3]) + (Log[3]*PolyGamma[1, 1/3])/36 - 
   Zeta[3]/54, H[{3, 1}, {3, 1}, 0, 1] -> Pi^3/(270*Sqrt[3]) - 
   (4*Im[PolyLog[3, I/Sqrt[3]]])/(5*Sqrt[3]) - (Pi^2*Log[3])/36 + 
   (Pi*Log[3]^2)/(60*Sqrt[3]) + (Pi*PolyGamma[1, 1/3])/(27*Sqrt[3]) - 
   (11*Zeta[3])/54, H[0, {3, 1}, 0, 1] -> (4*(Sqrt[3]*Pi^3 - 54*Zeta[3]))/
   243, H[{3, 0}, 1, {3, 0}, 1] -> 
  (Sqrt[3]*Pi*(-2*Pi^2 + 3*PolyGamma[1, 1/3]) - 18*Zeta[3])/243, 
 H[{3, 0}, 1, {3, 1}, 1] -> 
  (Sqrt[3]*Pi*(53*Pi^2 - 9*Log[3]^2 - 20*PolyGamma[1, 1/3]) + 
    24*(-72*Sqrt[3]*Im[PolyLog[3, I/Sqrt[3]]] + 5*Zeta[3]))/3240, 
 H[{3, 0}, {3, 1}, 1, 1] -> (-52*Sqrt[3]*Pi^3 - 495*Pi^2*Log[3] + 
    810*Log[3]*PolyGamma[1, 1/3] - 3*Sqrt[3]*Pi*
     (63*Log[3]^2 + 10*PolyGamma[1, 1/3]) + 
    36*(72*Sqrt[3]*Im[PolyLog[3, I/Sqrt[3]]] - 5*Zeta[3]))/9720, 
 H[{3, 1}, 0, 0, 1] -> (-2*Pi^3)/(81*Sqrt[3]) + (4*Zeta[3])/9, 
 H[0, 0, {3, 1}, 0, 1] -> (-13*Pi^4)/810 - (8*GI[0, 0, 0, r2])/(3*Sqrt[3]), 
 H[0, {3, 1}, 0, 0, 1] -> (13*Pi^4)/810 + (8*GI[0, 0, 0, r2])/(3*Sqrt[3]), 
 H[0, {3, 1}, {3, 1}, 0, 1] -> -1/5832*(7*Pi^2 - 6*PolyGamma[1, 1/3])^2, 
 H[{3, 0}, {3, 0}, 1, 1, 1] -> (-91*Pi^4 - 2160*GR[0, 0, r4, 1] + 
    1152*Pi*Im[PolyLog[3, I/Sqrt[3]]] - 9*Pi^2*Log[3]^2 + 
    20*Pi*(8*Pi + Sqrt[3]*Log[27])*PolyGamma[1, 1/3] - 
    120*PolyGamma[1, 1/3]^2 - 40*Log[3]*(Sqrt[3]*Pi^3 - 36*Zeta[3]))/6480, 
 H[{3, 0}, {3, 0}, 1, {3, 0}, 1] -> 
  (-1080*Sqrt[3]*GI[0, 0, 0, r2] + Pi*(2*Pi^3 - 3*Pi*PolyGamma[1, 1/3] - 
      240*Sqrt[3]*Zeta[3]))/2916, H[{3, 0}, {3, 0}, 1, {3, 1}, 1] -> 
  (1123*Pi^4 + 720*Sqrt[3]*Pi^3*Log[3] + 
    6*Pi^2*(27*Log[3]^2 - 110*PolyGamma[1, 1/3]) + 
    540*(40*Sqrt[3]*GI[0, 0, 0, r2] + 72*GR[0, 0, r4, 1] + 
      PolyGamma[1, 1/3]^2 - 48*Log[3]*Zeta[3]) - 
    24*Pi*(45*Sqrt[3]*Log[3]*PolyGamma[1, 1/3] + 
      (8*I)*(54*PolyLog[3, (-I)/Sqrt[3]] - 54*PolyLog[3, I/Sqrt[3]] + 
        (25*I)*Sqrt[3]*Zeta[3])))/116640, H[{3, 0}, {3, 0}, {3, 0}, 1, 1] -> 
  (360*Sqrt[3]*GI[0, 0, 0, r2] + Pi*(-2*Pi^3 - Sqrt[3]*Pi^2*Log[3] + 
      3*Pi*PolyGamma[1, 1/3] + 84*Sqrt[3]*Zeta[3]))/2916, 
 H[{3, 0}, {3, 0}, {3, 1}, 1, 1] -> 
  (641*Pi^4 + 20*Sqrt[3]*Pi^3*Log[3] - 
    6*Pi^2*(81*Log[3]^2 + 370*PolyGamma[1, 1/3]) + 
    (48*I)*Pi*(108*PolyLog[3, (-I)/Sqrt[3]] - 108*PolyLog[3, I/Sqrt[3]] + 
      (35*I)*Sqrt[3]*Zeta[3]) + 180*(-40*Sqrt[3]*GI[0, 0, 0, r2] + 
      9*(24*GR[0, 0, r4, 1] + PolyGamma[1, 1/3]^2 - 8*Log[3]*Zeta[3])))/
   116640, H[{3, 0}, {3, 1}, {3, 0}, 1, 1] -> 
  (-2507*Pi^4 - 1660*Sqrt[3]*Pi^3*Log[3] + 
    108*Pi^2*(-2*Log[3]^2 + 55*PolyGamma[1, 1/3]) + 
    24*Pi*(432*Im[PolyLog[3, I/Sqrt[3]]] + 35*Sqrt[3]*
       (Log[27]*PolyGamma[1, 1/3] - 2*Zeta[3])) - 
    180*(40*Sqrt[3]*GI[0, 0, 0, r2] + 432*GR[0, 0, r4, 1] + 
      25*PolyGamma[1, 1/3]^2 - 144*Log[3]*Zeta[3]))/116640, 
 H[{3, 1}, 0, 0, 0, 1] -> (-13*Pi^4)/2430 - (8*GI[0, 0, 0, r2])/(9*Sqrt[3]), 
 H[{3, 1}, 0, {3, 1}, 0, 1] -> (221*Pi^4 + 240*Sqrt[3]*Pi^3*Log[3] - 
    1380*Pi^2*PolyGamma[1, 1/3] - 3240*Sqrt[3]*Pi*Zeta[3] + 
    180*(-96*Sqrt[3]*GI[0, 0, 0, r2] - 108*GR[0, 0, r4, 1] + 
      5*PolyGamma[1, 1/3]^2 + 6*Log[3]*Zeta[3]))/29160, 
 H[{3, 1}, {3, 0}, {3, 0}, 1, 1] -> 
  (823*Pi^4 - 7200*Sqrt[3]*GI[0, 0, 0, r2] - 
    60*Pi*(53*Pi + 18*Sqrt[3]*Log[3])*PolyGamma[1, 1/3] + 
    2340*PolyGamma[1, 1/3]^2 - 1680*Sqrt[3]*Pi*Zeta[3] + 
    20*Log[3]*(37*Sqrt[3]*Pi^3 + 756*Zeta[3]))/116640, 
 H[{3, 1}, {3, 1}, 0, 0, 1] -> (Pi^4 + 720*Sqrt[3]*GI[0, 0, 0, r2] + 
    810*GR[0, 0, r4, 1] + 40*Pi^2*PolyGamma[1, 1/3] - 
    30*PolyGamma[1, 1/3]^2 + 135*Sqrt[3]*Pi*Zeta[3] - 
    5*Log[3]*(2*Sqrt[3]*Pi^3 + 9*Zeta[3]))/2430, 
 H[0, 0, 1, I/Sqrt[3]] :> PolyLog[3, I/Sqrt[3]], H[0, 3] :> Log[3], 
 H[0, 0, 0, {3, 1}, 1] -> (13*Pi^4)/2430 + (8*GI[0, 0, 0, r2])/(9*Sqrt[3]), 
 H[0, 0, {3, 1}, {3, 1}, 1] -> (7*Pi^4 - 120*Pi^2*PolyGamma[1, 1/3] - 
    585*Sqrt[3]*Pi*Zeta[3] - 45*(48*Sqrt[3]*GI[0, 0, 0, r2] + 
      54*GR[0, 0, r4, 1] - 2*PolyGamma[1, 1/3]^2 - 39*Log[3]*Zeta[3]))/7290, 
 H[0, 0, 1, (-I)/Sqrt[3]] -> PolyLog[3, (-I)/Sqrt[3]], 
 H[{3, 0}, {3, 1}, 1] -> (Pi^2 + Sqrt[3]*Pi*Log[9] - 2*PolyGamma[1, 1/3])/36, 
 H[{3, 0}, {3, 0}, {3, 0}, 1] -> Pi^3/(486*Sqrt[3]), 
 H[{3, 0}, {3, 0}, {3, 1}, 1] -> (-25*Sqrt[3]*Pi^3 + 27*Pi^2*Log[3] + 
    36*Sqrt[3]*Pi*PolyGamma[1, 1/3] - 756*Zeta[3])/2916}
