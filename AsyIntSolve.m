(*---------------------------------------
 |                                      |
 |    Toolkit II: solve integrals       |
 |                                      |
 |    AsyInt v.1.0.1                    |
 |                                      |
 |    by Hantian Zhang, Oct. 08, 2024   |
 |                                      | 
 ----------------------------------------*)

initAsyInt[relativeFiles_] := Module[{baseDir, fullPaths},
  Print["AsyInt 1.0.1"]
  Print["by Hantian Zhang [hep-ph:2407.12107]"];
  Print["-----------------------------"];
  Print["Loading AsyInt toolkit II: solve integrals"];
  baseDir = DirectoryName[$InputFileName];
  fullPaths = FileNameJoin[{baseDir, #}] & /@ relativeFiles;
  Do[Get[file], {file, fullPaths}];
  Print["-----------------------------"];
  Print["Note: to access full functionalities, please load MB.m, HarmonicSums.m, Sigma.m and EvaluateMultiSums.m before AsyInt"];
  ]

FilesToLoad = {
   "SourceCode/AutoSumFunc.m", 
   "SourceCode/AutoPSLQFunc.m", 
   "SourceCode/AutoSumFunc_Expand.m",
   "SourceCode/UtilityFunc.m"};

initAsyInt[FilesToLoad];


(* main commands usage *)

AISum1DMB::usage = "AISum1DMB[MBexp, Z1, k1, Zcontour, SmallInv, MBscale:'none', MBscale2:'none', LRpreset:'R', AddShift:1, BSpresent:0] => {Result}
    This command performs analytic summations for one-dimensional MB integrals.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AISum2DMB::usage = "AISum2DMB[MBexp, Z1, Z2, k1, k2, Zcontour, SmallInv, MBscale:'none', MBscale2:'none', LRpreset:{'R','R'}, AddShift:1, split:0] => {Result}
    This command performs analytic summations for two-dimensional MB integrals up to one scale.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AINumRec1DMB::usage = "AINumRec1DMB[MBexp, KinList, TransParaList, ConstList, Zcontour, PrecList, MaxPower:200, KnownRes:0] => {Result}
    This command performs numeratical reconstructions for scaleless one-dimensional MB integrals with PSLQ algorithm.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AIEucl2Phys::usage = "AIEucl2Phys[exp, SmallInv, TT, UU, SS, s, mus, IntDef, LoopOrd, epOrd, NoEpExp:False, EuclKinRelation:{UU->-TT-SS}] => {PhysResult}
    This command performs the analytic continuation from the Euclidean region to the physical region.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AC2Phys::usage = "AC2Phys[exp, SmallInv, TT, UU, SS, s, EuclKinRelation:{UU->-TT-SS}] => {PhysResult}
    This command is a lower-level command version of AIEucl2Phys.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AIExpandI1::usage = "AIExpandI1[MBexp, Z1, k1, Z2, k2, Zcontour, Z1ExpOrd, KinList, TransParaList, ConstList, PrecList, LRpreset:{'R','R'}, AddShift:2] => {ResExp}
    This command first performs expansion for two-dimensional MB integrals with the scale x^Z1 in the x -> 0 limit, and then apply numerical reconstructions to Z2-integrations for the nested MB integrals that can contain non-vanishing arc contributions on the semi-circles.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AIExpandI2::usage = "AIExpandI2[MBexp, Z1, k1, Z2, k2, Zcontour, Z1ExpOrd, MBscale, MBscale2, SmallInv, LRpreset:{'R','R'}, AddShift:2] => {ResExp}
    This command first performs expansion for two-dimensional MB integrals with the scales x^Z1 * y^Z2 in the x -> 0 limit, and then apply analytic summations to Z2-integrations for the nested MB integrals involving y^Z2.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

AIExpand2DMB::usage = "AIExpand2DMB[MBexp, Z1, k1, Z2, k2, Zcontour, Z1ExpOrd, LRpreset:{'R','R'}, AddShift:2, IgnoreBDconst:0, ToSum:0] => {MBexp, ExtraResidue}
    This command performs expansion for two-dimensional MB integrals with the scales x^Z1 * y^Z2 in the x -> 0 limit.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

FitAnsatz::usage = "FitAnsatz[Ansatz, ResExp, MBscale, SmallInv, CoeffSymbol, ExpOrd, AnsatzExpRule, TransParaList:{Pi}, InitGuess:{}] => {Result}
    This command performs the anastz fitting procedure.
    For details of inputs, please refer to the Appendix of [hep-ph:2407.12107].";

