(*--------------------------------------------
 |                                           |
 |   Massless two-loop four-point integrals  |
 |                                           |
 |   AsyInt v.1.0                            |
 |                                           |
 |   by Hantian Zhang, July 15, 2024         |
 |                                           | 
 --------------------------------------------*)

Here are master integrals for massless two-loop four-point integrals computed to weight-6.

The planar master integrals are provided in the Euclidean region.

The non-planar master integrals are provided in the physical region.

Note that in these MIs, the overall prefactors (mus/SS)^(2ep) in Euclidean region and (mus/s)^(2ep) in physical region are not attached