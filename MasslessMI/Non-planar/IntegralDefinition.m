(* LiteRed definition in physical region, family name: NPL *)

sp[q1] = sp[q2] = sp[q3] = 0;
sp[q1, q2] = s/2;
sp[q1, q3] = t/2;
sp[q2, q3] = u/2 /. {u -> -s - t};

loops = {l1, l2};
propagators = {-sp[l2, l2], -sp[l1, l1], -sp[-l2 + q1 + q2 + q3, -l2 + q1 + q2 + q3], -sp[l1 - q3, l1 - q3], -sp[l1 - l2, l1 - l2], -sp[l1 - l2 + q1 + q2, l1 - l2 + q1 + q2], -sp[l1 - l2 + q2, l1 - l2 + q2], -sp[l1 + q2, l1 + q2], -sp[l1 + q1, l1 + q1]}