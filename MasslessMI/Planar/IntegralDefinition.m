(* LiteRed definition in Euclidean region, family name: PL *)

sp[q1] = sp[q2] = sp[q3] = 0;
sp[q1, q2] = -SS/2;
sp[q1, q3] = -TT/2;
sp[q2, q3] = -UU/2 /. {UU -> -SS - TT};

loops = {l1, l2};
propagators = {-sp[l1 + q1, l1 + q1], -sp[l1 + q1 + q2, l1 + q1 + q2], -sp[l2 + q1 + q2, l2 + q1 + q2], -sp[l2 - q3, l2 - q3], -sp[l2, l2], -sp[l1, l1], -sp[-l1 + l2, -l1 + l2], -sp[l1 + q3, l1 + q3], -sp[l2 + q2, l2 + q2]};