emus^2*(-1/3*(im*Pi^3*(3*s + 10*TT))/(s^2*(s - TT)*TT) + 
  (80*im*Pi)/(s^2*TT - s*TT^2) + (Pi^4*(49*s + 134*TT))/
   (60*s^3*TT - 60*s^2*TT^2) + (47*H[0, mts/s]^4)/(12*s^2*TT - 12*s*TT^2) + 
  ((-7*im*mts*Pi^4)/(6*s*Sqrt[mts*s*(s - TT)*TT]) - 
    (32*im*mts*Pi^2*H[0, 2]^2)/(s*Sqrt[mts*s*(s - TT)*TT]) - 
    (16*im*mts*Pi^2*H[0, 2]*H[0, mts/s])/Sqrt[mts*s^3*(s - TT)*TT] - 
    (2*im*mts*Pi^2*H[0, mts/s]^2)/(s*Sqrt[mts*s*(s - TT)*TT]))/mts + 
  ((s + 8*TT)*H[0, TT/s]^4)/(12*s^3*TT - 12*s^2*TT^2) + 
  ((10*im*Pi)/(s^2*(s - TT)) + (5*Pi^2*(s - 2*TT))/(2*s^2*(s - TT)*TT))*
   H[1, TT/s]^2 + (10/(3*s^2*TT) - (4*im*Pi*(s + TT))/(3*s^2*(s - TT)*TT))*
   H[1, TT/s]^3 + ((9*s - 8*TT)*H[1, TT/s]^4)/(12*s^3*TT - 12*s^2*TT^2) + 
  H[0, TT/s]^3*(-10/(3*s^2*(s - TT)) + (Pi*(8*im*s - 4*im*TT))/
     (3*s^3*TT - 3*s^2*TT^2) + ((-6*s + 8*TT)*H[1, TT/s])/
     (3*s^2*(s - TT)*TT)) + H[0, mts/s]^3*((5*im*Pi)/(s^2*TT - s*TT^2) + 
    ((-7*s + 4*TT)*H[0, TT/s])/(3*s^2*(s - TT)*TT) + 
    ((3*s + 4*TT)*H[1, TT/s])/(3*s^3*TT - 3*s^2*TT^2)) + 
  H[0, mts/s]^2*((5*Pi^2)/(12*s^2*TT - 12*s*TT^2) - 
    (2*im*Pi)/(s^2*TT - s*TT^2) - (2*H[0, TT/s])/(s^2*(s - TT)) + 
    H[0, TT/s]^2/(2*s^2*TT - 2*s*TT^2) + (2*H[1, TT/s])/(s^2*TT) + 
    H[1, TT/s]^2/(2*s^2*TT - 2*s*TT^2)) + 
  (-((im*Pi^2)/(s*Sqrt[mts*s*(s - TT)*TT])) - 
    (5*Pi^2)/(3*s^2*TT - 3*s*TT^2) + (2*im*Pi)/(s^2*TT - s*TT^2) + 
    (3*H[0, mts/s]^2)/(2*s^2*TT - 2*s*TT^2) + 
    (2/(s^2*(s - TT)) - (im*Pi*(s - 2*TT))/(s^2*(s - TT)*TT))*H[0, TT/s] + 
    (-2/(s^2*TT) - (im*Pi*(s - 2*TT))/(s^2*(s - TT)*TT))*H[1, TT/s] + 
    H[0, mts/s]*((3*im*Pi)/(s^2*TT - s*TT^2) + ((-s + 2*TT)*H[0, TT/s])/
       (s^2*(s - TT)*TT) + ((-s + 2*TT)*H[1, TT/s])/(s^2*(s - TT)*TT)))/
   ep^2 + ((-16*im*Pi)/(s^2*TT - s*TT^2) - (3*im*Pi^3)/(s^2*TT - s*TT^2) - 
    (11*H[0, mts/s]^3)/(3*s^2*TT - 3*s*TT^2) + 
    ((8*im*mts*Pi^2*H[0, 2])/Sqrt[mts*s^3*(s - TT)*TT] + 
      (2*im*mts*Pi^2*H[0, mts/s])/(s*Sqrt[mts*s*(s - TT)*TT]))/mts + 
    ((s - 2*TT)*H[0, TT/s]^3)/(3*s^3*TT - 3*s^2*TT^2) + 
    ((8*im*Pi)/(s^2*(s - TT)) + 16/(s^2*TT) + (2*Pi^2*(s - 3*TT))/
       (s^2*(s - TT)*TT))*H[1, TT/s] - (im*Pi*H[1, TT/s]^2)/
     (s^2*TT - s*TT^2) + ((s - 2*TT)*H[1, TT/s]^3)/(3*s^3*TT - 3*s^2*TT^2) + 
    H[0, TT/s]^2*(-((im*Pi)/(s^2*TT - s*TT^2)) + ((s - 2*TT)*H[1, TT/s])/
       (s^2*(s - TT)*TT)) + H[0, mts/s]^2*((-4*im*Pi)/(s^2*TT - s*TT^2) + 
      ((3*s - 2*TT)*H[0, TT/s])/(s^2*(s - TT)*TT) - ((s + 2*TT)*H[1, TT/s])/
       (s^2*(s - TT)*TT)) + H[0, TT/s]*(-16/(s^2*(s - TT)) - 
      (8*im*Pi)/(s^2*TT) + (Pi^2*(4*s - 6*TT))/(s^2*(s - TT)*TT) + 
      (8/(s^2*TT - s*TT^2) + (2*im*Pi)/(s^2*TT - s*TT^2))*H[1, TT/s] + 
      ((s - 2*TT)*H[1, TT/s]^2)/(s^2*(s - TT)*TT)) + 
    H[0, mts/s]*(-(Pi^2/(3*s^2*TT - 3*s*TT^2)) + 
      (4*im*Pi)/(s^2*TT - s*TT^2) - H[0, TT/s]^2/(s^2*TT - s*TT^2) - 
      (4*H[1, TT/s])/(s^2*TT) - H[1, TT/s]^2/(s^2*TT - s*TT^2) + 
      H[0, TT/s]*(4/(s^2*(s - TT)) + (2*H[1, TT/s])/(s^2*TT - s*TT^2))))/ep + 
  ((-20*im*Pi*(s - 2*TT))/(s^2*(s - TT)*TT) + (10*Pi^2*(s - 2*TT))/
     (s^2*(s - TT)*TT))*H[0, 1, TT/s] + 
  H[0, TT/s]^2*((10*im*Pi)/(s^2*TT) - (5*Pi^2*(s - 2*TT))/
     (2*s^2*(s - TT)*TT) + (10/(s^2*TT) + (im*Pi*(s - 4*TT))/
       (s^2*(s - TT)*TT))*H[1, TT/s] - ((s + 2*TT)*H[1, TT/s]^2)/
     (2*s^3*TT - 2*s^2*TT^2) + (2*(6*s - 5*TT)*H[0, 1, TT/s])/
     (s^2*(s - TT)*TT)) + ((20*(2*s - TT))/(s^2*(s - TT)*TT) - 
    (2*im*Pi*(11*s + 6*TT))/(s^2*(s - TT)*TT))*H[0, 0, 1, TT/s] + 
  ((2*im*Pi*(17*s - 6*TT))/(s^2*(s - TT)*TT) - 
    (20*(s + TT))/(s^2*(s - TT)*TT))*H[0, 1, 1, TT/s] + 
  (12*(6*s - 5*TT)*H[0, 0, 0, 1, TT/s])/(s^2*(s - TT)*TT) - 
  (36*(s - 2*TT)*H[0, 0, 1, 1, TT/s])/(s^2*(s - TT)*TT) - 
  (12*(s + 5*TT)*H[0, 1, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
  (20/(s^2*(s - TT)) + (im*Pi*(-70*s + 12*TT))/(s^2*(s - TT)*TT))*Zeta[3] + 
  H[1, TT/s]*((-40*im*Pi)/(s^2*(s - TT)) - 80/(s^2*TT) + (11*Pi^2)/(s^2*TT) + 
    (Pi^3*(7*im*s - 38*im*TT))/(6*s^3*TT - 6*s^2*TT^2) - 
    (14*im*Pi*H[0, 1, TT/s])/(s^2*TT - s*TT^2) + 
    (10*(s - 2*TT)*H[0, 0, 1, TT/s])/(s^2*(s - TT)*TT) + 
    (4*(s + 5*TT)*H[0, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
    ((26*s - 36*TT)*Zeta[3])/(s^2*(s - TT)*TT)) + 
  H[0, TT/s]*(80/(s^2*(s - TT)) - (11*Pi^2)/(s^2*(s - TT)) + 
    (40*im*Pi)/(s^2*TT) + (Pi^3*(45*im*s - 38*im*TT))/
     (6*s^3*TT - 6*s^2*TT^2) + ((2*im*Pi)/(s^2*(s - TT)) + 
      10/(s^2*TT - s*TT^2))*H[1, TT/s]^2 + (2*(s - 4*TT)*H[1, TT/s]^3)/
     (3*s^2*(s - TT)*TT) + ((20*(-2*s + TT))/(s^2*(s - TT)*TT) + 
      (4*im*Pi*(2*s + 3*TT))/(s^2*(s - TT)*TT))*H[0, 1, TT/s] + 
    H[1, TT/s]*((20*im*Pi)/(s^2*TT) - (10*Pi^2)/(s^2*TT) - 
      40/(s^2*TT - s*TT^2) - (10*(s - 2*TT)*H[0, 1, TT/s])/
       (s^2*(s - TT)*TT)) - (8*(6*s - 5*TT)*H[0, 0, 1, TT/s])/
     (s^2*(s - TT)*TT) + (26*(s - 2*TT)*H[0, 1, 1, TT/s])/(s^2*(s - TT)*TT) - 
    (4*(s + 9*TT)*Zeta[3])/(s^2*(s - TT)*TT)) + 
  H[0, mts/s]*((-11*im*Pi^3)/(6*s^2*TT - 6*s*TT^2) - 
    (8*im*Pi)/(s^2*TT - s*TT^2) - H[0, TT/s]^3/(3*s^2*TT - 3*s*TT^2) + 
    ((4*im*Pi)/(s^2*(s - TT)) + 8/(s^2*TT) + (Pi^2*(3*s + 2*TT))/
       (2*s^3*TT - 2*s^2*TT^2))*H[1, TT/s] - (im*Pi*H[1, TT/s]^2)/
     (s^2*TT - s*TT^2) + H[1, TT/s]^3/(3*s^2*TT - 3*s*TT^2) + 
    H[0, TT/s]^2*(-((im*Pi)/(s^2*TT - s*TT^2)) - 
      H[1, TT/s]/(s^2*TT - s*TT^2)) + H[0, TT/s]*(-8/(s^2*(s - TT)) - 
      (4*im*Pi)/(s^2*TT) + (Pi^2*(-5*s + 2*TT))/(2*s^2*(s - TT)*TT) + 
      (4*H[1, TT/s])/(s^2*TT - s*TT^2) + H[1, TT/s]^2/(s^2*TT - s*TT^2)) + 
    (8*Zeta[3])/(s^2*TT - s*TT^2)) + 
  ep*((33*Pi^4)/(10*s^2*(s - TT)) + (4*im*Pi^3*(3*s + 11*TT))/
     (3*s^2*(s - TT)*TT) + (im*Pi^5*(-265*s + 273*TT))/(90*s^2*(s - TT)*TT) - 
    (352*im*Pi)/(s^2*TT - s*TT^2) - (35*H[0, mts/s]^5)/
     (12*s^2*TT - 12*s*TT^2) - ((7*s + 12*TT)*H[0, TT/s]^5)/
     (30*s^2*(s - TT)*TT) + ((22*im*Pi)/(3*s^2*(s - TT)) - 44/(3*s^2*TT) + 
      (Pi^2*(11*s - 18*TT))/(6*s^3*TT - 6*s^2*TT^2))*H[1, TT/s]^3 + 
    (11/(3*s^2*TT) - (im*Pi*(13*s + 16*TT))/(12*s^2*(s - TT)*TT))*
     H[1, TT/s]^4 + ((19*s - 12*TT)*H[1, TT/s]^5)/(30*s^3*TT - 30*s^2*TT^2) + 
    H[0, TT/s]^4*(11/(3*s^2*(s - TT)) + (im*Pi*(-29*s + 16*TT))/
       (12*s^2*(s - TT)*TT) + ((25*s - 24*TT)*H[1, TT/s])/
       (12*s^3*TT - 12*s^2*TT^2)) + H[0, mts/s]^4*
     ((-61*im*Pi)/(12*s^2*TT - 12*s*TT^2) + ((7*s - 4*TT)*H[0, TT/s])/
       (6*s^3*TT - 6*s^2*TT^2) - ((3*s + 4*TT)*H[1, TT/s])/
       (6*s^3*TT - 6*s^2*TT^2)) + H[0, mts/s]^3*
     ((7*Pi^2)/(9*s^2*TT - 9*s*TT^2) + (2*im*Pi)/(3*s^2*TT - 3*s*TT^2) - 
      H[0, TT/s]^2/(6*s^2*TT - 6*s*TT^2) - (2*H[1, TT/s])/(3*s^2*TT) - 
      H[1, TT/s]^2/(6*s^2*TT - 6*s*TT^2) + H[0, TT/s]*(2/(3*s^2*(s - TT)) - 
        H[1, TT/s]/(3*s^2*TT - 3*s*TT^2))) + (52*im*Pi*H[0, 1, TT/s]^2)/
     (s^2*TT - s*TT^2) + H[0, TT/s]^3*(44/(3*s^2*(s - TT)) - 
      (22*im*Pi)/(3*s^2*TT) + (Pi^2*(7*s - 18*TT))/(6*s^3*TT - 6*s^2*TT^2) + 
      ((-2*im*Pi*(3*s - 8*TT))/(3*s^2*(s - TT)*TT) - (22*(3*s - 2*TT))/
         (3*s^2*(s - TT)*TT))*H[1, TT/s] - ((s - 8*TT)*H[1, TT/s]^2)/
       (3*s^3*TT - 3*s^2*TT^2) - (5*(11*s - 8*TT)*H[0, 1, TT/s])/
       (3*s^2*(s - TT)*TT)) + ((6*Pi^2*(4*s - 5*TT))/(s^2*(s - TT)*TT) + 
      (88*(-2*s + TT))/(s^2*(s - TT)*TT) + (44*im*Pi)/(s^2*TT - s*TT^2))*
     H[0, 0, 1, TT/s] + ((6*Pi^2*(s - 5*TT))/(s^2*(s - TT)*TT) + 
      (88*(s + TT))/(s^2*(s - TT)*TT) - (44*im*Pi)/(s^2*TT - s*TT^2))*
     H[0, 1, 1, TT/s] + ((2*im*Pi*(43*s - 78*TT))/(s^2*(s - TT)*TT) + 
      (44*(-2*s + TT))/(s^2*(s - TT)*TT))*H[0, 0, 0, 1, TT/s] + 
    ((44*(s - 2*TT))/(s^2*(s - TT)*TT) - (72*im*Pi*(s - 2*TT))/
       (s^2*(s - TT)*TT))*H[0, 0, 1, 1, TT/s] + 
    ((2*im*Pi*(35*s - 78*TT))/(s^2*(s - TT)*TT) + 
      (44*(s + TT))/(s^2*(s - TT)*TT))*H[0, 1, 1, 1, TT/s] + 
    ((-214*s + 160*TT)*H[0, 0, 0, 0, 1, TT/s])/(s^2*(s - TT)*TT) + 
    (2*(71*s + 124*TT)*H[0, 0, 0, 1, 1, TT/s])/(s^2*(s - TT)*TT) - 
    (2*(5*s - 62*TT)*H[0, 0, 1, 0, 1, TT/s])/(s^2*(s - TT)*TT) + 
    ((-390*s + 248*TT)*H[0, 0, 1, 1, 1, TT/s])/(s^2*(s - TT)*TT) - 
    (2*(57*s - 62*TT)*H[0, 1, 0, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
    (2*(27*s + 80*TT)*H[0, 1, 1, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
    (-88/(s^2*(s - TT)) - (160*im*Pi)/(3*s^2*TT - 3*s*TT^2) + 
      (Pi^2*(151*s + 270*TT))/(9*s^3*TT - 9*s^2*TT^2))*Zeta[3] + 
    H[0, 1, TT/s]*((im*Pi^3*(s - 54*TT))/(3*s^2*(s - TT)*TT) + 
      (88*im*Pi*(s - 2*TT))/(s^2*(s - TT)*TT) + (22*Pi^2*(s - 2*TT))/
       (s^2*(s - TT)*TT) + (2*(5*s - 62*TT)*H[0, 0, 1, TT/s])/
       (s^2*(s - TT)*TT) + (2*(57*s - 62*TT)*Zeta[3])/(s^2*(s - TT)*TT)) + 
    H[1, TT/s]^2*((-44*im*Pi)/(s^2*(s - TT)) + (11*Pi^2)/(s^2*TT) + 
      (im*Pi^3*(s - 6*TT))/(2*s^2*(s - TT)*TT) - 
      (2*im*Pi*(7*s + 9*TT)*H[0, 1, TT/s])/(s^2*(s - TT)*TT) + 
      ((s - 22*TT)*H[0, 0, 1, TT/s])/(s^2*(s - TT)*TT) + 
      ((13*s + 40*TT)*H[0, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
      (10*(s - 4*TT)*Zeta[3])/(s^2*(s - TT)*TT)) + 
    ((256*im*mts*Pi^2*H[0, 2]^3)/(3*s*Sqrt[mts*s*(s - TT)*TT]) + 
      (7*im*mts*Pi^4*H[0, mts/s])/(3*s*Sqrt[mts*s*(s - TT)*TT]) + 
      (64*im*mts*Pi^2*H[0, 2]^2*H[0, mts/s])/(s*Sqrt[mts*s*(s - TT)*TT]) + 
      (4*im*mts*Pi^2*H[0, mts/s]^3)/(3*s*Sqrt[mts*s*(s - TT)*TT]) + 
      H[0, 2]*((28*im*mts*Pi^4)/(3*Sqrt[mts*s^3*(s - TT)*TT]) + 
        (16*im*mts*Pi^2*H[0, mts/s]^2)/Sqrt[mts*s^3*(s - TT)*TT]) + 
      (68*im*mts*Pi^2*Zeta[3])/(3*s*Sqrt[mts*s*(s - TT)*TT]))/mts + 
    H[0, TT/s]^2*((11*Pi^2)/(s^2*(s - TT)) - (44*im*Pi)/(s^2*TT) + 
      (im*Pi^3*(-47*s + 36*TT))/(6*s^2*(s - TT)*TT) + 
      (-11/(s^2*TT) - (im*Pi*(3*s + 4*TT))/(2*s^2*(s - TT)*TT))*
       H[1, TT/s]^2 - ((s + 6*TT)*H[1, TT/s]^3)/(6*s^3*TT - 6*s^2*TT^2) + 
      ((22*(2*s - TT))/(s^2*(s - TT)*TT) - (im*Pi*(11*s + 20*TT))/
         (s^2*(s - TT)*TT))*H[0, 1, TT/s] + H[1, TT/s]*
       (-44/(s^2*TT) - (22*im*Pi)/(s^2*TT) + (Pi^2*(19*s - 18*TT))/
         (2*s^3*TT - 2*s^2*TT^2) - (3*H[0, 1, TT/s])/(s^2*TT - s*TT^2)) + 
      ((54*s - 40*TT)*H[0, 0, 1, TT/s])/(s^2*(s - TT)*TT) - 
      (2*(4*s + 11*TT)*H[0, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
      (2*(s + 11*TT)*Zeta[3])/(s^2*(s - TT)*TT)) + 
    H[0, mts/s]^2*((2*im*Pi^3)/(3*s^2*TT - 3*s*TT^2) + 
      (4*im*Pi)/(s^2*TT - s*TT^2) + ((-2*im*Pi)/(s^2*(s - TT)) - 4/(s^2*TT) - 
        (Pi^2*(s + 2*TT))/(2*s^3*TT - 2*s^2*TT^2))*H[1, TT/s] + 
      (im*Pi*H[1, TT/s]^2)/(2*s^2*TT - 2*s*TT^2) + 
      H[0, TT/s]^2*((im*Pi)/(2*s^2*TT - 2*s*TT^2) + 
        H[1, TT/s]/(2*s^2*TT - 2*s*TT^2)) + H[0, TT/s]*
       (4/(s^2*(s - TT)) + (2*im*Pi)/(s^2*TT) + (Pi^2*(3*s - 2*TT))/
         (2*s^3*TT - 2*s^2*TT^2) - (2*H[1, TT/s])/(s^2*TT - s*TT^2) - 
        H[0, 1, TT/s]/(s^2*TT - s*TT^2)) + H[0, 0, 1, TT/s]/
       (s^2*TT - s*TT^2) - H[0, 1, 1, TT/s]/(s^2*TT - s*TT^2) - 
      (13*Zeta[3])/(s^2*TT - s*TT^2)) + 
    H[1, TT/s]*((176*im*Pi)/(s^2*(s - TT)) + 352/(s^2*TT) - 
      (48*Pi^2)/(s^2*TT) - (Pi^4*(-84*s + TT))/(30*s^2*(s - TT)*TT) - 
      (2*im*Pi^3*(11*s + 3*TT))/(3*s^2*(s - TT)*TT) + 
      ((44*im*Pi)/(s^2*TT - s*TT^2) - (3*Pi^2)/(s^2*TT - s*TT^2))*
       H[0, 1, TT/s] + (44/(s^2*(s - TT)) - (4*im*Pi*(10*s + 19*TT))/
         (s^2*(s - TT)*TT))*H[0, 0, 1, TT/s] + 
      ((-44*(s + TT))/(s^2*(s - TT)*TT) + (2*im*Pi*(5*s + 38*TT))/
         (s^2*(s - TT)*TT))*H[0, 1, 1, TT/s] + 
      ((-98*s + 160*TT)*H[0, 0, 0, 1, TT/s])/(s^2*(s - TT)*TT) + 
      (110*H[0, 0, 1, 1, TT/s])/(s^2*TT - s*TT^2) - 
      (2*(27*s + 80*TT)*H[0, 1, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
      ((424*s - 292*TT)/(3*s^3*TT - 3*s^2*TT^2) + (Pi*(26*im*s - 340*im*TT))/
         (3*s^3*TT - 3*s^2*TT^2))*Zeta[3]) + 
    H[0, mts/s]*((-2*im*Pi^3)/(3*s^2*(s - TT)) - 
      (9*Pi^4)/(20*s^2*TT - 20*s*TT^2) + (16*im*Pi)/(s^2*TT - s*TT^2) + 
      (5*H[0, TT/s]^4)/(12*s^2*TT - 12*s*TT^2) + 
      ((2*im*Pi)/(s^2*(s - TT)) + Pi^2/(s^2*TT - s*TT^2))*H[1, TT/s]^2 + 
      (2/(3*s^2*TT) - (2*im*Pi)/(3*s^2*TT - 3*s*TT^2))*H[1, TT/s]^3 + 
      (5*H[1, TT/s]^4)/(12*s^2*TT - 12*s*TT^2) + 
      H[0, TT/s]^3*(-2/(3*s^2*(s - TT)) + (2*im*Pi)/(3*s^2*TT - 3*s*TT^2) - 
        H[1, TT/s]/(3*s^2*TT - 3*s*TT^2)) - 
      (4*im*Pi*(s - 2*TT)*H[0, 1, TT/s])/(s^2*(s - TT)*TT) + 
      H[0, TT/s]^2*((2*im*Pi)/(s^2*TT) + Pi^2/(s^2*TT - s*TT^2) + 
        (2/(s^2*TT) - (im*Pi)/(s^2*TT - s*TT^2))*H[1, TT/s] - 
        H[1, TT/s]^2/(2*s^2*TT - 2*s*TT^2) + (4*H[0, 1, TT/s])/
         (s^2*TT - s*TT^2)) + ((8*s - 4*TT)/(s^2*(s - TT)*TT) - 
        (10*im*Pi)/(s^2*TT - s*TT^2))*H[0, 0, 1, TT/s] + 
      ((-4*(s + TT))/(s^2*(s - TT)*TT) + (10*im*Pi)/(s^2*TT - s*TT^2))*
       H[0, 1, 1, TT/s] + (16*H[0, 0, 0, 1, TT/s])/(s^2*TT - s*TT^2) - 
      (16*H[0, 1, 1, 1, TT/s])/(s^2*TT - s*TT^2) + 
      (4/(s^2*(s - TT)) - (26*im*Pi)/(s^2*TT - s*TT^2))*Zeta[3] + 
      H[0, TT/s]*(16/(s^2*(s - TT)) - (2*Pi^2)/(s^2*(s - TT)) + 
        (8*im*Pi)/(s^2*TT) + (5*im*Pi^3)/(3*s^2*TT - 3*s*TT^2) + 
        ((4*im*Pi)/(s^2*TT) - 8/(s^2*TT - s*TT^2) - Pi^2/(s^2*TT - s*TT^2))*
         H[1, TT/s] + (2*H[1, TT/s]^2)/(s^2*TT - s*TT^2) + 
        H[1, TT/s]^3/(3*s^2*TT - 3*s*TT^2) + 
        ((-8*s + 4*TT)/(s^2*(s - TT)*TT) + (6*im*Pi)/(s^2*TT - s*TT^2))*
         H[0, 1, TT/s] - (12*H[0, 0, 1, TT/s])/(s^2*TT - s*TT^2) + 
        (2*(s - 8*TT)*Zeta[3])/(3*s^2*(s - TT)*TT)) + 
      H[1, TT/s]*((-8*im*Pi)/(s^2*(s - TT)) - 16/(s^2*TT) + 
        (2*Pi^2)/(s^2*TT) - (im*Pi^3)/(s^2*TT - s*TT^2) - 
        (4*im*Pi*H[0, 1, TT/s])/(s^2*TT - s*TT^2) + (4*H[0, 1, 1, TT/s])/
         (s^2*TT - s*TT^2) + (2*(s - 8*TT)*Zeta[3])/(3*s^2*(s - TT)*TT))) + 
    H[0, TT/s]*(-352/(s^2*(s - TT)) + (48*Pi^2)/(s^2*(s - TT)) - 
      (176*im*Pi)/(s^2*TT) + (2*im*Pi^3)/(s^2*TT) - (Pi^4*(53*s + 2*TT))/
       (60*s^2*(s - TT)*TT) + ((2*im*Pi*(s + TT))/(3*s^2*(s - TT)*TT) + 
        22/(3*s^2*TT - 3*s*TT^2))*H[1, TT/s]^3 + ((s - 12*TT)*H[1, TT/s]^4)/
       (6*s^3*TT - 6*s^2*TT^2) + ((88*(2*s - TT))/(s^2*(s - TT)*TT) + 
        (Pi^2*(-27*s + 30*TT))/(s^2*(s - TT)*TT))*H[0, 1, TT/s] + 
      ((-5*s + 62*TT)*H[0, 1, TT/s]^2)/(s^2*(s - TT)*TT) + 
      H[1, TT/s]^2*((-22*im*Pi)/(s^2*TT) - 44/(s^2*TT - s*TT^2) - 
        (Pi^2*(s - 12*TT))/(2*s^3*TT - 2*s^2*TT^2) + 
        ((-s + 22*TT)*H[0, 1, TT/s])/(s^2*(s - TT)*TT)) + 
      (80*im*Pi*H[0, 0, 1, TT/s])/(s^2*(s - TT)) + 
      (44/(s^2*(s - TT)) - (4*im*Pi*(11*s + 17*TT))/(s^2*(s - TT)*TT))*
       H[0, 1, 1, TT/s] - (52*H[0, 0, 1, 1, TT/s])/(s^2*TT - s*TT^2) + 
      (2*(55*s - 18*TT)*H[0, 1, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
      (-292/(3*s^2*(s - TT)) + (4*im*Pi*(101*s - 85*TT))/(3*s^2*(s - TT)*TT))*
       Zeta[3] + H[1, TT/s]*((-88*im*Pi)/(s^2*TT) + (im*Pi^3*(-8*s + 12*TT))/
         (s^2*(s - TT)*TT) + (Pi^2*(-46*s + 22*TT))/(s^2*(s - TT)*TT) + 
        176/(s^2*TT - s*TT^2) + (-44/(s^2*(s - TT)) + (im*Pi*(6*s + 40*TT))/
           (s^2*(s - TT)*TT))*H[0, 1, TT/s] + 
        ((52*s - 80*TT)*H[0, 0, 1, TT/s])/(s^2*(s - TT)*TT) - 
        (2*(15*s + 22*TT)*H[0, 1, 1, TT/s])/(s^2*(s - TT)*TT) + 
        ((-80*s + 44*TT)*Zeta[3])/(s^2*(s - TT)*TT))) + 
    (13*(s + 2*TT)*Zeta[5])/(s^2*(s - TT)*TT)))
