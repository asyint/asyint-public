9/(SS^2*TT) + (2*Pi^2)/(SS^2*TT) + (103*Pi^4)/(240*SS^2*TT) + 
 (-1/2*1/(SS^2*TT) - 1/(2*SS^2*UU) - 5/(2*SS*TT*UU))/ep^4 + 9/(SS^2*UU) + 
 (2*Pi^2)/(SS^2*UU) + (103*Pi^4)/(240*SS^2*UU) + 36/(SS*TT*UU) + 
 (2*Pi^2)/(SS*TT*UU) + (21*Pi^4)/(80*SS*TT*UU) - 
 (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS^2*TT) - 
 (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS^2*UU) - 
 (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS*TT*UU) + 
 (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2)/
  UU - (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
   Gamma[1 + Z1]^2)/SS^2 + (2*EulerGamma*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*
   Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1])/SS + 
 (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2)/
  TT + (2*Log[mts])/(SS^2*TT) + (Pi^2*Log[mts])/(2*SS^2*TT) + 
 (2*Log[mts])/(SS^2*UU) + (Pi^2*Log[mts])/(2*SS^2*UU) - 
 (16*Log[mts])/(SS*TT*UU) + (Pi^2*Log[mts])/(2*SS*TT*UU) + 
 (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts])/(SS^2*TT) + 
 (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts])/(SS^2*UU) + 
 (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts])/(SS*TT*UU) + 
 (2*Log[mts]^2)/(SS^2*TT) + (29*Pi^2*Log[mts]^2)/(12*SS^2*TT) + 
 (2*Log[mts]^2)/(SS^2*UU) + (29*Pi^2*Log[mts]^2)/(12*SS^2*UU) + 
 (2*Log[mts]^2)/(SS*TT*UU) - (3*Pi^2*Log[mts]^2)/(2*SS*TT*UU) - 
 Log[mts]^3/(3*SS^2*TT) - Log[mts]^3/(3*SS^2*UU) - Log[mts]^3/(3*SS*TT*UU) + 
 (17*Log[mts]^4)/(6*SS^2*TT) + (17*Log[mts]^4)/(6*SS^2*UU) - 
 (13*Log[mts]^4)/(12*SS*TT*UU) + ((7*Pi^4)/(6*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + 
   (32*Pi^2*Log[2]^2)/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + 
   (16*Pi^2*Log[2]*Log[mts])/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + 
   (2*Pi^2*Log[mts]^2)/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]))/Sqrt[mts] + 
 (8*Log[SS])/(SS^2*TT) + (Pi^2*Log[SS])/(SS^2*TT) + (8*Log[SS])/(SS^2*UU) + 
 (Pi^2*Log[SS])/(SS^2*UU) - (8*Log[SS])/(SS*TT*UU) - 
 (Pi^2*Log[SS])/(SS*TT*UU) + (2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*
   Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Log[SS])/SS + 
 (4*Log[mts]*Log[SS])/(SS^2*TT) - (13*Pi^2*Log[mts]*Log[SS])/(6*SS^2*TT) + 
 (4*Log[mts]*Log[SS])/(SS^2*UU) - (13*Pi^2*Log[mts]*Log[SS])/(6*SS^2*UU) - 
 (4*Log[mts]*Log[SS])/(SS*TT*UU) - (3*Pi^2*Log[mts]*Log[SS])/(SS*TT*UU) + 
 (Log[mts]^2*Log[SS])/(SS^2*TT) + (Log[mts]^2*Log[SS])/(SS^2*UU) - 
 (Log[mts]^2*Log[SS])/(SS*TT*UU) - (6*Log[mts]^3*Log[SS])/(SS^2*TT) - 
 (6*Log[mts]^3*Log[SS])/(SS^2*UU) - (Log[mts]^3*Log[SS])/(SS*TT*UU) + 
 (2*Log[SS]^2)/(SS^2*TT) + (5*Pi^2*Log[SS]^2)/(12*SS^2*TT) + 
 (2*Log[SS]^2)/(SS^2*UU) + (5*Pi^2*Log[SS]^2)/(12*SS^2*UU) + 
 (2*Log[SS]^2)/(SS*TT*UU) + (Pi^2*Log[SS]^2)/(2*SS*TT*UU) + 
 (Log[mts]*Log[SS]^2)/(SS^2*TT) + (Log[mts]*Log[SS]^2)/(SS^2*UU) + 
 (Log[mts]*Log[SS]^2)/(SS*TT*UU) + (4*Log[mts]^2*Log[SS]^2)/(SS^2*TT) + 
 (4*Log[mts]^2*Log[SS]^2)/(SS^2*UU) + (Log[mts]^2*Log[SS]^2)/(2*SS*TT*UU) + 
 Log[SS]^3/(3*SS^2*TT) + Log[SS]^3/(3*SS^2*UU) - Log[SS]^3/(3*SS*TT*UU) - 
 (4*Log[mts]*Log[SS]^3)/(3*SS^2*TT) - (4*Log[mts]*Log[SS]^3)/(3*SS^2*UU) - 
 (Log[mts]*Log[SS]^3)/(3*SS*TT*UU) + Log[SS]^4/(6*SS^2*TT) + 
 Log[SS]^4/(6*SS^2*UU) + Log[SS]^4/(12*SS*TT*UU) + (8*Log[TT])/(SS^2*TT) + 
 (Pi^2*Log[TT])/(SS^2*TT) - (8*Log[TT])/(SS^2*UU) - 
 (Pi^2*Log[TT])/(SS^2*UU) + (8*Log[TT])/(SS*TT*UU) + 
 (Pi^2*Log[TT])/(SS*TT*UU) + (4*Log[mts]*Log[TT])/(SS^2*TT) - 
 (Pi^2*Log[mts]*Log[TT])/(2*SS^2*TT) - (4*Log[mts]*Log[TT])/(SS^2*UU) + 
 (Pi^2*Log[mts]*Log[TT])/(2*SS^2*UU) + (4*Log[mts]*Log[TT])/(SS*TT*UU) + 
 (3*Pi^2*Log[mts]*Log[TT])/(SS*TT*UU) + (Log[mts]^2*Log[TT])/(SS^2*TT) - 
 (Log[mts]^2*Log[TT])/(SS^2*UU) + (Log[mts]^2*Log[TT])/(SS*TT*UU) - 
 (2*Log[mts]^3*Log[TT])/(3*SS^2*TT) + (2*Log[mts]^3*Log[TT])/(3*SS^2*UU) + 
 (5*Log[mts]^3*Log[TT])/(3*SS*TT*UU) + (4*Log[SS]*Log[TT])/(SS^2*TT) - 
 (4*Log[SS]*Log[TT])/(SS^2*UU) - (4*Log[SS]*Log[TT])/(SS*TT*UU) - 
 (2*Pi^2*Log[SS]*Log[TT])/(SS*TT*UU) + (2*Log[mts]*Log[SS]*Log[TT])/
  (SS^2*TT) - (2*Log[mts]*Log[SS]*Log[TT])/(SS^2*UU) - 
 (2*Log[mts]*Log[SS]*Log[TT])/(SS*TT*UU) + (Log[SS]^2*Log[TT])/(SS^2*TT) - 
 (Log[SS]^2*Log[TT])/(SS^2*UU) + (Log[SS]^2*Log[TT])/(SS*TT*UU) + 
 (Log[mts]*Log[SS]^2*Log[TT])/(SS*TT*UU) - (Log[SS]^3*Log[TT])/(3*SS*TT*UU) + 
 (2*Log[TT]^2)/(SS^2*TT) + (2*Log[TT]^2)/(SS^2*UU) + 
 (2*Log[TT]^2)/(SS*TT*UU) + (2*Pi^2*Log[TT]^2)/(SS*TT*UU) + 
 (Log[mts]*Log[TT]^2)/(SS^2*TT) + (Log[mts]*Log[TT]^2)/(SS^2*UU) + 
 (Log[mts]*Log[TT]^2)/(SS*TT*UU) - (Log[mts]^2*Log[TT]^2)/(2*SS*TT*UU) + 
 (Log[SS]*Log[TT]^2)/(SS^2*TT) + (Log[SS]*Log[TT]^2)/(SS^2*UU) - 
 (Log[SS]*Log[TT]^2)/(SS*TT*UU) - (Log[mts]*Log[SS]*Log[TT]^2)/(SS*TT*UU) + 
 (Log[SS]^2*Log[TT]^2)/(2*SS*TT*UU) + Log[TT]^3/(3*SS^2*TT) - 
 Log[TT]^3/(3*SS^2*UU) + Log[TT]^3/(3*SS*TT*UU) + 
 (Log[mts]*Log[TT]^3)/(3*SS*TT*UU) - (2*Log[SS]*Log[TT]^3)/(3*SS*TT*UU) + 
 (7*Log[TT]^4)/(12*SS*TT*UU) - (8*Log[UU])/(SS^2*TT) - 
 (Pi^2*Log[UU])/(SS^2*TT) + (8*Log[UU])/(SS^2*UU) + 
 (Pi^2*Log[UU])/(SS^2*UU) + (8*Log[UU])/(SS*TT*UU) + 
 (Pi^2*Log[UU])/(SS*TT*UU) - (6*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*
   Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Log[UU])/SS - 
 (4*Log[mts]*Log[UU])/(SS^2*TT) + (Pi^2*Log[mts]*Log[UU])/(2*SS^2*TT) + 
 (4*Log[mts]*Log[UU])/(SS^2*UU) - (Pi^2*Log[mts]*Log[UU])/(2*SS^2*UU) + 
 (4*Log[mts]*Log[UU])/(SS*TT*UU) + (3*Pi^2*Log[mts]*Log[UU])/(SS*TT*UU) - 
 (Log[mts]^2*Log[UU])/(SS^2*TT) + (Log[mts]^2*Log[UU])/(SS^2*UU) + 
 (Log[mts]^2*Log[UU])/(SS*TT*UU) + (2*Log[mts]^3*Log[UU])/(3*SS^2*TT) - 
 (2*Log[mts]^3*Log[UU])/(3*SS^2*UU) + (5*Log[mts]^3*Log[UU])/(3*SS*TT*UU) - 
 (4*Log[SS]*Log[UU])/(SS^2*TT) + (4*Log[SS]*Log[UU])/(SS^2*UU) - 
 (4*Log[SS]*Log[UU])/(SS*TT*UU) - (Pi^2*Log[SS]*Log[UU])/(SS*TT*UU) - 
 (2*Log[mts]*Log[SS]*Log[UU])/(SS^2*TT) + (2*Log[mts]*Log[SS]*Log[UU])/
  (SS^2*UU) - (2*Log[mts]*Log[SS]*Log[UU])/(SS*TT*UU) - 
 (Log[SS]^2*Log[UU])/(SS^2*TT) + (Log[SS]^2*Log[UU])/(SS^2*UU) + 
 (Log[SS]^2*Log[UU])/(SS*TT*UU) + (Log[mts]*Log[SS]^2*Log[UU])/(SS*TT*UU) - 
 (Log[SS]^3*Log[UU])/(3*SS*TT*UU) - (4*Log[TT]*Log[UU])/(SS^2*TT) - 
 (4*Log[TT]*Log[UU])/(SS^2*UU) + (4*Log[TT]*Log[UU])/(SS*TT*UU) + 
 (2*Pi^2*Log[TT]*Log[UU])/(SS*TT*UU) - (2*Log[mts]*Log[TT]*Log[UU])/
  (SS^2*TT) - (2*Log[mts]*Log[TT]*Log[UU])/(SS^2*UU) + 
 (2*Log[mts]*Log[TT]*Log[UU])/(SS*TT*UU) - (2*Log[SS]*Log[TT]*Log[UU])/
  (SS^2*TT) - (2*Log[SS]*Log[TT]*Log[UU])/(SS^2*UU) - 
 (2*Log[SS]*Log[TT]*Log[UU])/(SS*TT*UU) + (Log[SS]^2*Log[TT]*Log[UU])/
  (SS*TT*UU) - (Log[TT]^2*Log[UU])/(SS^2*TT) + 
 (Log[TT]^2*Log[UU])/(SS^2*UU) + (Log[TT]^2*Log[UU])/(SS*TT*UU) - 
 (Log[mts]*Log[TT]^2*Log[UU])/(SS*TT*UU) - (2*Log[TT]^3*Log[UU])/
  (3*SS*TT*UU) + (2*Log[UU]^2)/(SS^2*TT) + (2*Log[UU]^2)/(SS^2*UU) + 
 (2*Log[UU]^2)/(SS*TT*UU) + (Pi^2*Log[UU]^2)/(2*SS*TT*UU) + 
 (Log[mts]*Log[UU]^2)/(SS^2*TT) + (Log[mts]*Log[UU]^2)/(SS^2*UU) + 
 (Log[mts]*Log[UU]^2)/(SS*TT*UU) - (Log[mts]^2*Log[UU]^2)/(2*SS*TT*UU) + 
 (Log[SS]*Log[UU]^2)/(SS^2*TT) + (Log[SS]*Log[UU]^2)/(SS^2*UU) - 
 (Log[SS]*Log[UU]^2)/(SS*TT*UU) - (Log[mts]*Log[SS]*Log[UU]^2)/(SS*TT*UU) + 
 (Log[SS]^2*Log[UU]^2)/(2*SS*TT*UU) + (Log[TT]*Log[UU]^2)/(SS^2*TT) - 
 (Log[TT]*Log[UU]^2)/(SS^2*UU) + (Log[TT]*Log[UU]^2)/(SS*TT*UU) - 
 (Log[mts]*Log[TT]*Log[UU]^2)/(SS*TT*UU) - (Log[SS]*Log[TT]*Log[UU]^2)/
  (SS*TT*UU) - (Log[TT]^2*Log[UU]^2)/(2*SS*TT*UU) - Log[UU]^3/(3*SS^2*TT) + 
 Log[UU]^3/(3*SS^2*UU) + Log[UU]^3/(3*SS*TT*UU) + 
 (Log[mts]*Log[UU]^3)/(3*SS*TT*UU) - (Log[SS]*Log[UU]^3)/(3*SS*TT*UU) + 
 (Log[TT]*Log[UU]^3)/(3*SS*TT*UU) + Log[UU]^4/(12*SS*TT*UU) + 
 (-3/(2*SS^2*TT) - 3/(2*SS^2*UU) - 3/(2*SS*TT*UU) + Log[SS]/(SS^2*TT) + 
   Log[SS]/(SS^2*UU) - Log[SS]/(SS*TT*UU) + Log[TT]/(2*SS^2*TT) - 
   Log[TT]/(2*SS^2*UU) + (3*Log[TT])/(SS*TT*UU) - Log[UU]/(2*SS^2*TT) + 
   Log[UU]/(2*SS^2*UU) + (3*Log[UU])/(SS*TT*UU))/ep^3 + 
 (4/(SS^2*TT) + Pi^2/(3*SS^2*TT) + 4/(SS^2*UU) + Pi^2/(3*SS^2*UU) + 
   4/(SS*TT*UU) + (7*Pi^2)/(4*SS*TT*UU) + 
   Pi^2/(Sqrt[mts]*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + Log[mts]/(SS^2*TT) + 
   Log[mts]/(SS^2*UU) + Log[mts]/(SS*TT*UU) + (3*Log[mts]^2)/(2*SS^2*TT) + 
   (3*Log[mts]^2)/(2*SS^2*UU) + (2*Log[SS])/(SS^2*TT) + 
   (2*Log[SS])/(SS^2*UU) - (2*Log[SS])/(SS*TT*UU) - 
   (3*Log[mts]*Log[SS])/(SS^2*TT) - (3*Log[mts]*Log[SS])/(SS^2*UU) + 
   Log[SS]^2/(2*SS^2*TT) + Log[SS]^2/(2*SS^2*UU) + Log[SS]^2/(SS*TT*UU) + 
   (2*Log[TT])/(SS^2*TT) - (2*Log[TT])/(SS^2*UU) + (2*Log[TT])/(SS*TT*UU) - 
   (Log[mts]*Log[TT])/(SS^2*TT) + (Log[mts]*Log[TT])/(SS^2*UU) - 
   Log[TT]^2/(SS*TT*UU) - (2*Log[UU])/(SS^2*TT) + (2*Log[UU])/(SS^2*UU) + 
   (2*Log[UU])/(SS*TT*UU) + (Log[mts]*Log[UU])/(SS^2*TT) - 
   (Log[mts]*Log[UU])/(SS^2*UU) - (4*Log[TT]*Log[UU])/(SS*TT*UU) - 
   Log[UU]^2/(SS*TT*UU))/ep^2 - (8*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*
   Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*PolyGamma[0, -1 - Z1])/SS + 
 (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1])/(SS^2*TT) + 
 (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1])/(SS^2*UU) - 
 (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1])/
  (SS*TT*UU) + (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
   PolyGamma[0, -1 + Z1])/(SS^2*TT) + 
 (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, -1 + Z1])/
  (SS^2*UU) + (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
   PolyGamma[0, -1 + Z1])/(SS*TT*UU) - 
 (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/(SS^2*TT) - 
 (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/(SS^2*UU) + 
 (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/(SS*TT*UU) + 
 (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
   Gamma[2 + Z1]*PolyGamma[0, 1 + Z1])/SS + 
 (6*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
   Gamma[2 + Z1]*PolyGamma[0, 2 + Z1])/SS + (4*Log[mts]*Zeta[3])/(SS^2*TT) + 
 (4*Log[mts]*Zeta[3])/(SS^2*UU) + (8*Log[mts]*Zeta[3])/(SS*TT*UU) - 
 (11*Log[SS]*Zeta[3])/(3*SS^2*TT) - (11*Log[SS]*Zeta[3])/(3*SS^2*UU) + 
 (26*Log[SS]*Zeta[3])/(3*SS*TT*UU) - (4*Log[TT]*Zeta[3])/(3*SS^2*TT) + 
 (4*Log[TT]*Zeta[3])/(3*SS^2*UU) - (12*Log[TT]*Zeta[3])/(SS*TT*UU) + 
 (4*Log[UU]*Zeta[3])/(3*SS^2*TT) - (4*Log[UU]*Zeta[3])/(3*SS^2*UU) - 
 (14*Log[UU]*Zeta[3])/(SS*TT*UU) + 
 (-5/(SS^2*TT) - (3*Pi^2)/(4*SS^2*TT) - 5/(SS^2*UU) - (3*Pi^2)/(4*SS^2*UU) + 
   4/(SS*TT*UU) - (3*Pi^2)/(4*SS*TT*UU) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS^2*TT) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS^2*UU) - 
   (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(SS*TT*UU) + 
   (2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1])/SS - (4*Log[mts])/(SS^2*TT) - 
   (4*Pi^2*Log[mts])/(3*SS^2*TT) - (4*Log[mts])/(SS^2*UU) - 
   (4*Pi^2*Log[mts])/(3*SS^2*UU) - (4*Log[mts])/(SS*TT*UU) - 
   (8*Log[mts]^3)/(3*SS^2*TT) - (8*Log[mts]^3)/(3*SS^2*UU) + 
   Log[mts]^3/(SS*TT*UU) + ((-8*Pi^2*Log[2])/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (2*Pi^2*Log[mts])/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]))/Sqrt[mts] - 
   (4*Log[SS])/(SS^2*TT) + (2*Pi^2*Log[SS])/(3*SS^2*TT) - 
   (4*Log[SS])/(SS^2*UU) + (2*Pi^2*Log[SS])/(3*SS^2*UU) + 
   (4*Log[SS])/(SS*TT*UU) + (5*Pi^2*Log[SS])/(2*SS*TT*UU) - 
   (2*Log[mts]*Log[SS])/(SS^2*TT) - (2*Log[mts]*Log[SS])/(SS^2*UU) + 
   (2*Log[mts]*Log[SS])/(SS*TT*UU) + (5*Log[mts]^2*Log[SS])/(SS^2*TT) + 
   (5*Log[mts]^2*Log[SS])/(SS^2*UU) + (Log[mts]^2*Log[SS])/(SS*TT*UU) - 
   Log[SS]^2/(SS^2*TT) - Log[SS]^2/(SS^2*UU) - Log[SS]^2/(SS*TT*UU) - 
   (2*Log[mts]*Log[SS]^2)/(SS^2*TT) - (2*Log[mts]*Log[SS]^2)/(SS^2*UU) - 
   (Log[mts]*Log[SS]^2)/(SS*TT*UU) + Log[SS]^3/(3*SS^2*TT) + 
   Log[SS]^3/(3*SS^2*UU) + Log[SS]^3/(3*SS*TT*UU) - (4*Log[TT])/(SS^2*TT) + 
   (Pi^2*Log[TT])/(4*SS^2*TT) + (4*Log[TT])/(SS^2*UU) - 
   (Pi^2*Log[TT])/(4*SS^2*UU) - (4*Log[TT])/(SS*TT*UU) - 
   (7*Pi^2*Log[TT])/(2*SS*TT*UU) - (2*Log[mts]*Log[TT])/(SS^2*TT) + 
   (2*Log[mts]*Log[TT])/(SS^2*UU) - (2*Log[mts]*Log[TT])/(SS*TT*UU) + 
   (Log[mts]^2*Log[TT])/(SS^2*TT) - (Log[mts]^2*Log[TT])/(SS^2*UU) - 
   (2*Log[mts]^2*Log[TT])/(SS*TT*UU) - (2*Log[SS]*Log[TT])/(SS^2*TT) + 
   (2*Log[SS]*Log[TT])/(SS^2*UU) + (2*Log[SS]*Log[TT])/(SS*TT*UU) - 
   (Log[SS]^2*Log[TT])/(SS*TT*UU) - Log[TT]^2/(SS^2*TT) - 
   Log[TT]^2/(SS^2*UU) - Log[TT]^2/(SS*TT*UU) + 
   (Log[mts]*Log[TT]^2)/(SS*TT*UU) + (Log[SS]*Log[TT]^2)/(SS*TT*UU) - 
   (2*Log[TT]^3)/(3*SS*TT*UU) + (4*Log[UU])/(SS^2*TT) - 
   (Pi^2*Log[UU])/(4*SS^2*TT) - (4*Log[UU])/(SS^2*UU) + 
   (Pi^2*Log[UU])/(4*SS^2*UU) - (4*Log[UU])/(SS*TT*UU) - 
   (5*Pi^2*Log[UU])/(2*SS*TT*UU) + (2*Log[mts]*Log[UU])/(SS^2*TT) - 
   (2*Log[mts]*Log[UU])/(SS^2*UU) - (2*Log[mts]*Log[UU])/(SS*TT*UU) - 
   (Log[mts]^2*Log[UU])/(SS^2*TT) + (Log[mts]^2*Log[UU])/(SS^2*UU) - 
   (2*Log[mts]^2*Log[UU])/(SS*TT*UU) + (2*Log[SS]*Log[UU])/(SS^2*TT) - 
   (2*Log[SS]*Log[UU])/(SS^2*UU) + (2*Log[SS]*Log[UU])/(SS*TT*UU) - 
   (Log[SS]^2*Log[UU])/(SS*TT*UU) + (2*Log[TT]*Log[UU])/(SS^2*TT) + 
   (2*Log[TT]*Log[UU])/(SS^2*UU) - (2*Log[TT]*Log[UU])/(SS*TT*UU) + 
   (2*Log[mts]*Log[TT]*Log[UU])/(SS*TT*UU) + (2*Log[TT]^2*Log[UU])/
    (SS*TT*UU) - Log[UU]^2/(SS^2*TT) - Log[UU]^2/(SS^2*UU) - 
   Log[UU]^2/(SS*TT*UU) + (Log[mts]*Log[UU]^2)/(SS*TT*UU) + 
   (Log[SS]*Log[UU]^2)/(SS*TT*UU) + (Log[TT]*Log[UU]^2)/(SS*TT*UU) - 
   Log[UU]^3/(3*SS*TT*UU) - Zeta[3]/(6*SS^2*TT) - Zeta[3]/(6*SS^2*UU) + 
   (14*Zeta[3])/(3*SS*TT*UU))/ep + 
 ep*(-17/(SS^2*TT) - (5*Pi^2)/(2*SS^2*TT) - (47*Pi^4)/(240*SS^2*TT) - 
   17/(SS^2*UU) - (5*Pi^2)/(2*SS^2*UU) - (47*Pi^4)/(240*SS^2*UU) + 
   16/(SS*TT*UU) + (2*Pi^2)/(SS*TT*UU) - (47*Pi^4)/(240*SS*TT*UU) - 
   (Pi^2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(3*SS^2*TT) - 
   (Pi^2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(3*SS^2*UU) + 
   (2*Pi^2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1])/(3*SS*TT*UU) + 
   (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2)/UU - (8*EulerGamma*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2)/UU - 
   (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2)/SS^2 + (4*EulerGamma*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2)/SS^2 - 
   (3*EulerGamma^2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1])/SS - 
   (5*Pi^2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1])/(6*SS) + (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2)/TT - 
   (8*EulerGamma*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2)/TT - 4*SS^(-3 - Z1 - Z2)*TT^Z1*UU^Z2*Gamma[-1 - Z1]*
    Gamma[-Z1]*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
    Gamma[1 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
   (2*Log[mts])/(SS^2*TT) - (2*Pi^2*Log[mts])/(SS^2*TT) - 
   (7*Pi^4*Log[mts])/(6*SS^2*TT) - (2*Log[mts])/(SS^2*UU) - 
   (2*Pi^2*Log[mts])/(SS^2*UU) - (7*Pi^4*Log[mts])/(6*SS^2*UU) - 
   (56*Log[mts])/(SS*TT*UU) - (2*Pi^2*Log[mts])/(SS*TT*UU) + 
   (2*Pi^4*Log[mts])/(5*SS*TT*UU) + (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*Log[mts])/(SS^2*TT) + 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts])/(SS^2*UU) + 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts])/(SS*TT*UU) - 
   (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Log[mts])/UU + (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Log[mts])/SS^2 - 
   (4*EulerGamma*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Log[mts])/SS - 
   (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Log[mts])/TT + (2*Log[mts]^2)/(SS^2*TT) + 
   (2*Log[mts]^2)/(SS^2*UU) + (20*Log[mts]^2)/(SS*TT*UU) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]^2)/(SS^2*TT) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]^2)/(SS^2*UU) - 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]^2)/(SS*TT*UU) - 
   (TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]^2)/SS - (2*Log[mts]^3)/(3*SS^2*TT) - 
   (26*Pi^2*Log[mts]^3)/(9*SS^2*TT) - (2*Log[mts]^3)/(3*SS^2*UU) - 
   (26*Pi^2*Log[mts]^3)/(9*SS^2*UU) - (2*Log[mts]^3)/(3*SS*TT*UU) + 
   (3*Pi^2*Log[mts]^3)/(2*SS*TT*UU) + Log[mts]^4/(4*SS^2*TT) + 
   Log[mts]^4/(4*SS^2*UU) + Log[mts]^4/(4*SS*TT*UU) - 
   (34*Log[mts]^5)/(15*SS^2*TT) - (34*Log[mts]^5)/(15*SS^2*UU) + 
   (13*Log[mts]^5)/(20*SS*TT*UU) - (16*Log[SS])/(SS^2*TT) - 
   (2*Pi^2*Log[SS])/(SS^2*TT) + (37*Pi^4*Log[SS])/(120*SS^2*TT) - 
   (16*Log[SS])/(SS^2*UU) - (2*Pi^2*Log[SS])/(SS^2*UU) + 
   (37*Pi^4*Log[SS])/(120*SS^2*UU) + (16*Log[SS])/(SS*TT*UU) + 
   (2*Pi^2*Log[SS])/(SS*TT*UU) + (79*Pi^4*Log[SS])/(120*SS*TT*UU) - 
   (2*EulerGamma*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Log[SS])/SS - 
   (8*Log[mts]*Log[SS])/(SS^2*TT) - (Pi^2*Log[mts]*Log[SS])/(SS^2*TT) - 
   (8*Log[mts]*Log[SS])/(SS^2*UU) - (Pi^2*Log[mts]*Log[SS])/(SS^2*UU) + 
   (8*Log[mts]*Log[SS])/(SS*TT*UU) + (Pi^2*Log[mts]*Log[SS])/(SS*TT*UU) - 
   (2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]*Log[SS])/SS - (2*Log[mts]^2*Log[SS])/(SS^2*TT) + 
   (23*Pi^2*Log[mts]^2*Log[SS])/(6*SS^2*TT) - (2*Log[mts]^2*Log[SS])/
    (SS^2*UU) + (23*Pi^2*Log[mts]^2*Log[SS])/(6*SS^2*UU) + 
   (2*Log[mts]^2*Log[SS])/(SS*TT*UU) + (2*Pi^2*Log[mts]^2*Log[SS])/
    (SS*TT*UU) - (Log[mts]^3*Log[SS])/(3*SS^2*TT) - 
   (Log[mts]^3*Log[SS])/(3*SS^2*UU) + (Log[mts]^3*Log[SS])/(3*SS*TT*UU) + 
   (17*Log[mts]^4*Log[SS])/(3*SS^2*TT) + (17*Log[mts]^4*Log[SS])/
    (3*SS^2*UU) + (7*Log[mts]^4*Log[SS])/(12*SS*TT*UU) - 
   (4*Log[SS]^2)/(SS^2*TT) - (Pi^2*Log[SS]^2)/(2*SS^2*TT) - 
   (4*Log[SS]^2)/(SS^2*UU) - (Pi^2*Log[SS]^2)/(2*SS^2*UU) - 
   (4*Log[SS]^2)/(SS*TT*UU) - (Pi^2*Log[SS]^2)/(2*SS*TT*UU) - 
   (2*Log[mts]*Log[SS]^2)/(SS^2*TT) - (5*Pi^2*Log[mts]*Log[SS]^2)/
    (3*SS^2*TT) - (2*Log[mts]*Log[SS]^2)/(SS^2*UU) - 
   (5*Pi^2*Log[mts]*Log[SS]^2)/(3*SS^2*UU) - (2*Log[mts]*Log[SS]^2)/
    (SS*TT*UU) - (Pi^2*Log[mts]*Log[SS]^2)/(2*SS*TT*UU) - 
   (Log[mts]^2*Log[SS]^2)/(2*SS^2*TT) - (Log[mts]^2*Log[SS]^2)/(2*SS^2*UU) - 
   (Log[mts]^2*Log[SS]^2)/(2*SS*TT*UU) - (16*Log[mts]^3*Log[SS]^2)/
    (3*SS^2*TT) - (16*Log[mts]^3*Log[SS]^2)/(3*SS^2*UU) - 
   (Log[mts]^3*Log[SS]^2)/(6*SS*TT*UU) - (2*Log[SS]^3)/(3*SS^2*TT) + 
   (5*Pi^2*Log[SS]^3)/(18*SS^2*TT) - (2*Log[SS]^3)/(3*SS^2*UU) + 
   (5*Pi^2*Log[SS]^3)/(18*SS^2*UU) + (2*Log[SS]^3)/(3*SS*TT*UU) + 
   (Pi^2*Log[SS]^3)/(6*SS*TT*UU) - (Log[mts]*Log[SS]^3)/(3*SS^2*TT) - 
   (Log[mts]*Log[SS]^3)/(3*SS^2*UU) + (Log[mts]*Log[SS]^3)/(3*SS*TT*UU) + 
   (8*Log[mts]^2*Log[SS]^3)/(3*SS^2*TT) + (8*Log[mts]^2*Log[SS]^3)/
    (3*SS^2*UU) + (Log[mts]^2*Log[SS]^3)/(6*SS*TT*UU) - 
   Log[SS]^4/(12*SS^2*TT) - Log[SS]^4/(12*SS^2*UU) - 
   Log[SS]^4/(12*SS*TT*UU) - (2*Log[mts]*Log[SS]^4)/(3*SS^2*TT) - 
   (2*Log[mts]*Log[SS]^4)/(3*SS^2*UU) - (Log[mts]*Log[SS]^4)/(12*SS*TT*UU) + 
   Log[SS]^5/(15*SS^2*TT) + Log[SS]^5/(15*SS^2*UU) + 
   Log[SS]^5/(60*SS*TT*UU) - (16*Log[TT])/(SS^2*TT) - 
   (2*Pi^2*Log[TT])/(SS^2*TT) + (7*Pi^4*Log[TT])/(80*SS^2*TT) + 
   (16*Log[TT])/(SS^2*UU) + (2*Pi^2*Log[TT])/(SS^2*UU) - 
   (7*Pi^4*Log[TT])/(80*SS^2*UU) - (16*Log[TT])/(SS*TT*UU) - 
   (2*Pi^2*Log[TT])/(SS*TT*UU) - (103*Pi^4*Log[TT])/(120*SS*TT*UU) - 
   (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Log[TT])/TT - (8*Log[mts]*Log[TT])/(SS^2*TT) - 
   (Pi^2*Log[mts]*Log[TT])/(SS^2*TT) + (8*Log[mts]*Log[TT])/(SS^2*UU) + 
   (Pi^2*Log[mts]*Log[TT])/(SS^2*UU) - (8*Log[mts]*Log[TT])/(SS*TT*UU) - 
   (Pi^2*Log[mts]*Log[TT])/(SS*TT*UU) - (2*Log[mts]^2*Log[TT])/(SS^2*TT) + 
   (Pi^2*Log[mts]^2*Log[TT])/(2*SS^2*TT) + (2*Log[mts]^2*Log[TT])/(SS^2*UU) - 
   (Pi^2*Log[mts]^2*Log[TT])/(2*SS^2*UU) - (2*Log[mts]^2*Log[TT])/
    (SS*TT*UU) - (3*Pi^2*Log[mts]^2*Log[TT])/(2*SS*TT*UU) - 
   (Log[mts]^3*Log[TT])/(3*SS^2*TT) + (Log[mts]^3*Log[TT])/(3*SS^2*UU) - 
   (Log[mts]^3*Log[TT])/(3*SS*TT*UU) + (Log[mts]^4*Log[TT])/(3*SS^2*TT) - 
   (Log[mts]^4*Log[TT])/(3*SS^2*UU) - (5*Log[mts]^4*Log[TT])/(6*SS*TT*UU) - 
   (8*Log[SS]*Log[TT])/(SS^2*TT) - (Pi^2*Log[SS]*Log[TT])/(SS^2*TT) + 
   (8*Log[SS]*Log[TT])/(SS^2*UU) + (Pi^2*Log[SS]*Log[TT])/(SS^2*UU) + 
   (8*Log[SS]*Log[TT])/(SS*TT*UU) + (Pi^2*Log[SS]*Log[TT])/(SS*TT*UU) - 
   (4*Log[mts]*Log[SS]*Log[TT])/(SS^2*TT) + (4*Log[mts]*Log[SS]*Log[TT])/
    (SS^2*UU) + (4*Log[mts]*Log[SS]*Log[TT])/(SS*TT*UU) + 
   (2*Pi^2*Log[mts]*Log[SS]*Log[TT])/(SS*TT*UU) - 
   (Log[mts]^2*Log[SS]*Log[TT])/(SS^2*TT) + (Log[mts]^2*Log[SS]*Log[TT])/
    (SS^2*UU) + (Log[mts]^2*Log[SS]*Log[TT])/(SS*TT*UU) - 
   (2*Log[SS]^2*Log[TT])/(SS^2*TT) + (2*Log[SS]^2*Log[TT])/(SS^2*UU) - 
   (2*Log[SS]^2*Log[TT])/(SS*TT*UU) - (Pi^2*Log[SS]^2*Log[TT])/(2*SS*TT*UU) - 
   (Log[mts]*Log[SS]^2*Log[TT])/(SS^2*TT) + (Log[mts]*Log[SS]^2*Log[TT])/
    (SS^2*UU) - (Log[mts]*Log[SS]^2*Log[TT])/(SS*TT*UU) - 
   (Log[mts]^2*Log[SS]^2*Log[TT])/(2*SS*TT*UU) - 
   (Log[SS]^3*Log[TT])/(3*SS^2*TT) + (Log[SS]^3*Log[TT])/(3*SS^2*UU) + 
   (Log[SS]^3*Log[TT])/(3*SS*TT*UU) + (Log[mts]*Log[SS]^3*Log[TT])/
    (3*SS*TT*UU) - (Log[SS]^4*Log[TT])/(12*SS*TT*UU) - 
   (4*Log[TT]^2)/(SS^2*TT) - (Pi^2*Log[TT]^2)/(2*SS^2*TT) - 
   (4*Log[TT]^2)/(SS^2*UU) - (Pi^2*Log[TT]^2)/(2*SS^2*UU) - 
   (4*Log[TT]^2)/(SS*TT*UU) - (Pi^2*Log[TT]^2)/(2*SS*TT*UU) - 
   (2*Log[mts]*Log[TT]^2)/(SS^2*TT) - (2*Log[mts]*Log[TT]^2)/(SS^2*UU) - 
   (2*Log[mts]*Log[TT]^2)/(SS*TT*UU) - (3*Pi^2*Log[mts]*Log[TT]^2)/
    (2*SS*TT*UU) - (Log[mts]^2*Log[TT]^2)/(2*SS^2*TT) - 
   (Log[mts]^2*Log[TT]^2)/(2*SS^2*UU) - (Log[mts]^2*Log[TT]^2)/(2*SS*TT*UU) + 
   (Log[mts]^3*Log[TT]^2)/(6*SS*TT*UU) - (2*Log[SS]*Log[TT]^2)/(SS^2*TT) - 
   (2*Log[SS]*Log[TT]^2)/(SS^2*UU) + (2*Log[SS]*Log[TT]^2)/(SS*TT*UU) + 
   (Pi^2*Log[SS]*Log[TT]^2)/(SS*TT*UU) - (Log[mts]*Log[SS]*Log[TT]^2)/
    (SS^2*TT) - (Log[mts]*Log[SS]*Log[TT]^2)/(SS^2*UU) + 
   (Log[mts]*Log[SS]*Log[TT]^2)/(SS*TT*UU) + (Log[mts]^2*Log[SS]*Log[TT]^2)/
    (2*SS*TT*UU) - (Log[SS]^2*Log[TT]^2)/(2*SS^2*TT) - 
   (Log[SS]^2*Log[TT]^2)/(2*SS^2*UU) - (Log[SS]^2*Log[TT]^2)/(2*SS*TT*UU) - 
   (Log[mts]*Log[SS]^2*Log[TT]^2)/(2*SS*TT*UU) + 
   (Log[SS]^3*Log[TT]^2)/(6*SS*TT*UU) - (2*Log[TT]^3)/(3*SS^2*TT) + 
   (2*Log[TT]^3)/(3*SS^2*UU) - (2*Log[TT]^3)/(3*SS*TT*UU) - 
   (5*Pi^2*Log[TT]^3)/(6*SS*TT*UU) - (Log[mts]*Log[TT]^3)/(3*SS^2*TT) + 
   (Log[mts]*Log[TT]^3)/(3*SS^2*UU) - (Log[mts]*Log[TT]^3)/(3*SS*TT*UU) - 
   (Log[SS]*Log[TT]^3)/(3*SS^2*TT) + (Log[SS]*Log[TT]^3)/(3*SS^2*UU) + 
   (Log[SS]*Log[TT]^3)/(3*SS*TT*UU) + (2*Log[mts]*Log[SS]*Log[TT]^3)/
    (3*SS*TT*UU) - (Log[SS]^2*Log[TT]^3)/(6*SS*TT*UU) - 
   Log[TT]^4/(12*SS^2*TT) - Log[TT]^4/(12*SS^2*UU) - 
   Log[TT]^4/(12*SS*TT*UU) - (5*Log[mts]*Log[TT]^4)/(12*SS*TT*UU) + 
   (Log[SS]*Log[TT]^4)/(4*SS*TT*UU) - (7*Log[TT]^5)/(30*SS*TT*UU) + 
   (16*Log[UU])/(SS^2*TT) + (2*Pi^2*Log[UU])/(SS^2*TT) - 
   (7*Pi^4*Log[UU])/(80*SS^2*TT) - (16*Log[UU])/(SS^2*UU) - 
   (2*Pi^2*Log[UU])/(SS^2*UU) + (7*Pi^4*Log[UU])/(80*SS^2*UU) - 
   (16*Log[UU])/(SS*TT*UU) - (2*Pi^2*Log[UU])/(SS*TT*UU) - 
   (29*Pi^4*Log[UU])/(40*SS*TT*UU) - (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Log[UU])/UU + 
   (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Log[UU])/SS^2 + (2*EulerGamma*TT^Z1*UU^(-2 - Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Log[UU])/SS + 
   (8*Log[mts]*Log[UU])/(SS^2*TT) + (Pi^2*Log[mts]*Log[UU])/(SS^2*TT) - 
   (8*Log[mts]*Log[UU])/(SS^2*UU) - (Pi^2*Log[mts]*Log[UU])/(SS^2*UU) - 
   (8*Log[mts]*Log[UU])/(SS*TT*UU) - (Pi^2*Log[mts]*Log[UU])/(SS*TT*UU) + 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]*Log[UU])/SS + (2*Log[mts]^2*Log[UU])/(SS^2*TT) - 
   (Pi^2*Log[mts]^2*Log[UU])/(2*SS^2*TT) - (2*Log[mts]^2*Log[UU])/(SS^2*UU) + 
   (Pi^2*Log[mts]^2*Log[UU])/(2*SS^2*UU) - (2*Log[mts]^2*Log[UU])/
    (SS*TT*UU) - (2*Pi^2*Log[mts]^2*Log[UU])/(SS*TT*UU) + 
   (Log[mts]^3*Log[UU])/(3*SS^2*TT) - (Log[mts]^3*Log[UU])/(3*SS^2*UU) - 
   (Log[mts]^3*Log[UU])/(3*SS*TT*UU) - (Log[mts]^4*Log[UU])/(3*SS^2*TT) + 
   (Log[mts]^4*Log[UU])/(3*SS^2*UU) - (5*Log[mts]^4*Log[UU])/(6*SS*TT*UU) + 
   (8*Log[SS]*Log[UU])/(SS^2*TT) + (Pi^2*Log[SS]*Log[UU])/(SS^2*TT) - 
   (8*Log[SS]*Log[UU])/(SS^2*UU) - (Pi^2*Log[SS]*Log[UU])/(SS^2*UU) + 
   (8*Log[SS]*Log[UU])/(SS*TT*UU) + (Pi^2*Log[SS]*Log[UU])/(SS*TT*UU) - 
   (2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[SS]*Log[UU])/SS + (4*Log[mts]*Log[SS]*Log[UU])/
    (SS^2*TT) - (4*Log[mts]*Log[SS]*Log[UU])/(SS^2*UU) + 
   (4*Log[mts]*Log[SS]*Log[UU])/(SS*TT*UU) + (Pi^2*Log[mts]*Log[SS]*Log[UU])/
    (SS*TT*UU) + (Log[mts]^2*Log[SS]*Log[UU])/(SS^2*TT) - 
   (Log[mts]^2*Log[SS]*Log[UU])/(SS^2*UU) + (Log[mts]^2*Log[SS]*Log[UU])/
    (SS*TT*UU) + (2*Log[SS]^2*Log[UU])/(SS^2*TT) - 
   (2*Log[SS]^2*Log[UU])/(SS^2*UU) - (2*Log[SS]^2*Log[UU])/(SS*TT*UU) - 
   (Pi^2*Log[SS]^2*Log[UU])/(2*SS*TT*UU) + (Log[mts]*Log[SS]^2*Log[UU])/
    (SS^2*TT) - (Log[mts]*Log[SS]^2*Log[UU])/(SS^2*UU) - 
   (Log[mts]*Log[SS]^2*Log[UU])/(SS*TT*UU) - (Log[mts]^2*Log[SS]^2*Log[UU])/
    (2*SS*TT*UU) + (Log[SS]^3*Log[UU])/(3*SS^2*TT) - 
   (Log[SS]^3*Log[UU])/(3*SS^2*UU) + (Log[SS]^3*Log[UU])/(3*SS*TT*UU) + 
   (Log[mts]*Log[SS]^3*Log[UU])/(3*SS*TT*UU) - (Log[SS]^4*Log[UU])/
    (12*SS*TT*UU) + (8*Log[TT]*Log[UU])/(SS^2*TT) + 
   (Pi^2*Log[TT]*Log[UU])/(SS^2*TT) + (8*Log[TT]*Log[UU])/(SS^2*UU) + 
   (Pi^2*Log[TT]*Log[UU])/(SS^2*UU) - (8*Log[TT]*Log[UU])/(SS*TT*UU) - 
   (Pi^2*Log[TT]*Log[UU])/(SS*TT*UU) + (4*Log[mts]*Log[TT]*Log[UU])/
    (SS^2*TT) + (4*Log[mts]*Log[TT]*Log[UU])/(SS^2*UU) - 
   (4*Log[mts]*Log[TT]*Log[UU])/(SS*TT*UU) - 
   (2*Pi^2*Log[mts]*Log[TT]*Log[UU])/(SS*TT*UU) + 
   (Log[mts]^2*Log[TT]*Log[UU])/(SS^2*TT) + (Log[mts]^2*Log[TT]*Log[UU])/
    (SS^2*UU) - (Log[mts]^2*Log[TT]*Log[UU])/(SS*TT*UU) - 
   (Log[mts]^3*Log[TT]*Log[UU])/(3*SS*TT*UU) + (4*Log[SS]*Log[TT]*Log[UU])/
    (SS^2*TT) + (4*Log[SS]*Log[TT]*Log[UU])/(SS^2*UU) + 
   (4*Log[SS]*Log[TT]*Log[UU])/(SS*TT*UU) + (Pi^2*Log[SS]*Log[TT]*Log[UU])/
    (SS*TT*UU) + (2*Log[mts]*Log[SS]*Log[TT]*Log[UU])/(SS^2*TT) + 
   (2*Log[mts]*Log[SS]*Log[TT]*Log[UU])/(SS^2*UU) + 
   (2*Log[mts]*Log[SS]*Log[TT]*Log[UU])/(SS*TT*UU) + 
   (Log[SS]^2*Log[TT]*Log[UU])/(SS^2*TT) + (Log[SS]^2*Log[TT]*Log[UU])/
    (SS^2*UU) - (Log[SS]^2*Log[TT]*Log[UU])/(SS*TT*UU) - 
   (Log[mts]*Log[SS]^2*Log[TT]*Log[UU])/(SS*TT*UU) + 
   (Log[SS]^3*Log[TT]*Log[UU])/(3*SS*TT*UU) + (2*Log[TT]^2*Log[UU])/
    (SS^2*TT) - (2*Log[TT]^2*Log[UU])/(SS^2*UU) - 
   (2*Log[TT]^2*Log[UU])/(SS*TT*UU) - (Pi^2*Log[TT]^2*Log[UU])/(SS*TT*UU) + 
   (Log[mts]*Log[TT]^2*Log[UU])/(SS^2*TT) - (Log[mts]*Log[TT]^2*Log[UU])/
    (SS^2*UU) - (Log[mts]*Log[TT]^2*Log[UU])/(SS*TT*UU) + 
   (Log[SS]*Log[TT]^2*Log[UU])/(SS^2*TT) - (Log[SS]*Log[TT]^2*Log[UU])/
    (SS^2*UU) + (Log[SS]*Log[TT]^2*Log[UU])/(SS*TT*UU) - 
   (Log[SS]^2*Log[TT]^2*Log[UU])/(2*SS*TT*UU) + 
   (Log[TT]^3*Log[UU])/(3*SS^2*TT) + (Log[TT]^3*Log[UU])/(3*SS^2*UU) - 
   (Log[TT]^3*Log[UU])/(3*SS*TT*UU) + (Log[mts]*Log[TT]^3*Log[UU])/
    (3*SS*TT*UU) + (Log[TT]^4*Log[UU])/(6*SS*TT*UU) - 
   (4*Log[UU]^2)/(SS^2*TT) - (Pi^2*Log[UU]^2)/(2*SS^2*TT) - 
   (4*Log[UU]^2)/(SS^2*UU) - (Pi^2*Log[UU]^2)/(2*SS^2*UU) - 
   (4*Log[UU]^2)/(SS*TT*UU) - (Pi^2*Log[UU]^2)/(2*SS*TT*UU) + 
   (5*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[UU]^2)/SS - (2*Log[mts]*Log[UU]^2)/(SS^2*TT) - 
   (2*Log[mts]*Log[UU]^2)/(SS^2*UU) - (2*Log[mts]*Log[UU]^2)/(SS*TT*UU) - 
   (Pi^2*Log[mts]*Log[UU]^2)/(2*SS*TT*UU) - (Log[mts]^2*Log[UU]^2)/
    (2*SS^2*TT) - (Log[mts]^2*Log[UU]^2)/(2*SS^2*UU) - 
   (Log[mts]^2*Log[UU]^2)/(2*SS*TT*UU) + (Log[mts]^3*Log[UU]^2)/
    (6*SS*TT*UU) - (2*Log[SS]*Log[UU]^2)/(SS^2*TT) - 
   (2*Log[SS]*Log[UU]^2)/(SS^2*UU) + (2*Log[SS]*Log[UU]^2)/(SS*TT*UU) + 
   (Pi^2*Log[SS]*Log[UU]^2)/(2*SS*TT*UU) - (Log[mts]*Log[SS]*Log[UU]^2)/
    (SS^2*TT) - (Log[mts]*Log[SS]*Log[UU]^2)/(SS^2*UU) + 
   (Log[mts]*Log[SS]*Log[UU]^2)/(SS*TT*UU) + (Log[mts]^2*Log[SS]*Log[UU]^2)/
    (2*SS*TT*UU) - (Log[SS]^2*Log[UU]^2)/(2*SS^2*TT) - 
   (Log[SS]^2*Log[UU]^2)/(2*SS^2*UU) - (Log[SS]^2*Log[UU]^2)/(2*SS*TT*UU) - 
   (Log[mts]*Log[SS]^2*Log[UU]^2)/(2*SS*TT*UU) + 
   (Log[SS]^3*Log[UU]^2)/(6*SS*TT*UU) - (2*Log[TT]*Log[UU]^2)/(SS^2*TT) + 
   (2*Log[TT]*Log[UU]^2)/(SS^2*UU) - (2*Log[TT]*Log[UU]^2)/(SS*TT*UU) - 
   (Pi^2*Log[TT]*Log[UU]^2)/(2*SS*TT*UU) - (Log[mts]*Log[TT]*Log[UU]^2)/
    (SS^2*TT) + (Log[mts]*Log[TT]*Log[UU]^2)/(SS^2*UU) - 
   (Log[mts]*Log[TT]*Log[UU]^2)/(SS*TT*UU) + (Log[mts]^2*Log[TT]*Log[UU]^2)/
    (2*SS*TT*UU) - (Log[SS]*Log[TT]*Log[UU]^2)/(SS^2*TT) + 
   (Log[SS]*Log[TT]*Log[UU]^2)/(SS^2*UU) + (Log[SS]*Log[TT]*Log[UU]^2)/
    (SS*TT*UU) + (Log[mts]*Log[SS]*Log[TT]*Log[UU]^2)/(SS*TT*UU) - 
   (Log[SS]^2*Log[TT]*Log[UU]^2)/(2*SS*TT*UU) - (Log[TT]^2*Log[UU]^2)/
    (2*SS^2*TT) - (Log[TT]^2*Log[UU]^2)/(2*SS^2*UU) - 
   (Log[TT]^2*Log[UU]^2)/(2*SS*TT*UU) + (Log[mts]*Log[TT]^2*Log[UU]^2)/
    (2*SS*TT*UU) + (Log[SS]*Log[TT]^2*Log[UU]^2)/(2*SS*TT*UU) + 
   (Log[TT]^3*Log[UU]^2)/(6*SS*TT*UU) + (2*Log[UU]^3)/(3*SS^2*TT) - 
   (2*Log[UU]^3)/(3*SS^2*UU) - (2*Log[UU]^3)/(3*SS*TT*UU) - 
   (Pi^2*Log[UU]^3)/(6*SS*TT*UU) + (Log[mts]*Log[UU]^3)/(3*SS^2*TT) - 
   (Log[mts]*Log[UU]^3)/(3*SS^2*UU) - (Log[mts]*Log[UU]^3)/(3*SS*TT*UU) - 
   (Log[mts]^2*Log[UU]^3)/(6*SS*TT*UU) + (Log[SS]*Log[UU]^3)/(3*SS^2*TT) - 
   (Log[SS]*Log[UU]^3)/(3*SS^2*UU) + (Log[SS]*Log[UU]^3)/(3*SS*TT*UU) + 
   (Log[mts]*Log[SS]*Log[UU]^3)/(3*SS*TT*UU) - (Log[SS]^2*Log[UU]^3)/
    (6*SS*TT*UU) + (Log[TT]*Log[UU]^3)/(3*SS^2*TT) + 
   (Log[TT]*Log[UU]^3)/(3*SS^2*UU) - (Log[TT]*Log[UU]^3)/(3*SS*TT*UU) - 
   (Log[mts]*Log[TT]*Log[UU]^3)/(3*SS*TT*UU) + (Log[SS]*Log[TT]*Log[UU]^3)/
    (3*SS*TT*UU) - (Log[TT]^2*Log[UU]^3)/(6*SS*TT*UU) - 
   Log[UU]^4/(12*SS^2*TT) - Log[UU]^4/(12*SS^2*UU) - 
   Log[UU]^4/(12*SS*TT*UU) - (Log[mts]*Log[UU]^4)/(12*SS*TT*UU) + 
   (Log[SS]*Log[UU]^4)/(12*SS*TT*UU) - (Log[TT]*Log[UU]^4)/(12*SS*TT*UU) - 
   Log[UU]^5/(60*SS*TT*UU) - (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*PolyGamma[0, -1 - Z1])/UU + 
   (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*PolyGamma[0, -1 - Z1])/SS^2 + 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]*PolyGamma[0, -1 - Z1])/SS - 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[SS]*PolyGamma[0, -1 - Z1])/SS + 
   (16*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[UU]*PolyGamma[0, -1 - Z1])/SS + 
   (12*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[0, -1 - Z1]^2)/SS + 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1])/
    (SS^2*TT) + (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, 1 - Z1])/(SS^2*UU) - 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1])/
    (SS*TT*UU) - (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, 1 - Z1])/(SS^2*TT) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, 1 - Z1])/(SS^2*UU) + 
   (16*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, 1 - Z1])/(SS*TT*UU) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]^2)/
    (2*SS^2*TT) - (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, 1 - Z1]^2)/(2*SS^2*UU) - 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]^2)/
    (SS*TT*UU) + (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, -1 + Z1])/(SS^2*TT) + 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, -1 + Z1])/
    (SS^2*UU) + (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, -1 + Z1])/(SS*TT*UU) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, -1 + Z1])/(SS^2*TT) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, -1 + Z1])/(SS^2*UU) - 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, -1 + Z1])/(SS*TT*UU) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]*
     PolyGamma[0, -1 + Z1])/(SS^2*TT) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]*
     PolyGamma[0, -1 + Z1])/(SS^2*UU) + 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]*
     PolyGamma[0, -1 + Z1])/(SS*TT*UU) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, -1 + Z1]^2)/
    (2*SS^2*TT) - (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, -1 + Z1]^2)/(2*SS^2*UU) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, -1 + Z1]^2)/
    (SS*TT*UU) + (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*PolyGamma[0, -Z1])/SS^2 - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/(SS^2*TT) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/(SS^2*UU) + 
   (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1])/
    (SS*TT*UU) - (4*SS^(-2 - Z1)*TT^Z1*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*PolyGamma[0, Z1])/UU + 
   (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*PolyGamma[0, Z1])/
    (SS^2*TT) + (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*Log[mts]*
     PolyGamma[0, Z1])/(SS^2*UU) - (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*Log[mts]*PolyGamma[0, Z1])/(SS*TT*UU) + 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]*
     PolyGamma[0, Z1])/(SS^2*TT) + (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*PolyGamma[0, 1 - Z1]*PolyGamma[0, Z1])/(SS^2*UU) + 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, 1 - Z1]*
     PolyGamma[0, Z1])/(SS*TT*UU) + (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*PolyGamma[0, -1 + Z1]*PolyGamma[0, Z1])/(SS^2*TT) + 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, -1 + Z1]*
     PolyGamma[0, Z1])/(SS^2*UU) - (4*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*PolyGamma[0, -1 + Z1]*PolyGamma[0, Z1])/(SS*TT*UU) - 
   (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[0, Z1]^2)/
    (SS^2*TT) - (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[0, Z1]^2)/(SS^2*UU) - (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*
     Gamma[-Z1]*PolyGamma[0, Z1]^2)/(SS*TT*UU) - 
   (4*TT^Z1*UU^(-1 - Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*PolyGamma[0, 1 + Z1])/SS^2 - 
   (4*EulerGamma*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*PolyGamma[0, 1 + Z1])/SS - 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]*PolyGamma[0, 1 + Z1])/SS - 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[UU]*PolyGamma[0, 1 + Z1])/SS - 
   (8*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1])/SS - 
   (2*EulerGamma*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*PolyGamma[0, 2 + Z1])/SS - 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[mts]*PolyGamma[0, 2 + Z1])/SS + 
   (2*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[SS]*PolyGamma[0, 2 + Z1])/SS - 
   (10*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Log[UU]*PolyGamma[0, 2 + Z1])/SS - 
   (16*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1])/SS + 
   (4*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[0, 1 + Z1]*PolyGamma[0, 2 + Z1])/SS + 
   (5*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[0, 2 + Z1]^2)/SS - 
   (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*PolyGamma[0, -1 - Z2])/TT - 
   (4*SS^(-2 - Z2)*UU^Z2*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*PolyGamma[0, Z2])/TT + 
   (6*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*PolyGamma[1, -1 - Z1])/SS - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[1, 1 - Z1])/
    (2*SS^2*TT) - (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[1, 1 - Z1])/(2*SS^2*UU) - 
   (8*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[1, 1 - Z1])/
    (SS*TT*UU) - (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[1, -1 + Z1])/(2*SS^2*TT) - 
   (Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[1, -1 + Z1])/
    (2*SS^2*UU) - (2*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*
     PolyGamma[1, -1 + Z1])/(SS*TT*UU) - 
   (6*Gamma[1 - Z1]*Gamma[-1 + Z1]^2*Gamma[-Z1]*PolyGamma[1, Z1])/
    (SS*TT*UU) + (5*TT^Z1*UU^(-2 - Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*PolyGamma[1, 2 + Z1])/SS - 
   (8*Zeta[3])/(3*SS^2*TT) - (65*Pi^2*Zeta[3])/(36*SS^2*TT) - 
   (8*Zeta[3])/(3*SS^2*UU) - (65*Pi^2*Zeta[3])/(36*SS^2*UU) - 
   (8*Zeta[3])/(3*SS*TT*UU) - (14*Pi^2*Zeta[3])/(3*SS*TT*UU) + 
   (4*Log[mts]*Zeta[3])/(3*SS^2*TT) + (4*Log[mts]*Zeta[3])/(3*SS^2*UU) + 
   (4*Log[mts]*Zeta[3])/(3*SS*TT*UU) - (10*Log[mts]^2*Zeta[3])/(SS^2*TT) - 
   (10*Log[mts]^2*Zeta[3])/(SS^2*UU) - (9*Log[mts]^2*Zeta[3])/(SS*TT*UU) - 
   (4*Log[SS]*Zeta[3])/(3*SS^2*TT) - (4*Log[SS]*Zeta[3])/(3*SS^2*UU) + 
   (4*Log[SS]*Zeta[3])/(3*SS*TT*UU) + (12*Log[mts]*Log[SS]*Zeta[3])/
    (SS^2*TT) + (12*Log[mts]*Log[SS]*Zeta[3])/(SS^2*UU) - 
   (6*Log[mts]*Log[SS]*Zeta[3])/(SS*TT*UU) - (7*Log[SS]^2*Zeta[3])/
    (3*SS^2*TT) - (7*Log[SS]^2*Zeta[3])/(3*SS^2*UU) - 
   (2*Log[SS]^2*Zeta[3])/(3*SS*TT*UU) - (4*Log[TT]*Zeta[3])/(3*SS^2*TT) + 
   (4*Log[TT]*Zeta[3])/(3*SS^2*UU) - (4*Log[TT]*Zeta[3])/(3*SS*TT*UU) + 
   (8*Log[mts]*Log[TT]*Zeta[3])/(3*SS^2*TT) - (8*Log[mts]*Log[TT]*Zeta[3])/
    (3*SS^2*UU) + (2*Log[mts]*Log[TT]*Zeta[3])/(SS*TT*UU) - 
   (6*Log[SS]*Log[TT]*Zeta[3])/(SS*TT*UU) + (17*Log[TT]^2*Zeta[3])/
    (3*SS*TT*UU) + (4*Log[UU]*Zeta[3])/(3*SS^2*TT) - 
   (4*Log[UU]*Zeta[3])/(3*SS^2*UU) - (4*Log[UU]*Zeta[3])/(3*SS*TT*UU) - 
   (8*Log[mts]*Log[UU]*Zeta[3])/(3*SS^2*TT) + (8*Log[mts]*Log[UU]*Zeta[3])/
    (3*SS^2*UU) + (6*Log[mts]*Log[UU]*Zeta[3])/(SS*TT*UU) - 
   (4*Log[SS]*Log[UU]*Zeta[3])/(SS*TT*UU) + (50*Log[TT]*Log[UU]*Zeta[3])/
    (3*SS*TT*UU) + (14*Log[UU]^2*Zeta[3])/(3*SS*TT*UU) + 
   ((-28*Pi^4*Log[2])/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (256*Pi^2*Log[2]^3)/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (7*Pi^4*Log[mts])/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (64*Pi^2*Log[2]^2*Log[mts])/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (16*Pi^2*Log[2]*Log[mts]^2)/(SS^(3/2)*Sqrt[TT]*Sqrt[UU]) - 
     (4*Pi^2*Log[mts]^3)/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + 
     (5*Pi^2*PolyGamma[2, 1/2])/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]) + 
     (2*Pi^2*Zeta[3])/(3*SS^(3/2)*Sqrt[TT]*Sqrt[UU]))/Sqrt[mts] - 
   (53*Zeta[5])/(10*SS^2*TT) - (53*Zeta[5])/(10*SS^2*UU) + 
   (6*Zeta[5])/(SS*TT*UU))
