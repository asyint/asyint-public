mus^(2*ep)*s^(-3 - 2*ep)*((16*s)/TT + (4*s)/(ep^2*TT) - (8*s)/(ep*TT) - 
  (32*ep*s)/TT - (8*im*Pi*s)/TT + (im*Pi*s)/(ep^2*TT) + (4*im*Pi*s)/(ep*TT) + 
  (16*ep*im*Pi*s)/TT - (7*Pi^2*s)/(6*ep^2*TT) - (11*Pi^2*s)/(4*ep*TT) - 
  (2*im*Pi^3*s)/TT - (im*Pi^3*s)/(ep*TT) + (107*Pi^4*s)/(240*TT) + 
  (277*ep*Pi^4*s)/(240*TT) + (13*ep*im*Pi^5*s)/(360*TT) + 16/(1 - TT/s) + 
  4/(ep^2*(1 - TT/s)) - 8/(ep*(1 - TT/s)) - (32*ep)/(1 - TT/s) - 
  (8*im*Pi)/(1 - TT/s) + (im*Pi)/(ep^2*(1 - TT/s)) + 
  (4*im*Pi)/(ep*(1 - TT/s)) + (16*ep*im*Pi)/(1 - TT/s) - 
  (7*Pi^2)/(6*ep^2*(1 - TT/s)) - (11*Pi^2)/(4*ep*(1 - TT/s)) - 
  (4*im*Pi^3)/(3*(1 - TT/s)) - (im*Pi^3)/(ep*(1 - TT/s)) - 
  (4*ep*im*Pi^3)/(3*(1 - TT/s)) + (107*Pi^4)/(240*(1 - TT/s)) + 
  (293*ep*Pi^4)/(240*(1 - TT/s)) + (13*ep*im*Pi^5)/(360*(1 - TT/s)) - 
  (16*s)/(TT*(1 - TT/s)) - (4*s)/(ep^2*TT*(1 - TT/s)) + 
  (8*s)/(ep*TT*(1 - TT/s)) + (32*ep*s)/(TT*(1 - TT/s)) - 
  (8*im*Pi*s)/(TT*(1 - TT/s)) - (5*im*Pi*s)/(ep^2*TT*(1 - TT/s)) + 
  (4*im*Pi*s)/(ep*TT*(1 - TT/s)) + (16*ep*im*Pi*s)/(TT*(1 - TT/s)) + 
  (17*Pi^2*s)/(4*ep^2*TT*(1 - TT/s)) + (11*Pi^2*s)/(4*ep*TT*(1 - TT/s)) + 
  (2*im*Pi^3*s)/(TT*(1 - TT/s)) + (11*im*Pi^3*s)/(2*ep*TT*(1 - TT/s)) - 
  (427*Pi^4*s)/(240*TT*(1 - TT/s)) - (277*ep*Pi^4*s)/(240*TT*(1 - TT/s)) - 
  (17*ep*im*Pi^5*s)/(360*TT*(1 - TT/s)) - 
  (im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s])/(ep^2*mts*TT*Sqrt[1 - TT/s]) - 
  (7*im*Pi^4*Sqrt[mts/s]*s^2*Sqrt[TT/s])/(6*mts*TT*Sqrt[1 - TT/s]) - 
  (2*s)/(ep^4*TT*(-1 + TT/s)) + (2*im*Pi*s)/(ep^3*TT*(-1 + TT/s)) + 
  (4*Pi^2*s)/(ep^2*TT*(-1 + TT/s)) + (8*im*Pi^3*s)/(3*ep*TT*(-1 + TT/s)) - 
  (4*Pi^4*s)/(3*TT*(-1 + TT/s)) - (8*ep*im*Pi^5*s)/(15*TT*(-1 + TT/s)) + 
  (8*s*H[0, mts/s])/TT + (s*H[0, mts/s])/(ep^2*TT) - 
  (4*s*H[0, mts/s])/(ep*TT) - (16*ep*s*H[0, mts/s])/TT - 
  (4*im*Pi*s*H[0, mts/s])/TT + (3*im*Pi*s*H[0, mts/s])/(ep^2*TT) + 
  (2*im*Pi*s*H[0, mts/s])/(ep*TT) + (8*ep*im*Pi*s*H[0, mts/s])/TT - 
  (Pi^2*s*H[0, mts/s])/(2*TT) + (2*Pi^2*s*H[0, mts/s])/(3*ep*TT) + 
  (5*im*Pi^3*s*H[0, mts/s])/(6*TT) - (ep*Pi^4*s*H[0, mts/s])/(30*TT) + 
  (8*H[0, mts/s])/(1 - TT/s) + H[0, mts/s]/(ep^2*(1 - TT/s)) - 
  (4*H[0, mts/s])/(ep*(1 - TT/s)) - (16*ep*H[0, mts/s])/(1 - TT/s) - 
  (4*im*Pi*H[0, mts/s])/(1 - TT/s) + (3*im*Pi*H[0, mts/s])/
   (ep^2*(1 - TT/s)) + (2*im*Pi*H[0, mts/s])/(ep*(1 - TT/s)) + 
  (8*ep*im*Pi*H[0, mts/s])/(1 - TT/s) - (Pi^2*H[0, mts/s])/(2*(1 - TT/s)) + 
  (2*Pi^2*H[0, mts/s])/(3*ep*(1 - TT/s)) + (5*im*Pi^3*H[0, mts/s])/
   (6*(1 - TT/s)) - (2*ep*im*Pi^3*H[0, mts/s])/(3*(1 - TT/s)) - 
  (ep*Pi^4*H[0, mts/s])/(30*(1 - TT/s)) - (8*s*H[0, mts/s])/(TT*(1 - TT/s)) - 
  (s*H[0, mts/s])/(ep^2*TT*(1 - TT/s)) + (4*s*H[0, mts/s])/
   (ep*TT*(1 - TT/s)) + (16*ep*s*H[0, mts/s])/(TT*(1 - TT/s)) - 
  (4*im*Pi*s*H[0, mts/s])/(TT*(1 - TT/s)) + (2*im*Pi*s*H[0, mts/s])/
   (ep*TT*(1 - TT/s)) + (8*ep*im*Pi*s*H[0, mts/s])/(TT*(1 - TT/s)) + 
  (Pi^2*s*H[0, mts/s])/(2*TT*(1 - TT/s)) - (Pi^2*s*H[0, mts/s])/
   (ep*TT*(1 - TT/s)) - (8*im*Pi^3*s*H[0, mts/s])/(3*TT*(1 - TT/s)) - 
  (5*ep*Pi^4*s*H[0, mts/s])/(12*TT*(1 - TT/s)) + 
  (2*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s]*H[0, mts/s])/
   (ep*mts*TT*Sqrt[1 - TT/s]) + (7*ep*im*Pi^4*Sqrt[mts/s]*s^2*Sqrt[TT/s]*
    H[0, mts/s])/(3*mts*TT*Sqrt[1 - TT/s]) + (2*s*H[0, mts/s]^2)/TT + 
  (3*s*H[0, mts/s]^2)/(2*ep^2*TT) - (4*ep*s*H[0, mts/s]^2)/TT - 
  (im*Pi*s*H[0, mts/s]^2)/TT - (5*im*Pi*s*H[0, mts/s]^2)/(ep*TT) + 
  (2*ep*im*Pi*s*H[0, mts/s]^2)/TT - (19*Pi^2*s*H[0, mts/s]^2)/(12*TT) + 
  (ep*Pi^2*s*H[0, mts/s]^2)/(2*TT) - (7*ep*im*Pi^3*s*H[0, mts/s]^2)/(6*TT) + 
  (2*H[0, mts/s]^2)/(1 - TT/s) + (3*H[0, mts/s]^2)/(2*ep^2*(1 - TT/s)) - 
  (4*ep*H[0, mts/s]^2)/(1 - TT/s) - (im*Pi*H[0, mts/s]^2)/(1 - TT/s) - 
  (5*im*Pi*H[0, mts/s]^2)/(ep*(1 - TT/s)) + (2*ep*im*Pi*H[0, mts/s]^2)/
   (1 - TT/s) - (19*Pi^2*H[0, mts/s]^2)/(12*(1 - TT/s)) + 
  (ep*Pi^2*H[0, mts/s]^2)/(2*(1 - TT/s)) - (7*ep*im*Pi^3*H[0, mts/s]^2)/
   (6*(1 - TT/s)) - (2*s*H[0, mts/s]^2)/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, mts/s]^2)/(TT*(1 - TT/s)) - (im*Pi*s*H[0, mts/s]^2)/
   (TT*(1 - TT/s)) + (im*Pi*s*H[0, mts/s]^2)/(ep*TT*(1 - TT/s)) + 
  (2*ep*im*Pi*s*H[0, mts/s]^2)/(TT*(1 - TT/s)) + 
  (2*Pi^2*s*H[0, mts/s]^2)/(TT*(1 - TT/s)) - (ep*Pi^2*s*H[0, mts/s]^2)/
   (2*TT*(1 - TT/s)) + (11*ep*im*Pi^3*s*H[0, mts/s]^2)/(6*TT*(1 - TT/s)) - 
  (2*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s]*H[0, mts/s]^2)/
   (mts*TT*Sqrt[1 - TT/s]) - (s*H[0, mts/s]^3)/(3*TT) - 
  (8*s*H[0, mts/s]^3)/(3*ep*TT) - (2*ep*s*H[0, mts/s]^3)/(3*TT) + 
  (6*im*Pi*s*H[0, mts/s]^3)/TT + (ep*im*Pi*s*H[0, mts/s]^3)/(3*TT) + 
  (22*ep*Pi^2*s*H[0, mts/s]^3)/(9*TT) - H[0, mts/s]^3/(3*(1 - TT/s)) - 
  (8*H[0, mts/s]^3)/(3*ep*(1 - TT/s)) - (2*ep*H[0, mts/s]^3)/(3*(1 - TT/s)) + 
  (6*im*Pi*H[0, mts/s]^3)/(1 - TT/s) + (ep*im*Pi*H[0, mts/s]^3)/
   (3*(1 - TT/s)) + (22*ep*Pi^2*H[0, mts/s]^3)/(9*(1 - TT/s)) + 
  (s*H[0, mts/s]^3)/(3*TT*(1 - TT/s)) - (s*H[0, mts/s]^3)/
   (ep*TT*(1 - TT/s)) + (2*ep*s*H[0, mts/s]^3)/(3*TT*(1 - TT/s)) - 
  (im*Pi*s*H[0, mts/s]^3)/(TT*(1 - TT/s)) + (ep*im*Pi*s*H[0, mts/s]^3)/
   (3*TT*(1 - TT/s)) - (5*ep*Pi^2*s*H[0, mts/s]^3)/(3*TT*(1 - TT/s)) + 
  (4*ep*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s]*H[0, mts/s]^3)/
   (3*mts*TT*Sqrt[1 - TT/s]) + (17*s*H[0, mts/s]^4)/(6*TT) + 
  (ep*s*H[0, mts/s]^4)/(4*TT) - (17*ep*im*Pi*s*H[0, mts/s]^4)/(3*TT) + 
  (17*H[0, mts/s]^4)/(6*(1 - TT/s)) + (ep*H[0, mts/s]^4)/(4*(1 - TT/s)) - 
  (17*ep*im*Pi*H[0, mts/s]^4)/(3*(1 - TT/s)) + 
  (13*s*H[0, mts/s]^4)/(12*TT*(1 - TT/s)) - (ep*s*H[0, mts/s]^4)/
   (4*TT*(1 - TT/s)) + (7*ep*im*Pi*s*H[0, mts/s]^4)/(12*TT*(1 - TT/s)) - 
  (34*ep*s*H[0, mts/s]^5)/(15*TT) - (34*ep*H[0, mts/s]^5)/(15*(1 - TT/s)) - 
  (13*ep*s*H[0, mts/s]^5)/(20*TT*(1 - TT/s)) + (8*s*H[0, TT/s])/TT + 
  (2*s*H[0, TT/s])/(ep^2*TT) - (4*s*H[0, TT/s])/(ep*TT) - 
  (16*ep*s*H[0, TT/s])/TT - (4*im*Pi*s*H[0, TT/s])/TT - 
  (im*Pi*s*H[0, TT/s])/(ep^2*TT) + (2*im*Pi*s*H[0, TT/s])/(ep*TT) + 
  (8*ep*im*Pi*s*H[0, TT/s])/TT + (5*Pi^2*s*H[0, TT/s])/(4*ep*TT) + 
  (2*im*Pi^3*s*H[0, TT/s])/(3*TT) - (59*ep*Pi^4*s*H[0, TT/s])/(240*TT) - 
  (8*H[0, TT/s])/(1 - TT/s) - (2*H[0, TT/s])/(ep^2*(1 - TT/s)) + 
  (4*H[0, TT/s])/(ep*(1 - TT/s)) + (16*ep*H[0, TT/s])/(1 - TT/s) + 
  (4*im*Pi*H[0, TT/s])/(1 - TT/s) - (im*Pi*H[0, TT/s])/(ep^2*(1 - TT/s)) - 
  (2*im*Pi*H[0, TT/s])/(ep*(1 - TT/s)) - (8*ep*im*Pi*H[0, TT/s])/(1 - TT/s) + 
  (2*Pi^2*H[0, TT/s])/(1 - TT/s) + (3*Pi^2*H[0, TT/s])/(4*ep*(1 - TT/s)) + 
  (2*im*Pi^3*H[0, TT/s])/(3*(1 - TT/s)) - (2*ep*im*Pi^3*H[0, TT/s])/
   (3*(1 - TT/s)) - (101*ep*Pi^4*H[0, TT/s])/(240*(1 - TT/s)) - 
  (8*s*H[0, TT/s])/(TT*(1 - TT/s)) - (2*s*H[0, TT/s])/(ep^2*TT*(1 - TT/s)) + 
  (4*s*H[0, TT/s])/(ep*TT*(1 - TT/s)) + (16*ep*s*H[0, TT/s])/
   (TT*(1 - TT/s)) - (4*im*Pi*s*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (im*Pi*s*H[0, TT/s])/(ep^2*TT*(1 - TT/s)) + (2*im*Pi*s*H[0, TT/s])/
   (ep*TT*(1 - TT/s)) + (8*ep*im*Pi*s*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (3*Pi^2*s*H[0, TT/s])/(2*ep*TT*(1 - TT/s)) - (7*im*Pi^3*s*H[0, TT/s])/
   (3*TT*(1 - TT/s)) + (109*ep*Pi^4*s*H[0, TT/s])/(120*TT*(1 - TT/s)) + 
  H[0, TT/s]/(ep^3*(-1 + TT/s)) + (5*s*H[0, TT/s])/(2*ep^3*TT*(-1 + TT/s)) + 
  (4*s*H[0, mts/s]*H[0, TT/s])/TT - (s*H[0, mts/s]*H[0, TT/s])/(ep^2*TT) - 
  (2*s*H[0, mts/s]*H[0, TT/s])/(ep*TT) - (8*ep*s*H[0, mts/s]*H[0, TT/s])/TT - 
  (2*im*Pi*s*H[0, mts/s]*H[0, TT/s])/TT + 
  (4*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s])/TT - (Pi^2*s*H[0, mts/s]*H[0, TT/s])/
   (2*TT) - (4*H[0, mts/s]*H[0, TT/s])/(1 - TT/s) + 
  (H[0, mts/s]*H[0, TT/s])/(ep^2*(1 - TT/s)) + (2*H[0, mts/s]*H[0, TT/s])/
   (ep*(1 - TT/s)) + (8*ep*H[0, mts/s]*H[0, TT/s])/(1 - TT/s) + 
  (2*im*Pi*H[0, mts/s]*H[0, TT/s])/(1 - TT/s) - 
  (4*ep*im*Pi*H[0, mts/s]*H[0, TT/s])/(1 - TT/s) + 
  (Pi^2*H[0, mts/s]*H[0, TT/s])/(2*(1 - TT/s)) - 
  (2*ep*Pi^2*H[0, mts/s]*H[0, TT/s])/(1 - TT/s) - 
  (4*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, mts/s]*H[0, TT/s])/(ep*TT*(1 - TT/s)) + 
  (8*ep*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) - 
  (2*im*Pi*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) - 
  (2*Pi^2*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (5*ep*im*Pi^3*s*H[0, mts/s]*H[0, TT/s])/(3*TT*(1 - TT/s)) + 
  (s*H[0, mts/s]^2*H[0, TT/s])/TT + (s*H[0, mts/s]^2*H[0, TT/s])/(ep*TT) - 
  (2*ep*s*H[0, mts/s]^2*H[0, TT/s])/TT + 
  (ep*im*Pi*s*H[0, mts/s]^2*H[0, TT/s])/TT + 
  (ep*Pi^2*s*H[0, mts/s]^2*H[0, TT/s])/(2*TT) - 
  (H[0, mts/s]^2*H[0, TT/s])/(1 - TT/s) - (H[0, mts/s]^2*H[0, TT/s])/
   (ep*(1 - TT/s)) + (2*ep*H[0, mts/s]^2*H[0, TT/s])/(1 - TT/s) - 
  (ep*im*Pi*H[0, mts/s]^2*H[0, TT/s])/(1 - TT/s) - 
  (ep*Pi^2*H[0, mts/s]^2*H[0, TT/s])/(2*(1 - TT/s)) - 
  (s*H[0, mts/s]^2*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, mts/s]^2*H[0, TT/s])/(ep*TT*(1 - TT/s)) + 
  (2*ep*s*H[0, mts/s]^2*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, mts/s]^2*H[0, TT/s])/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, mts/s]^2*H[0, TT/s])/(TT*(1 - TT/s)) - 
  (2*s*H[0, mts/s]^3*H[0, TT/s])/(3*TT) - (ep*s*H[0, mts/s]^3*H[0, TT/s])/
   (3*TT) + (2*H[0, mts/s]^3*H[0, TT/s])/(3*(1 - TT/s)) + 
  (ep*H[0, mts/s]^3*H[0, TT/s])/(3*(1 - TT/s)) - 
  (5*s*H[0, mts/s]^3*H[0, TT/s])/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^3*H[0, TT/s])/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^4*H[0, TT/s])/(3*TT) - (ep*H[0, mts/s]^4*H[0, TT/s])/
   (3*(1 - TT/s)) + (5*ep*s*H[0, mts/s]^4*H[0, TT/s])/(6*TT*(1 - TT/s)) + 
  (2*s*H[0, TT/s]^2)/TT - (s*H[0, TT/s]^2)/(ep*TT) - 
  (4*ep*s*H[0, TT/s]^2)/TT - (im*Pi*s*H[0, TT/s]^2)/TT + 
  (2*ep*im*Pi*s*H[0, TT/s]^2)/TT + (2*H[0, TT/s]^2)/(1 - TT/s) - 
  H[0, TT/s]^2/(ep*(1 - TT/s)) - (4*ep*H[0, TT/s]^2)/(1 - TT/s) + 
  (im*Pi*H[0, TT/s]^2)/(1 - TT/s) - (2*ep*im*Pi*H[0, TT/s]^2)/(1 - TT/s) - 
  (ep*Pi^2*H[0, TT/s]^2)/(1 - TT/s) - (2*s*H[0, TT/s]^2)/(TT*(1 - TT/s)) + 
  (s*H[0, TT/s]^2)/(ep^2*TT*(1 - TT/s)) + (s*H[0, TT/s]^2)/
   (ep*TT*(1 - TT/s)) + (4*ep*s*H[0, TT/s]^2)/(TT*(1 - TT/s)) - 
  (im*Pi*s*H[0, TT/s]^2)/(TT*(1 - TT/s)) + (im*Pi*s*H[0, TT/s]^2)/
   (ep*TT*(1 - TT/s)) + (2*ep*im*Pi*s*H[0, TT/s]^2)/(TT*(1 - TT/s)) - 
  (3*Pi^2*s*H[0, TT/s]^2)/(2*TT*(1 - TT/s)) + (5*ep*im*Pi^3*s*H[0, TT/s]^2)/
   (6*TT*(1 - TT/s)) + (s*H[0, mts/s]*H[0, TT/s]^2)/TT - 
  (2*ep*s*H[0, mts/s]*H[0, TT/s]^2)/TT + 
  (ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]^2)/TT + (H[0, mts/s]*H[0, TT/s]^2)/
   (1 - TT/s) - (2*ep*H[0, mts/s]*H[0, TT/s]^2)/(1 - TT/s) - 
  (ep*im*Pi*H[0, mts/s]*H[0, TT/s]^2)/(1 - TT/s) - 
  (s*H[0, mts/s]*H[0, TT/s]^2)/(TT*(1 - TT/s)) - 
  (s*H[0, mts/s]*H[0, TT/s]^2)/(ep*TT*(1 - TT/s)) + 
  (2*ep*s*H[0, mts/s]*H[0, TT/s]^2)/(TT*(1 - TT/s)) - 
  (im*Pi*s*H[0, mts/s]*H[0, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, mts/s]*H[0, TT/s]^2)/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]^2)/(2*TT) - (ep*H[0, mts/s]^2*H[0, TT/s]^2)/
   (2*(1 - TT/s)) + (s*H[0, mts/s]^2*H[0, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, mts/s]^2*H[0, TT/s]^2)/(2*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^3*H[0, TT/s]^2)/(6*TT*(1 - TT/s)) + 
  (s*H[0, TT/s]^3)/(3*TT) - (2*ep*s*H[0, TT/s]^3)/(3*TT) + 
  (ep*im*Pi*s*H[0, TT/s]^3)/(3*TT) + H[0, TT/s]^3/(1 - TT/s) - 
  (2*ep*H[0, TT/s]^3)/(3*(1 - TT/s)) - (ep*im*Pi*H[0, TT/s]^3)/
   (3*(1 - TT/s)) - (s*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (2*s*H[0, TT/s]^3)/(3*ep*TT*(1 - TT/s)) + (2*ep*s*H[0, TT/s]^3)/
   (3*TT*(1 - TT/s)) - (2*im*Pi*s*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (2*ep*Pi^2*s*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]*H[0, TT/s]^3)/(3*TT) - (ep*H[0, mts/s]*H[0, TT/s]^3)/
   (1 - TT/s) - (s*H[0, mts/s]*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (2*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, TT/s]^4)/(12*TT) - (5*ep*H[0, TT/s]^4)/(12*(1 - TT/s)) - 
  (7*s*H[0, TT/s]^4)/(12*TT*(1 - TT/s)) + (ep*s*H[0, TT/s]^4)/
   (12*TT*(1 - TT/s)) + (ep*im*Pi*s*H[0, TT/s]^4)/(4*TT*(1 - TT/s)) + 
  (5*ep*s*H[0, mts/s]*H[0, TT/s]^4)/(12*TT*(1 - TT/s)) + 
  (7*ep*s*H[0, TT/s]^5)/(30*TT*(1 - TT/s)) + (8*s*H[1, TT/s])/TT + 
  (2*s*H[1, TT/s])/(ep^2*TT) - (4*s*H[1, TT/s])/(ep*TT) - 
  (16*ep*s*H[1, TT/s])/TT - (4*im*Pi*s*H[1, TT/s])/TT - 
  (im*Pi*s*H[1, TT/s])/(ep^2*TT) + (2*im*Pi*s*H[1, TT/s])/(ep*TT) + 
  (8*ep*im*Pi*s*H[1, TT/s])/TT - (2*Pi^2*s*H[1, TT/s])/TT + 
  (5*Pi^2*s*H[1, TT/s])/(4*ep*TT) + (2*im*Pi^3*s*H[1, TT/s])/(3*TT) - 
  (59*ep*Pi^4*s*H[1, TT/s])/(240*TT) - (8*H[1, TT/s])/(1 - TT/s) - 
  (2*H[1, TT/s])/(ep^2*(1 - TT/s)) + (4*H[1, TT/s])/(ep*(1 - TT/s)) + 
  (16*ep*H[1, TT/s])/(1 - TT/s) + (4*im*Pi*H[1, TT/s])/(1 - TT/s) - 
  (im*Pi*H[1, TT/s])/(ep^2*(1 - TT/s)) - (2*im*Pi*H[1, TT/s])/
   (ep*(1 - TT/s)) - (8*ep*im*Pi*H[1, TT/s])/(1 - TT/s) + 
  (3*Pi^2*H[1, TT/s])/(4*ep*(1 - TT/s)) + (2*im*Pi^3*H[1, TT/s])/
   (3*(1 - TT/s)) - (2*ep*im*Pi^3*H[1, TT/s])/(3*(1 - TT/s)) - 
  (101*ep*Pi^4*H[1, TT/s])/(240*(1 - TT/s)) + 
  (8*s*H[1, TT/s])/(TT*(1 - TT/s)) + (2*s*H[1, TT/s])/(ep^2*TT*(1 - TT/s)) - 
  (4*s*H[1, TT/s])/(ep*TT*(1 - TT/s)) - (16*ep*s*H[1, TT/s])/
   (TT*(1 - TT/s)) + (4*im*Pi*s*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (im*Pi*s*H[1, TT/s])/(ep^2*TT*(1 - TT/s)) - (2*im*Pi*s*H[1, TT/s])/
   (ep*TT*(1 - TT/s)) - (8*ep*im*Pi*s*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (7*Pi^2*s*H[1, TT/s])/(2*ep*TT*(1 - TT/s)) + 
  (im*Pi^3*s*H[1, TT/s])/(3*TT*(1 - TT/s)) + (4*ep*im*Pi^3*s*H[1, TT/s])/
   (3*TT*(1 - TT/s)) - (7*ep*Pi^4*s*H[1, TT/s])/(40*TT*(1 - TT/s)) + 
  H[1, TT/s]/(ep^3*(-1 + TT/s)) - (7*s*H[1, TT/s])/(2*ep^3*TT*(-1 + TT/s)) + 
  (4*s*H[0, mts/s]*H[1, TT/s])/TT - (s*H[0, mts/s]*H[1, TT/s])/(ep^2*TT) - 
  (2*s*H[0, mts/s]*H[1, TT/s])/(ep*TT) - (8*ep*s*H[0, mts/s]*H[1, TT/s])/TT - 
  (2*im*Pi*s*H[0, mts/s]*H[1, TT/s])/TT + 
  (4*ep*im*Pi*s*H[0, mts/s]*H[1, TT/s])/TT - (Pi^2*s*H[0, mts/s]*H[1, TT/s])/
   (2*TT) + (2*ep*Pi^2*s*H[0, mts/s]*H[1, TT/s])/TT - 
  (4*H[0, mts/s]*H[1, TT/s])/(1 - TT/s) + (H[0, mts/s]*H[1, TT/s])/
   (ep^2*(1 - TT/s)) + (2*H[0, mts/s]*H[1, TT/s])/(ep*(1 - TT/s)) + 
  (8*ep*H[0, mts/s]*H[1, TT/s])/(1 - TT/s) + (2*im*Pi*H[0, mts/s]*H[1, TT/s])/
   (1 - TT/s) - (4*ep*im*Pi*H[0, mts/s]*H[1, TT/s])/(1 - TT/s) + 
  (Pi^2*H[0, mts/s]*H[1, TT/s])/(2*(1 - TT/s)) + 
  (4*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (2*s*H[0, mts/s]*H[1, TT/s])/(ep*TT*(1 - TT/s)) - 
  (8*ep*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*im*Pi*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*im*Pi*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*Pi^2*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*im*Pi^3*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (s*H[0, mts/s]^2*H[1, TT/s])/TT + (s*H[0, mts/s]^2*H[1, TT/s])/(ep*TT) - 
  (2*ep*s*H[0, mts/s]^2*H[1, TT/s])/TT + 
  (ep*im*Pi*s*H[0, mts/s]^2*H[1, TT/s])/TT + 
  (ep*Pi^2*s*H[0, mts/s]^2*H[1, TT/s])/(2*TT) - 
  (H[0, mts/s]^2*H[1, TT/s])/(1 - TT/s) - (H[0, mts/s]^2*H[1, TT/s])/
   (ep*(1 - TT/s)) + (2*ep*H[0, mts/s]^2*H[1, TT/s])/(1 - TT/s) - 
  (ep*im*Pi*H[0, mts/s]^2*H[1, TT/s])/(1 - TT/s) - 
  (ep*Pi^2*H[0, mts/s]^2*H[1, TT/s])/(2*(1 - TT/s)) + 
  (s*H[0, mts/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (2*s*H[0, mts/s]^2*H[1, TT/s])/(ep*TT*(1 - TT/s)) - 
  (2*ep*s*H[0, mts/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*im*Pi*s*H[0, mts/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*Pi^2*s*H[0, mts/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (2*s*H[0, mts/s]^3*H[1, TT/s])/(3*TT) - (ep*s*H[0, mts/s]^3*H[1, TT/s])/
   (3*TT) + (2*H[0, mts/s]^3*H[1, TT/s])/(3*(1 - TT/s)) + 
  (ep*H[0, mts/s]^3*H[1, TT/s])/(3*(1 - TT/s)) + 
  (5*s*H[0, mts/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^4*H[1, TT/s])/(3*TT) - (ep*H[0, mts/s]^4*H[1, TT/s])/
   (3*(1 - TT/s)) - (5*ep*s*H[0, mts/s]^4*H[1, TT/s])/(6*TT*(1 - TT/s)) + 
  (4*s*H[0, TT/s]*H[1, TT/s])/TT - (2*s*H[0, TT/s]*H[1, TT/s])/(ep*TT) - 
  (8*ep*s*H[0, TT/s]*H[1, TT/s])/TT - (2*im*Pi*s*H[0, TT/s]*H[1, TT/s])/TT + 
  (4*ep*im*Pi*s*H[0, TT/s]*H[1, TT/s])/TT + 
  (2*ep*Pi^2*s*H[0, TT/s]*H[1, TT/s])/TT + (4*H[0, TT/s]*H[1, TT/s])/
   (1 - TT/s) - (2*H[0, TT/s]*H[1, TT/s])/(ep*(1 - TT/s)) - 
  (8*ep*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) + (2*im*Pi*H[0, TT/s]*H[1, TT/s])/
   (1 - TT/s) - (4*ep*im*Pi*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) + 
  (2*ep*Pi^2*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) + 
  (4*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (4*s*H[0, TT/s]*H[1, TT/s])/(ep^2*TT*(1 - TT/s)) - 
  (2*s*H[0, TT/s]*H[1, TT/s])/(ep*TT*(1 - TT/s)) - 
  (8*ep*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (2*im*Pi*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*im*Pi*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*Pi^2*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*im*Pi^3*s*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/TT - 
  (4*ep*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/TT + 
  (2*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/TT + 
  (2*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) - 
  (4*ep*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) - 
  (2*ep*im*Pi*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) + 
  (2*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(ep*TT*(1 - TT/s)) - 
  (4*ep*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (2*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*Pi^2*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]*H[1, TT/s])/TT - 
  (ep*H[0, mts/s]^2*H[0, TT/s]*H[1, TT/s])/(1 - TT/s) - 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^3*H[0, TT/s]*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
  (s*H[0, TT/s]^2*H[1, TT/s])/TT - (2*ep*s*H[0, TT/s]^2*H[1, TT/s])/TT + 
  (ep*im*Pi*s*H[0, TT/s]^2*H[1, TT/s])/TT + (H[0, TT/s]^2*H[1, TT/s])/
   (1 - TT/s) - (2*ep*H[0, TT/s]^2*H[1, TT/s])/(1 - TT/s) - 
  (ep*im*Pi*H[0, TT/s]^2*H[1, TT/s])/(1 - TT/s) - 
  (s*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (s*H[0, TT/s]^2*H[1, TT/s])/(ep*TT*(1 - TT/s)) + 
  (2*ep*s*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (im*Pi*s*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (3*ep*Pi^2*s*H[0, TT/s]^2*H[1, TT/s])/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s])/TT - 
  (ep*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s])/(1 - TT/s) - 
  (s*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]^2*H[1, TT/s])/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]^3*H[1, TT/s])/TT + (ep*H[0, TT/s]^3*H[1, TT/s])/
   (3*(1 - TT/s)) + (2*s*H[0, TT/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
  (2*ep*im*Pi*s*H[0, TT/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]*H[0, TT/s]^3*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
  (7*ep*s*H[0, TT/s]^4*H[1, TT/s])/(12*TT*(1 - TT/s)) + 
  (2*s*H[1, TT/s]^2)/TT - (s*H[1, TT/s]^2)/(ep*TT) - 
  (4*ep*s*H[1, TT/s]^2)/TT + (im*Pi*s*H[1, TT/s]^2)/TT - 
  (2*ep*im*Pi*s*H[1, TT/s]^2)/TT - (ep*Pi^2*s*H[1, TT/s]^2)/TT + 
  (2*H[1, TT/s]^2)/(1 - TT/s) - H[1, TT/s]^2/(ep*(1 - TT/s)) - 
  (4*ep*H[1, TT/s]^2)/(1 - TT/s) - (im*Pi*H[1, TT/s]^2)/(1 - TT/s) + 
  (2*ep*im*Pi*H[1, TT/s]^2)/(1 - TT/s) - (2*s*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (s*H[1, TT/s]^2)/(ep^2*TT*(1 - TT/s)) + (s*H[1, TT/s]^2)/
   (ep*TT*(1 - TT/s)) + (4*ep*s*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (im*Pi*s*H[1, TT/s]^2)/(TT*(1 - TT/s)) + (im*Pi*s*H[1, TT/s]^2)/
   (ep*TT*(1 - TT/s)) + (2*ep*im*Pi*s*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (3*Pi^2*s*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + (ep*im*Pi^3*s*H[1, TT/s]^2)/
   (2*TT*(1 - TT/s)) + (s*H[0, mts/s]*H[1, TT/s]^2)/TT - 
  (2*ep*s*H[0, mts/s]*H[1, TT/s]^2)/TT - 
  (ep*im*Pi*s*H[0, mts/s]*H[1, TT/s]^2)/TT + (H[0, mts/s]*H[1, TT/s]^2)/
   (1 - TT/s) - (2*ep*H[0, mts/s]*H[1, TT/s]^2)/(1 - TT/s) + 
  (ep*im*Pi*H[0, mts/s]*H[1, TT/s]^2)/(1 - TT/s) - 
  (s*H[0, mts/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (s*H[0, mts/s]*H[1, TT/s]^2)/(ep*TT*(1 - TT/s)) + 
  (2*ep*s*H[0, mts/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (im*Pi*s*H[0, mts/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, mts/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, mts/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^2*H[1, TT/s]^2)/(2*TT) - (ep*H[0, mts/s]^2*H[1, TT/s]^2)/
   (2*(1 - TT/s)) + (s*H[0, mts/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, mts/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^3*H[1, TT/s]^2)/(6*TT*(1 - TT/s)) - 
  (s*H[0, TT/s]*H[1, TT/s]^2)/TT - (2*ep*s*H[0, TT/s]*H[1, TT/s]^2)/TT - 
  (ep*im*Pi*s*H[0, TT/s]*H[1, TT/s]^2)/TT - (H[0, TT/s]*H[1, TT/s]^2)/
   (1 - TT/s) + (2*ep*H[0, TT/s]*H[1, TT/s]^2)/(1 - TT/s) - 
  (ep*im*Pi*H[0, TT/s]*H[1, TT/s]^2)/(1 - TT/s) - 
  (s*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (2*s*H[0, TT/s]*H[1, TT/s]^2)/(ep*TT*(1 - TT/s)) + 
  (2*ep*s*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s]^2)/TT + 
  (ep*H[0, mts/s]*H[0, TT/s]*H[1, TT/s]^2)/(1 - TT/s) + 
  (s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (ep*s*H[0, TT/s]^2*H[1, TT/s]^2)/(2*TT) - (ep*H[0, TT/s]^2*H[1, TT/s]^2)/
   (2*(1 - TT/s)) + (s*H[0, TT/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*im*Pi*s*H[0, TT/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]*H[0, TT/s]^2*H[1, TT/s]^2)/(2*TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]^3*H[1, TT/s]^2)/(3*TT*(1 - TT/s)) - (s*H[1, TT/s]^3)/TT + 
  (2*ep*s*H[1, TT/s]^3)/(3*TT) + (ep*im*Pi*s*H[1, TT/s]^3)/(3*TT) - 
  H[1, TT/s]^3/(3*(1 - TT/s)) + (2*ep*H[1, TT/s]^3)/(3*(1 - TT/s)) - 
  (ep*im*Pi*H[1, TT/s]^3)/(3*(1 - TT/s)) + 
  (s*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - (2*s*H[1, TT/s]^3)/
   (3*ep*TT*(1 - TT/s)) - (2*ep*s*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (2*im*Pi*s*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - (ep*im*Pi*s*H[1, TT/s]^3)/
   (3*TT*(1 - TT/s)) - (2*ep*Pi^2*s*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[1, TT/s]^3)/TT + (ep*H[0, mts/s]*H[1, TT/s]^3)/
   (3*(1 - TT/s)) + (s*H[0, mts/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (2*ep*im*Pi*s*H[0, mts/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, TT/s]*H[1, TT/s]^3)/(3*TT) - (ep*H[0, TT/s]*H[1, TT/s]^3)/
   (3*(1 - TT/s)) - (2*s*H[0, TT/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) - 
  (ep*s*H[0, TT/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]*H[0, TT/s]*H[1, TT/s]^3)/(3*TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]^2*H[1, TT/s]^3)/(6*TT*(1 - TT/s)) - 
  (5*ep*s*H[1, TT/s]^4)/(12*TT) - (ep*H[1, TT/s]^4)/(12*(1 - TT/s)) - 
  (7*s*H[1, TT/s]^4)/(12*TT*(1 - TT/s)) + (ep*s*H[1, TT/s]^4)/
   (12*TT*(1 - TT/s)) + (ep*im*Pi*s*H[1, TT/s]^4)/(4*TT*(1 - TT/s)) + 
  (5*ep*s*H[0, mts/s]*H[1, TT/s]^4)/(12*TT*(1 - TT/s)) - 
  (ep*s*H[0, TT/s]*H[1, TT/s]^4)/(6*TT*(1 - TT/s)) - 
  (7*ep*s*H[1, TT/s]^5)/(30*TT*(1 - TT/s)) + (4*im*Pi*s*H[0, 1, TT/s])/TT - 
  (8*ep*im*Pi*s*H[0, 1, TT/s])/TT - (4*im*Pi*H[0, 1, TT/s])/(1 - TT/s) + 
  (8*ep*im*Pi*H[0, 1, TT/s])/(1 - TT/s) + (4*ep*im*Pi^3*s*H[0, 1, TT/s])/
   (3*TT*(1 - TT/s)) - (4*ep*im*Pi*s*H[0, mts/s]*H[0, 1, TT/s])/TT + 
  (4*ep*im*Pi*H[0, mts/s]*H[0, 1, TT/s])/(1 - TT/s) + 
  (4*s*H[0, TT/s]*H[0, 1, TT/s])/TT - (4*ep*im*Pi*s*H[0, TT/s]*H[0, 1, TT/s])/
   TT + (8*ep*H[0, TT/s]*H[0, 1, TT/s])/(1 - TT/s) + 
  (4*s*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, TT/s]*H[0, 1, TT/s])/(ep*TT*(1 - TT/s)) - 
  (8*ep*s*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (6*im*Pi*s*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*im*Pi*s*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*s*H[0, mts/s]*H[0, TT/s]*H[0, 1, TT/s])/TT - 
  (4*ep*s*H[0, mts/s]*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (6*ep*im*Pi*s*H[0, mts/s]*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^2*H[0, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (2*ep*s*H[0, TT/s]^2*H[0, 1, TT/s])/TT - (2*ep*H[0, TT/s]^2*H[0, 1, TT/s])/
   (1 - TT/s) - (5*s*H[0, TT/s]^2*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (3*ep*im*Pi*s*H[0, TT/s]^2*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, mts/s]*H[0, TT/s]^2*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (7*ep*s*H[0, TT/s]^3*H[0, 1, TT/s])/(3*TT*(1 - TT/s)) + 
  (4*ep*im*Pi*s*H[1, TT/s]*H[0, 1, TT/s])/TT + 
  (4*ep*im*Pi*H[1, TT/s]*H[0, 1, TT/s])/(1 - TT/s) + 
  (4*im*Pi*s*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (8*ep*im*Pi*s*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*im*Pi*s*H[0, mts/s]*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, TT/s]*H[1, TT/s]*H[0, 1, TT/s])/TT + 
  (8*ep*H[0, TT/s]*H[1, TT/s]*H[0, 1, TT/s])/(1 - TT/s) + 
  (2*s*H[0, TT/s]*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*s*H[0, TT/s]*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (10*ep*im*Pi*s*H[0, TT/s]*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (5*ep*s*H[0, TT/s]^2*H[1, TT/s]*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (2*ep*im*Pi*s*H[1, TT/s]^2*H[0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*s*H[0, TT/s]*H[1, TT/s]^2*H[0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (8*ep*im*Pi*s*H[0, 1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (4*ep*s*H[0, TT/s]*H[0, 1, TT/s]^2)/(TT*(1 - TT/s)) - 
  (4*s*H[0, 0, 1, TT/s])/TT + (8*ep*im*Pi*s*H[0, 0, 1, TT/s])/TT - 
  (8*ep*H[0, 0, 1, TT/s])/(1 - TT/s) + (4*ep*im*Pi*H[0, 0, 1, TT/s])/
   (1 - TT/s) - (4*s*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (2*s*H[0, 0, 1, TT/s])/(ep*TT*(1 - TT/s)) + (8*ep*s*H[0, 0, 1, TT/s])/
   (TT*(1 - TT/s)) + (10*im*Pi*s*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (12*ep*im*Pi*s*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*Pi^2*s*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, mts/s]*H[0, 0, 1, TT/s])/TT + 
  (4*ep*s*H[0, mts/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (10*ep*im*Pi*s*H[0, mts/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*s*H[0, mts/s]^2*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*H[0, TT/s]*H[0, 0, 1, TT/s])/(1 - TT/s) + 
  (12*s*H[0, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*s*H[0, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*im*Pi*s*H[0, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (12*ep*s*H[0, mts/s]*H[0, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*s*H[0, TT/s]^2*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*s*H[1, TT/s]*H[0, 0, 1, TT/s])/TT - 
  (8*ep*H[1, TT/s]*H[0, 0, 1, TT/s])/(1 - TT/s) - 
  (2*s*H[1, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[1, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (18*ep*im*Pi*s*H[1, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (12*ep*s*H[0, TT/s]*H[1, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[1, TT/s]^2*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (8*ep*s*H[0, 1, TT/s]*H[0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (8*ep*s*H[0, 1, 1, TT/s])/TT - (4*ep*im*Pi*s*H[0, 1, 1, TT/s])/TT + 
  (4*H[0, 1, 1, TT/s])/(1 - TT/s) - (8*ep*im*Pi*H[0, 1, 1, TT/s])/
   (1 - TT/s) + (4*s*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (2*s*H[0, 1, 1, TT/s])/(ep*TT*(1 - TT/s)) - (8*ep*s*H[0, 1, 1, TT/s])/
   (TT*(1 - TT/s)) - (10*im*Pi*s*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (12*ep*im*Pi*s*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (ep*Pi^2*s*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (4*ep*H[0, mts/s]*H[0, 1, 1, TT/s])/(1 - TT/s) - 
  (4*ep*s*H[0, mts/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (10*ep*im*Pi*s*H[0, mts/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[0, mts/s]^2*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (12*ep*H[0, TT/s]*H[0, 1, 1, TT/s])/(1 - TT/s) - 
  (2*s*H[0, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (18*ep*im*Pi*s*H[0, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (7*ep*s*H[0, TT/s]^2*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*H[1, TT/s]*H[0, 1, 1, TT/s])/(1 - TT/s) - 
  (2*s*H[1, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[1, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (6*ep*im*Pi*s*H[1, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, mts/s]*H[1, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (2*ep*s*H[0, TT/s]*H[1, TT/s]*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (ep*s*H[1, TT/s]^2*H[0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, 0, 0, 1, TT/s])/TT - (4*ep*H[0, 0, 0, 1, TT/s])/(1 - TT/s) - 
  (14*s*H[0, 0, 0, 1, TT/s])/(TT*(1 - TT/s)) + (8*ep*s*H[0, 0, 0, 1, TT/s])/
   (TT*(1 - TT/s)) - (2*ep*im*Pi*s*H[0, 0, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (16*ep*s*H[0, mts/s]*H[0, 0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (14*ep*s*H[1, TT/s]*H[0, 0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (8*ep*s*H[0, 0, 1, 1, TT/s])/TT + (8*ep*H[0, 0, 1, 1, TT/s])/(1 - TT/s) - 
  (4*ep*s*H[0, TT/s]*H[0, 0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (8*ep*s*H[1, TT/s]*H[0, 0, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*ep*s*H[0, 1, 1, 1, TT/s])/TT - (4*ep*H[0, 1, 1, 1, TT/s])/(1 - TT/s) + 
  (14*s*H[0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) - (8*ep*s*H[0, 1, 1, 1, TT/s])/
   (TT*(1 - TT/s)) + (2*ep*im*Pi*s*H[0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (16*ep*s*H[0, mts/s]*H[0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (10*ep*s*H[0, TT/s]*H[0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (10*ep*s*H[1, TT/s]*H[0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (10*ep*s*H[0, 0, 0, 0, 1, TT/s])/(TT*(1 - TT/s)) - 
  (20*ep*s*H[0, 0, 0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (8*ep*s*H[0, 0, 1, 0, 1, TT/s])/(TT*(1 - TT/s)) + 
  (20*ep*s*H[0, 0, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (8*ep*s*H[0, 1, 0, 1, 1, TT/s])/(TT*(1 - TT/s)) - 
  (10*ep*s*H[0, 1, 1, 1, 1, TT/s])/(TT*(1 - TT/s)) + 
  (4*im*Pi^2*(6 + 7*ep^2*Pi^2 - 12*ep*H[0, mts/s] + 12*ep^2*H[0, mts/s]^2)*
    Log[2])/(3*ep*Sqrt[mts/s]*Sqrt[((s - TT)*TT)/s^2]) + 
  ((-32*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s])/(mts*TT*Sqrt[1 - TT/s]) + 
    (64*ep*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s]*H[0, mts/s])/
     (mts*TT*Sqrt[1 - TT/s]))*Log[2]^2 + 
  (256*ep*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s]*Log[2]^3)/
   (3*mts*TT*Sqrt[1 - TT/s]) + ((4*s)/TT + (11*s)/(6*ep*TT) - 
    (32*ep*s)/(3*TT) + (11*im*Pi*s)/(3*TT) + (16*ep*im*Pi*s)/(3*TT) + 
    (55*ep*Pi^2*s)/(36*TT) + (70*ep*im*Pi^2*Sqrt[mts/s]*s^3*
      Sqrt[((s - TT)*TT)/s^2])/(3*mts*(s - TT)*TT) + 11/(6*ep*(1 - TT/s)) - 
    (8*ep)/(3*(1 - TT/s)) + (11*im*Pi)/(3*(1 - TT/s)) + 
    (28*ep*im*Pi)/(3*(1 - TT/s)) + (55*ep*Pi^2)/(36*(1 - TT/s)) - 
    (4*s)/(TT*(1 - TT/s)) - (38*s)/(3*ep*TT*(1 - TT/s)) + 
    (32*ep*s)/(3*TT*(1 - TT/s)) + (50*im*Pi*s)/(3*TT*(1 - TT/s)) + 
    (16*ep*im*Pi*s)/(3*TT*(1 - TT/s)) - 
    (2*ep*im*Pi^2*Sqrt[mts/s]*s^2*Sqrt[TT/s])/(3*mts*TT*Sqrt[1 - TT/s]) - 
    (8*ep*s*H[0, mts/s])/(3*TT) - (12*ep*im*Pi*s*H[0, mts/s])/TT + 
    (4*ep*H[0, mts/s])/(3*(1 - TT/s)) - (12*ep*im*Pi*H[0, mts/s])/
     (1 - TT/s) + (8*s*H[0, mts/s])/(TT*(1 - TT/s)) + 
    (8*ep*s*H[0, mts/s])/(3*TT*(1 - TT/s)) - (14*ep*im*Pi*s*H[0, mts/s])/
     (TT*(1 - TT/s)) - (6*ep*s*H[0, mts/s]^2)/TT - 
    (6*ep*H[0, mts/s]^2)/(1 - TT/s) - (7*ep*s*H[0, mts/s]^2)/
     (TT*(1 - TT/s)) - (4*s*H[0, TT/s])/(3*TT) - (16*ep*s*H[0, TT/s])/
     (3*TT) + (4*H[0, TT/s])/(3*(1 - TT/s)) + (40*ep*H[0, TT/s])/
     (3*(1 - TT/s)) + (12*s*H[0, TT/s])/(TT*(1 - TT/s)) + 
    (16*ep*s*H[0, TT/s])/(3*TT*(1 - TT/s)) - (14*ep*im*Pi*s*H[0, TT/s])/
     (TT*(1 - TT/s)) + (8*ep*s*H[0, mts/s]*H[0, TT/s])/(3*TT) - 
    (8*ep*H[0, mts/s]*H[0, TT/s])/(3*(1 - TT/s)) - 
    (2*ep*s*H[0, mts/s]*H[0, TT/s])/(TT*(1 - TT/s)) - 
    (17*ep*s*H[0, TT/s]^2)/(3*TT*(1 - TT/s)) - (4*s*H[1, TT/s])/(3*TT) - 
    (28*ep*s*H[1, TT/s])/(3*TT) + (4*H[1, TT/s])/(3*(1 - TT/s)) + 
    (28*ep*H[1, TT/s])/(3*(1 - TT/s)) - (8*s*H[1, TT/s])/(TT*(1 - TT/s)) - 
    (40*ep*s*H[1, TT/s])/(3*TT*(1 - TT/s)) + (2*ep*im*Pi*s*H[1, TT/s])/
     (TT*(1 - TT/s)) + (8*ep*s*H[0, mts/s]*H[1, TT/s])/(3*TT) - 
    (8*ep*H[0, mts/s]*H[1, TT/s])/(3*(1 - TT/s)) - 
    (2*ep*s*H[0, mts/s]*H[1, TT/s])/(TT*(1 - TT/s)) + 
    (32*ep*s*H[0, TT/s]*H[1, TT/s])/(3*TT*(1 - TT/s)) - 
    (11*ep*s*H[1, TT/s]^2)/(3*TT*(1 - TT/s)) - (8*ep*s*H[0, 1, TT/s])/
     (TT*(1 - TT/s)))*Zeta[3] + 
  ((87*ep*s)/(10*TT) + (87*ep)/(10*(1 - TT/s)) - (34*ep*s)/(TT*(1 - TT/s)))*
   Zeta[5])
