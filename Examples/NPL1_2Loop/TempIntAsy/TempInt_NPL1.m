R2tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d1 - d2 - d5 - d7 - 2 ep) SS^(-2 - d3 - d6)
     UU^(-1 - d4)
     Gamma[-d2 - d5 - ep] Gamma[d1 + d2 + d5 + d7 + 2 ep] Gamma[
     1 - d2 + d4 - d7 - ep - Z1] Gamma[-Z1] Gamma[-1 + d2 - d4 - d6 + 
      Z1] Gamma[-1 - d3 - d4 - d6 - ep + Z1] Gamma[
     d2 - d4 + d5 + d7 + ep + Z1])/(Gamma[1 + d1] Gamma[1 + d2] Gamma[
     1 + d5] Gamma[
     1 + d7] Gamma[-1 - d3 - d4 - d6 - ep] Gamma[-d4 - d6 - ep + Z1])

R3tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-(1/2) - d1 - d2/2 - d4/2 - d5 - d6/2 - d7 - 2 ep)
     SS^(-(3/2) - d2/2 - d3 + d4/2 - d6/2)
     TT^(-(1/2) + d2/2 - d4/2 - d6/2) UU^(-(1/2) - d2/2 - d4/2 + d6/2)
     Gamma[1/2 + d2/2 + d4/2 - d6/2] Gamma[
     1/2 + d2/2 - d4/2 + d6/2] Gamma[
     1/2 - d2/2 + d4/2 + d6/2] Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - 
      ep] Gamma[1/2 + d2/2 - d4/2 + d5 + d6/2 + d7 + ep] Gamma[
     1/2 + d1 + d2/2 + d4/2 + d5 + d6/2 + d7 + 2 ep])/(2 Gamma[
     1 + d1] Gamma[1 + d2] Gamma[1 + d4] Gamma[1 + d5] Gamma[
     1 + d6] Gamma[1 + d7] Gamma[1/2 - d2/2 - d4/2 - d6/2 - ep])

R4tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d1 - d4 - d5 - d7 - 2 ep) SS^(-1 - d3) TT^(-1 - d6)
     UU^(-1 - d2)
     Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - ep] Gamma[
     d5 + d7 + ep] Gamma[
     1 + d1 + d2 + d5 + d6 + d7 + 2 ep - Z1] Gamma[-Z1] Gamma[-1 - 
      d2 + d4 - d6 + Z1] Gamma[-d2 - d5 - d6 - d7 - 2 ep + 
      Z1] Gamma[-1 - d2 - d3 - d6 - ep + Z1])/(Gamma[1 + d1] Gamma[
     1 + d4] Gamma[1 + d5] Gamma[
     1 + d7] Gamma[-d2 - d5 - d6 - d7 - 2 ep] Gamma[-1 - d2 - d3 - 
      d6 - ep] Gamma[-d2 - d6 - ep + Z1])

R5tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d1 - d4 - ep)
     SS^(-3 - d2 - d3 - d5 - d6 - d7 - ep - Z1 - Z2) TT^Z1 UU^
    Z2 Gamma[
     d1 + d4 + ep] Gamma[-1 - d5 - d6 - d7 - ep - 
      Z1] Gamma[-Z1] Gamma[
     1 + d5 + Z1] Gamma[-1 - d2 - d5 - d7 - ep - Z2] Gamma[-2 + d1 - 
      d2 - d3 - d5 - d6 - d7 - ep - Z1 - Z2] Gamma[-Z2] Gamma[
     1 + d7 + Z2] Gamma[1 + d4 + Z1 + Z2] Gamma[
     2 + d2 + d5 + d6 + d7 + ep + Z1 + Z2])/(Gamma[1 + d1] Gamma[
     1 + d2] Gamma[1 + d4] Gamma[1 + d5] Gamma[1 + d6] Gamma[
     1 + d7] Gamma[-d2 - d5 - d6 - d7 - 2 ep] Gamma[-1 + d1 - d2 - 
      d3 + d4 - d5 - d6 - d7 - ep])

R6tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d1 - d5 - d6 - d7 - 2 ep) SS^(-2 - d2 - d3)
     TT^(-1 - d4)
     Gamma[-d6 - d7 - ep] Gamma[d1 + d5 + d6 + d7 + 2 ep] Gamma[
     1 + d4 - d5 - d6 - ep - Z1] Gamma[-Z1] Gamma[-1 - d2 - d4 + d6 + 
      Z1] Gamma[-1 - d2 - d3 - d4 - ep + Z1] Gamma[-d4 + d5 + d6 + 
      d7 + ep + Z1])/(Gamma[1 + d1] Gamma[1 + d5] Gamma[1 + d6] Gamma[
     1 + d7] Gamma[-1 - d2 - d3 - d4 - ep] Gamma[-d2 - d4 - ep + Z1])

R7tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d2 - d3 - d4 - d6 - 2 ep) SS^(-1 - d1) TT^(-1 - d5)
     UU^(-1 - d7)
     Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - ep] Gamma[
     d2 + d6 + ep] Gamma[
     1 + d2 + d3 + d5 + d6 + d7 + 2 ep - Z1] Gamma[-Z1] Gamma[-1 + 
      d4 - d5 - d7 + Z1] Gamma[-d2 - d5 - d6 - d7 - 2 ep + 
      Z1] Gamma[-1 - d1 - d5 - d7 - ep + Z1])/(Gamma[1 + d2] Gamma[
     1 + d3] Gamma[1 + d4] Gamma[
     1 + d6] Gamma[-d2 - d5 - d6 - d7 - 2 ep] Gamma[-1 - d1 - d5 - 
      d7 - ep] Gamma[-d5 - d7 - ep + Z1])

R8tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-(1/2) - d2 - d3 - d4/2 - d5/2 - d6 - d7/2 - 2 ep)
     SS^(-(3/2) - d1 + d4/2 - d5/2 - d7/2)
     TT^(-(1/2) - d4/2 - d5/2 + d7/2) UU^(-(1/2) - d4/2 + d5/2 - d7/2)
     Gamma[1/2 + d4/2 + d5/2 - d7/2] Gamma[
     1/2 + d4/2 - d5/2 + d7/2] Gamma[
     1/2 - d4/2 + d5/2 + d7/2] Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - 
      ep] Gamma[1/2 + d2 - d4/2 + d5/2 + d6 + d7/2 + ep] Gamma[
     1/2 + d2 + d3 + d4/2 + d5/2 + d6 + d7/2 + 2 ep])/(2 Gamma[
     1 + d2] Gamma[1 + d3] Gamma[1 + d4] Gamma[1 + d5] Gamma[
     1 + d6] Gamma[1 + d7] Gamma[1/2 - d4/2 - d5/2 - d7/2 - ep])

R9tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d2 - d3 - d5 - d6 - 2 ep) SS^(-2 - d1 - d7)
     TT^(-1 - d4)
     Gamma[-d2 - d5 - ep] Gamma[d2 + d3 + d5 + d6 + 2 ep] Gamma[
     1 + d4 - d5 - d6 - ep - Z1] Gamma[-Z1] Gamma[-1 - d4 + d5 - d7 + 
      Z1] Gamma[-1 - d1 - d4 - d7 - ep + Z1] Gamma[
     d2 - d4 + d5 + d6 + ep + Z1])/(Gamma[1 + d2] Gamma[1 + d3] Gamma[
     1 + d5] Gamma[
     1 + d6] Gamma[-1 - d1 - d4 - d7 - ep] Gamma[-d4 - d7 - ep + Z1])

R10tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d2 - d3 - d6 - d7 - 2 ep) SS^(-2 - d1 - d5)
     UU^(-1 - d4)
     Gamma[-d6 - d7 - ep] Gamma[d2 + d3 + d6 + d7 + 2 ep] Gamma[
     1 - d2 + d4 - d7 - ep - Z1] Gamma[-Z1] Gamma[-1 - d4 - d5 + d7 + 
      Z1] Gamma[-1 - d1 - d4 - d5 - ep + Z1] Gamma[
     d2 - d4 + d6 + d7 + ep + Z1])/(Gamma[1 + d2] Gamma[1 + d3] Gamma[
     1 + d6] Gamma[
     1 + d7] Gamma[-1 - d1 - d4 - d5 - ep] Gamma[-d4 - d5 - ep + Z1])

R11tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, ep_] := (
 mts^(-d1 - 2 d2 - d3 - d5 - d6 - 2 d7 - 4 ep)
   SS^(-2 + d2 + d7 + 2 ep) UU^(-1 - d4)
   Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - ep] Gamma[
   d1 + d2 + d5 + d7 + 2 ep] Gamma[d2 + d3 + d6 + d7 + 2 ep])/(
 Gamma[1 + d1] Gamma[1 + d2] Gamma[1 + d3] Gamma[1 + d7])

R12tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, 
  ep_] := (mts^(-d3 - d4 - ep)
     SS^(-3 - d1 - d2 - d5 - d6 - d7 - ep - Z1 - Z2) TT^Z1 UU^
    Z2 Gamma[
     d3 + d4 + ep] Gamma[-1 - d2 - d5 - d6 - ep - 
      Z1] Gamma[-Z1] Gamma[
     1 + d6 + Z1] Gamma[-1 - d2 - d6 - d7 - ep - Z2] Gamma[-2 - d1 - 
      d2 + d3 - d5 - d6 - d7 - ep - Z1 - Z2] Gamma[-Z2] Gamma[
     1 + d2 + Z2] Gamma[1 + d4 + Z1 + Z2] Gamma[
     2 + d2 + d5 + d6 + d7 + ep + Z1 + Z2])/(Gamma[1 + d2] Gamma[
     1 + d3] Gamma[1 + d4] Gamma[1 + d5] Gamma[1 + d6] Gamma[
     1 + d7] Gamma[-d2 - d5 - d6 - d7 - 2 ep] Gamma[-1 - d1 - d2 + 
      d3 + d4 - d5 - d6 - d7 - ep])

R13tmp[{d1_, d2_, d3_, d4_, d5_, d6_, d7_}, ep_] := (
 mts^(-d1 - d2 - d3 - 2 d5 - 2 d6 - d7 - 4 ep)
   SS^(-2 + d5 + d6 + 2 ep) TT^(-1 - d4)
   Gamma[-d2 - d5 - ep] Gamma[-d6 - d7 - ep] Gamma[
   d2 + d3 + d5 + d6 + 2 ep] Gamma[d1 + d5 + d6 + d7 + 2 ep])/(
 Gamma[1 + d1] Gamma[1 + d3] Gamma[1 + d5] Gamma[1 + d6])

TempIntList = {R2tmp, R3tmp, R4tmp, R5tmp, R6tmp, R7tmp, R8tmp, R9tmp, R10tmp, R11tmp, R12tmp, R13tmp};

HardIntList = {R1tmp};
