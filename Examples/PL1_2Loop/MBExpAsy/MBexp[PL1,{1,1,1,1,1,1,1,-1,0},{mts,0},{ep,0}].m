((3*SS - 7*UU)/(2*SS^2*UU) + (-3*SS + 2*UU)/(SS^2*UU))/ep^4 + 
 ((-2*SS*Log[mts] + 6*UU*Log[mts] + UU*Log[SS] - SS*Log[UU])/(SS^2*UU) + 
   (2*(SS*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2 + 
      2*SS*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2] + 
      SS*Gamma[-1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[3 + Z2] + 
      SS*Log[mts] - 3*UU*Log[mts] + 2*SS*Log[UU] + UU*Log[UU]))/(SS^2*UU))/
  ep^3 + 
 (-1/12*(SS^(-2 - Z1)*(Pi^2*SS^(1 + Z1) + 11*Pi^2*SS^Z1*UU + 
       12*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2 + 
       12*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2 + 
       24*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1] + 
       24*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3 + 
       24*SS^(1 + Z1)*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2] + 
       12*SS^(1 + Z1)*Gamma[-1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2 + 
       24*SS^Z1*UU*Gamma[-1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2 - 
       12*SS^(1 + Z1)*Log[mts]^2 + 60*SS^Z1*UU*Log[mts]^2 + 
       24*SS^Z1*UU*Log[mts]*Log[SS] - 24*SS^(1 + Z1)*Log[mts]*Log[UU]))/UU + 
   (SS^(-3 - Z1)*(-6*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]^2*Gamma[1 + Z2] - 18*SS^(1 + Z1)*UU*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2 - 6*SS^(1 + Z1)*UU*Gamma[1 - Z2]*
       Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2 - 
      18*SS^(1 + Z1)*UU*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^3 - 
      4*Pi^2*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2] + 
      4*Pi^2*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 12*SS*UU^(1 + Z1)*
       Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2] - 12*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      12*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2] - 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      48*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      12*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2] - 12*UU^(2 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[2 + Z2] - 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2] - 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2] - 12*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2] - 12*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2] - 6*SS^(2 + Z1)*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2] - 
      12*SS^(1 + Z1)*UU*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2] + 24*SS^(2 + Z1)*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2] - 
      30*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
       Gamma[2 + Z2] - 24*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]^2*Gamma[2 + Z2] - 18*SS^(2 + Z1)*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2 - 
      6*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2 - 18*SS^(1 + Z1)*UU*Gamma[-1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2 + 48*SS^(2 + Z1)*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2 - 
      30*SS^(2 + Z1)*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2 - 
      18*SS^(1 + Z1)*UU*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2 - 
      6*SS^(2 + Z1)*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2] - 6*SS^(1 + Z1)*UU*Gamma[-1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2] + 
      24*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2] - 18*SS^(2 + Z1)*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2] - 
      6*SS^(1 + Z1)*UU*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2] - 12*SS^(2 + Z1)*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Log[mts] - 
      24*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Log[mts] - 12*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Log[mts] - 
      3*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[mts]^2 + 
      30*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[mts]^2 + 
      6*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[SS] + 
      12*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[mts]*Log[SS] - 
      6*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[UU] - 
      12*SS^(2 + Z1)*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Log[UU] - 24*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Log[UU] - 12*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Log[UU] - 
      18*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[mts]*Log[UU] - 
      12*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[SS]*Log[UU] - 
      15*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[UU]^2 - 
      6*SS^(1 + Z1)*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Log[UU]^2 - 
      12*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*PolyGamma[0, -1 - Z2] + 
      24*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       PolyGamma[0, 1 - Z2] + 12*SS^(2 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       PolyGamma[0, 1 - Z2] - 24*SS^(2 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*PolyGamma[0, -Z2]))/
    (6*UU*Gamma[1 - Z2]*Gamma[2 + Z2]))/ep^2 + 
 (SS^(-2 - Z1)*(-1440*SS^(1 + Z1)*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[Z1 + Z2]*Gamma[1 + Z1 + Z2] - 1440*SS^(1 + Z1)*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[Z1 + Z2]*Gamma[1 + Z1 + Z2] + 2160*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2 - 
    1440*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2 - 2880*SS^Z1*UU*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2 + 2160*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2 - 1440*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2 - 720*SS^(1 + Z1)*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]^2 - 1440*SS^Z1*UU*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]^2 + 2880*EulerGamma*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^3*
     Gamma[2 + Z1 + Z2] - 1440*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    1440*EulerGamma*SS^(1 + Z1)*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    240*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2] - 151*Pi^4*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 53*Pi^4*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    1440*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    720*EulerGamma^2*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    240*Pi^2*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    1440*EulerGamma^2*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    240*Pi^2*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    2880*EulerGamma^2*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    480*Pi^2*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 4320*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    2160*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 2160*EulerGamma^2*UU^(1 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2] + 600*Pi^2*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    720*EulerGamma^2*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    120*Pi^2*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    360*EulerGamma^2*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    60*Pi^2*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
    720*EulerGamma^2*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    120*Pi^2*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
    2160*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 1440*EulerGamma*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] + 2160*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*
     Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 1440*EulerGamma*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*
     Log[mts] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2*Log[mts] - 5760*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^3*
     Gamma[2 + Z1 + Z2]*Log[mts] + 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    1440*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    2880*EulerGamma*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    5760*EulerGamma*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 
    2880*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 2880*EulerGamma*SS^(1 + Z1)*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 1440*EulerGamma*SS^(1 + Z1)*
     Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 2880*EulerGamma*SS^Z1*UU*
     Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts] - 840*Pi^2*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]^2 - 
    600*Pi^2*SS^Z1*UU*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 1440*SS^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 1440*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[mts]^2 - 2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]^2 + 
    720*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 2880*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 2880*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^2 - 240*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]^4 - 
    720*SS^Z1*UU*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[mts]^4 - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2*Log[SS] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*Log[SS] + 
    4320*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[SS] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[SS] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[SS] - 720*Pi^2*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*Log[SS] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]*Log[SS] - 960*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]^3*Log[SS] - 
    2160*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[SS]^2 + 720*Pi^2*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*Log[UU] + 
    960*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]^3*Log[UU] + 720*EulerGamma*SS^(1 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
    1440*EulerGamma*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] + 1440*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, 1 - Z1] - 360*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]^2 - 360*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]^2 - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*PolyGamma[0, -Z1] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2*PolyGamma[0, -Z1] + 2880*EulerGamma*SS^Z1*UU*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 8640*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -Z1] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 2880*SS^Z1*UU*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, -Z1] - 2880*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 8640*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 720*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1]^2 - 
    8640*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1]^2 + 1440*SS^(1 + Z1)*Gamma[2 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z1] - 1440*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z1] - 2880*EulerGamma*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z1] - 2880*EulerGamma*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z1] - 1440*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, Z1] - 2880*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, Z1] - 2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, Z1] + 720*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, Z1] + 1440*SS^Z1*UU*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, Z1] + 
    1440*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, Z1] - 
    720*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1]^2 - 
    1440*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1]^2 - 
    720*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1]^2 + 
    1440*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    720*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 1440*EulerGamma*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 5760*EulerGamma*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 10800*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    4320*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    1440*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] - 5760*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 5760*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 8640*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 720*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 720*SS^Z1*UU*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1]*
     PolyGamma[0, 1 + Z1] + 17280*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1]*
     PolyGamma[0, 1 + Z1] - 720*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z1]*PolyGamma[0, 1 + Z1] - 1440*SS^Z1*UU*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1]*PolyGamma[0, 1 + Z1] - 
    2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1]*PolyGamma[0, 1 + Z1] - 
    360*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    360*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    6480*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 + 2880*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^3*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 1440*EulerGamma*SS^(1 + Z1)*
     Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    2880*EulerGamma*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] + 2880*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z2] + 
    5760*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2] - 1080*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 - 2160*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 + 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1 - Z2] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*PolyGamma[0, -Z1 - Z2] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]^2*PolyGamma[0, -Z1 - Z2] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    2880*EulerGamma*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -Z2] - 1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2] + 5760*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z2] - 
    2160*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2]^2 + 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z2] - 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2]*
     PolyGamma[0, Z2] + 720*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z2]^2 + 1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z2] + 1440*EulerGamma*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 720*EulerGamma*SS^(1 + Z1)*
     Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 
    1440*EulerGamma*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z2] - 2880*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, 1 + Z2] - 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z2] - 
    2880*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z2] + 1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -Z2]*PolyGamma[0, 1 + Z2] + 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, Z2]*PolyGamma[0, 1 + Z2] + 
    1080*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 + Z2]^2 + 2160*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2]^2 - 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z2] + 
    720*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*
     PolyGamma[0, 2 + Z2] + 1440*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*PolyGamma[0, 2 + Z2] + 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2]*
     PolyGamma[0, 2 + Z2] - 1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, Z2]*PolyGamma[0, 2 + Z2] - 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2]*PolyGamma[0, 2 + Z2] - 
    1440*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2]*
     PolyGamma[0, 2 + Z2] - 2880*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2]*PolyGamma[0, 2 + Z2] + 
    720*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z2]^2 + 
    360*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z2]^2 + 720*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z2]^2 + 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*
     PolyGamma[0, 1 + Z1 + Z2] + 1440*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]^2*PolyGamma[0, 1 + Z1 + Z2] + 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1 + Z2] + 
    1440*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^3*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1 + Z2] - 
    360*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 
    360*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 
    720*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, -Z1] - 
    4320*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[1, -Z1] - 720*SS^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[1, Z1] - 720*SS^Z1*UU*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[1, Z1] - 720*SS^Z1*UU*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[1, Z1] - 360*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
     Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[1, 1 + Z1] - 1080*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]^2*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[1, 1 + Z1] - 2880*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[1, 1 + Z1] - 3600*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 
    1080*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 
    2160*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 
    2160*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, -Z2] + 
    720*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, Z2] + 
    360*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 1 + Z2] + 
    720*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 1 + Z2] - 
    720*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 2 + Z2] - 
    360*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 2 + Z2] - 
    720*SS^Z1*UU*Gamma[1 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[1, 2 + Z2] + 
    240*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*(1 - Zeta[3]) - 1440*SS^(1 + Z1)*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*(1 - Zeta[3]) + 
    240*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Zeta[3] - 480*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]*Zeta[3] - 
    7200*SS^Z1*UU*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[mts]*Zeta[3] - 1920*SS^Z1*UU*Gamma[1 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS]*Zeta[3] + 
    1920*SS^(1 + Z1)*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Log[UU]*Zeta[3]))/(720*UU*Gamma[1 + Z1]*Gamma[1 - Z2]*
   Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]) + 
 (SS^(-3 - Z1)*(240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] + 480*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 480*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 + 720*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 + 720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*
     Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 
    720*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 - 360*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2 - 360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 - 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]^2 + 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]^2 + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]^2*
     Gamma[1 + Z2]*Gamma[3 + Z1 + Z2] - 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*
     Gamma[3 + Z1 + Z2] - 480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
     Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2] - 
    480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2] - 1440*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2] - 4*Pi^4*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 16*Pi^4*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 360*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 360*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 1440*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 840*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    720*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 360*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 960*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 480*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 600*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 180*EulerGamma^2*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    50*Pi^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 480*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 360*EulerGamma^2*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    100*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 1560*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 960*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 1320*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 720*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 1320*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 600*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 40*Pi^2*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 1320*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 600*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 40*Pi^2*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    1080*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    360*EulerGamma^2*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    100*Pi^2*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    960*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    360*EulerGamma^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    100*Pi^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    3240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    2160*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    1440*EulerGamma^2*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    400*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    1320*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    600*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma^2*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    40*Pi^2*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    1320*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 600*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 40*Pi^2*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 240*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 180*EulerGamma^2*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 50*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    720*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    360*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 720*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 360*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    720*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 360*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 240*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 600*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 360*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    1320*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 600*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 40*Pi^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 1320*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    600*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    40*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 840*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 240*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 720*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    360*EulerGamma^2*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 100*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 1320*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 600*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    40*Pi^2*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 1320*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 600*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma^2*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    40*Pi^2*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] - 480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    1280*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
    2400*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 1920*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
    1440*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2] + 2560*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
    2400*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2] - 1440*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] + 
    1280*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
    1440*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
    480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 480*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 960*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 480*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 480*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 720*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] - 360*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 720*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] - 360*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 360*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 480*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 480*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 960*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] - 480*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    1200*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 600*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 960*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    480*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    360*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[mts] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[mts] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[mts] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*
     Log[mts] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[mts] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[mts] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]^2*Log[mts] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]^2*
     Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts] - 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 2080*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    840*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    480*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 960*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 720*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 600*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 240*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 600*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 480*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 480*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    1920*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] + 600*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 240*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 240*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 480*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 1200*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] + 960*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] - 
    1920*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    1200*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts] + 720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    40*Pi^2*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 80*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 + 
    60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 + 120*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 + 480*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 + 60*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 + 120*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 + 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
    300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 + 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^2 - 40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^3 - 
    80*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^3 - 
    40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]^3 + 5*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^4 + 90*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^4 - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[SS] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[SS] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[SS] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*Log[SS] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]^2*Log[SS] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 360*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 840*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 480*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    960*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    600*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    600*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    3600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    600*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 240*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 360*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 600*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 720*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] + 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS] - 120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS] - 480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[SS] - 240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[SS] - 240*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[SS] - 960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[SS] - 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[SS] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS] - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[SS] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^3*Log[SS] - 
    180*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]^2 - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS]^2 - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS]^2 - 360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS]^2 - 1440*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS]^2 - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[SS]^2 - 120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    180*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]^2 - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]^2 - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]^2 - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]^2*
     Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 120*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*
     Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 240*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     Log[UU] + 720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU] - 
    1360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] - 1040*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
    120*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] + 1200*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] + 960*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] + 720*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] - 1920*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU] + 1200*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
    720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
    960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    80*Pi^2*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] + 80*Pi^2*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] - 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] - 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] + 
    960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 
    600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU] + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]*Log[UU] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] + 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*
     Log[UU] - 20*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^3*Log[UU] + 80*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^3*Log[UU] - 
    480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*Log[UU] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS]*
     Log[UU] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[SS]*Log[UU] - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    40*Pi^2*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 + 480*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 + 
    160*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 + 240*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 300*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 + 480*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]^2 - 300*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     Log[UU]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 - 240*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 - 
    90*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU]^2 - 60*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*
     Log[UU]^2 + 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*Log[UU]^2 - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[SS]*
     Log[UU]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^3 - 40*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^3 - 80*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]^3 - 40*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^3 - 
    100*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^3 - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*Log[UU]^3 - 
    35*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]^4 + 50*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^4 - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 840*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 120*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 960*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] + 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    3600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] + 960*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 120*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] + 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    1560*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 600*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z1] - 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z1] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z1] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -1 - Z1] - 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    2880*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -1 - Z1] - 720*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -1 - Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -1 - Z1] - 180*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]^2 - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]^2 - 720*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]^2 - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]^2 - 
    60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]^2 - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] - 360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 600*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] + 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 480*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 960*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
    360*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] - 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 600*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 240*EulerGamma*UU^(2 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
    360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 600*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z1] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 - Z1] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 - Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 - Z1] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 - Z1] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 - Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 - Z1] - 
    180*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]^2 - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]^2 - 60*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]^2 - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]^2 - 60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]^2 - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]^2 - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 - Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 - Z1] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -Z1] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -Z1] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*
     PolyGamma[0, -Z1] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1] - 360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 720*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 240*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 600*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 600*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1] - 120*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 960*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 960*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 3720*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 960*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 360*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 960*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 360*EulerGamma*UU^(2 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 360*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    720*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 240*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 240*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1] - 240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1] - 240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 480*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z1] - 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 360*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z1] - 240*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -Z1] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 1440*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -Z1] - 
    2880*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 360*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, -Z1] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, -Z1] - 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, -Z1] - 
    2880*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, -Z1] - 120*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*
     PolyGamma[0, -Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, -Z1] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, -Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]^2 - 60*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]^2 - 1440*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]^2 - 360*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]^2 - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]^2 - 300*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]^2 - 300*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]^2 - 60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]^2 - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]^2 + 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 600*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*
     Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 360*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 480*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 240*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 480*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 360*SS*UU^(1 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 120*EulerGamma*UU^(2 + Z1)*
     Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    480*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 240*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 840*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 480*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 240*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 360*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 120*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 120*EulerGamma*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1] + 120*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 480*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 480*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*
     PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*
     PolyGamma[0, 1 + Z1] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 1 + Z1] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 120*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 
    960*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*
     PolyGamma[0, 1 + Z1] + 240*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 240*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]*PolyGamma[0, 1 + Z1] - 60*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 60*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]^2 - 
    60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z1]^2 - 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] + 120*EulerGamma*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1] + 240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 
    1440*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] + 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 
    1920*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1] + 480*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 8640*UU^(2 + Z1)*
     Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] + 
    2400*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1] - 1200*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1] + 360*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 1440*UU^(2 + Z1)*
     Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] + 
    240*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] + 240*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z1] + 480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 2 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z1] + 720*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 2 + Z1] + 
    3360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z1] + 480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z1] + 480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 2 + Z1] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 2 + Z1] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 2 + Z1] + 960*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 2 + Z1] + 1200*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 2 + Z1] + 
    5280*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 2 + Z1] + 720*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*
     PolyGamma[0, 2 + Z1] + 960*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 2 + Z1] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1] + 
    1200*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1] + 
    5280*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1] + 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*
     PolyGamma[0, 2 + Z1] + 1920*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1]*PolyGamma[0, 2 + Z1] + 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1]*PolyGamma[0, 2 + Z1] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z1]*PolyGamma[0, 2 + Z1] + 
    1920*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*
     PolyGamma[0, 2 + Z1] + 1200*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1]*PolyGamma[0, 2 + Z1] + 
    5280*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1]*PolyGamma[0, 2 + Z1] - 240*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]*
     PolyGamma[0, 2 + Z1] - 480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]*PolyGamma[0, 2 + Z1] - 
    480*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]*
     PolyGamma[0, 2 + Z1] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]*PolyGamma[0, 2 + Z1] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1]*
     PolyGamma[0, 2 + Z1] - 300*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1]^2 - 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1]^2 - 600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1]^2 - 840*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1]^2 - 
    3840*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1]^2 - 540*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1]^2 - 600*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1]^2 - 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 3 + Z1] + 480*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 3 + Z1] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 3 + Z1] + 480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*PolyGamma[0, 3 + Z1] + 
    960*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1]*PolyGamma[0, 3 + Z1] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1]*PolyGamma[0, 3 + Z1] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 720*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 240*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 120*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z2] - 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, -1 - Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z2] + 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -1 - Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2*PolyGamma[0, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -1 - Z2] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -1 - Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -1 - Z2] + 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -1 - Z2] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -1 - Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -1 - Z2] - 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]*PolyGamma[0, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2*
     PolyGamma[0, -1 - Z2] - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 - 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]^2 - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]^2 - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2]^2 - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -1 - Z2]^2 - 
    40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]^3 - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 720*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    1920*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 
    1200*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 720*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 120*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] - 720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 240*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
    240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2] + 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2] + 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, 1 - Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2] - 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2] + 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]^2*PolyGamma[0, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2] + 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[UU]*PolyGamma[0, 1 - Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2] - 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2] + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU]*PolyGamma[0, 1 - Z2] + 240*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]*PolyGamma[0, 1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2*PolyGamma[0, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] - 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*
     PolyGamma[0, 1 - Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]^2*PolyGamma[0, 1 - Z2] - 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^2 - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2]^2 - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 1 - Z2]^2 - 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2]^2 - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2]^2 - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2]^2 + 
    80*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^3 + 
    40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]^3 - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -2 - Z1 - Z2] - 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -2 - Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -2 - Z1 - Z2] - 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -2 - Z1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -2 - Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -2 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -1 - Z1 - Z2] - 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1 - Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1 - Z2] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -Z1 - Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, -Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1 - Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1 - Z2] - 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1 - Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1 - Z2] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1 - Z2] - 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z1 - Z2] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1 - Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1 - Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] + 
    720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2] - 1920*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2] + 1200*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] + 
    720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] - 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] + 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] - 600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, -Z2] - 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]^2*
     PolyGamma[0, -Z2] - 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] + 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] - 600*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, -Z2] - 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     Log[UU]*PolyGamma[0, -Z2] - 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2*
     PolyGamma[0, -Z2] + 360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] + 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*
     PolyGamma[0, -Z2] - 960*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] + 600*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] + 360*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] + 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2]*
     PolyGamma[0, -Z2] + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[0, 1 - Z2]*PolyGamma[0, -Z2] - 240*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2*PolyGamma[0, -Z2] - 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 - 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 + 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 - 
    300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 - 180*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]^2 - 240*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z2]^2 - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -Z2]^2 + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*
     PolyGamma[0, -Z2]^2 - 80*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2]^3 - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
     Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
     Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, Z2] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, Z2] - 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, Z2] - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, Z2]^2 - 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, Z2]^2 + 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 
    720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z2] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 + Z2] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 + Z2] - 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 + Z2] - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2]^2 - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2]^2 + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]^2*
     Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
    720*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
     Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     Log[mts]*PolyGamma[0, 2 + Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[0, 2 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
     Log[UU]*PolyGamma[0, 2 + Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     Log[UU]*PolyGamma[0, 2 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 2 + Z2] + 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 2 + Z2] + 
    120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, Z2]*
     PolyGamma[0, 2 + Z2] + 120*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, Z2]*PolyGamma[0, 2 + Z2] + 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 + Z2]*PolyGamma[0, 2 + Z2] + 
    360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2]*
     PolyGamma[0, 2 + Z2] - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z2]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z2]^2 - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z2]^2 - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z2]^2 + 120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
     Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] - 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
     Gamma[2 + Z1 + Z2]^2*PolyGamma[0, 2 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    240*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
     Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    480*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
     Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 480*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*
     Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
     Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
     Gamma[-Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1 + Z2] + 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] + 240*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 2 + Z1 + Z2] - 180*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 
    360*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -1 - Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 
    1440*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -1 - Z1] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
     Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 
    60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 
    720*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -1 - Z1] - 120*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 60*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z1] - 
    180*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 360*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 
    60*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 1 - Z1] - 120*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 60*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 1 - Z1] - 120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z1] - 
    120*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
     Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z1] - 60*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 720*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 360*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 1440*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 180*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 180*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z1] - 
    120*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z1] - 60*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 60*UU^(2 + Z1)*Gamma[1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 60*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 60*UU^(2 + Z1)*Gamma[-Z1]^3*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 60*SS*UU^(1 + Z1)*
     Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 1 + Z1] - 60*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
     Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 
    60*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 60*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z1] - 
    300*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
     Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z1] - 600*UU^(2 + Z1)*Gamma[-1 - Z1]*
     Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z1] - 
    600*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z1] - 
    600*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
     Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 2 + Z1] - 2400*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
     Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z1] - 
    300*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z1] - 
    600*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
     Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 2 + Z1] - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 180*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 60*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 60*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -1 - Z2] - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 60*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[1, -1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[1, -1 - Z2] - 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[1, -1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, 1 - Z2]*PolyGamma[1, -1 - Z2] + 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] - 
    480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 180*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 60*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 60*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 1 - Z2] + 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 60*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[1, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
     PolyGamma[1, 1 - Z2] + 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
     PolyGamma[1, 1 - Z2] + 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[1, 1 - Z2] + 
    120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -1 - Z2]*PolyGamma[1, 1 - Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*
     PolyGamma[1, 1 - Z2] - 120*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*PolyGamma[1, 1 - Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2]*
     PolyGamma[1, 1 - Z2] - 180*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z2] - 60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z2] - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z2] + 480*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, -Z2] - 300*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
     Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z2] - 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[1, -Z2] - 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[1, -Z2] + 
    240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*
     PolyGamma[1, -Z2] - 240*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[0, -Z2]*PolyGamma[1, -Z2] - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, Z2] - 60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[1, Z2] - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[1, 1 + Z2] - 180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
     Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 + Z2] + 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z2] + 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
     Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z2] + 
    60*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
     Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z2] + 
    180*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
     Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[1, 2 + Z2] - 
    40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     PolyGamma[2, -1 - Z2] + 80*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
     PolyGamma[2, 1 - Z2] + 40*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
     Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
     Gamma[3 + Z1 + Z2]*PolyGamma[2, 1 - Z2] - 80*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
     Gamma[3 + Z1 + Z2]*PolyGamma[2, -Z2] - 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*(1 - Zeta[3]) + 
    640*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*(1 - Zeta[3]) + 
    1280*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
     Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*(1 - Zeta[3]) + 
    640*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
     Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
     (1 - Zeta[3]) + 720*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*(1 - Zeta[3]) - 
    2080*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*(1 - Zeta[3]) + 
    240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[SS]*(1 - Zeta[3]) + 
    1360*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*(1 - Zeta[3]) + 560*SS^(1 + Z1)*UU*
     Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*
     (1 - Zeta[3]) - 360*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Zeta[3] + 80*SS^(2 + Z1)*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Zeta[3] + 
    560*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[mts]*Zeta[3] + 240*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS]*Zeta[3] + 
    80*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[3 + Z1 + Z2]*Log[UU]*Zeta[3] + 160*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
     Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]*Zeta[3]))/
  (120*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]) + 
 ((SS^(-2 - Z1)*(12*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]^2 + 12*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]^2 + 24*SS^Z1*UU*Gamma[-1 - Z2]*Gamma[1 - Z2]*
       Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[2 + Z1 + Z2] - 
      6*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
      6*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
      12*EulerGamma*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
      24*EulerGamma*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
      18*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2] - 12*EulerGamma*UU^(1 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
      12*EulerGamma*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
      6*EulerGamma*SS^(1 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] - 
      12*EulerGamma*SS^Z1*UU*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2] + 
      12*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 12*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]^2*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
      4*Pi^2*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Log[mts] + 8*Pi^2*SS^Z1*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Log[mts] + 12*SS^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 
      12*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 24*SS^Z1*UU*Gamma[-Z1]^2*
       Gamma[Z1]*Gamma[1 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Log[mts] + 24*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 
      12*SS^(1 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts] + 24*SS^Z1*UU*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Log[mts] + 16*SS^Z1*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Log[mts]^3 + 3*Pi^2*SS^Z1*UU*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[SS] + 24*UU^(1 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Log[SS] + 12*SS^Z1*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Log[mts]^2*Log[SS] - 3*Pi^2*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Log[UU] - 12*SS^(1 + Z1)*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Log[mts]^2*Log[UU] - 
      6*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 - Z1] - 
      12*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 
      48*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z1] + 6*SS^(1 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       PolyGamma[0, Z1] + 12*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1] + 
      12*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, Z1] + 
      6*SS^Z1*UU*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] + 
      24*SS^Z1*UU*Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      60*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 12*SS^(1 + Z1)*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 24*SS^Z1*UU*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
      24*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, -Z2] + 
      12*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 
      6*SS^(1 + Z1)*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 
      12*SS^Z1*UU*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*PolyGamma[0, 1 + Z2] + 
      6*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       (1 - Zeta[3]) - 6*SS^(1 + Z1)*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Zeta[3] + 38*SS^Z1*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Zeta[3]))/(6*UU*Gamma[1 - Z2]*Gamma[2 + Z2]*
     Gamma[2 + Z1 + Z2]) + 
   (SS^(-3 - Z1)*(6*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
       Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
       Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
       Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
       Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
       Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
      12*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*
       Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*
       Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
      12*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] - 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
       Gamma[1 + Z1 + Z2]*Gamma[2 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]^2 + 18*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*
       Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]^2 + 18*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]^2 + 18*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]^2 + 18*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]^2 - 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]^2 + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
       Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2] - 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*
       Gamma[3 + Z1 + Z2] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2] - 
      36*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2] + 
      52*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] - 32*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 3*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      18*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*
       Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 42*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]^3*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^3*
       Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      24*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
       Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] - 6*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 24*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 12*EulerGamma*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      48*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      36*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      30*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*EulerGamma*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      30*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*EulerGamma*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*EulerGamma*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      84*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      48*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      30*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*EulerGamma*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      30*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 12*EulerGamma*UU^(2 + Z1)*
       Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] - 12*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
       Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*Gamma[-Z1]*Gamma[Z1]*
       Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] - 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[2 - Z1]*
       Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 18*UU^(2 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]^2*Gamma[Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 18*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 18*UU^(2 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]^3*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] - 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 6*UU^(2 + Z1)*Gamma[-Z1]^3*
       Gamma[Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 30*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 30*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*EulerGamma*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 12*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]^2*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 12*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 12*EulerGamma*UU^(2 + Z1)*
       Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      30*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 12*EulerGamma*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 30*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 12*EulerGamma*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      48*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 
      60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] - 48*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2] + 18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
      36*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] + 
      18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2] + 96*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
      60*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2] - 36*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2] - 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] + 
      48*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
      36*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[Z2]*
       Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
       Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 12*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 24*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      18*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 18*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 12*UU^(2 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-1 - Z2]*
       Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*
       Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*
       Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*
       Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*UU^(2 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 24*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^3*Gamma[-1 - Z2]*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[2 + Z1]^3*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 18*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z2]*
       Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*
       Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      18*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[3 + Z1]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 30*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*
       Gamma[3 + Z1 + Z2] + 18*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]^2*
       Gamma[2 + Z2]^2*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      24*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[-Z1 - Z2]*Gamma[-Z2]^2*Gamma[2 + Z2]^2*
       Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 6*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
       Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
       Gamma[3 + Z1]*Gamma[-1 - Z2]*Gamma[-2 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      18*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[-1 - Z2]*Gamma[-1 - Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z1 - Z2]*Gamma[-Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[2 + Z1 + Z2]*Gamma[3 + Z1 + Z2] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
       Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[mts] + 
      4*Pi^2*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 2*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      12*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[mts] + 12*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[mts] + 12*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[mts] + 12*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] - 24*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 30*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 24*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 18*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[mts] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[mts] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[mts] - 48*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[mts] + 30*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       Log[mts] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts] + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[mts] - 24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 18*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts]^2 + 12*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[mts]^2 + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2 - 
      14*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts]^3 + 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[SS] + 24*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 12*SS*UU^(1 + Z1)*
       Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[SS] + 24*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      24*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[SS] + 96*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[SS] + 12*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[SS] + 24*UU^(2 + Z1)*Gamma[-1 - Z1]^2*
       Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[SS] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[SS] - 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[SS] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
       Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*Log[UU] + 4*Pi^2*SS^(2 + Z1)*
       Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[UU] - 10*Pi^2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      30*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 24*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[UU] + 18*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[UU] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[UU] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[UU] - 48*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*Log[UU] + 30*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU] + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] - 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU] + 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] + 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU] + 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
       Log[UU] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU] - 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]^2*Log[UU] - 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[SS]*Log[UU] + 12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]^2*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^2 + 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]^2 + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       Log[UU]^2 + 12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*Log[UU]^2 + 
      12*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[SS]*Log[UU]^2 + 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Log[UU]^3 - 
      2*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[UU]^3 + 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
      24*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z1] + 24*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
      96*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z1] + 24*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
       Gamma[2 + Z1]^4*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z1] + 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
      48*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z1] + 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z1] + 
      12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 24*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 - Z1] + 6*SS*UU^(1 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 12*UU^(2 + Z1)*Gamma[1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
      6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z1] + 12*SS*UU^(1 + Z1)*
       Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
      6*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z1] + 48*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z1] + 24*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 96*UU^(2 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 18*SS*UU^(1 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 18*UU^(2 + Z1)*Gamma[-Z1]^3*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*
       Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] + 
      12*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z1] - 12*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 + Z1] - 24*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 6*SS*UU^(1 + Z1)*
       Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      6*UU^(2 + Z1)*Gamma[1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 + Z1] - 24*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 + Z1] - 12*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 24*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      6*SS*UU^(1 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      6*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      6*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 6*SS*UU^(1 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 + Z1] - 6*UU^(2 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*
       Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z1] - 
      18*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 36*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 2 + Z1] - 36*UU^(2 + Z1)*Gamma[-Z1]^3*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]^2*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 2 + Z1] - 48*SS*UU^(1 + Z1)*Gamma[-1 - Z1]*Gamma[-Z1]^2*
       Gamma[1 + Z1]*Gamma[2 + Z1]^3*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 216*UU^(2 + Z1)*
       Gamma[-1 - Z1]*Gamma[-Z1]^2*Gamma[1 + Z1]*Gamma[2 + Z1]^3*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 
      30*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^4*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 
      36*UU^(2 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[2 + Z1]^2*Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z1] - 24*UU^(2 + Z1)*
       Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]^2*
       Gamma[3 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 3 + Z1] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 
      18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z2] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]^2*
       Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 6*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] - 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z2] + 18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 6*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, -1 - Z2] + 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
       PolyGamma[0, -1 - Z2] + 12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -1 - Z2] + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z2]^2 - 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 18*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 18*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 18*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
      48*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 
      30*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 18*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 6*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 6*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-1 - Z2]^2*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] + 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 - Z2] - 18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 6*SS^(1 + Z1)*UU*
       Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2] - 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, 1 - Z2] - 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*Log[mts]*
       PolyGamma[0, 1 - Z2] - 24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*
       PolyGamma[0, 1 - Z2] - 12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, 1 - Z2] - 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -1 - Z2]*PolyGamma[0, 1 - Z2] + 
      12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]^2 + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 - Z2]^2 + 18*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]^2*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z2] + 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z2] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z2] - 48*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z2] + 30*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-Z2]^3*
       Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] + 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[-Z2]^3*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, -Z2] + 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[mts]*PolyGamma[0, -Z2] + 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*Log[UU]*PolyGamma[0, -Z2] - 
      24*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*
       Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 - Z2]*
       PolyGamma[0, -Z2] + 12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, -Z2]^2 + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, Z2] + 6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[2 - Z2]*Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, Z2] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[0, 1 + Z2] + 18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*
       Gamma[-Z2]*Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 1 + Z2] - 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]^2*Gamma[1 + Z2]*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*Gamma[Z2]*
       Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
      6*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 - Z2]*Gamma[-Z2]*
       Gamma[Z2]*Gamma[1 + Z2]^2*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] - 
      18*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]^2*Gamma[-Z2]*
       Gamma[1 + Z2]^3*Gamma[3 + Z1 + Z2]*PolyGamma[0, 2 + Z2] + 
      6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*Gamma[1 - Z2]*Gamma[-Z2]*
       Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*Gamma[3 + Z1 + Z2]*
       PolyGamma[1, -1 - Z2] - 12*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[1 - Z2]*
       Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*Gamma[3 + Z1 + Z2]*
       PolyGamma[1, 1 - Z2] - 6*SS^(2 + Z1)*Gamma[2 + Z1]*Gamma[-1 - Z2]*
       Gamma[1 - Z2]*Gamma[-Z2]*Gamma[1 + Z2]*Gamma[2 + Z2]*Gamma[3 + Z2]*
       Gamma[3 + Z1 + Z2]*PolyGamma[1, 1 - Z2] + 12*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[-Z2]^2*Gamma[1 + Z2]*Gamma[2 + Z2]^2*
       Gamma[3 + Z1 + Z2]*PolyGamma[1, -Z2] - 52*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*(1 - Zeta[3]) + 
      32*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*(1 - Zeta[3]) - 4*SS^(2 + Z1)*Gamma[2 + Z1]*
       Gamma[1 - Z2]*Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]*Zeta[3] - 
      24*SS^(1 + Z1)*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*Gamma[2 + Z2]*
       Gamma[3 + Z1 + Z2]*Zeta[3]))/(6*UU*Gamma[2 + Z1]*Gamma[1 - Z2]*
     Gamma[2 + Z2]*Gamma[3 + Z1 + Z2]))/ep
