(mus^(2*ep)*((-1/6*(Pi^2*s)/(s - TT) - (s*H[0, mts/s]^2)/(2*(s - TT)) - 
     (s*H[0, mts/s]*H[1, TT/s])/(s - TT) - (s*H[1, TT/s]^2)/(2*(s - TT)))/
    ep^2 + (H[0, mts/s]^3/3 - (Pi^2*(5*s - 3*TT)*H[1, TT/s])/(3*(s - TT)) - 
     (TT*H[0, mts/s]^2*H[1, TT/s])/(s - TT) - ((4*s - TT)*H[1, TT/s]^3)/
      (3*(s - TT)) + H[0, mts/s]*(-1/3*(Pi^2*TT)/(s - TT) - 
       (2*s*H[1, TT/s]^2)/(s - TT)) + im*(-1/6*(Pi^3*(s + 2*TT))/(s - TT) + 
       2*Pi*H[0, mts/s]*H[1, TT/s] + Pi*H[1, TT/s]^2 + 
       (Pi*(5*s - 2*TT)*H[0, 1, TT/s])/(s - TT)) - 
     ((5*s - 2*TT)*H[0, 1, 1, TT/s])/(s - TT) + ((3*s + 4*TT)*Zeta[3])/
      (s - TT))/ep))/s^(2*(1 + ep))
