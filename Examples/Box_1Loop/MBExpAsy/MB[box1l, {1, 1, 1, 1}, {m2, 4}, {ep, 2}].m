(Pi^2 + 6*Log[m2]^2)/(6*SS*TT) + (Pi^2 + 6*Log[m2]^2 - 12*Log[m2]*Log[SS])/
  (12*SS*TT) + (Pi^2 + 6*Log[m2]^2 - 12*Log[m2]*Log[TT])/(12*SS*TT) + 
 (-(Log[m2]*Log[SS]) - Log[m2]*Log[TT])/(SS*TT) - 
 (2*(2*Pi^2 - 3*Log[SS]*Log[TT]))/(3*SS*TT) + 
 m2^4*(-544/(9*SS*TT^5) - (70*Pi^2)/(SS*TT^5) - 5608/(9*SS^2*TT^4) - 
   (280*Pi^2)/(SS^2*TT^4) - 1054/(SS^3*TT^3) - (420*Pi^2)/(SS^3*TT^3) - 
   5608/(9*SS^4*TT^2) - (280*Pi^2)/(SS^4*TT^2) - 544/(9*SS^5*TT) - 
   (70*Pi^2)/(SS^5*TT) + (647*Log[m2])/(SS*TT^5) + 
   (2168*Log[m2])/(SS^2*TT^4) + (3112*Log[m2])/(SS^3*TT^3) + 
   (2168*Log[m2])/(SS^4*TT^2) + (647*Log[m2])/(SS^5*TT) + 
   (140*Log[m2]^2)/(SS*TT^5) + (560*Log[m2]^2)/(SS^2*TT^4) + 
   (840*Log[m2]^2)/(SS^3*TT^3) + (560*Log[m2]^2)/(SS^4*TT^2) + 
   (140*Log[m2]^2)/(SS^5*TT) - (533*Log[SS])/(3*SS*TT^5) - 
   (2552*Log[SS])/(3*SS^2*TT^4) - (1556*Log[SS])/(SS^3*TT^3) - 
   (3952*Log[SS])/(3*SS^4*TT^2) - (1408*Log[SS])/(3*SS^5*TT) - 
   (140*Log[m2]*Log[SS])/(SS*TT^5) - (560*Log[m2]*Log[SS])/(SS^2*TT^4) - 
   (840*Log[m2]*Log[SS])/(SS^3*TT^3) - (560*Log[m2]*Log[SS])/(SS^4*TT^2) - 
   (140*Log[m2]*Log[SS])/(SS^5*TT) - (1408*Log[TT])/(3*SS*TT^5) - 
   (3952*Log[TT])/(3*SS^2*TT^4) - (1556*Log[TT])/(SS^3*TT^3) - 
   (2552*Log[TT])/(3*SS^4*TT^2) - (533*Log[TT])/(3*SS^5*TT) - 
   (140*Log[m2]*Log[TT])/(SS*TT^5) - (560*Log[m2]*Log[TT])/(SS^2*TT^4) - 
   (840*Log[m2]*Log[TT])/(SS^3*TT^3) - (560*Log[m2]*Log[TT])/(SS^4*TT^2) - 
   (140*Log[m2]*Log[TT])/(SS^5*TT) + (140*Log[SS]*Log[TT])/(SS*TT^5) + 
   (560*Log[SS]*Log[TT])/(SS^2*TT^4) + (840*Log[SS]*Log[TT])/(SS^3*TT^3) + 
   (560*Log[SS]*Log[TT])/(SS^4*TT^2) + (140*Log[SS]*Log[TT])/(SS^5*TT)) + 
 m2^3*(296/(9*SS*TT^4) + (20*Pi^2)/(SS*TT^4) + 164/(SS^2*TT^3) + 
   (60*Pi^2)/(SS^2*TT^3) + 164/(SS^3*TT^2) + (60*Pi^2)/(SS^3*TT^2) + 
   296/(9*SS^4*TT) + (20*Pi^2)/(SS^4*TT) - (172*Log[m2])/(SS*TT^4) - 
   (436*Log[m2])/(SS^2*TT^3) - (436*Log[m2])/(SS^3*TT^2) - 
   (172*Log[m2])/(SS^4*TT) - (40*Log[m2]^2)/(SS*TT^4) - 
   (120*Log[m2]^2)/(SS^2*TT^3) - (120*Log[m2]^2)/(SS^3*TT^2) - 
   (40*Log[m2]^2)/(SS^4*TT) + (148*Log[SS])/(3*SS*TT^4) + 
   (188*Log[SS])/(SS^2*TT^3) + (248*Log[SS])/(SS^3*TT^2) + 
   (368*Log[SS])/(3*SS^4*TT) + (40*Log[m2]*Log[SS])/(SS*TT^4) + 
   (120*Log[m2]*Log[SS])/(SS^2*TT^3) + (120*Log[m2]*Log[SS])/(SS^3*TT^2) + 
   (40*Log[m2]*Log[SS])/(SS^4*TT) + (368*Log[TT])/(3*SS*TT^4) + 
   (248*Log[TT])/(SS^2*TT^3) + (188*Log[TT])/(SS^3*TT^2) + 
   (148*Log[TT])/(3*SS^4*TT) + (40*Log[m2]*Log[TT])/(SS*TT^4) + 
   (120*Log[m2]*Log[TT])/(SS^2*TT^3) + (120*Log[m2]*Log[TT])/(SS^3*TT^2) + 
   (40*Log[m2]*Log[TT])/(SS^4*TT) - (40*Log[SS]*Log[TT])/(SS*TT^4) - 
   (120*Log[SS]*Log[TT])/(SS^2*TT^3) - (120*Log[SS]*Log[TT])/(SS^3*TT^2) - 
   (40*Log[SS]*Log[TT])/(SS^4*TT)) + 
 m2^2*(-16/(SS*TT^3) - (6*Pi^2)/(SS*TT^3) - 40/(SS^2*TT^2) - 
   (12*Pi^2)/(SS^2*TT^2) - 16/(SS^3*TT) - (6*Pi^2)/(SS^3*TT) + 
   (46*Log[m2])/(SS*TT^3) + (80*Log[m2])/(SS^2*TT^2) + 
   (46*Log[m2])/(SS^3*TT) + (12*Log[m2]^2)/(SS*TT^3) + 
   (24*Log[m2]^2)/(SS^2*TT^2) + (12*Log[m2]^2)/(SS^3*TT) - 
   (14*Log[SS])/(SS*TT^3) - (40*Log[SS])/(SS^2*TT^2) - 
   (32*Log[SS])/(SS^3*TT) - (12*Log[m2]*Log[SS])/(SS*TT^3) - 
   (24*Log[m2]*Log[SS])/(SS^2*TT^2) - (12*Log[m2]*Log[SS])/(SS^3*TT) - 
   (32*Log[TT])/(SS*TT^3) - (40*Log[TT])/(SS^2*TT^2) - 
   (14*Log[TT])/(SS^3*TT) - (12*Log[m2]*Log[TT])/(SS*TT^3) - 
   (24*Log[m2]*Log[TT])/(SS^2*TT^2) - (12*Log[m2]*Log[TT])/(SS^3*TT) + 
   (12*Log[SS]*Log[TT])/(SS*TT^3) + (24*Log[SS]*Log[TT])/(SS^2*TT^2) + 
   (12*Log[SS]*Log[TT])/(SS^3*TT)) + 
 m2*(8/(SS*TT^2) + (2*Pi^2)/(SS*TT^2) + 8/(SS^2*TT) + (2*Pi^2)/(SS^2*TT) - 
   (12*Log[m2])/(SS*TT^2) - (12*Log[m2])/(SS^2*TT) - 
   (4*Log[m2]^2)/(SS*TT^2) - (4*Log[m2]^2)/(SS^2*TT) + 
   (4*Log[SS])/(SS*TT^2) + (8*Log[SS])/(SS^2*TT) + 
   (4*Log[m2]*Log[SS])/(SS*TT^2) + (4*Log[m2]*Log[SS])/(SS^2*TT) + 
   (8*Log[TT])/(SS*TT^2) + (4*Log[TT])/(SS^2*TT) + 
   (4*Log[m2]*Log[TT])/(SS*TT^2) + (4*Log[m2]*Log[TT])/(SS^2*TT) - 
   (4*Log[SS]*Log[TT])/(SS*TT^2) - (4*Log[SS]*Log[TT])/(SS^2*TT)) + 
 (Log[SS]/(SS*TT) + Log[TT]/(SS*TT) - (Log[SS] + Log[TT])/(SS*TT))/ep + 
 ep*(((-Pi^2 - 6*Log[m2]^2)/(6*SS*TT) + (Pi^2 + 6*Log[m2]^2)/(6*SS*TT))/d3 + 
   ((-Pi^2 - 6*Log[m2]^2)/(6*SS*TT) + (Pi^2 + 6*Log[m2]^2)/(6*SS*TT))/d4 + 
   (Pi^2*Log[SS] + 6*Log[m2]^2*Log[SS] + Pi^2*Log[TT] + 6*Log[m2]^2*Log[TT])/
    (12*SS*TT) + (-2*Pi^2*Log[m2] - 4*Log[m2]^3 + Pi^2*Log[SS] + 
     6*Log[m2]^2*Log[SS] - 8*Zeta[3])/(12*SS*TT) + 
   (-2*Pi^2*Log[m2] - 4*Log[m2]^3 + Pi^2*Log[TT] + 6*Log[m2]^2*Log[TT] - 
     8*Zeta[3])/(12*SS*TT) + (-(Pi^2*Log[m2]) - 2*Log[m2]^3 - 4*Zeta[3])/
    (3*SS*TT) + (SS^(-2 - Z1)*(-12*TT^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1] + Pi^2*SS^(1 + Z1)*Log[SS] + 
      7*Pi^2*SS^(1 + Z1)*Log[TT] - 6*SS^(1 + Z1)*Log[SS]*Log[TT]^2 + 
      2*SS^(1 + Z1)*Log[TT]^3 - 68*SS^(1 + Z1)*Zeta[3]))/(6*TT) + 
   m2^4*(-1040647/(1296*SS*TT^5) - (685*Pi^2)/(2*SS*TT^5) - 
     248770/(81*SS^2*TT^4) - (1370*Pi^2)/(SS^2*TT^4) - 
     174047/(36*SS^3*TT^3) - (6515*Pi^2)/(3*SS^3*TT^3) - 
     11210/(3*SS^4*TT^2) - (4810*Pi^2)/(3*SS^4*TT^2) - 63911/(48*SS^5*TT) - 
     (1465*Pi^2)/(3*SS^5*TT) - 12*SS^(-6 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1] - 16*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[6 + Z1] - 4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1] - 96*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
      Gamma[6 + Z1] - 32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-2 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1] - 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[6 + Z1] - 48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*
      Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1] - 48*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] - 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] - 96*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*
      Gamma[6 + Z1] - 12*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*
      Gamma[3 + Z1]^2*Gamma[6 + Z1] - 32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1] - 
     16*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[4 + Z1]*Gamma[6 + Z1] - 4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[5 + Z1]*Gamma[6 + Z1] + 
     (34175*Log[m2])/(36*SS*TT^5) - (140*Pi^2*Log[m2])/(3*SS*TT^5) + 
     (26300*Log[m2])/(9*SS^2*TT^4) - (560*Pi^2*Log[m2])/(3*SS^2*TT^4) + 
     (12275*Log[m2])/(3*SS^3*TT^3) - (280*Pi^2*Log[m2])/(SS^3*TT^3) + 
     (26300*Log[m2])/(9*SS^4*TT^2) - (560*Pi^2*Log[m2])/(3*SS^4*TT^2) + 
     (34175*Log[m2])/(36*SS^5*TT) - (140*Pi^2*Log[m2])/(3*SS^5*TT) - 
     (191*Log[m2]^2)/(6*SS*TT^5) + (248*Log[m2]^2)/(3*SS^2*TT^4) + 
     (194*Log[m2]^2)/(SS^3*TT^3) + (248*Log[m2]^2)/(3*SS^4*TT^2) - 
     (191*Log[m2]^2)/(6*SS^5*TT) - (280*Log[m2]^3)/(3*SS*TT^5) - 
     (1120*Log[m2]^3)/(3*SS^2*TT^4) - (560*Log[m2]^3)/(SS^3*TT^3) - 
     (1120*Log[m2]^3)/(3*SS^4*TT^2) - (280*Log[m2]^3)/(3*SS^5*TT) - 
     (1987*Log[SS])/(36*SS*TT^5) + (70*Pi^2*Log[SS])/(3*SS*TT^5) - 
     (6211*Log[SS])/(9*SS^2*TT^4) + (280*Pi^2*Log[SS])/(3*SS^2*TT^4) - 
     (12263*Log[SS])/(6*SS^3*TT^3) + (140*Pi^2*Log[SS])/(SS^3*TT^3) - 
     (22181*Log[SS])/(9*SS^4*TT^2) + (280*Pi^2*Log[SS])/(3*SS^4*TT^2) - 
     (44537*Log[SS])/(36*SS^5*TT) + (70*Pi^2*Log[SS])/(3*SS^5*TT) - 
     (875*Log[m2]*Log[SS])/(3*SS*TT^5) - (3500*Log[m2]*Log[SS])/
      (3*SS^2*TT^4) - (1750*Log[m2]*Log[SS])/(SS^3*TT^3) - 
     (3500*Log[m2]*Log[SS])/(3*SS^4*TT^2) - (875*Log[m2]*Log[SS])/
      (3*SS^5*TT) + (70*Log[m2]^2*Log[SS])/(SS*TT^5) + 
     (280*Log[m2]^2*Log[SS])/(SS^2*TT^4) + (420*Log[m2]^2*Log[SS])/
      (SS^3*TT^3) + (280*Log[m2]^2*Log[SS])/(SS^4*TT^2) + 
     (70*Log[m2]^2*Log[SS])/(SS^5*TT) - (2501*Log[TT])/(3*SS*TT^5) + 
     (280*Pi^2*Log[TT])/(3*SS*TT^5) - (1609*Log[TT])/(SS^2*TT^4) + 
     (1120*Pi^2*Log[TT])/(3*SS^2*TT^4) - (5963*Log[TT])/(6*SS^3*TT^3) + 
     (560*Pi^2*Log[TT])/(SS^3*TT^3) + (1489*Log[TT])/(9*SS^4*TT^2) + 
     (1120*Pi^2*Log[TT])/(3*SS^4*TT^2) + (6269*Log[TT])/(18*SS^5*TT) + 
     (280*Pi^2*Log[TT])/(3*SS^5*TT) - (875*Log[m2]*Log[TT])/(3*SS*TT^5) - 
     (3500*Log[m2]*Log[TT])/(3*SS^2*TT^4) - (1750*Log[m2]*Log[TT])/
      (SS^3*TT^3) - (3500*Log[m2]*Log[TT])/(3*SS^4*TT^2) - 
     (875*Log[m2]*Log[TT])/(3*SS^5*TT) + (70*Log[m2]^2*Log[TT])/(SS*TT^5) + 
     (280*Log[m2]^2*Log[TT])/(SS^2*TT^4) + (420*Log[m2]^2*Log[TT])/
      (SS^3*TT^3) + (280*Log[m2]^2*Log[TT])/(SS^4*TT^2) + 
     (70*Log[m2]^2*Log[TT])/(SS^5*TT) + (1408*Log[SS]*Log[TT])/(3*SS*TT^5) + 
     (6052*Log[SS]*Log[TT])/(3*SS^2*TT^4) + (3306*Log[SS]*Log[TT])/
      (SS^3*TT^3) + (2484*Log[SS]*Log[TT])/(SS^4*TT^2) + 
     (761*Log[SS]*Log[TT])/(SS^5*TT) + (875*Log[TT]^2)/(6*SS*TT^5) + 
     (700*Log[TT]^2)/(3*SS^2*TT^4) - (700*Log[TT]^2)/(3*SS^4*TT^2) - 
     (875*Log[TT]^2)/(6*SS^5*TT) - (70*Log[SS]*Log[TT]^2)/(SS*TT^5) - 
     (280*Log[SS]*Log[TT]^2)/(SS^2*TT^4) - (420*Log[SS]*Log[TT]^2)/
      (SS^3*TT^3) - (280*Log[SS]*Log[TT]^2)/(SS^4*TT^2) - 
     (70*Log[SS]*Log[TT]^2)/(SS^5*TT) + (70*Log[TT]^3)/(3*SS*TT^5) + 
     (280*Log[TT]^3)/(3*SS^2*TT^4) + (140*Log[TT]^3)/(SS^3*TT^3) + 
     (280*Log[TT]^3)/(3*SS^4*TT^2) + (70*Log[TT]^3)/(3*SS^5*TT) - 
     (560*(1 - Zeta[3]))/(3*SS^4*TT^2) - (280*(9/8 - Zeta[3]))/(SS^3*TT^3) - 
     (560*(251/216 - Zeta[3]))/(3*SS^2*TT^4) - (140*(2035/1728 - Zeta[3]))/
      (3*SS*TT^5) - (3080*Zeta[3])/(3*SS*TT^5) - 
     (12320*Zeta[3])/(3*SS^2*TT^4) - (6160*Zeta[3])/(SS^3*TT^3) - 
     (12320*Zeta[3])/(3*SS^4*TT^2) - (980*Zeta[3])/(SS^5*TT)) + 
   m2^3*(17237/(81*SS*TT^4) + (90*Pi^2)/(SS*TT^4) + 5357/(9*SS^2*TT^3) + 
     (830*Pi^2)/(3*SS^2*TT^3) + 6077/(9*SS^3*TT^2) + 
     (920*Pi^2)/(3*SS^3*TT^2) + 3013/(9*SS^4*TT) + (380*Pi^2)/(3*SS^4*TT) - 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1] - 4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[5 + Z1] - 24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1] - 24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1] - 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[3 + Z1]*Gamma[5 + Z1] - 4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[5 + Z1] - 
     (1858*Log[m2])/(9*SS*TT^4) + (40*Pi^2*Log[m2])/(3*SS*TT^4) - 
     (1418*Log[m2])/(3*SS^2*TT^3) + (40*Pi^2*Log[m2])/(SS^2*TT^3) - 
     (1418*Log[m2])/(3*SS^3*TT^2) + (40*Pi^2*Log[m2])/(SS^3*TT^2) - 
     (1858*Log[m2])/(9*SS^4*TT) + (40*Pi^2*Log[m2])/(3*SS^4*TT) + 
     (38*Log[m2]^2)/(3*SS*TT^4) - (2*Log[m2]^2)/(SS^2*TT^3) - 
     (2*Log[m2]^2)/(SS^3*TT^2) + (38*Log[m2]^2)/(3*SS^4*TT) + 
     (80*Log[m2]^3)/(3*SS*TT^4) + (80*Log[m2]^3)/(SS^2*TT^3) + 
     (80*Log[m2]^3)/(SS^3*TT^2) + (80*Log[m2]^3)/(3*SS^4*TT) + 
     (14*Log[SS])/(9*SS*TT^4) - (20*Pi^2*Log[SS])/(3*SS*TT^4) + 
     (382*Log[SS])/(3*SS^2*TT^3) - (20*Pi^2*Log[SS])/(SS^2*TT^3) + 
     (994*Log[SS])/(3*SS^3*TT^2) - (20*Pi^2*Log[SS])/(SS^3*TT^2) + 
     (2398*Log[SS])/(9*SS^4*TT) - (20*Pi^2*Log[SS])/(3*SS^4*TT) + 
     (220*Log[m2]*Log[SS])/(3*SS*TT^4) + (220*Log[m2]*Log[SS])/(SS^2*TT^3) + 
     (220*Log[m2]*Log[SS])/(SS^3*TT^2) + (220*Log[m2]*Log[SS])/(3*SS^4*TT) - 
     (20*Log[m2]^2*Log[SS])/(SS*TT^4) - (60*Log[m2]^2*Log[SS])/(SS^2*TT^3) - 
     (60*Log[m2]^2*Log[SS])/(SS^3*TT^2) - (20*Log[m2]^2*Log[SS])/(SS^4*TT) + 
     (172*Log[TT])/(SS*TT^4) - (80*Pi^2*Log[TT])/(3*SS*TT^4) + 
     (544*Log[TT])/(3*SS^2*TT^3) - (80*Pi^2*Log[TT])/(SS^2*TT^3) - 
     (68*Log[TT])/(3*SS^3*TT^2) - (80*Pi^2*Log[TT])/(SS^3*TT^2) - 
     (836*Log[TT])/(9*SS^4*TT) - (80*Pi^2*Log[TT])/(3*SS^4*TT) + 
     (220*Log[m2]*Log[TT])/(3*SS*TT^4) + (220*Log[m2]*Log[TT])/(SS^2*TT^3) + 
     (220*Log[m2]*Log[TT])/(SS^3*TT^2) + (220*Log[m2]*Log[TT])/(3*SS^4*TT) - 
     (20*Log[m2]^2*Log[TT])/(SS*TT^4) - (60*Log[m2]^2*Log[TT])/(SS^2*TT^3) - 
     (60*Log[m2]^2*Log[TT])/(SS^3*TT^2) - (20*Log[m2]^2*Log[TT])/(SS^4*TT) - 
     (368*Log[SS]*Log[TT])/(3*SS*TT^4) - (408*Log[SS]*Log[TT])/(SS^2*TT^3) - 
     (468*Log[SS]*Log[TT])/(SS^3*TT^2) - (196*Log[SS]*Log[TT])/(SS^4*TT) - 
     (110*Log[TT]^2)/(3*SS*TT^4) - (30*Log[TT]^2)/(SS^2*TT^3) + 
     (30*Log[TT]^2)/(SS^3*TT^2) + (110*Log[TT]^2)/(3*SS^4*TT) + 
     (20*Log[SS]*Log[TT]^2)/(SS*TT^4) + (60*Log[SS]*Log[TT]^2)/(SS^2*TT^3) + 
     (60*Log[SS]*Log[TT]^2)/(SS^3*TT^2) + (20*Log[SS]*Log[TT]^2)/(SS^4*TT) - 
     (20*Log[TT]^3)/(3*SS*TT^4) - (20*Log[TT]^3)/(SS^2*TT^3) - 
     (20*Log[TT]^3)/(SS^3*TT^2) - (20*Log[TT]^3)/(3*SS^4*TT) + 
     (40*(1 - Zeta[3]))/(SS^3*TT^2) + (40*(9/8 - Zeta[3]))/(SS^2*TT^3) + 
     (40*(251/216 - Zeta[3]))/(3*SS*TT^4) + (880*Zeta[3])/(3*SS*TT^4) + 
     (880*Zeta[3])/(SS^2*TT^3) + (880*Zeta[3])/(SS^3*TT^2) + 
     (280*Zeta[3])/(SS^4*TT)) + m2^2*(-103/(2*SS*TT^3) - 
     (71*Pi^2)/(3*SS*TT^3) - 98/(SS^2*TT^2) - (154*Pi^2)/(3*SS^2*TT^2) - 
     157/(2*SS^3*TT) - (98*Pi^2)/(3*SS^3*TT) - 4*SS^(-4 - Z1)*TT^Z1*
      Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1] - 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[4 + Z1] - 16*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1] - 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[4 + Z1] - 4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[4 + Z1] + (39*Log[m2])/(SS*TT^3) - 
     (4*Pi^2*Log[m2])/(SS*TT^3) + (60*Log[m2])/(SS^2*TT^2) - 
     (8*Pi^2*Log[m2])/(SS^2*TT^2) + (39*Log[m2])/(SS^3*TT) - 
     (4*Pi^2*Log[m2])/(SS^3*TT) - (5*Log[m2]^2)/(SS*TT^3) - 
     (4*Log[m2]^2)/(SS^2*TT^2) - (5*Log[m2]^2)/(SS^3*TT) - 
     (8*Log[m2]^3)/(SS*TT^3) - (16*Log[m2]^3)/(SS^2*TT^2) - 
     (8*Log[m2]^3)/(SS^3*TT) + (5*Log[SS])/(SS*TT^3) + 
     (2*Pi^2*Log[SS])/(SS*TT^3) - (22*Log[SS])/(SS^2*TT^2) + 
     (4*Pi^2*Log[SS])/(SS^2*TT^2) - (49*Log[SS])/(SS^3*TT) + 
     (2*Pi^2*Log[SS])/(SS^3*TT) - (18*Log[m2]*Log[SS])/(SS*TT^3) - 
     (36*Log[m2]*Log[SS])/(SS^2*TT^2) - (18*Log[m2]*Log[SS])/(SS^3*TT) + 
     (6*Log[m2]^2*Log[SS])/(SS*TT^3) + (12*Log[m2]^2*Log[SS])/(SS^2*TT^2) + 
     (6*Log[m2]^2*Log[SS])/(SS^3*TT) - (28*Log[TT])/(SS*TT^3) + 
     (8*Pi^2*Log[TT])/(SS*TT^3) + (2*Log[TT])/(SS^2*TT^2) + 
     (16*Pi^2*Log[TT])/(SS^2*TT^2) + (26*Log[TT])/(SS^3*TT) + 
     (8*Pi^2*Log[TT])/(SS^3*TT) - (18*Log[m2]*Log[TT])/(SS*TT^3) - 
     (36*Log[m2]*Log[TT])/(SS^2*TT^2) - (18*Log[m2]*Log[TT])/(SS^3*TT) + 
     (6*Log[m2]^2*Log[TT])/(SS*TT^3) + (12*Log[m2]^2*Log[TT])/(SS^2*TT^2) + 
     (6*Log[m2]^2*Log[TT])/(SS^3*TT) + (32*Log[SS]*Log[TT])/(SS*TT^3) + 
     (76*Log[SS]*Log[TT])/(SS^2*TT^2) + (50*Log[SS]*Log[TT])/(SS^3*TT) + 
     (9*Log[TT]^2)/(SS*TT^3) - (9*Log[TT]^2)/(SS^3*TT) - 
     (6*Log[SS]*Log[TT]^2)/(SS*TT^3) - (12*Log[SS]*Log[TT]^2)/(SS^2*TT^2) - 
     (6*Log[SS]*Log[TT]^2)/(SS^3*TT) + (2*Log[TT]^3)/(SS*TT^3) + 
     (4*Log[TT]^3)/(SS^2*TT^2) + (2*Log[TT]^3)/(SS^3*TT) - 
     (8*(1 - Zeta[3]))/(SS^2*TT^2) - (4*(9/8 - Zeta[3]))/(SS*TT^3) - 
     (88*Zeta[3])/(SS*TT^3) - (176*Zeta[3])/(SS^2*TT^2) - 
     (84*Zeta[3])/(SS^3*TT)) + m2*(20/(3*SS*TT^2) + (6*Pi^2)/(SS*TT^2) + 
     12/(SS^2*TT) + (8*Pi^2)/(SS^2*TT) - 4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1] - 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1] - (4*Log[m2])/(SS*TT^2) + 
     (4*Pi^2*Log[m2])/(3*SS*TT^2) - (4*Log[m2])/(SS^2*TT) + 
     (4*Pi^2*Log[m2])/(3*SS^2*TT) + (2*Log[m2]^2)/(SS*TT^2) + 
     (2*Log[m2]^2)/(SS^2*TT) + (8*Log[m2]^3)/(3*SS*TT^2) + 
     (8*Log[m2]^3)/(3*SS^2*TT) - (4*Log[SS])/(SS*TT^2) - 
     (2*Pi^2*Log[SS])/(3*SS*TT^2) + (4*Log[SS])/(SS^2*TT) - 
     (2*Pi^2*Log[SS])/(3*SS^2*TT) + (4*Log[m2]*Log[SS])/(SS*TT^2) + 
     (4*Log[m2]*Log[SS])/(SS^2*TT) - (2*Log[m2]^2*Log[SS])/(SS*TT^2) - 
     (2*Log[m2]^2*Log[SS])/(SS^2*TT) - (8*Pi^2*Log[TT])/(3*SS*TT^2) - 
     (8*Log[TT])/(SS^2*TT) - (8*Pi^2*Log[TT])/(3*SS^2*TT) + 
     (4*Log[m2]*Log[TT])/(SS*TT^2) + (4*Log[m2]*Log[TT])/(SS^2*TT) - 
     (2*Log[m2]^2*Log[TT])/(SS*TT^2) - (2*Log[m2]^2*Log[TT])/(SS^2*TT) - 
     (8*Log[SS]*Log[TT])/(SS*TT^2) - (12*Log[SS]*Log[TT])/(SS^2*TT) - 
     (2*Log[TT]^2)/(SS*TT^2) + (2*Log[TT]^2)/(SS^2*TT) + 
     (2*Log[SS]*Log[TT]^2)/(SS*TT^2) + (2*Log[SS]*Log[TT]^2)/(SS^2*TT) - 
     (2*Log[TT]^3)/(3*SS*TT^2) - (2*Log[TT]^3)/(3*SS^2*TT) + 
     (4*(1 - Zeta[3]))/(3*SS*TT^2) + (88*Zeta[3])/(3*SS*TT^2) + 
     (28*Zeta[3])/(SS^2*TT))) + 
 ep^2*((3*Pi^4 + 20*Pi^2*Log[m2]^2 + 20*Log[m2]^4 + 160*Log[m2]*Zeta[3])/
    (80*SS*TT) + (9*Pi^4 + 60*Pi^2*Log[m2]^2 + 60*Log[m2]^4 - 
     40*Pi^2*Log[m2]*Log[SS] - 80*Log[m2]^3*Log[SS] + 480*Log[m2]*Zeta[3] - 
     160*Log[SS]*Zeta[3])/(480*SS*TT) + 
   (9*Pi^4 + 60*Pi^2*Log[m2]^2 + 60*Log[m2]^4 - 40*Pi^2*Log[m2]*Log[TT] - 
     80*Log[m2]^3*Log[TT] + 480*Log[m2]*Zeta[3] - 160*Log[TT]*Zeta[3])/
    (480*SS*TT) + (-(Pi^2*Log[m2]*Log[SS]) - 2*Log[m2]^3*Log[SS] - 
     Pi^2*Log[m2]*Log[TT] - 2*Log[m2]^3*Log[TT] - 4*Log[SS]*Zeta[3] - 
     4*Log[TT]*Zeta[3])/(12*SS*TT) - 
   (SS^(-2 - Z1)*(41*Pi^4*SS^(1 + Z1) - 720*EulerGamma*TT^(1 + Z1)*
       Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[2 + Z1] - 
      720*TT^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*Log[SS] + 60*Pi^2*SS^(1 + Z1)*Log[SS]*Log[TT] + 
      180*Pi^2*SS^(1 + Z1)*Log[TT]^2 - 120*SS^(1 + Z1)*Log[SS]*Log[TT]^3 + 
      60*SS^(1 + Z1)*Log[TT]^4 - 1440*TT^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[2 + Z1]*PolyGamma[0, -1 - Z1] + 
      720*TT^(1 + Z1)*Gamma[-1 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
       Gamma[2 + Z1]*PolyGamma[0, 2 + Z1] - 1680*SS^(1 + Z1)*Log[SS]*
       Zeta[3] - 2400*SS^(1 + Z1)*Log[TT]*Zeta[3]))/(360*TT) + 
   m2^4*(-13401733/(15552*SS*TT^5) - (27541*Pi^2)/(72*SS*TT^5) - 
     (49*Pi^4)/(18*SS*TT^5) - 4321523/(1944*SS^2*TT^4) - 
     (14284*Pi^2)/(9*SS^2*TT^4) - (98*Pi^4)/(9*SS^2*TT^4) - 
     83273/(24*SS^3*TT^3) - (25438*Pi^2)/(9*SS^3*TT^3) - 
     (49*Pi^4)/(3*SS^3*TT^3) - 2398625/(648*SS^4*TT^2) - 
     (7423*Pi^2)/(3*SS^4*TT^2) - (98*Pi^4)/(9*SS^4*TT^2) - 
     12689081/(5184*SS^5*TT) - (70091*Pi^2)/(72*SS^5*TT) - 
     (49*Pi^4)/(18*SS^5*TT) - 50*SS^(-6 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1] + 12*EulerGamma*SS^(-6 - Z1)*
      TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1] - 
     (200*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]^2*Gamma[6 + Z1])/3 + 16*EulerGamma*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[6 + Z1] - (50*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1])/3 + 
     4*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[6 + Z1] - 400*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1] + 
     96*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1] - 
     (400*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1])/3 + 
     32*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-2 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1] - 
     200*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[6 + Z1] + 48*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*
      Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1] - 200*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[6 + Z1] + 48*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1] - 
     200*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[3 + Z1]*Gamma[6 + Z1] + 48*EulerGamma*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] - 
     200*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] + 48*EulerGamma*SS^(-6 - Z1)*
      TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[3 + Z1]*Gamma[6 + Z1] - 400*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] + 
     96*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*
      Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1] - 
     50*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[3 + Z1]^2*
      Gamma[6 + Z1] + 12*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*
      Gamma[-Z1]*Gamma[3 + Z1]^2*Gamma[6 + Z1] - 
     (400*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
       Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1])/3 + 
     32*EulerGamma*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1] - 
     (200*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
       Gamma[4 + Z1]*Gamma[6 + Z1])/3 + 16*EulerGamma*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1] - 
     (50*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[5 + Z1]*Gamma[6 + Z1])/3 + 4*EulerGamma*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[5 + Z1]*Gamma[6 + Z1] + 
     (304315*Log[m2])/(432*SS*TT^5) - (1559*Pi^2*Log[m2])/(36*SS*TT^5) + 
     (86795*Log[m2])/(54*SS^2*TT^4) - (1874*Pi^2*Log[m2])/(9*SS^2*TT^4) + 
     (4015*Log[m2])/(2*SS^3*TT^3) - (324*Pi^2*Log[m2])/(SS^3*TT^3) + 
     (86795*Log[m2])/(54*SS^4*TT^2) - (1874*Pi^2*Log[m2])/(9*SS^4*TT^2) + 
     (304315*Log[m2])/(432*SS^5*TT) - (1559*Pi^2*Log[m2])/(36*SS^5*TT) - 
     (5125*Log[m2]^2)/(72*SS*TT^5) + (35*Pi^2*Log[m2]^2)/(SS*TT^5) + 
     (1375*Log[m2]^2)/(9*SS^2*TT^4) + (140*Pi^2*Log[m2]^2)/(SS^2*TT^4) + 
     (375*Log[m2]^2)/(SS^3*TT^3) + (210*Pi^2*Log[m2]^2)/(SS^3*TT^3) + 
     (1375*Log[m2]^2)/(9*SS^4*TT^2) + (140*Pi^2*Log[m2]^2)/(SS^4*TT^2) - 
     (5125*Log[m2]^2)/(72*SS^5*TT) + (35*Pi^2*Log[m2]^2)/(SS^5*TT) - 
     (1559*Log[m2]^3)/(18*SS*TT^5) - (3748*Log[m2]^3)/(9*SS^2*TT^4) - 
     (648*Log[m2]^3)/(SS^3*TT^3) - (3748*Log[m2]^3)/(9*SS^4*TT^2) - 
     (1559*Log[m2]^3)/(18*SS^5*TT) + (35*Log[m2]^4)/(SS*TT^5) + 
     (140*Log[m2]^4)/(SS^2*TT^4) + (210*Log[m2]^4)/(SS^3*TT^3) + 
     (140*Log[m2]^4)/(SS^4*TT^2) + (35*Log[m2]^4)/(SS^5*TT) + 
     (231883*Log[SS])/(648*SS*TT^5) + (761*Pi^2*Log[SS])/(12*SS*TT^5) + 
     (314213*Log[SS])/(324*SS^2*TT^4) + (796*Pi^2*Log[SS])/(3*SS^2*TT^4) + 
     (21047*Log[SS])/(72*SS^3*TT^3) + (1264*Pi^2*Log[SS])/(3*SS^3*TT^3) - 
     (142201*Log[SS])/(108*SS^4*TT^2) + (2738*Pi^2*Log[SS])/(9*SS^4*TT^2) - 
     (602833*Log[SS])/(432*SS^5*TT) + (1579*Pi^2*Log[SS])/(18*SS^5*TT) + 
     12*SS^(-6 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[6 + Z1]*Log[SS] + 16*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1]*Log[SS] + 
     4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[6 + Z1]*Log[SS] + 96*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
      Gamma[6 + Z1]*Log[SS] + 32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*
      Log[SS] + 48*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[6 + Z1]*Log[SS] + 48*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1]*
      Log[SS] + 48*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*Log[SS] + 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*Log[SS] + 
     96*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*Log[SS] + 
     12*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[3 + Z1]^2*
      Gamma[6 + Z1]*Log[SS] + 32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1]*
      Log[SS] + 16*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*
      Gamma[2 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1]*Log[SS] + 
     4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[5 + Z1]*Gamma[6 + Z1]*Log[SS] - (14525*Log[m2]*Log[SS])/
      (36*SS*TT^5) - (35*Pi^2*Log[m2]*Log[SS])/(3*SS*TT^5) - 
     (14525*Log[m2]*Log[SS])/(9*SS^2*TT^4) - (140*Pi^2*Log[m2]*Log[SS])/
      (3*SS^2*TT^4) - (14525*Log[m2]*Log[SS])/(6*SS^3*TT^3) - 
     (70*Pi^2*Log[m2]*Log[SS])/(SS^3*TT^3) - (14525*Log[m2]*Log[SS])/
      (9*SS^4*TT^2) - (140*Pi^2*Log[m2]*Log[SS])/(3*SS^4*TT^2) - 
     (14525*Log[m2]*Log[SS])/(36*SS^5*TT) - (35*Pi^2*Log[m2]*Log[SS])/
      (3*SS^5*TT) + (875*Log[m2]^2*Log[SS])/(6*SS*TT^5) + 
     (1750*Log[m2]^2*Log[SS])/(3*SS^2*TT^4) + (875*Log[m2]^2*Log[SS])/
      (SS^3*TT^3) + (1750*Log[m2]^2*Log[SS])/(3*SS^4*TT^2) + 
     (875*Log[m2]^2*Log[SS])/(6*SS^5*TT) - (70*Log[m2]^3*Log[SS])/
      (3*SS*TT^5) - (280*Log[m2]^3*Log[SS])/(3*SS^2*TT^4) - 
     (140*Log[m2]^3*Log[SS])/(SS^3*TT^3) - (280*Log[m2]^3*Log[SS])/
      (3*SS^4*TT^2) - (70*Log[m2]^3*Log[SS])/(3*SS^5*TT) - 
     (21004*Log[TT])/(81*SS*TT^5) + (5803*Pi^2*Log[TT])/(18*SS*TT^5) + 
     (160097*Log[TT])/(324*SS^2*TT^4) + (11816*Pi^2*Log[TT])/(9*SS^2*TT^4) + 
     (182507*Log[TT])/(72*SS^3*TT^3) + (6223*Pi^2*Log[TT])/(3*SS^3*TT^3) + 
     (124057*Log[TT])/(36*SS^4*TT^2) + (4522*Pi^2*Log[TT])/(3*SS^4*TT^2) + 
     (291239*Log[TT])/(144*SS^5*TT) + (5327*Pi^2*Log[TT])/(12*SS^5*TT) - 
     (14525*Log[m2]*Log[TT])/(36*SS*TT^5) - (35*Pi^2*Log[m2]*Log[TT])/
      (3*SS*TT^5) - (14525*Log[m2]*Log[TT])/(9*SS^2*TT^4) - 
     (140*Pi^2*Log[m2]*Log[TT])/(3*SS^2*TT^4) - (14525*Log[m2]*Log[TT])/
      (6*SS^3*TT^3) - (70*Pi^2*Log[m2]*Log[TT])/(SS^3*TT^3) - 
     (14525*Log[m2]*Log[TT])/(9*SS^4*TT^2) - (140*Pi^2*Log[m2]*Log[TT])/
      (3*SS^4*TT^2) - (14525*Log[m2]*Log[TT])/(36*SS^5*TT) - 
     (35*Pi^2*Log[m2]*Log[TT])/(3*SS^5*TT) + (875*Log[m2]^2*Log[TT])/
      (6*SS*TT^5) + (1750*Log[m2]^2*Log[TT])/(3*SS^2*TT^4) + 
     (875*Log[m2]^2*Log[TT])/(SS^3*TT^3) + (1750*Log[m2]^2*Log[TT])/
      (3*SS^4*TT^2) + (875*Log[m2]^2*Log[TT])/(6*SS^5*TT) - 
     (70*Log[m2]^3*Log[TT])/(3*SS*TT^5) - (280*Log[m2]^3*Log[TT])/
      (3*SS^2*TT^4) - (140*Log[m2]^3*Log[TT])/(SS^3*TT^3) - 
     (280*Log[m2]^3*Log[TT])/(3*SS^4*TT^2) - (70*Log[m2]^3*Log[TT])/
      (3*SS^5*TT) + (1376*Log[SS]*Log[TT])/(3*SS*TT^5) - 
     (35*Pi^2*Log[SS]*Log[TT])/(3*SS*TT^5) + (2304*Log[SS]*Log[TT])/
      (SS^2*TT^4) - (140*Pi^2*Log[SS]*Log[TT])/(3*SS^2*TT^4) + 
     (13394*Log[SS]*Log[TT])/(3*SS^3*TT^3) - (70*Pi^2*Log[SS]*Log[TT])/
      (SS^3*TT^3) + (36706*Log[SS]*Log[TT])/(9*SS^4*TT^2) - 
     (140*Pi^2*Log[SS]*Log[TT])/(3*SS^4*TT^2) + (29531*Log[SS]*Log[TT])/
      (18*SS^5*TT) - (35*Pi^2*Log[SS]*Log[TT])/(3*SS^5*TT) + 
     (28025*Log[TT]^2)/(72*SS*TT^5) - (35*Pi^2*Log[TT]^2)/(SS*TT^5) + 
     (4135*Log[TT]^2)/(9*SS^2*TT^4) - (140*Pi^2*Log[TT]^2)/(SS^2*TT^4) - 
     (525*Log[TT]^2)/(SS^3*TT^3) - (210*Pi^2*Log[TT]^2)/(SS^3*TT^3) - 
     (1315*Log[TT]^2)/(SS^4*TT^2) - (140*Pi^2*Log[TT]^2)/(SS^4*TT^2) - 
     (19025*Log[TT]^2)/(24*SS^5*TT) - (35*Pi^2*Log[TT]^2)/(SS^5*TT) - 
     (704*Log[SS]*Log[TT]^2)/(3*SS*TT^5) - (3026*Log[SS]*Log[TT]^2)/
      (3*SS^2*TT^4) - (1653*Log[SS]*Log[TT]^2)/(SS^3*TT^3) - 
     (1242*Log[SS]*Log[TT]^2)/(SS^4*TT^2) - (761*Log[SS]*Log[TT]^2)/
      (2*SS^5*TT) + (533*Log[TT]^3)/(18*SS*TT^5) + 
     (2326*Log[TT]^3)/(9*SS^2*TT^4) + (551*Log[TT]^3)/(SS^3*TT^3) + 
     (4426*Log[TT]^3)/(9*SS^4*TT^2) + (1579*Log[TT]^3)/(9*SS^5*TT) + 
     (70*Log[SS]*Log[TT]^3)/(3*SS*TT^5) + (280*Log[SS]*Log[TT]^3)/
      (3*SS^2*TT^4) + (140*Log[SS]*Log[TT]^3)/(SS^3*TT^3) + 
     (280*Log[SS]*Log[TT]^3)/(3*SS^4*TT^2) + (70*Log[SS]*Log[TT]^3)/
      (3*SS^5*TT) - (35*Log[TT]^4)/(3*SS*TT^5) - 
     (140*Log[TT]^4)/(3*SS^2*TT^4) - (70*Log[TT]^4)/(SS^3*TT^3) - 
     (140*Log[TT]^4)/(3*SS^4*TT^2) - (35*Log[TT]^4)/(3*SS^5*TT) + 
     4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 
     32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 
     96*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 
     24*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[3 + Z1]^2*
      Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 32*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*
      Gamma[6 + Z1]*PolyGamma[0, -5 - Z1] + 32*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, -5 - Z1] + 8*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[5 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, -5 - Z1] + 16*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1]*
      PolyGamma[0, -4 - Z1] + 96*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, -4 - Z1] + 96*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*
      Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, -4 - Z1] + 
     96*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[3 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -4 - Z1] + 
     96*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -4 - Z1] + 
     32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1]*PolyGamma[0, -4 - Z1] + 
     24*SS^(-6 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[6 + Z1]*PolyGamma[0, -3 - Z1] + 96*SS^(-6 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
      Gamma[6 + Z1]*PolyGamma[0, -3 - Z1] + 48*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1]*
      PolyGamma[0, -3 - Z1] + 48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, -3 - Z1] + 16*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1]*
      PolyGamma[0, -2 - Z1] + 32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, -2 - Z1] + 4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1]*
      PolyGamma[0, -1 - Z1] - 12*SS^(-6 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     16*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     96*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     48*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 48*SS^(-6 - Z1)*TT^Z1*
      Gamma[-5 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[6 + Z1]*
      PolyGamma[0, 6 + Z1] - 48*SS^(-6 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, 6 + Z1] - 48*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, 6 + Z1] - 96*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*
      Gamma[-4 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[6 + Z1]*
      PolyGamma[0, 6 + Z1] - 12*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*
      Gamma[-Z1]*Gamma[3 + Z1]^2*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     32*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]*Gamma[-4 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     16*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[4 + Z1]*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     4*SS^(-6 - Z1)*TT^Z1*Gamma[-5 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[5 + Z1]*Gamma[6 + Z1]*PolyGamma[0, 6 + Z1] - 
     (4*(1 - Zeta[3]))/(3*SS^3*TT^3) - (996*(1 - Zeta[3]))/(SS^4*TT^2) - 
     (112*(1 - Zeta[3]))/(3*SS^5*TT) - (560*Log[SS]*(1 - Zeta[3]))/
      (3*SS^4*TT^2) + (1120*Log[TT]*(1 - Zeta[3]))/(3*SS^4*TT^2) - 
     (1102*(9/8 - Zeta[3]))/(SS^3*TT^3) + (8*(9/8 - Zeta[3]))/(9*SS^4*TT^2) + 
     (64*(9/8 - Zeta[3]))/(9*SS^5*TT) - (280*Log[SS]*(9/8 - Zeta[3]))/
      (SS^3*TT^3) + (560*Log[TT]*(9/8 - Zeta[3]))/(SS^3*TT^3) - 
     (4652*(251/216 - Zeta[3]))/(9*SS^2*TT^4) - (2*(251/216 - Zeta[3]))/
      (3*SS^5*TT) - (560*Log[SS]*(251/216 - Zeta[3]))/(3*SS^2*TT^4) + 
     (1120*Log[TT]*(251/216 - Zeta[3]))/(3*SS^2*TT^4) - 
     (533*(2035/1728 - Zeta[3]))/(9*SS*TT^5) - 
     (140*Log[SS]*(2035/1728 - Zeta[3]))/(3*SS*TT^5) + 
     (280*Log[TT]*(2035/1728 - Zeta[3]))/(3*SS*TT^5) - 
     (32153*Zeta[3])/(9*SS*TT^5) - (124832*Zeta[3])/(9*SS^2*TT^4) - 
     (63400*Zeta[3])/(3*SS^3*TT^3) - (133336*Zeta[3])/(9*SS^4*TT^2) - 
     (34523*Zeta[3])/(9*SS^5*TT) + (280*Log[m2]*Zeta[3])/(SS*TT^5) + 
     (1120*Log[m2]*Zeta[3])/(SS^2*TT^4) + (1680*Log[m2]*Zeta[3])/
      (SS^3*TT^3) + (1120*Log[m2]*Zeta[3])/(SS^4*TT^2) + 
     (280*Log[m2]*Zeta[3])/(SS^5*TT) + (700*Log[SS]*Zeta[3])/(3*SS*TT^5) + 
     (2800*Log[SS]*Zeta[3])/(3*SS^2*TT^4) + (1400*Log[SS]*Zeta[3])/
      (SS^3*TT^3) + (2800*Log[SS]*Zeta[3])/(3*SS^4*TT^2) + 
     (280*Log[SS]*Zeta[3])/(SS^5*TT) + (1540*Log[TT]*Zeta[3])/(3*SS*TT^5) + 
     (6160*Log[TT]*Zeta[3])/(3*SS^2*TT^4) + (3080*Log[TT]*Zeta[3])/
      (SS^3*TT^3) + (6160*Log[TT]*Zeta[3])/(3*SS^4*TT^2) + 
     (420*Log[TT]*Zeta[3])/(SS^5*TT)) + 
   m2^3*(69613/(486*SS*TT^4) + (239*Pi^2)/(3*SS*TT^4) + 
     (7*Pi^4)/(9*SS*TT^4) + 4081/(18*SS^2*TT^3) + (2434*Pi^2)/(9*SS^2*TT^3) + 
     (7*Pi^4)/(3*SS^2*TT^3) + 6613/(18*SS^3*TT^2) + 
     (3352*Pi^2)/(9*SS^3*TT^2) + (7*Pi^4)/(3*SS^3*TT^2) + 
     73081/(162*SS^4*TT) + (1909*Pi^2)/(9*SS^4*TT) + (7*Pi^4)/(9*SS^4*TT) - 
     44*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1] + 12*EulerGamma*SS^(-5 - Z1)*TT^Z1*
      Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[5 + Z1] - (44*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-1 - Z1]*
       Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[5 + Z1])/3 + 
     4*EulerGamma*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1] - 88*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1] + 
     24*EulerGamma*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1] - 88*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*
      Gamma[5 + Z1] + 24*EulerGamma*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1] - 
     88*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1] + 24*EulerGamma*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[5 + Z1] - 88*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1] + 
     24*EulerGamma*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1] - 
     44*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[3 + Z1]*Gamma[5 + Z1] + 12*EulerGamma*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1] - 
     (44*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
       Gamma[4 + Z1]*Gamma[5 + Z1])/3 + 4*EulerGamma*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[4 + Z1]*Gamma[5 + Z1] - 
     (3065*Log[m2])/(27*SS*TT^4) + (91*Pi^2*Log[m2])/(9*SS*TT^4) - 
     (455*Log[m2])/(3*SS^2*TT^3) + (37*Pi^2*Log[m2])/(SS^2*TT^3) - 
     (455*Log[m2])/(3*SS^3*TT^2) + (37*Pi^2*Log[m2])/(SS^3*TT^2) - 
     (3065*Log[m2])/(27*SS^4*TT) + (91*Pi^2*Log[m2])/(9*SS^4*TT) + 
     (79*Log[m2]^2)/(9*SS*TT^4) - (10*Pi^2*Log[m2]^2)/(SS*TT^4) - 
     (47*Log[m2]^2)/(SS^2*TT^3) - (30*Pi^2*Log[m2]^2)/(SS^2*TT^3) - 
     (47*Log[m2]^2)/(SS^3*TT^2) - (30*Pi^2*Log[m2]^2)/(SS^3*TT^2) + 
     (79*Log[m2]^2)/(9*SS^4*TT) - (10*Pi^2*Log[m2]^2)/(SS^4*TT) + 
     (182*Log[m2]^3)/(9*SS*TT^4) + (74*Log[m2]^3)/(SS^2*TT^3) + 
     (74*Log[m2]^3)/(SS^3*TT^2) + (182*Log[m2]^3)/(9*SS^4*TT) - 
     (10*Log[m2]^4)/(SS*TT^4) - (30*Log[m2]^4)/(SS^2*TT^3) - 
     (30*Log[m2]^4)/(SS^3*TT^2) - (10*Log[m2]^4)/(SS^4*TT) - 
     (8152*Log[SS])/(81*SS*TT^4) - (49*Pi^2*Log[SS])/(3*SS*TT^4) - 
     (1840*Log[SS])/(9*SS^2*TT^3) - (157*Pi^2*Log[SS])/(3*SS^2*TT^3) + 
     (53*Log[SS])/(9*SS^3*TT^2) - (172*Pi^2*Log[SS])/(3*SS^3*TT^2) + 
     (5945*Log[SS])/(27*SS^4*TT) - (202*Pi^2*Log[SS])/(9*SS^4*TT) + 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*Log[SS] + 4*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[5 + Z1]*
      Log[SS] + 24*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1]*Log[SS] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1]*Log[SS] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1]*Log[SS] + 24*SS^(-5 - Z1)*TT^Z1*
      Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*
      Gamma[5 + Z1]*Log[SS] + 12*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*
      Gamma[-Z1]*Gamma[2 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1]*Log[SS] + 
     4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[4 + Z1]*Gamma[5 + Z1]*Log[SS] + (850*Log[m2]*Log[SS])/
      (9*SS*TT^4) + (10*Pi^2*Log[m2]*Log[SS])/(3*SS*TT^4) + 
     (850*Log[m2]*Log[SS])/(3*SS^2*TT^3) + (10*Pi^2*Log[m2]*Log[SS])/
      (SS^2*TT^3) + (850*Log[m2]*Log[SS])/(3*SS^3*TT^2) + 
     (10*Pi^2*Log[m2]*Log[SS])/(SS^3*TT^2) + (850*Log[m2]*Log[SS])/
      (9*SS^4*TT) + (10*Pi^2*Log[m2]*Log[SS])/(3*SS^4*TT) - 
     (110*Log[m2]^2*Log[SS])/(3*SS*TT^4) - (110*Log[m2]^2*Log[SS])/
      (SS^2*TT^3) - (110*Log[m2]^2*Log[SS])/(SS^3*TT^2) - 
     (110*Log[m2]^2*Log[SS])/(3*SS^4*TT) + (20*Log[m2]^3*Log[SS])/
      (3*SS*TT^4) + (20*Log[m2]^3*Log[SS])/(SS^2*TT^3) + 
     (20*Log[m2]^3*Log[SS])/(SS^3*TT^2) + (20*Log[m2]^3*Log[SS])/
      (3*SS^4*TT) + (110*Log[TT])/(81*SS*TT^4) - (754*Pi^2*Log[TT])/
      (9*SS*TT^4) - (2152*Log[TT])/(9*SS^2*TT^3) - 
     (784*Pi^2*Log[TT])/(3*SS^2*TT^3) - (4765*Log[TT])/(9*SS^3*TT^2) - 
     (859*Pi^2*Log[TT])/(3*SS^3*TT^2) - (3973*Log[TT])/(9*SS^4*TT) - 
     (343*Pi^2*Log[TT])/(3*SS^4*TT) + (850*Log[m2]*Log[TT])/(9*SS*TT^4) + 
     (10*Pi^2*Log[m2]*Log[TT])/(3*SS*TT^4) + (850*Log[m2]*Log[TT])/
      (3*SS^2*TT^3) + (10*Pi^2*Log[m2]*Log[TT])/(SS^2*TT^3) + 
     (850*Log[m2]*Log[TT])/(3*SS^3*TT^2) + (10*Pi^2*Log[m2]*Log[TT])/
      (SS^3*TT^2) + (850*Log[m2]*Log[TT])/(9*SS^4*TT) + 
     (10*Pi^2*Log[m2]*Log[TT])/(3*SS^4*TT) - (110*Log[m2]^2*Log[TT])/
      (3*SS*TT^4) - (110*Log[m2]^2*Log[TT])/(SS^2*TT^3) - 
     (110*Log[m2]^2*Log[TT])/(SS^3*TT^2) - (110*Log[m2]^2*Log[TT])/
      (3*SS^4*TT) + (20*Log[m2]^3*Log[TT])/(3*SS*TT^4) + 
     (20*Log[m2]^3*Log[TT])/(SS^2*TT^3) + (20*Log[m2]^3*Log[TT])/
      (SS^3*TT^2) + (20*Log[m2]^3*Log[TT])/(3*SS^4*TT) - 
     (96*Log[SS]*Log[TT])/(SS*TT^4) + (10*Pi^2*Log[SS]*Log[TT])/(3*SS*TT^4) - 
     (1232*Log[SS]*Log[TT])/(3*SS^2*TT^3) + (10*Pi^2*Log[SS]*Log[TT])/
      (SS^2*TT^3) - (1844*Log[SS]*Log[TT])/(3*SS^3*TT^2) + 
     (10*Pi^2*Log[SS]*Log[TT])/(SS^3*TT^2) - (3248*Log[SS]*Log[TT])/
      (9*SS^4*TT) + (10*Pi^2*Log[SS]*Log[TT])/(3*SS^4*TT) - 
     (767*Log[TT]^2)/(9*SS*TT^4) + (10*Pi^2*Log[TT]^2)/(SS*TT^4) - 
     (27*Log[TT]^2)/(SS^2*TT^3) + (30*Pi^2*Log[TT]^2)/(SS^2*TT^3) + 
     (177*Log[TT]^2)/(SS^3*TT^2) + (30*Pi^2*Log[TT]^2)/(SS^3*TT^2) + 
     (539*Log[TT]^2)/(3*SS^4*TT) + (10*Pi^2*Log[TT]^2)/(SS^4*TT) + 
     (184*Log[SS]*Log[TT]^2)/(3*SS*TT^4) + (204*Log[SS]*Log[TT]^2)/
      (SS^2*TT^3) + (234*Log[SS]*Log[TT]^2)/(SS^3*TT^2) + 
     (98*Log[SS]*Log[TT]^2)/(SS^4*TT) - (74*Log[TT]^3)/(9*SS*TT^4) - 
     (58*Log[TT]^3)/(SS^2*TT^3) - (88*Log[TT]^3)/(SS^3*TT^2) - 
     (404*Log[TT]^3)/(9*SS^4*TT) - (20*Log[SS]*Log[TT]^3)/(3*SS*TT^4) - 
     (20*Log[SS]*Log[TT]^3)/(SS^2*TT^3) - (20*Log[SS]*Log[TT]^3)/
      (SS^3*TT^2) - (20*Log[SS]*Log[TT]^3)/(3*SS^4*TT) + 
     (10*Log[TT]^4)/(3*SS*TT^4) + (10*Log[TT]^4)/(SS^2*TT^3) + 
     (10*Log[TT]^4)/(SS^3*TT^2) + (10*Log[TT]^4)/(3*SS^4*TT) + 
     4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[3 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     8*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[4 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -4 - Z1] + 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -3 - Z1] + 
     48*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -3 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -3 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -3 - Z1] + 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -2 - Z1] + 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1]*PolyGamma[0, -2 - Z1] + 
     4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, -1 - Z1] - 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     24*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]*Gamma[-3 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     12*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]*
      Gamma[3 + Z1]*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] - 
     4*SS^(-5 - Z1)*TT^Z1*Gamma[-4 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[4 + Z1]*Gamma[5 + Z1]*PolyGamma[0, 5 + Z1] + 
     (532*(1 - Zeta[3]))/(3*SS^3*TT^2) + (8*(1 - Zeta[3]))/(SS^4*TT) + 
     (40*Log[SS]*(1 - Zeta[3]))/(SS^3*TT^2) - (80*Log[TT]*(1 - Zeta[3]))/
      (SS^3*TT^2) + (116*(9/8 - Zeta[3]))/(SS^2*TT^3) - 
     (8*(9/8 - Zeta[3]))/(9*SS^4*TT) + (40*Log[SS]*(9/8 - Zeta[3]))/
      (SS^2*TT^3) - (80*Log[TT]*(9/8 - Zeta[3]))/(SS^2*TT^3) + 
     (148*(251/216 - Zeta[3]))/(9*SS*TT^4) + (40*Log[SS]*(251/216 - Zeta[3]))/
      (3*SS*TT^4) - (80*Log[TT]*(251/216 - Zeta[3]))/(3*SS*TT^4) + 
     (8308*Zeta[3])/(9*SS*TT^4) + (2716*Zeta[3])/(SS^2*TT^3) + 
     (8512*Zeta[3])/(3*SS^3*TT^2) + (8884*Zeta[3])/(9*SS^4*TT) - 
     (80*Log[m2]*Zeta[3])/(SS*TT^4) - (240*Log[m2]*Zeta[3])/(SS^2*TT^3) - 
     (240*Log[m2]*Zeta[3])/(SS^3*TT^2) - (80*Log[m2]*Zeta[3])/(SS^4*TT) - 
     (200*Log[SS]*Zeta[3])/(3*SS*TT^4) - (200*Log[SS]*Zeta[3])/(SS^2*TT^3) - 
     (200*Log[SS]*Zeta[3])/(SS^3*TT^2) - (80*Log[SS]*Zeta[3])/(SS^4*TT) - 
     (440*Log[TT]*Zeta[3])/(3*SS*TT^4) - (440*Log[TT]*Zeta[3])/(SS^2*TT^3) - 
     (440*Log[TT]*Zeta[3])/(SS^3*TT^2) - (120*Log[TT]*Zeta[3])/(SS^4*TT)) + 
   m2^2*(-23/(4*SS*TT^3) - (79*Pi^2)/(6*SS*TT^3) - (7*Pi^4)/(30*SS*TT^3) + 
     22/(3*SS^2*TT^2) - (116*Pi^2)/(3*SS^2*TT^2) - (7*Pi^4)/(15*SS^2*TT^2) - 
     683/(12*SS^3*TT) - (241*Pi^2)/(6*SS^3*TT) - (7*Pi^4)/(30*SS^3*TT) - 
     12*SS^(-4 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[4 + Z1] + 4*EulerGamma*SS^(-4 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1] - 12*SS^(-4 - Z1)*TT^Z1*
      Gamma[-3 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[4 + Z1] + 4*EulerGamma*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1] - 
     48*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1] + 16*EulerGamma*SS^(-4 - Z1)*
      TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[4 + Z1] - 12*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[4 + Z1] + 4*EulerGamma*SS^(-4 - Z1)*
      TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*Gamma[4 + Z1] - 
     12*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[3 + Z1]*Gamma[4 + Z1] + 4*EulerGamma*SS^(-4 - Z1)*TT^Z1*
      Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[4 + Z1] + 
     (17*Log[m2])/(2*SS*TT^3) - (13*Pi^2*Log[m2])/(6*SS*TT^3) - 
     (4*Log[m2])/(SS^2*TT^2) - (16*Pi^2*Log[m2])/(3*SS^2*TT^2) + 
     (17*Log[m2])/(2*SS^3*TT) - (13*Pi^2*Log[m2])/(6*SS^3*TT) + 
     (3*Log[m2]^2)/(2*SS*TT^3) + (3*Pi^2*Log[m2]^2)/(SS*TT^3) + 
     (12*Log[m2]^2)/(SS^2*TT^2) + (6*Pi^2*Log[m2]^2)/(SS^2*TT^2) + 
     (3*Log[m2]^2)/(2*SS^3*TT) + (3*Pi^2*Log[m2]^2)/(SS^3*TT) - 
     (13*Log[m2]^3)/(3*SS*TT^3) - (32*Log[m2]^3)/(3*SS^2*TT^2) - 
     (13*Log[m2]^3)/(3*SS^3*TT) + (3*Log[m2]^4)/(SS*TT^3) + 
     (6*Log[m2]^4)/(SS^2*TT^2) + (3*Log[m2]^4)/(SS^3*TT) + 
     (27*Log[SS])/(SS*TT^3) + (25*Pi^2*Log[SS])/(6*SS*TT^3) + 
     (37*Log[SS])/(SS^2*TT^2) + (28*Pi^2*Log[SS])/(3*SS^2*TT^2) - 
     (35*Log[SS])/(2*SS^3*TT) + (17*Pi^2*Log[SS])/(3*SS^3*TT) + 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[4 + Z1]*Log[SS] + 4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1]*Log[SS] + 
     16*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*Log[SS] + 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[4 + Z1]*Log[SS] + 4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[4 + Z1]*Log[SS] - 
     (21*Log[m2]*Log[SS])/(SS*TT^3) - (Pi^2*Log[m2]*Log[SS])/(SS*TT^3) - 
     (42*Log[m2]*Log[SS])/(SS^2*TT^2) - (2*Pi^2*Log[m2]*Log[SS])/
      (SS^2*TT^2) - (21*Log[m2]*Log[SS])/(SS^3*TT) - 
     (Pi^2*Log[m2]*Log[SS])/(SS^3*TT) + (9*Log[m2]^2*Log[SS])/(SS*TT^3) + 
     (18*Log[m2]^2*Log[SS])/(SS^2*TT^2) + (9*Log[m2]^2*Log[SS])/(SS^3*TT) - 
     (2*Log[m2]^3*Log[SS])/(SS*TT^3) - (4*Log[m2]^3*Log[SS])/(SS^2*TT^2) - 
     (2*Log[m2]^3*Log[SS])/(SS^3*TT) + (16*Log[TT])/(SS*TT^3) + 
     (65*Pi^2*Log[TT])/(3*SS*TT^3) + (65*Log[TT])/(SS^2*TT^2) + 
     (142*Pi^2*Log[TT])/(3*SS^2*TT^2) + (175*Log[TT])/(2*SS^3*TT) + 
     (175*Pi^2*Log[TT])/(6*SS^3*TT) - (21*Log[m2]*Log[TT])/(SS*TT^3) - 
     (Pi^2*Log[m2]*Log[TT])/(SS*TT^3) - (42*Log[m2]*Log[TT])/(SS^2*TT^2) - 
     (2*Pi^2*Log[m2]*Log[TT])/(SS^2*TT^2) - (21*Log[m2]*Log[TT])/(SS^3*TT) - 
     (Pi^2*Log[m2]*Log[TT])/(SS^3*TT) + (9*Log[m2]^2*Log[TT])/(SS*TT^3) + 
     (18*Log[m2]^2*Log[TT])/(SS^2*TT^2) + (9*Log[m2]^2*Log[TT])/(SS^3*TT) - 
     (2*Log[m2]^3*Log[TT])/(SS*TT^3) - (4*Log[m2]^3*Log[TT])/(SS^2*TT^2) - 
     (2*Log[m2]^3*Log[TT])/(SS^3*TT) + (16*Log[SS]*Log[TT])/(SS*TT^3) - 
     (Pi^2*Log[SS]*Log[TT])/(SS*TT^3) + (64*Log[SS]*Log[TT])/(SS^2*TT^2) - 
     (2*Pi^2*Log[SS]*Log[TT])/(SS^2*TT^2) + (70*Log[SS]*Log[TT])/(SS^3*TT) - 
     (Pi^2*Log[SS]*Log[TT])/(SS^3*TT) + (33*Log[TT]^2)/(2*SS*TT^3) - 
     (3*Pi^2*Log[TT]^2)/(SS*TT^3) - (12*Log[TT]^2)/(SS^2*TT^2) - 
     (6*Pi^2*Log[TT]^2)/(SS^2*TT^2) - (75*Log[TT]^2)/(2*SS^3*TT) - 
     (3*Pi^2*Log[TT]^2)/(SS^3*TT) - (16*Log[SS]*Log[TT]^2)/(SS*TT^3) - 
     (38*Log[SS]*Log[TT]^2)/(SS^2*TT^2) - (25*Log[SS]*Log[TT]^2)/(SS^3*TT) + 
     (7*Log[TT]^3)/(3*SS*TT^3) + (38*Log[TT]^3)/(3*SS^2*TT^2) + 
     (34*Log[TT]^3)/(3*SS^3*TT) + (2*Log[SS]*Log[TT]^3)/(SS*TT^3) + 
     (4*Log[SS]*Log[TT]^3)/(SS^2*TT^2) + (2*Log[SS]*Log[TT]^3)/(SS^3*TT) - 
     Log[TT]^4/(SS*TT^3) - (2*Log[TT]^4)/(SS^2*TT^2) - Log[TT]^4/(SS^3*TT) + 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[4 + Z1]*PolyGamma[0, -3 - Z1] + 
     16*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*PolyGamma[0, -3 - Z1] + 
     8*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[2 + Z1]^2*
      Gamma[4 + Z1]*PolyGamma[0, -3 - Z1] + 8*SS^(-4 - Z1)*TT^Z1*
      Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[3 + Z1]*Gamma[4 + Z1]*
      PolyGamma[0, -3 - Z1] + 8*SS^(-4 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*
      Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1]*PolyGamma[0, -2 - Z1] + 
     16*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-2 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*PolyGamma[0, -2 - Z1] + 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[4 + Z1]*PolyGamma[0, -1 - Z1] - 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[4 + Z1]*PolyGamma[0, 4 + Z1] - 4*SS^(-4 - Z1)*TT^Z1*
      Gamma[-3 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[4 + Z1]*
      PolyGamma[0, 4 + Z1] - 16*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]*
      Gamma[-2 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[4 + Z1]*
      PolyGamma[0, 4 + Z1] - 4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*
      Gamma[2 + Z1]^2*Gamma[4 + Z1]*PolyGamma[0, 4 + Z1] - 
     4*SS^(-4 - Z1)*TT^Z1*Gamma[-3 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[3 + Z1]*Gamma[4 + Z1]*PolyGamma[0, 4 + Z1] - 
     (76*(1 - Zeta[3]))/(3*SS^2*TT^2) - (4*(1 - Zeta[3]))/(3*SS^3*TT) - 
     (8*Log[SS]*(1 - Zeta[3]))/(SS^2*TT^2) + (16*Log[TT]*(1 - Zeta[3]))/
      (SS^2*TT^2) - (14*(9/8 - Zeta[3]))/(3*SS*TT^3) - 
     (4*Log[SS]*(9/8 - Zeta[3]))/(SS*TT^3) + (8*Log[TT]*(9/8 - Zeta[3]))/
      (SS*TT^3) - (710*Zeta[3])/(3*SS*TT^3) - (1432*Zeta[3])/(3*SS^2*TT^2) - 
     (754*Zeta[3])/(3*SS^3*TT) + (24*Log[m2]*Zeta[3])/(SS*TT^3) + 
     (48*Log[m2]*Zeta[3])/(SS^2*TT^2) + (24*Log[m2]*Zeta[3])/(SS^3*TT) + 
     (20*Log[SS]*Zeta[3])/(SS*TT^3) + (40*Log[SS]*Zeta[3])/(SS^2*TT^2) + 
     (24*Log[SS]*Zeta[3])/(SS^3*TT) + (44*Log[TT]*Zeta[3])/(SS*TT^3) + 
     (88*Log[TT]*Zeta[3])/(SS^2*TT^2) + (36*Log[TT]*Zeta[3])/(SS^3*TT)) + 
   m2*(-28/(3*SS*TT^2) + (7*Pi^4)/(90*SS*TT^2) - 4/(SS^2*TT) + 
     (4*Pi^2)/(SS^2*TT) + (7*Pi^4)/(90*SS^2*TT) - 8*SS^(-3 - Z1)*TT^Z1*
      Gamma[-2 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*
      Gamma[3 + Z1] + 4*EulerGamma*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1] - 
     8*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1] + 4*EulerGamma*SS^(-3 - Z1)*TT^Z1*
      Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*Gamma[2 + Z1]*Gamma[3 + Z1] + 
     (4*Log[m2])/(SS*TT^2) + (Pi^2*Log[m2])/(3*SS*TT^2) + 
     (4*Log[m2])/(SS^2*TT) + (Pi^2*Log[m2])/(3*SS^2*TT) - 
     (2*Log[m2]^2)/(SS*TT^2) - (Pi^2*Log[m2]^2)/(SS*TT^2) - 
     (2*Log[m2]^2)/(SS^2*TT) - (Pi^2*Log[m2]^2)/(SS^2*TT) + 
     (2*Log[m2]^3)/(3*SS*TT^2) + (2*Log[m2]^3)/(3*SS^2*TT) - 
     Log[m2]^4/(SS*TT^2) - Log[m2]^4/(SS^2*TT) - (16*Log[SS])/(3*SS*TT^2) - 
     (Pi^2*Log[SS])/(SS*TT^2) - (4*Log[SS])/(SS^2*TT) - 
     (4*Pi^2*Log[SS])/(3*SS^2*TT) + 4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*
      Gamma[-1 - Z1]*Gamma[-Z1]*Gamma[1 + Z1]^2*Gamma[3 + Z1]*Log[SS] + 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*Log[SS] + (4*Log[m2]*Log[SS])/(SS*TT^2) + 
     (Pi^2*Log[m2]*Log[SS])/(3*SS*TT^2) + (4*Log[m2]*Log[SS])/(SS^2*TT) + 
     (Pi^2*Log[m2]*Log[SS])/(3*SS^2*TT) - (2*Log[m2]^2*Log[SS])/(SS*TT^2) - 
     (2*Log[m2]^2*Log[SS])/(SS^2*TT) + (2*Log[m2]^3*Log[SS])/(3*SS*TT^2) + 
     (2*Log[m2]^3*Log[SS])/(3*SS^2*TT) - (16*Log[TT])/(3*SS*TT^2) - 
     (16*Pi^2*Log[TT])/(3*SS*TT^2) - (12*Log[TT])/(SS^2*TT) - 
     (7*Pi^2*Log[TT])/(SS^2*TT) + (4*Log[m2]*Log[TT])/(SS*TT^2) + 
     (Pi^2*Log[m2]*Log[TT])/(3*SS*TT^2) + (4*Log[m2]*Log[TT])/(SS^2*TT) + 
     (Pi^2*Log[m2]*Log[TT])/(3*SS^2*TT) - (2*Log[m2]^2*Log[TT])/(SS*TT^2) - 
     (2*Log[m2]^2*Log[TT])/(SS^2*TT) + (2*Log[m2]^3*Log[TT])/(3*SS*TT^2) + 
     (2*Log[m2]^3*Log[TT])/(3*SS^2*TT) + (Pi^2*Log[SS]*Log[TT])/(3*SS*TT^2) - 
     (8*Log[SS]*Log[TT])/(SS^2*TT) + (Pi^2*Log[SS]*Log[TT])/(3*SS^2*TT) - 
     (2*Log[TT]^2)/(SS*TT^2) + (Pi^2*Log[TT]^2)/(SS*TT^2) + 
     (6*Log[TT]^2)/(SS^2*TT) + (Pi^2*Log[TT]^2)/(SS^2*TT) + 
     (4*Log[SS]*Log[TT]^2)/(SS*TT^2) + (6*Log[SS]*Log[TT]^2)/(SS^2*TT) - 
     (2*Log[TT]^3)/(3*SS*TT^2) - (8*Log[TT]^3)/(3*SS^2*TT) - 
     (2*Log[SS]*Log[TT]^3)/(3*SS*TT^2) - (2*Log[SS]*Log[TT]^3)/(3*SS^2*TT) + 
     Log[TT]^4/(3*SS*TT^2) + Log[TT]^4/(3*SS^2*TT) + 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[3 + Z1]*PolyGamma[0, -2 - Z1] + 
     8*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*PolyGamma[0, -2 - Z1] + 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[3 + Z1]*PolyGamma[0, -1 - Z1] - 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]*Gamma[-1 - Z1]*Gamma[-Z1]*
      Gamma[1 + Z1]^2*Gamma[3 + Z1]*PolyGamma[0, 3 + Z1] - 
     4*SS^(-3 - Z1)*TT^Z1*Gamma[-2 - Z1]^2*Gamma[-Z1]*Gamma[1 + Z1]*
      Gamma[2 + Z1]*Gamma[3 + Z1]*PolyGamma[0, 3 + Z1] + 
     (4*(1 - Zeta[3]))/(3*SS*TT^2) + (4*Log[SS]*(1 - Zeta[3]))/(3*SS*TT^2) - 
     (8*Log[TT]*(1 - Zeta[3]))/(3*SS*TT^2) + (172*Zeta[3])/(3*SS*TT^2) + 
     (60*Zeta[3])/(SS^2*TT) - (8*Log[m2]*Zeta[3])/(SS*TT^2) - 
     (8*Log[m2]*Zeta[3])/(SS^2*TT) - (20*Log[SS]*Zeta[3])/(3*SS*TT^2) - 
     (8*Log[SS]*Zeta[3])/(SS^2*TT) - (44*Log[TT]*Zeta[3])/(3*SS*TT^2) - 
     (12*Log[TT]*Zeta[3])/(SS^2*TT)) + 
   ((-(Pi^2*Log[m2]) - 2*Log[m2]^3 - 4*Zeta[3])/(6*SS*TT) + 
     (Pi^2*Log[m2] + 2*Log[m2]^3 + 4*Zeta[3])/(6*SS*TT))/d3 + 
   ((-(Pi^2*Log[m2]) - 2*Log[m2]^3 - 4*Zeta[3])/(6*SS*TT) + 
     (Pi^2*Log[m2] + 2*Log[m2]^3 + 4*Zeta[3])/(6*SS*TT))/d4)
